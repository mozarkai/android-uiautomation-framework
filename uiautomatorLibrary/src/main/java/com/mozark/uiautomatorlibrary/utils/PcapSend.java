package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPOutputStream;

import static com.mozark.uiautomatorlibrary.utils.Utility.copy;


public class PcapSend {

    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String result, decode;
    private String OUTPUT_GZIP_FILE = "";

    //    PcapRequestClass pcapRequestClass;
    public void compressPcap(String testId, String fileName, String ipAdress, String device_id, boolean pCap) {
        if (!pCap) {
            return;
        }
        byte[] buffer = new byte[1024];

        try {
            System.out.println("compressPcap mLogFileName compress  testId : " + testId);
            System.out.println("compressPcap mLogFileName compress  fileName : " + fileName);
            System.out.println("compressPcap mLogFileName compress  ipAdress : " + ipAdress);
            System.out.println("compressPcap mLogFileName compress  device_id : " + device_id);
            File file = new File("/sdcard/MozarkUtility/Mozark");
            if (!file.exists()) {
                file.mkdir();
            }
            File file1 = new File(file + "/" + testId);
            // String mLogFileName = "/storage/emulated/0/rooted_pcap/" + fileName;
            if (!file1.exists()) {
                file1.mkdir();
            }
            OUTPUT_GZIP_FILE = file1 + "/" + testId + ".pcap.gz";

            if (fileName != null && !fileName.isEmpty()) {

                File source = new File(fileName);
                UtilityClass.printFileSize(source);
                if (!source.exists()) {
                    Log.e("Aquamark", "File does not exit.....");
                    return;
                }
                GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(OUTPUT_GZIP_FILE));

                FileInputStream in = new FileInputStream(source);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    gzos.write(buffer, 0, len);
                }
                in.close();

                gzos.finish();
                gzos.close();
                gzos.flush();
                System.out.println("mLogFileName Done");

                File videoFile = new File(OUTPUT_GZIP_FILE);
                // File portFile = new File(OUTPUT_PORT_FILE);

                System.out.println("mLogFileName videoFile  " + videoFile);
                UtilityClass.printFileSize(videoFile);
                //  pcapRequestToServer(videoFile,jobId,fileName);
                // portRequestToServer(portFile,mobileIme,jobId,ipAddress);

                //    retrofit(testId, videoFile, ipAdress, device_id, source);
                //  pcapRequestToServer(videoFile,jobId,storedPcapFilePath,ipAdress);
                //                if (source.exists()) {
                //                    source.delete();
                //                }


                // This is only for test demo
//                if (false) {
//                    File copyFile = new File("/sdcard/Copy" + "/" + testId);
//                    if (!copyFile.exists()) {
//                        copyFile.mkdirs();
//                    }
//                    if (!copyFile.exists()) {
//                        copyFile.mkdirs();
//                    }
//                    File copyFileSrc = new File(copyFile + "/" + testId + ".pcap.gz");
//                    copy(videoFile, copyFileSrc);
//                    UtilityClass.printFileSize(copyFileSrc);
//                }
            }
            UtilityClass.appendLogFileByDate("\n\n Gotted Pcap Compressed");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String sendPcapWithLegacy(
            byte[] imagebyteArray, String fileName, JSONObject finalJsonStr, String ipAdress) {
        String requestUrl = ipAdress + "performance/uploadmedia?request=";

        String resultString = null;
        String charset = "UTF-8";
        String LINE_FEED = "\r\n";
        try {
            UtilityClass.appendLogFileByDate(
                    "UIAutomator sendPcapWithLegacy start File finalJsonStr : " + finalJsonStr);
            System.out.println("SendPhototoServer: 1");

            String jStrParameters = finalJsonStr.toString();
            System.out.println("PHOTO URL FINAL00 : " + jStrParameters);

            System.out.println("URL FINAL01 : " + requestUrl + jStrParameters);

            String finalURL =
                    requestUrl + finalJsonStr; // URLEncoder.encode(jStrParameters.trim(), "UTF-8");
            System.out.println("URL FINAL11 : " + finalURL);

            URL url = new URL(finalURL);
            System.out.println("URL FINAL22 : " + url);

            String boundary = "===" + System.currentTimeMillis() + "===";

            if (imagebyteArray != null) {

                URLConnection connection = url.openConnection();

                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.setConnectTimeout(300000);
                httpConnection.setReadTimeout(300000);
                httpConnection.setRequestProperty(
                        "Content-Type", "multipart/form-data; boundary=" + boundary);

                OutputStream outputStream = httpConnection.getOutputStream();

                PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);

                String encodedString = "test";

                System.out.println("Encoded String : " + encodedString);

                writer.append("--" + boundary).append(LINE_FEED);
                writer.append("Content-Disposition: form-data; name=\"groupzkey\"").append(LINE_FEED);
                writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                writer.append(LINE_FEED);
                writer.append(encodedString).append(LINE_FEED);
                writer.flush();

                writer.append("--" + boundary).append(LINE_FEED);
                writer
                        .append(
                                "Content-Disposition: form-data; name=\"document\"; filename=\"" + fileName + "\"")
                        .append(LINE_FEED);

                writer
                        .append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                        .append(LINE_FEED);
                writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                writer.append(LINE_FEED);
                writer.flush();
                System.out.println("Send photo in bg ByteArr: " + imagebyteArray);
                outputStream.write(imagebyteArray);

                outputStream.flush();

                writer.append(LINE_FEED);
                writer.flush();
                writer.append(LINE_FEED).flush();
                writer.append("--" + boundary + "--").append(LINE_FEED);
                writer.close();

                // checks server's status code first
                int status = httpConnection.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    System.out.println("Photourl innnn");
                    StringBuffer res = new StringBuffer();
                    BufferedReader in =
                            new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                    String inputLine;

                    while ((inputLine = in.readLine()) != null) {
                        System.out.println("InputLine: " + inputLine + " **-** " + in);
                        res.append(inputLine);
                    }

                    resultString = res.toString();

                    System.out.println("PhotoUrl response " + resultString);

                    if (resultString.contains("json") == false) {
                        resultString = null;
                    }
                    in.close();

                    httpConnection.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        System.out.println("PhotoUrl reponse " + resultString);

        return resultString;
    }

    public void retrofit(
            String testId, File videoFile, String ipAdress, String device_id, File pCap) {
        Log.d("Aquamark", "Start sending pcap and Video Size : " + videoFile.length());
        JSONObject json = new JSONObject();
        JSONObject requestu = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject detailsExtra = new JSONObject();
        try {

            detailsExtra.put("deviceId", device_id);
            detailsExtra.put("testId", testId);

            data.put("data", detailsExtra);
            requestu.put("request", data);
            json.put("json", requestu);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        byte[] logo = null;

        try {
            logo = FileUtils.readFileToByteArray(videoFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String resulr = sendPcapWithLegacy(logo, videoFile.getName(), json, ipAdress);
        UtilityClass.appendLogFileByDate("UIAutomator sendPcapWithLegacy Result : " + resulr);
        if (videoFile.exists()) {
            videoFile.delete();
        }
        if (pCap.exists()) {
            pCap.delete();
        }
    }

    //    public void pcapRequestToServer(File videoFile, int jobId, String storedPcapFilePath,String
    // ipaddress)
    //    {
    //        try
    //        {
    //            byte[] logo = null;
    //
    //            logo = FileUtils.readFileToByteArray(videoFile);
    //            String filename  = videoFile.getName( );
    //            final byte[] logoz = logo;
    //            String finalJsonStr = uploadVideoReqJsonStr(jobId);
    //
    //            String requestUrl = "http://52.183.134.250:8080/IceBerg/uploadmedia?request=";
    //
    //            pcapRequestClass = new PcapRequestClass() {
    //                @Override
    //                public void onResponseReceived(String result) {
    //
    //                    try {
    //                        if (result != null && !result.isEmpty())
    //                        {
    //
    //                            JSONObject resultJson = new JSONObject(result);
    //                            JSONObject joresponse = resultJson.getJSONObject("json");
    //                            JSONObject jores = joresponse.getJSONObject("response");
    //
    //                            String statusCode = null;
    //                            statusCode = jores.getString("statusCode");
    //                            String statusMsg = jores.getString("statusMessage");
    //
    //                            if (statusCode.equals("0"))
    //                            {
    //
    //                                System.out.println("RegisterOrLogin FinalResponseStr1 : " +
    // statusMsg);
    //                                File outFile = new File(OUTPUT_GZIP_FILE);
    //                                if (outFile.exists())
    //                                {
    //                                    outFile.delete();
    //                                }
    //
    //                                if (videoFile.exists())
    //                                {
    //                                    videoFile.delete();
    //                                }
    //
    //                                File storedfile = new File(storedPcapFilePath);
    //                                if(storedfile.exists())
    //                                {
    //                                    storedfile.delete();
    //                                }
    //
    //                                //finish();
    //                                //startActivity(new
    // Intent(RecordScreenActivity.this,MainActivity.class));
    //                            }
    //                            else
    //                            {
    //
    //                            }
    //                        }
    //                    }
    //                    catch (Exception e)
    //                    {
    //                        e.printStackTrace();
    //                    }
    //                }
    //            };
    //            pcapRequestClass.setJson(finalJsonStr);
    //            pcapRequestClass.setImage(logoz);
    //            pcapRequestClass.setFileName(filename);
    //            pcapRequestClass.execute(requestUrl);
    //
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //    }

    //    public void videCapture(int job_id, String filepath, String ipAdress, String device_id)
    // throws IOException {
    //        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    //        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    //        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    //
    //        Retrofit retroclient = new Retrofit.Builder()
    //                .baseUrl(ipAdress)
    //                .addConverterFactory(GsonConverterFactory.create())
    //                .client(client)
    //                .build();
    //
    //        File file = new File(filepath);
    //
    //
    //
    //        // URLConnection connection=file.toURL().openConnection();
    //        //String mime=connection.getContentType();
    //        // create RequestBody instance from file
    //        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"),
    // file);
    //
    //        // MultipartBody.Part is used to send also the actual file name
    //        MultipartBody.Part body =
    //                MultipartBody.Part.createFormData("file", "request", requestFile);
    //
    //        System.out.println("before  Done " + body.toString());
    //
    //        try {
    //            PcapSendApi service = retroclient.create(PcapSendApi.class);
    //            Call<PcapResponse> call = service.postData(body, new PcapRequest(new Json(new
    // Request(new Data(job_id, device_id)))));
    //            System.out.println("call " + call.isExecuted() + " " + call.toString());
    //            call.enqueue(new Callback<PcapResponse>() {
    //                @Override
    //                public void onResponse(Call<PcapResponse> call, Response<PcapResponse> response)
    // {
    //                    System.out.println("inside the response");
    //                    System.out.println("done Done");
    //
    //                }
    //
    //                @Override
    //                public void onFailure(Call<PcapResponse> call, Throwable t) {
    //                    System.out.println("if failed Done");
    //
    //                    t.printStackTrace();
    //                }
    //            });
    //        }
    //        catch (Exception e)
    //        {
    //            e.getMessage();
    //            Log.d("Aquamark",e.getMessage());
    //        }jjjj
    //
    //
    //
    //    }

}
