package com.mozark.uiautomatorlibrary.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.aqua5gmark.Aqua5GMark;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class CellInfoApp {

        public static String CELLINFO_PACKAGE =
                "com.mozark.aquamarkcellinfo"; // "com.agence3pp/qosi.fr.usingqosiframework.splashscreen.SplashscreenActivity";

        public static void launchApp(Context context, UiDevice device) {
        try {
            File file = new File("/sdcard/QosbeeFiles/cellInfo.txt");
            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(CELLINFO_PACKAGE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            if (file.exists()) {
                file.delete();
            }
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            file = new File("/sdcard/QosbeeFiles/cellInfo.txt");

            while (!Utility.checkFileExit(file) || stopWatch.getTime() >= 60000) {
                Log.d(
                        "Aquamark",
                        "Waiting the result of CellInfo network result stopWatch.getTime() : "
                                + stopWatch.getTime());
                try {
                    Thread.sleep(2000);
                    file = new File("/sdcard/QosbeeFiles/cellInfo.txt");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Log.d("Aquamark", "EXIT... Waiting the result of CellInfo network result");
        } catch (Exception e) {
            Log.e("Aquamark", "EXIT... Waiting the result of CellInfo network result " + e.getMessage());
        }
    }

        // aqua5gmark.txt
        public static String readCellInfoData(File file) {
        try {
            // final String FILE_NAME = "example.txt";
            FileInputStream fis = new FileInputStream(file);
            //  fis = con.openFileInput(yourFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }
            System.out.println("the file details is " + sb.toString());
            fis.close();
            return sb.toString();
        } catch (Exception e) {
            Log.e("", "" + e.getMessage());
        }
        return "";
    }

        public static void LaunchCellInfo(Context context, UiDevice device) {
        try {
            CellInfoApp.launchApp(context, device);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Launching Aquamark 5Mark");
            e.printStackTrace();
        }
    }
}
