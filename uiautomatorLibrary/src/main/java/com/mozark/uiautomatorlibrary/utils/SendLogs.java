package com.mozark.uiautomatorlibrary.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SendLogs extends AsyncTask<String, String, String> {

  Context mContext = null;
  String statusOfCapture = null;

  public SendLogs(Context _ctx, String captureStatus) {
    mContext = _ctx;
    statusOfCapture = captureStatus;
  }

  public static void fetch(OutputStream out, boolean close) throws IOException {
    byte[] log = new byte[1024 * 2];
    InputStream in = null;
    try {
      Process proc = Runtime.getRuntime().exec("logcat com.example.testapp.test:d ");
      in = proc.getInputStream();
      int read = in.read(log);
      while (-1 != read) {
        out.write(log, 0, read);
        read = in.read(log);
      }
    } finally {
      if (null != in) {
        try {
          in.close();
        } catch (IOException e) {
          // ignore
        }
      }

      if (null != out) {
        try {
          out.flush();
          if (close) {
            out.close();
          }
        } catch (IOException e) {
          // ignore
        }
      }
    }
  }

  public static void fetch(File file) throws IOException {
    FileOutputStream fos = new FileOutputStream(file);
    System.out.println("Aquamark:Logcat is start");
    fetch(fos, true);
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
  }

  @TargetApi(Build.VERSION_CODES.O)
  @Override
  protected String doInBackground(String... strings) {
    StringBuffer output = new StringBuffer();
    try {

      if ("start".equals(statusOfCapture)) {
        System.out.println("Aquamark:Logcat is start");
        //
        //
        //                Process processcmd = Runtime.getRuntime().exec(" logcat
        // com.android.mozark:d >"/sdcard/log.txt"");
        //                OutputStream os = processcmd.getOutputStream();
        //                //below code for capture screenshot and placed in internal storage.
        //                os.write("adb shell\n".getBytes());
        //                //below code for increase brightness.
        //
        //
        //                os.write("logcat -c".getBytes());
        //                os.write("logcat -w /sdcard/Logcat.txt".getBytes());
        //                os.flush();
        // File file = new File("/sdcard/log.txt");
        Process processcmd = Runtime.getRuntime().exec(" logcat   com.example.testapp:d >");
        OutputStream os = processcmd.getOutputStream();
        os.write("/sdcard/log.txt".getBytes());

        // fetch(file);

        Log.d("Aquamark", "end of start.equals(statusOfCapture)");
      } else if ("stop".equals(statusOfCapture)) {
        try {
          Process processcmd = Runtime.getRuntime().exec("adb logcat -c");

          Log.d("Aquamark", "the logcat is stopped ");

        } catch (Exception e) {
          e.printStackTrace();
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    String response = output.toString();
    return response;
  }
}
