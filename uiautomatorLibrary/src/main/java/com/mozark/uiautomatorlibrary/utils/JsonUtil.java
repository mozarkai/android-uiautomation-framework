package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

/**
 * This Class Handle JaksonJSON to Object and object to JaksonJSON conversion
 *
 * @author Sheshnath
 */
public class JsonUtil {

  private static final String TAG = JsonUtil.class.getName();

  public static String toJson(Object object) {
    try {
      Gson gson = new Gson();
      return gson.toJson(object);
    } catch (Exception e) {
      Log.e(TAG, "Error in Converting Model to Json", e);
    }
    return null;
  }

  public static Object toModel(String json, Type listType) {

    try {
      Gson gson = new Gson();
      return gson.fromJson(json, listType);
    } catch (Exception e) {
      Log.e(TAG, "Error in Converting JSON to Model " + e + "\n " + json);
    }
    return null;
  }

  public static ArrayList<Object> toArrayModel(String json, Object listType) {

    try {
      Gson gson = new Gson();
      return (ArrayList<Object>)
          JsonUtil.fromJson(
              json, new com.google.gson.reflect.TypeToken<ArrayList<Object>>() {}.getType());
    } catch (Exception e) {
      Log.e(TAG, "Error in Converting JSON to Model " + e + "\n " + json);
    }
    return null;
  }

  public static Object fromJson(String jsonString, Type type) {
    try {
      return new Gson().fromJson(jsonString, type);
    } catch (Exception e) {
      return null;
    }
  }

  /** This Method map Json Data to listType */
  public static Object mapJsonToModel(Type listType, String jsonData) {
    Gson gson = new Gson();
    return gson.fromJson(jsonData, listType);
  }

  public static String getJsonOfMap(Map<String, String> map) {
    if (map.size() == 0) {
      return "{}";
    }
    String json = "{";
    if (map.size() > 0) {
      for (Map.Entry<String, String> entry : map.entrySet()) {
        json = json + "\"" + (entry.getKey()) + "\"" + ":" + "\"" + entry.getValue() + "\"" + ",";
      }
    } else {
      for (Map.Entry<String, String> entry : map.entrySet()) {
        json = json + "\"" + (entry.getKey()) + "\"" + ":" + "\"" + entry.getValue() + ",";
      }
    }
    json = json.substring(0, json.length() - 1);
    return json + "}";
  }

  public static String getJsonDataByField(String field, String json) {
    try {
      JSONObject obj = new JSONObject(json);
      return obj.getString(field);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return "";
  }
}
