package com.mozark.uiautomatorlibrary.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.AquaMarkSharePreferences;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.Aqua5GMark;
import com.mozark.uiautomatorlibrary.utils.aqua5gmarkPro.Aqua5gmarkPro;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.mozark.uiautomatorlibrary.utils.UtilityClass.getDateString;

public class Utility {

    public static final String RUNNING_LOG_STATUS = "/sdcard/QosbeeFiles/runningLogStatus.txt";
    public static String SCRIPT_RESULT = "ScriptResult.txt";
    public static String SCRIPT_RUNNING = "ScriptRunning.txt";
    private static Writer writer;
    public static void writeLogResultForLaterUse(Data sendData, String fileName) {
        System.out.println("inside writing history -> writeLogResultForLaterUse");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File gpxfile = new File(file + "/" + fileName);
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append("!@#").append(JsonUtil.toJson(sendData));
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void writeLogResultForIntentApplication(Data sendData, String fileName, String status) {
        System.out.println("inside writing history -> writeLogResultForLaterUse");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File gpxfile = new File(file + "/" + fileName);
            FileWriter writer = new FileWriter(gpxfile, true);
            String json = JsonUtil.toJson(sendData);
            String jsonFinal = dataJson(json, status);
            // json.replaceAll("request","response");
            writer.append(sendData.getStatus());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteFileIfxist(String fileName) {
        System.out.println("inside writing history -> writeLogResultForLaterUse");

        try {
            File file = new File(fileName);
            if (!file.exists()) {
                file.delete();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteFileByPathIfxist(String fileName) {
        System.out.println(
                "deleteFileByPathIfxist inside writing history -> writeLogResultForLaterUse");
        try {
            File gpxfile = new File(fileName);
            if (gpxfile.exists()) {
                gpxfile.delete();
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void copy(File src, File dst) {
        try {
            InputStream in = new FileInputStream(src);
            try {
                OutputStream out = new FileOutputStream(dst);
                try {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                } finally {
                    out.flush();
                    out.close();
                }
            } finally {

                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkFileExit(File file) {
        System.out.println("inside checkFileExit for Aquamark");
        try {
            if (file.exists()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void logData() {
        try {
            Runtime.getRuntime().exec(" logcat  > " + "/sdcard/log.txt");

            // os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logDatastop() {
        try {
            Process processcmd = Runtime.getRuntime().exec("su");
            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su\n".getBytes());
            os.write("exit".getBytes());

            os.flush();
            // os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearLog() {
        try {
            Process process =
                    new ProcessBuilder().command("logcat", "-c").redirectErrorStream(true).start();

            Runtime.getRuntime().exec("logcat -b all -c");

        } catch (IOException e) {
        }
    }

    public static void cpuUsage(String cpu, String file) {
        try {
            //  appendAllStrToFile(file, "Date " +
            // (Utility.getCurrentDateTime("yyyy-MM-dd'T'HH:mm:ss"))+"|");
            //            Process install = Runtime.getRuntime().exec("su\n");
            //            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            //            os.writeBytes("adb shell\n");
            ////            os.writeBytes(" top -n1 | grep   '" + cpu + "' >>" + file + "\n");
            ////            os.writeBytes("Q\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //
            //            os.flush();
            //            os.close();
            //  killTop();

        } catch (Exception e) {

        }
    }

    public static void cleanBattery() {
        try {
            //            Process install = Runtime.getRuntime().exec("su\n");
            //            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            //            os.writeBytes("adb shell\n");
            //            os.writeBytes(" dumpsys batterystats --reset\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //
            //            os.flush();
            ////            install.destroy();
            //            os.close();
        } catch (Exception e) {

        }
    }

    public static void batteryStats(String file) {
        try {
            //            Process install = Runtime.getRuntime().exec("su\n");
            //            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            //            os.writeBytes("adb shell\n");
            //         //   os.writeBytes("dumpsys batterystats com.example.testapp.test |grep -E
            // \"Estimated battery capacity| Discharge\">>" + (new
            // File(OUTPUT_LOG_FILE_TEMP_BATTARY_STATS)) + "\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            os.flush();
            //            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getNumOfCores() {
        //        try {
        //            int i = new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {
        //                public boolean accept(File params) {
        //                    return Pattern.matches("cpu[0-9]", params.getName());
        //                }
        //            }).length;
        //            return i;
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        return 1;
    }

    public static String getTotalRAM() {

        //        RandomAccessFile reader = null;
        //        String load = null;
        //        DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
        //        double totRam = 0;
        //        String lastValue = "";
        //        try {
        //            reader = new RandomAccessFile("/proc/meminfo", "r");
        //            load = reader.readLine();
        //
        //            // Get the Number value from the string
        //            Pattern p = Pattern.compile("(\\d+)");
        //            Matcher m = p.matcher(load);
        //            String value = "";
        //            while (m.find()) {
        //                value = m.group(1);
        //                // System.out.println("Ram : " + value);
        //            }
        //            reader.close();
        //
        //            totRam = Double.parseDouble(value);
        //            // totRam = totRam / 1024;
        //
        //            double mb = totRam / 1024.0;
        //            double gb = totRam / 1048576.0;
        //            double tb = totRam / 1073741824.0;
        //
        //            if (tb > 1) {
        //                lastValue = twoDecimalForm.format(tb).concat(" TB");
        //            } else if (gb > 1) {
        //                lastValue = twoDecimalForm.format(gb).concat(" GB");
        //            } else if (mb > 1) {
        //                lastValue = twoDecimalForm.format(mb).concat(" MB");
        //            } else {
        //                lastValue = twoDecimalForm.format(totRam).concat(" KB");
        //            }

        //
        //        } catch (IOException ex) {
        //            ex.printStackTrace();
        //        } finally {
        //            // Streams.close(reader);
        //        }

        return "lastValue";
    }

    public static void logFileGeneration(
            String testId, String log, String ipAdress, String device_id) {
        try {
            SendLogsToServer sendLogsToServer = new SendLogsToServer();
            File files = new File(log);
            sendLogsToServer.getLogsPath(testId, files, ipAdress, device_id);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Log File Generation");
            e.printStackTrace();
        }
    }

    public static void sendPcap(String testId, String ipAdress, String device_id, boolean pCap) {
        try {
            PcapSend pcapSend = new PcapSend();
            pcapSend.compressPcap(testId, GlobalVariables.storedPcapFilePath, ipAdress, device_id, pCap);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Send Pcap");
            e.printStackTrace();
        }
    }

    public static boolean log(int id, String log) {
        try {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.Q){
                // Do something for lollipop and above versions
                Runtime.getRuntime().exec(new String[]{"logcat", "-f", log});

            } else{
                Runtime.getRuntime().exec("logcat  >" + log);
            }

//            String cmd = "logcat -v time -r 100 -f  "+ log + " Aquamark:D TestApp:D *:S";
//            Runtime.getRuntime().exec(cmd);
//            Process processcmd = Runtime.getRuntime().exec("su");
//
//            OutputStream os = processcmd.getOutputStream();
//            //below code for capture screenshot and placed in internal storage.
//            os.write("adb shell\n".getBytes());
//            //below code for increase brightness.
//            os.write("su\n".getBytes());
//            String s="logcat  >"+log;
//            os.write(s.getBytes());
//            os.flush();
//            os.close();
        } catch (Exception e) {
                e.printStackTrace();
        }
        return true;
    }

    public static boolean logVoice(int id, String log) {
        try {
            String command = "logcat --pid=" + id + " -v";
            //            Process process = Runtime.getRuntime().exec(command);
            //            BufferedReader bufferedReader = new BufferedReader(
            //                    new InputStreamReader(process.getInputStream()));
            //
            //            StringBuilder logs=new StringBuilder();
            //            String line = "";
            //            while ((line = bufferedReader.readLine()) != null) {
            //                logs.append(line +"\n");
            //            }
            //
            //            BufferedWriter writer = new BufferedWriter(new FileWriter(log));
            //            writer.write(logs.toString());
            //            writer.close();
            Runtime.getRuntime().exec("adb shell logcat qosi.fr.pro5gmark:I >" + log);
            //            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            //            os.writeBytes("adb shell\n");
            //            os.writeBytes(""+command+">"+log+"\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            os.writeBytes("exit\n");
            //            Thread.sleep(2000);
            //            os.flush();
            ////            install.destroy();
            //            os.close();

        } catch (Exception e) {

        }
        return true;
    }

    public static String fileName(String testId) {
        String OUTPUT_LOG_FILE = "";
        File file = new File("/sdcard/MozarkUtility/Mozark");
        if (!file.exists()) {
            file.mkdir();
        }
        File file1 = new File(file + "/" + testId);
        // String mLogFileName = "/storage/emulated/0/rooted_pcap/" + fileName;
        if (!file1.exists()) {
            file1.mkdir();
        }

        OUTPUT_LOG_FILE = file1 + "/" + testId;

        return OUTPUT_LOG_FILE;
    }

    public static void killTop() {
        try {
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());

            os.write("for i in `ps -ef| grep 'grep'|awk '{print $2}'` \n".getBytes());
            os.write("do \n".getBytes());
            os.write(" kill $i \n".getBytes());
            os.write("done \n".getBytes());

            os.flush();
        } catch (Exception e) {

        }
    }

    public static void appendAllStrToFile(String fileName, String detail) {
        try {
            // Open given file in append mode.
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName, true));
            out.write(detail);
            //            out.write("   ");
            out.flush();
            out.close();
            //     Log.d("CPU USAGE", "appendAllStrToFile Data : " + detail);
        } catch (IOException e) {
            System.out.println("exception occoured" + e);
        }
    }

    public static String getCurrentDateTime(String formate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formate);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }
    //    public static String getCurrentTime(String formate) {
    //        SimpleDateFormat dateFormat = new SimpleDateFormat(formate);
    //        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    //        Date today = Calendar.getInstance().getTime();
    //        return dateFormat.format(today);
    //    }

    public static void permission(String packageName) {
        try {

            Process processcmd = null;

            try {
                Process install = Runtime.getRuntime().exec("su\n");
                DataOutputStream os = new DataOutputStream(install.getOutputStream());
                os.writeBytes("adb shell\n");
                {
                    os.writeBytes("pm grant " + packageName + " android.permission.ACCESS_COARSE_LOCATION\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.ACCESS_FINE_LOCATION\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.WRITE_EXTERNAL_STORAGE\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.READ_EXTERNAL_STORAGE\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.CAMERA\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.RECORD_AUDIO\n");
                    os.writeBytes("pm grant " + packageName + " android.permission.CALL_PHONE\n");
                }
                os.writeBytes("exit\n");
                os.flush();
                os.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearCache(String packageName) {
        try {
            Process install = Runtime.getRuntime().exec("su\n");
            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            os.writeBytes("adb shell\n");
            os.writeBytes("su\n");
            os.writeBytes(" rm -rf /data/data/" + packageName + "/cache/* \n");
            os.writeBytes("exit\n");
            os.flush();
            os.close();
            Log.d(GlobalVariables.Tag_Name, "Cleared Cache Successfully");
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clearing Cache");
            e.printStackTrace();
        }
    }
    public static void clearLogCache() {
        try {
            Process install = Runtime.getRuntime().exec("su\n");
            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            os.writeBytes("adb shell\n");
            os.writeBytes("su\n");
            os.writeBytes(" logcat -c \n");
            os.writeBytes("exit\n");
            os.flush();
            os.close();
            Log.d(GlobalVariables.Tag_Name, "Cleared Cache Successfully");

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clearing Cache");
            e.printStackTrace();
        }
    }

    public static void checkFile(String flag) {
        System.out.println("inside send data");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        JSONObject Jmain = new JSONObject();
        try {
            Jmain.put("flag", flag);

            File gpxfile = new File(file + "/check.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(Jmain.toString());
            writer.flush();
            writer.close();

        } catch (Exception e) {
            Log.e("", "" + e.getMessage());
        }
    }

    public static void stopScreenRecording() {
        try {
            DataHolder.getInstance().setVideoEndTime(getDateString());
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());

            os.write(
                    "for i in `ps -ef| grep -v grep |grep 'screenrecord'|awk '{print $2}'` \n"
                            .getBytes());
            os.write("do \n".getBytes());
            os.write(" kill -2 $i \n".getBytes());
            os.write("done \n".getBytes());

            os.flush();
            os.close();
            Log.d("Aquamark", "Screen Recorder Stopped ");
            UtilityClass.appendLogFileByDate("UIAutomator Screen Record Stop");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name, "Error in Stop Recording");
        }
    }

    public static String dataJson(String jsonResult, String status) {
        String response_str = null;
        JSONObject json = new JSONObject();
        JSONObject response = new JSONObject();
        JSONObject data = new JSONObject();

        JSONObject mainJson = new JSONObject();

        JSONObject detailsExtra = new JSONObject();
        try {


            if (status.equalsIgnoreCase("Failed")) {
                detailsExtra.put("reason", "Element ID was not found or didnt match");
                data.put("data", detailsExtra);
            } else {
                data.put("data", jsonResult);
            }
            json.put("json", getJsonObject(status, data.toString()));
            response_str = json.toString();

        } catch (JSONException e) {
            response_str = e.getMessage();
            e.printStackTrace();
            return response_str;
        }
        return response_str;
    }

    public static JSONObject getJsonObject(String status, String data) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONObject userJson = new JSONObject();
        userJson.put("status", status);
        userJson.put("data", data);
        jsonObject.put("response", userJson);
        return jsonObject;
    }

    public static void startScreenshot() {
        try {
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));
            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());
            os.write(UtilityClass.getScreenshotCommand().getBytes());
            os.flush();
            os.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void callCellInfo(boolean check, String device_id, String ipAdress, Context context, UiDevice device, String sendDataToUIAutomator) {
        if (check) {
            CellInfoApp.launchApp(context,device);
            tearDown(device);
//            Aqua5GMark.LaunchAqua5GMark(
//                    device_id, ipAdress, context, device, sendDataToUIAutomator);
        } else {
//            Aqua5gmarkPro aqua5gmarkPro = new Aqua5gmarkPro();
//            aqua5gmarkPro.Aqua5gmarkRun(device,context);

        }
    }

    private static String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public static void tearDown(UiDevice device) {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand("com.mozark.aquamarkcellinfo"));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public static void copyFromAsset(Context context, String testId)
    {
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        InputStream in = null;
        OutputStream out = null;
        String filename = "/Users/archanajayappa/StudioProjects/android-uiautomation-framework/app/src/main/assets/pcap.pcap";
        try
        {
            String pcapName=Utility.fileName(testId) + ".pcap.gz";
            in = assetManager.open(filename);
            out = new FileOutputStream(pcapName);
            File file=new File(pcapName);
            if(!file.exists())
            {
                file.createNewFile();
            }
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }
        catch(IOException e)
        {
            Log.e("aquamark", "Failed to copy asset file: " + filename, e);
        }
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }


    public static void copyFpsFile(String path)
    {
        try {
            //device.executeShellCommand("sh /sdcard/QosbeeFiles/FPSUtility/gfx.sh");

            String fpsPath ="mv //sdcard/mozark-resources/fps.csv "+path+"dataUsage.csv";
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));
            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());
            os.write(fpsPath.getBytes());
            //  os.write(fpsPath1.getBytes());
            Log.d(GlobalVariables.Tag_Name, "Copied FPS csv file Successfully" + fpsPath);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.getMessage();
            Log.d(GlobalVariables.Tag_Name, "Exception in moving" + e.getMessage());

        }
    }

    public static void writeForIntent(Data data, String fileName, String status) {
        File root = Environment.getExternalStorageDirectory();
        File outDir = new File("/sdcard/QosbeeFiles");
        if (!outDir.isDirectory()) {
            outDir.mkdir();
        }
        try {
            if (!outDir.isDirectory()) {
                throw new IOException(
                        "Unable to create directory ViserAquamark. Maybe the SD card is mounted?");
            }
            String json = JsonUtil.toJson(data);
            String jsonFinal = dataJson(json, status);
            File outputFile = new File(outDir, fileName);
            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(jsonFinal);
            writer.close();
        } catch (IOException e) {
            Log.w("eztt", e.getMessage(), e);

        }

    }
}
