package com.mozark.uiautomatorlibrary.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class LogsChecker extends AsyncTask<String, String, String> {

  Context mContext = null;
  String statusOfCapture = null;

  public LogsChecker(Context _ctx, String captureStatus) {
    mContext = _ctx;
    statusOfCapture = captureStatus;
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
  }

  @TargetApi(Build.VERSION_CODES.O)
  @Override
  protected String doInBackground(String... strings) {
    StringBuffer output = new StringBuffer();
    try {

      if ("start".equals(statusOfCapture)) {
        // Utility.deleteFileByPathIfxist("/sdcard/capturedpcapfile.pcap");
        System.out.println("Aquamark:ExecuteCommand:statusOfCapture is start");

        //                Process processcmd = Runtime.getRuntime().exec("su");
        //                OutputStream os = processcmd.getOutputStream();
        //                //below code for capture screenshot and placed in internal storage.
        //                os.write("adb shell\n".getBytes());
        //                //below code for increase brightness.
        //
        //                os.write("nohup logcat  com.example.testapp.test:D *:D > /sdcard/log.txt
        // &\n".getBytes());
        Runtime.getRuntime().exec("su");
        Process process =
            Runtime.getRuntime()
                .exec("nohup logcat  com.example.testapp.test:D *:D >" + "/sdcard/log.txt");
        process.destroy();
        //                os.flush();
        // os.close();
        // nohup /data/media/tcpdump -i any -p -s 0 -w /sdcard/capturedpcapfile.pcap &
        //
        Log.d("Aquamark", "end of start.equals(statusOfCapture)");
      } else if ("stop".equals(statusOfCapture)) {
        try {
          Process processcmd = Runtime.getRuntime().exec("su");
          BufferedReader bufferedReader =
              new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

          OutputStream os = processcmd.getOutputStream();
          // below code for capture screenshot and placed in internal storage.
          os.write("adb shell\n".getBytes());
          // below code for increase brightness.
          os.write("su \n".getBytes());

          os.write(
              "for i in `ps -ef| grep -v grep |grep '/sdcard/log.txt'|awk '{print $2}'` \n"
                  .getBytes());
          os.write("do \n".getBytes());
          os.write(" kill $i \n".getBytes());
          os.write("done \n".getBytes());

          os.flush();
          // os.close();
          Log.d("Aquamark", "the pp is stopped ");

        } catch (Exception e) {
          e.printStackTrace();
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    String response = output.toString();
    return response;
  }
}
