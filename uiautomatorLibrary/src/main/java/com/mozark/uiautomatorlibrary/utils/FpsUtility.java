package com.mozark.uiautomatorlibrary.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.TestApp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;

public class FpsUtility
{
    public static void startFpsCapture(UiDevice device, Context context)
    {
        try {
            //device.executeShellCommand("sh /sdcard/QosbeeFiles/FPSUtility/gfx.sh");
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));
            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            copyFromAsset(context);
            // below code for increase brightness.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());
                String path = "sh /sdcard/QosbeeFiles/FPSUtility/gfx.sh";
                os.write(path.getBytes());
                //os.write("sh /sdcard/QosbeeFiles/FPSUtility/gfx.sh".getBytes());
                os.flush();
                os.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }


    public static void stopFpsCapture(boolean withDebug)
    {
        try {
            if(withDebug)
            {
                Process processcmd = Runtime.getRuntime().exec("su");
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

                OutputStream os = processcmd.getOutputStream();
                // below code for capture screenshot and placed in internal storage.
                os.write("adb shell\n".getBytes());
                // below code for increase brightness.
                os.write("su \n".getBytes());

                os.write(
                        "for i in `ps -ef| grep -v grep |grep 'cat /sys/kernel/debug/tracing/trace_pipe'|awk '{print $2}'` \n"
                                .getBytes());
                os.write("do \n".getBytes());
                os.write(" kill $i \n".getBytes());
                os.write("done \n".getBytes());

                os.flush();
                // os.close();
                Log.d(GlobalVariables.Tag_Name, "FPS Capture is stopped");
                UtilityClass.appendLogFileByDate("UIAutomator Stops FPS Capture");
            }
            else
            {
                Process processcmd = Runtime.getRuntime().exec("su");
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

                OutputStream os = processcmd.getOutputStream();
                // below code for capture screenshot and placed in internal storage.
                os.write("adb shell\n".getBytes());
                // below code for increase brightness.
                os.write("su \n".getBytes());

                os.write(
                        "for i in `ps -ef| grep -v grep |grep 'cat /sys/kernel/tracing/trace_pipe'|awk '{print $2}'` \n"
                                .getBytes());
                os.write("do \n".getBytes());
                os.write(" kill $i \n".getBytes());
                os.write("done \n".getBytes());

                os.flush();
                // os.close();
                Log.d(GlobalVariables.Tag_Name, "FPS Capture is stopped");
                UtilityClass.appendLogFileByDate("UIAutomator Stops FPS Capture");
            }


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void copyFpsFile(String path,String testId)
    {
        try {
            //device.executeShellCommand("sh /sdcard/QosbeeFiles/FPSUtility/gfx.sh");

            String fpsPath ="mv /data/local/tmp/fps/fps.csv "+path+"fps.csv"+ " && rm -rf /data/local/tmp/fps";
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));
            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());
            os.write(fpsPath.getBytes());
            //  os.write(fpsPath1.getBytes());
            Log.d(GlobalVariables.Tag_Name, "Copied FPS csv file Successfully" + fpsPath);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.getMessage();
            Log.d(GlobalVariables.Tag_Name, "Exception in moving" + e.getMessage());

        }
    }

    private static String getGfxPath(Context context) {
        AssetManager assetManager = context.getAssets();
        InputStream reader = null;
        File mFile = new File("/sdcard/QosbeeFiles/FPSUtility/gfx.sh");
        try {
            if(!mFile.exists()){
                mFile.createNewFile();
            }
            FileOutputStream fileWriter = new FileOutputStream("/sdcard/QosbeeFiles/FPSUtility/gfx.sh");
            reader = assetManager.open("gfx.sh");
            // do reading, usually loop until end of file reading
            int mLine;
            byte[] buffer = new byte[1024];
            while ((mLine = reader.read(buffer)) != -1) {
                //process line
                fileWriter.write(buffer,0,mLine);
            }
        } catch (IOException e) {
            //log the exception
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return mFile.getAbsolutePath();
    }



    private static void copyFromAsset(Context context)
    {
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        InputStream in = null;
        OutputStream out = null;
        String filename = "busybox";
        try
        {
            in = assetManager.open(filename);
            out = new FileOutputStream("/sdcard/QosbeeFiles/FPSUtility/" + filename);
            File file=new File("/sdcard/QosbeeFiles/FPSUtility/" + filename);
            if(!file.exists())
            {
                file.createNewFile();
            }
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }
        catch(IOException e)
        {
            Log.e("tag", "Failed to copy asset file: " + filename, e);
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }
}
