package com.mozark.uiautomatorlibrary.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class Helper {

  public static String OUTPUT_LOG_FILE_TEMP_APP_VERSION_NAME = "/sdcard/QosbeeFiles/";
  public static boolean isAppRunning(final Context context, final String packageName) {
    try {
      final ActivityManager activityManager =
          (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
      final List<ActivityManager.RunningAppProcessInfo> procInfos =
          activityManager.getRunningAppProcesses();
      if (procInfos != null) {
        for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
          if (processInfo.processName.equals(packageName)) {
            return true;
          }
        }
      }
      return false;
    } catch (Exception e) {
      Log.e("", "" + e.getMessage());
    }
    return false;
  }

  public static void detectAppVersionName(String airtelTV_package) {
    try {
      Process processcmd = Runtime.getRuntime().exec("su");
      OutputStream os = processcmd.getOutputStream();
      //                    os.write("adb shell ".getBytes());
      os.write(
          ("dumpsys package "
                  + airtelTV_package
                  + " | grep versionName >"
                  + OUTPUT_LOG_FILE_TEMP_APP_VERSION_NAME
                  + "version.txt")
              .getBytes());
      os.flush();
      os.close();

      //  Runtime.getRuntime().exec("dumpsys package com.google.android.youtube | grep versionName
      // >"+"/sdcard/version.txt");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
