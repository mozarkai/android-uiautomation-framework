package com.mozark.uiautomatorlibrary.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/** Created by ProgrammingKnowledge on 4/3/2015. */
public class DatabaseHelper extends SQLiteOpenHelper {

  public static final String DATABASE_NAME = "UiAutomator.db";
  public static final String TABLE_NAME = "UiAutomatorDetails_table";
  public static final String COL_1 = "Job_Id";
  public static final String COL_2 = "Device_Id";
  public static final String COL_3 = "Data_Id";
  public static final String COL_4 = "Total_time";
  public static final String COL_5 = "Play_start_time";
  public static final String COL_6 = "Launch_Time";
  public static final String COL_7 = "Buffer_Count";
  public static final String COL_8 = "Buffer_Percentage";

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, 1);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(
        "create table "
            + TABLE_NAME
            + " (job_Id INTEGER PRIMARY KEY AUTOINCREMENT,Device_Id TEXT,Data_Id INTEGER,Total_time TEXT,Play_start_time TEXT,Launch_Time TEXT,Buffer_Count TEXT,Buffer_Percentage TEXT)");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }

  public boolean insertData(
      Integer Job_Id,
      String Device_Id,
      Integer Data_Id,
      String Total_time,
      String Play_start_time,
      String Launch_Time,
      String Buffer_Count,
      String Buffer_Percentage) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_1, Job_Id);
    contentValues.put(COL_2, Device_Id);
    contentValues.put(COL_3, Data_Id);
    contentValues.put(COL_4, Total_time);
    contentValues.put(COL_5, Play_start_time);
    contentValues.put(COL_6, Launch_Time);
    contentValues.put(COL_7, Buffer_Count);
    contentValues.put(COL_8, Buffer_Percentage);

    long result = db.insert(TABLE_NAME, null, contentValues);
    return result != -1;
  }

  public Cursor getAllData() {
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
    return res;
  }

  public boolean updateData(
      Integer Job_Id,
      String Device_Id,
      Integer Data_Id,
      String Total_time,
      String Play_start_time,
      String Launch_Time,
      String Buffer_Count,
      String Buffer_Percentage) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put(COL_1, Job_Id);
    contentValues.put(COL_2, Device_Id);
    contentValues.put(COL_3, Data_Id);
    contentValues.put(COL_4, Total_time);
    contentValues.put(COL_5, Play_start_time);
    contentValues.put(COL_6, Launch_Time);
    contentValues.put(COL_7, Buffer_Count);
    contentValues.put(COL_8, Buffer_Percentage);

    db.update(TABLE_NAME, contentValues, "ID = ?", new String[] {String.valueOf(Job_Id)});
    return true;
  }

  public Integer deleteData(String Job_Id) {
    SQLiteDatabase db = this.getWritableDatabase();
    return db.delete(TABLE_NAME, "ID = ?", new String[] {Job_Id});
  }
}
