package com.mozark.uiautomatorlibrary.utils.aqua5gmarkPro;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.BySelector;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.UiSelector;

import com.mozark.uiautomatorlibrary.TestApp;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;


import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.mozark.uiautomatorlibrary.utils.UtilityClass.getJson5gmarkPro;


public class Aqua5gmarkPro {
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    private Context context;
    String resultUplaod,resultDownload,resultULatency;


    public void Aqua5gmarkRun(UiDevice device, Context context) {
        try {

            this.device = device;
            this.context = context;
            startAppActivity(GlobalVariables.Five_gmarkPro_Package, device, context);
            Utility.permission(GlobalVariables.Five_gmarkPro_Package);

            selectTest();
            launchButton();
            duringTest();
            uploadResult();

            String result=getJson5gmarkPro(resultDownload,resultUplaod,"-s","-s",resultULatency);
            writeResult(result);
            tearDown();



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Five_gmarkPro_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean selectTest() {
        try {
            boolean selectTest = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name,
                            "Trying to find  selectTest button with Attempt no." + (i + 1));

                    UiCollection selectTestID =
                            new UiCollection(new UiSelector().resourceId("qosi.fr.pro5gmark:id/scenario_list"));

                    UiSelector selectBtnId = new UiSelector().textContains("Pre Aquamark Test");

                    UiObject selectBtn = device.findObject(selectBtnId);


                    if (selectBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        selectBtn.click();
                        selectTest = true;
                        break;
                    } else {

                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on selectTest  Button Failed");
                }
            }
            return selectTest;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding  selectTest Button Method");
            return false;
        }
    }

    private boolean launchButton() {
        try {
            boolean launchButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name,
                            "Trying to find  LaunchButton  with Attempt no." + (i + 1));

                    UiSelector launchButtonID =
                            new UiSelector().resourceId("qosi.fr.pro5gmark:id/launch_test_button");
                    UiObject launchBtn = device.findObject(launchButtonID);

                    if (launchBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        launchBtn.click();
                        launchButton = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on LaunchButton   Failed");
                }
            }
            return launchButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding  LaunchButton  Method");
            return false;
        }
    }

    private boolean duringTest() {
        try {
            boolean duringTest = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to duringTest with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectvideoId = new UiSelector().textContains("Scenario finished");
                    UiObject select = device.findObject(selectvideoId);
                    if (select.waitForExists(GlobalVariables.Video_Run_Time)) {
                        duringTest = true;
                        break;

                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on duringTest Failed");
                }
            }
            return duringTest;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in duringTest");
            return false;
        }
    }

    private boolean uploadResult() {
        try {
            boolean result = false;
            NetworkDetails networkDetails = new NetworkDetails();

            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name,
                            "Trying to find  result  with Attempt no." + (i + 1));

                    UiObject upload = new UiObject(new UiSelector().className("androidx.recyclerview.widget.RecyclerView").instance(0).className("android.widget.RelativeLayout").index(8).className("android.widget.TextView").index(3));

                    UiObject download = new UiObject(new UiSelector().className("androidx.recyclerview.widget.RecyclerView").instance(1).className("android.widget.RelativeLayout").index(8).className("android.widget.TextView").index(3));

                    UiObject latency = new UiObject(new UiSelector().className("androidx.recyclerview.widget.RecyclerView").instance(2).className("android.widget.RelativeLayout").index(8).className("android.widget.TextView").index(3));

                    if (upload.waitForExists(GlobalVariables.Wait_Timeout)) {
                        resultUplaod = upload.getText().replace("Mbps", "mbps");
                        if (resultUplaod.contains("mbps")) {
                            resultUplaod=upload.getText().replace("Mbps", "mbps");;
                        }
                        else
                        {
                            resultUplaod="timeout";
                        }
                    }

                    if (download.waitForExists(GlobalVariables.Wait_Timeout)) {
                        resultDownload = download.getText().replace("Mbps", "mbps");
                        if (resultDownload.contains("mbps")) {
                            resultDownload=download.getText().replace("Mbps", "mbps");
                        }
                        else
                        {
                            resultDownload="timeout";
                        }
                    }

                    if (latency.waitForExists(GlobalVariables.Wait_Timeout)) {
                        resultULatency = latency.getText();
                        if (resultULatency.contains("ms")) {
                            resultULatency=latency.getText();
                        }
                        else
                        {
                            resultULatency="timeout";
                        }

                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on result   Failed");
                }
            }
            return result;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding  result  Method");
            return false;
        }
    }

    private void startAppActivity(String appPackage, UiDevice device, Context context) {
        try {
            Log.d(GlobalVariables.Tag_Name, "Launching the App Activity");

            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(appPackage);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            device.hasObject(By.pkg(appPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Launching the App Activity");
            e.printStackTrace();
        }
    }

    public void writeResult(String obj) {
        FileWriter file = null;
        try {

            File files = new File("/sdcard/QosbeeFiles");
            if(!files.exists())
            {
                files.mkdir();
            }
            // Constructs a FileWriter given a file name, using the platform's default charset
            file = new FileWriter("/sdcard/QosbeeFiles/aqua5gmark.txt");
            file.write(obj);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {

            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
