package com.mozark.uiautomatorlibrary.utils.record;


import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.UtilityClass;

import java.io.File;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.UtilityClass.getDateString;

public class RecordScreen {

  public static void startScreenRecording(String testId, int duration, boolean vCap) {
    new Thread(
            new Runnable() {
              @Override
              public void run() {
                try {

                  if (vCap) {
                    File file = new File("/sdcard/MozarkUtility/Mozark/" + testId + "/");
                    if (!file.exists()) {
                      file.mkdirs();
                    }
                    DataHolder.getInstance().setVideoStartTime(getDateString());
                    Process processcmd = Runtime.getRuntime().exec("su");

                    OutputStream os = processcmd.getOutputStream();
                    // below code for capture screenshot and placed in internal storage.
                    os.write("adb shell\n".getBytes());
                    // below code for increase brightness.
                    os.write(
                            ("screenrecord --time-limit="
                                    + duration
                                    + " --bit-rate  400000  /mnt/sdcard/MozarkUtility/Mozark/"
                                    + testId
                                    + "/"
                                    + testId
                                    + "video.mp4"
                                    + "\n")
                                    .getBytes());
                    os.flush();
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            })
        .start();
  }

  public void startRecord(String testId, int duration, boolean vCap) {
    new Thread(
            new Runnable() {
              @Override
              public void run() {
                try {

                  if (vCap) {
                    File file = new File("/sdcard/MozarkUtility/Mozark/" + testId + "/");
                    if (!file.exists()) {
                      file.mkdirs();
                    }
                    DataHolder.getInstance().setVideoStartTime(getDateString());
//                    ExampleInstrumentedTest.vCapEndDateTime = date.toUpperCase();
                    Process processcmd = Runtime.getRuntime().exec("su");

                    OutputStream os = processcmd.getOutputStream();
                    // below code for capture screenshot and placed in internal storage.
                    os.write("adb shell\n".getBytes());
                    // below code for increase brightness.
                    os.write(
                            ("screenrecord --time-limit="
                                    + duration
                                    + " --size 720x1280 --bit-rate  10M  /mnt/sdcard/MozarkUtility/Mozark/"
                                    + testId
                                    + "/"
                                    + testId
                                    + "video.mp4"
                                    + "\n")
                                    .getBytes());
                    os.flush();

                    UtilityClass.appendLogFileByDate(
                            "UIAutomator Record screen :"
                                    + ("screenrecord --time-limit="
                                    + duration
                                    + " --bit-rate 4000000 /mnt/sdcard/MozarkUtility/Mozark/"
                                    + testId
                                    + "/"
                                    + testId
                                    + "video.mp4"
                                    + "\n"));
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            })
            .start();
  }
}
