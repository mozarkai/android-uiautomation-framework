package com.mozark.uiautomatorlibrary.utils;

import com.mozark.uiautomatorlibrary.senddata.SendDataApi;
import com.mozark.uiautomatorlibrary.updatedevicestatus.PutDeviceStatus;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateDeviceStatus {

  public void update(String deviceId, String ipAdress, String run) {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    Retrofit retroclient =
        new Retrofit.Builder()
            .baseUrl(ipAdress)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    SendDataApi service = retroclient.create(SendDataApi.class);
    Call<PutDeviceStatus> call = service.updateDeviceStatus(deviceId, run);
    try {
      Response<PutDeviceStatus> result = call.execute();
//      Log.d(GlobalVariables.Tag_Name, "updateDeviceStatus result.body() :" + result.body());
      UtilityClass.appendLogFileByDate(
          "UIAutomator updateDeviceStatus API result.body() :" + result.body());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
