package com.mozark.uiautomatorlibrary.utils;


import java.io.File;

public interface GlobalVariables {

    boolean fiveGmark=true;

    String Hotstar_Package = "in.startv.hotstar";
    String HotstarSingapore_Package = "in.startv.hotstaronly";
    String YouTube_Package = "com.google.android.youtube";
    String TimesNow_Package = "com.timesnowmobile.TimesNow";
    String IndiaToday_Package = "com.indiatoday";
    String NDTV_Package = "com.july.ndtv";
    String Ola_Package = "com.olacabs.customer";
    String Uber_Package = "com.ubercab";
    String Netflix_Package = "com.netflix.mediaclient";
    String WhatsApp_Package = "com.whatsapp";
    String Zee5_Package = "com.graymatrix.did";
    String Voot_Package = "com.tv.v18.viola";
    String SonyLiv_Package = "com.sonyliv";
    String ZoomPackage = "us.zoom.videomeetings";
    String WebexPackage = "com.cisco.webex.meetings";
    String AirtelTV_Package = "tv.accedo.airtel.wynk";
    String Prime_Package = "com.amazon.avod.thirdpartyclient";
    String OCS_Package = "com.orange.ocsgo";
    String GoogleDrivePackage = "com.google.android.apps.docs";
    String TeamsPackage = "com.microsoft.teams";
    String GoogleMeetPackage = "com.google.android.apps.meetings";
    String UnionBank_Package = "com.unionbankph.online";
    String SecurityBank_Package = "com.securitybank";
    String BPI_Mobile_Package = "com.bpi.ng.mobilebanking";
    String CLOUD_NINE_PACKAGE = "com.cloudnine";
    String Chrome_Package = "com.android.chrome";
    String PURE_GOLD_PACKAGE = "com.grocery.puregold";
    String A5GMarkPro_Package = "fr.qosi.framework.3RD_PARTY";
    String Twitter_Package = "com.twitter.android";
    String Qatar_Classifieds_Package = "com.qatarliving.classifieds";
    String BlueJeans_Package = "com.bluejeansnet.Base";
    String GoToMeeting_Package = "com.gotomeeting";
    String AltBalaji_Package = "com.balaji.alt";
    String Hoichoi_Package = "com.viewlift.hoichoi";
    String JioMeet_Package = "com.jio.rilconferences";
    String Shahid_Package = "net.mbc.shahid";
    String Starzplay_Package = "com.parsifal.starz";
    String SwitchTV_Package = "ae.etisalat.switchtv";
    String Icflix_Package = "com.mautilus.icflix";
    String Seevii_Package = "com.vianeos.pcplayer";
    String OSN_Package = "com.osn.go";
    String VodafoneTV_Package = "com.vodafone.vtv.hu";
    String MAGISK_Package = "com.topjohnwu.magisk";
    String MPL_Package = "com.mpl.androidapp";
    String DisneyPlusUSA_Package = "com.disney.disneyplus";
    String DisneyPlus_Package = "in.startv.hotstar.dplus";
    String Toda_Passenger_Package = "com.toda.lns";
    String BajajFinserv_Package = "org.altruist.BajajExperia";
    String MobileLegends_Package = "com.mobile.legends";
    String Facebook_Package = "com.facebook.katana";
    String TikTokQatar_Package = "com.zhiliaoapp.musically";
    String Tiktok_Package = "com.ss.android.ugc.trill";
    String AGENT_NAME = "com.mozark.aquamark";
    String AGENT_FPS_ACTION = "com.mozark.aquamark.fps";
    String AGENT_FPS_RECEIVER_CLASS = "com.mozark.aquamark.network.receivers.StartFpsService";
    String HDFCSecurities_Package = "com.snapwork.hdfcsec";
    String AngelBroking_Package = "com.msf.angelmobile";
    String IIFLMarkets_Package = "com.indiainfoline";
    String UpstoxPro_Package = "in.upstox.pro";
    String Five_gmarkPro_Package="qosi.fr.pro5gmark";
    String Pulse_Package = "com.prudential.pulse.onepulse";
    String OTVPreProd_Package="com.telekom.onetv.hu.preprodLidNDDebug";
    String OTVProd_Package = "com.telekom.onetv.hu.prodDebug";
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String MKRef_Package = "hu.telekom.telekomtv";
    String VodafoneTVHungary_Package = "com.vodafone.vtv.hu";
    String Viber_Package = "com.viber.voip";
    String Shopee_Package = "com.shopee.ph";
    String Lazada_Package = "com.lazada.android";
    String Telegram_Package = "org.telegram.messenger";
    String CODM_Package = "com.garena.game.codm";
    String GCash_Package = "com.globe.gcash.android";
    String Waze_Package = "com.waze";
    String Grab_Package = "com.grabtaxi.passenger";
    String Spotify_Package = "com.spotify.music";
    String GoogleMap_Package = "com.google.android.apps.maps";
    String GmoviePackage="com.globereel";
    String OrangeMaliPackage="com.oml.dsi.orangemobile";
    String Messenger_Package = "com.facebook.orca";
    String Hangouts_Package = "com.google.android.talk";
    String GDrive_Package = "com.google.android.apps.docs";
    String Kumu_Package = "im.kumu.ph";
    String GlobeAtHome_Package = "ph.com.globe.globeathome";
    String GlobeOne_Package = "ph.com.globe.one";
    String PhonePe_Package="com.phonepe.app";
    String NewGlobeOne_Package = "ph.com.globe.globeonesuperapp";
    String MicrosoftTeams_Package = "com.microsoft.teams";
    String Skype_Package = "com.skype.raider";
    String GPAY_Package="com.google.android.apps.nbu.paisa.user";
    String Nykaa_Package="com.fsn.nds";
    String Myntra_Package="com.myntra.android";
    String Paytm_Package = "net.one97.paytm";
    String InstagramPackage = "com.instagram.android";
    String SnapchatPackage = "com.snapchat.android";
    String ShahidPackage = "net.mbc.shahid";
    String Mcdo_Package = "ph.mobext.mcdelivery";

    int LAUNCH_TIMEOUT = 3000;
    int TimeOut = 5000;
    int GoogleTime = 2000;
    int Time = 0;
    int Delay_Time = 55000;
    int Wait_Timeout = 12000;

    String SonyLivURL = "sonyliv.com";
    String VootURL = "voot.com";

    String PremiumEmail = "mdcproject20121995@gmail.com";
    String PremiumPwd = "20dk1995@";
    String NonPremiumEmail = "krishna.k@infosoftjoin.in";
    String NonPremiumPwd = "Test@123";

    String TwitterEmail = "lccqatar1";
    String TwitterPassword = "asdf123*";
    //    String TwitterEmail = "akanksha.srivastava201295@gmail.com";
    //    String TwitterPassword = "Test@123";
    String TwitterText =
            "QATAR - A land that understands a traveler can never be a stranger, just a friend not yet met. That ultimately everyone is on their own journey. A land rooted in authentic soul. A land offers enlightenment,inspiration through its warmth of soul. Tweet Time:";

    String Tag_Name = "Aquamark";
    String App_Launch_Time = "App Launch Time:";
    String Search_Appear_Time = "Search Button Appear Time:";
    String Play_Video_Time = "Click on Play Video Time:";
    String Loader_Appear_Yes = "Loader Appear:YES Time:";
    String Loader_Appear_No = "Loader Appear:NO Time:";
    String Enter_Search_Time = "Enter Search Time:";
    String Search_Result_Time = "Search Result Time:";
    String Select_Video_Time = "Select Video Time:";
    String Load_HomePage_Time = "Home Page Loading Time:";
    String Live_Button_Time = "Click on Live Button Time:";
    String Select_Game_Time = "Select Game Time:";
    String Free_Appear_Time = "Free Button Appear Time:";
    String Battle_Free_Time = "Click on Battle For Free Time:";
    String GameLoading_Yes = "Game Loader:YES Time:";
    String GameLoading_No = "Game Loader:NO Time:";
    String Video_Start_Time = "Video Started Time:";

    String Enter_Tag_Search_Time = "Enter #Tag Search Time:";
    String Search_Tag_Result_Time = "Search #Tag Result Time:";
    String Home_Button_Time = "Click Home Button Time:";
    String Load_Page_Time = "Time to Load Page:";
    String Intestitial_Page_Time = "Intestitial Page Click Time:";

    String JoinMeetingClickTime = "Click on Join Meeting Time:";
    String UploadClickTime = "Click on Upload Button Time:";
    String DownloadClickTime = "Click on Download Button Time:";
    String Loading_Yes = "Loading:YES Time:";
    String Loading_No = "Loading:NO Time:";
    String File_Uploading = "File is Uploading Time:";
    String File_Uploaded = "File Uploaded Time:";
    String File_Downloading = "File is Downloading Time:";
    String File_Downloaded = "File Downloaded Time:";
    String Click_On_Cart_Time = "Click on Add to cart Time:";
    String Cart_items_appear_time = "Cart items appear Time:";
    String Click_On_CheckOut_time = "Click on checkout Time:";
    String Checkout_page_appear_time = "Checkout page appear Time:";
    String click_on_delivery = "Click on delivery Time:";
    String Load_Yes = "Load-Yes Time:";
    String Load_No = "Load-No Time:";
    String Click_On_Menu_Time = "Click on Menu Time:";
    String Menu_page_appear_Time = "Menu page appear Time:";

    String Product_Enter_Search_Time = "Enter Search Time Product:";
    String Product_Search_Result_Time = "Search Result Time Product:";
    long Browse_Time = 200000;
    long HalfMin_Timout = 30000;
    long OneMin_Timeout = 60000;
    long TwoMins_Timeout = 120000;
    long Video_Run_Time = 60000;
    long Video_Run_TimeMid = 180000;
    long Video_Run_TimeLong = 300000;
    long Long_TimeOut = 20000;
    int screenRecord_Time = 180;

    String textType = "text";
    String descriptionType = "description";
    String resourceIDType = "resourceID";
    String collectionType = "collection";

    String MPL_OTP = "110011";
    long Timeout = 3000;

    String Game_Screenshot = "/sdcard/GameApp_Screenshots/";
}
