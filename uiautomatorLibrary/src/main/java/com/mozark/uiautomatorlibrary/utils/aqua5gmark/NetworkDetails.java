package com.mozark.uiautomatorlibrary.utils.aqua5gmark;


public class NetworkDetails {

  public String latency;
  public String downLoadSpeed;
  public String uploadSpeed;
  public String stream;
  public String web;
  public String isp;

  public String getLatency() {
    return latency;
  }

  public void setLatency(String latency) {
    this.latency = latency;
  }

  public String getDownLoadSpeed() {
    return downLoadSpeed;
  }

  public void setDownLoadSpeed(String downLoadSpeed) {
    this.downLoadSpeed = downLoadSpeed;
  }

  public String getUploadSpeed() {
    return uploadSpeed;
  }

  public void setUploadSpeed(String uploadSpeed) {
    this.uploadSpeed = uploadSpeed;
  }

  public String getStream() {
    return stream;
  }

  public void setStream(String stream) {
    this.stream = stream;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getIsp() {
    return isp;
  }

  public void setIsp(String isp) {
    this.isp = isp;
  }

  public NetworkDetails() {
  }
}
