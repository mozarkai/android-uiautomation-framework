package com.mozark.uiautomatorlibrary.utils.aqua5gmark;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.google.gson.Gson;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Aqua5GMark {

    public static String AQUA5GMARK_PACKAGE =
            "com.agence3pp"; // "com.agence3pp/qosi.fr.usingqosiframework.splashscreen.SplashscreenActivity";

    public static void launchApp(Context context, UiDevice device) {
        try {
            File file = new File("/sdcard/QosbeeFiles/aqua5gmark.txt");
            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(AQUA5GMARK_PACKAGE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            if (file.exists()) {
                file.delete();
            }
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            file = new File("/sdcard/QosbeeFiles/aqua5gmark.txt");

            while (!Utility.checkFileExit(file) || stopWatch.getTime() >= 60000) {
                Log.d(
                        "Aquamark",
                        "Waiting the result of 5G Mark network result stopWatch.getTime() : "
                                + stopWatch.getTime());
                try {
                    Thread.sleep(2000);
                    file = new File("/sdcard/QosbeeFiles/aqua5gmark.txt");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Log.d("Aquamark", "EXIT... Waiting the result of 5G Mark network result");
        } catch (Exception e) {
            Log.e("Aquamark", "EXIT... Waiting the result of 5G Mark network result " + e.getMessage());
        }
    }

    // aqua5gmark.txt
    public static String read5GMarkMeasurement(File file) {
        try {
            // final String FILE_NAME = "example.txt";
            FileInputStream fis = new FileInputStream(file);
            //  fis = con.openFileInput(yourFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }
            System.out.println("the file details is " + sb.toString());
            fis.close();
            return sb.toString();
        } catch (Exception e) {
            Log.e("", "" + e.getMessage());
        }
        return "";
    }

    public static void LaunchAqua5GMark(
            String device_id, String ipAdress, Context context, UiDevice device, String json) {
        try {
            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            updateDeviceStatus.update(device_id, ipAdress, "Running");
            Aqua5GMark.launchApp(context, device);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Launching Aquamark 5Mark");
            e.printStackTrace();
        }
    }
}
