package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;


import java.io.File;

public class LogGenerationFile {

  private String OUTPUT_LOG_FILE = "";

  public void logFileGeneration(String testId, String ipAddress, String deviceId) {
    try {
      File file = new File("/sdcard/Mozark");
      if (!file.exists()) {
        file.mkdir();
      }
      File file1 = new File(file + "/" + testId);
      // String mLogFileName = "/storage/emulated/0/rooted_pcap/" + fileName;
      if (!file1.exists()) {
        file1.mkdir();
      }

      File source = new File("/sdcard/log.txt");
      UtilityClass.printFileSize(source);
      //  File sourceM = new File("/sdcard/logM.txt");
      OUTPUT_LOG_FILE = file1 + "/" + testId + ".txt";
      File destFile = new File(OUTPUT_LOG_FILE);
      Utility.copy(source, destFile);
      UtilityClass.appendLogFileByDate("UIAutomator LogFile generated");
//      if (ExampleInstrumentedTest.captureCopy) {
//        try {
//          /*
//          File logcatDestFile = new File(file1 + "/" + testId + "applaunch.txt");
//          Utility.copy(new File("/sdcard/logcat.txt"), logcatDestFile);
//          String result = UtilityClass.searchAndGetSpecificLine("/sdcard/logcat.txt");
//          Log.d(GlobalVariables.Tag_Name, "logFileGeneration Appload time -> " + result);
//          FileWriter writer = new FileWriter(destFile, true);
//          writer.append(JsonUtil.toJson(result));
//          writer.flush();
//          writer.close();*/
//        } catch (Exception e) {
//          e.printStackTrace();
//        }
//      }

      sendLogs(testId, new File(OUTPUT_LOG_FILE), ipAddress, deviceId);
//      ExampleInstrumentedTest.stopLog = true;
    } catch (Exception e) {
      Log.e("", "Log file generation -> " + e.getMessage());
    }
  }

  public void sendLogs(String testId, File Output, String ipAddress, String deviceId) {
    Log.d(
        "",
        "Log file generation -> "
            + "testId : "
            + testId
            + " FileName : "
            + Output.getAbsolutePath());
    UtilityClass.printFileSize(Output);
//    if (ExampleInstrumentedTest.captureCopy) {
//      File copyFile = new File("/sdcard/Copy" + "/" + testId);
//      if (!copyFile.exists()) {
//        copyFile.mkdirs();
//      }
//      File copyFileSrc = new File(copyFile + "/" + testId + ".txt");
//      try {
//
//        copy(Output, copyFileSrc);
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//      UtilityClass.printFileSize(copyFileSrc);
//    }
    SendLogsToServer sendLogsToServer = new SendLogsToServer();
    sendLogsToServer.getLogsPath(testId, Output, ipAddress, deviceId);
  }
}
