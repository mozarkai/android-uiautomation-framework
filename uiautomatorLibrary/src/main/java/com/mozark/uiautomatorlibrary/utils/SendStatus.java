package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;

import com.mozark.uiautomatorlibrary.getdata.GetData;
import com.mozark.uiautomatorlibrary.senddata.Json;
import com.mozark.uiautomatorlibrary.senddata.Request;
import com.mozark.uiautomatorlibrary.senddata.SendData;
import com.mozark.uiautomatorlibrary.senddata.SendDataApi;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mozark.uiautomatorlibrary.utils.Utility.SCRIPT_RESULT;
import static com.mozark.uiautomatorlibrary.utils.UtilityClass.Intent_Launching_Application_Result;

public class SendStatus {

    public boolean Flag = false;
    String testId;
    private String OUTPUT_LOG_FILE = "";

    public boolean status(
            int jobId,
            String deviceId,
            String testId,
            String status,
            boolean liveStreaming,
            String orderId,
            String scriptId,
            String appVersion,
            String appName,
            String ipAdress,String startDate) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retroclient =
                new Retrofit.Builder()
                        .baseUrl(ipAdress)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();
        this.testId = testId;

        NetworkDetails networkDetails=UtilityClass.get5gMarkData();
        SendDataApi service = retroclient.create(SendDataApi.class);
        SendData sendData =
                new SendData(
                        new Json(
                                new Request(
                                        new Data(
                                                jobId,
                                                deviceId,
                                                testId,
                                                status,
                                                liveStreaming,
                                                Integer.valueOf(orderId),
                                                Integer.valueOf(scriptId),
                                                appVersion,
                                                appName,startDate))));

        if(networkDetails!=null)
        {
            String isp=DataHolder.getInstance().getIsp();
            if(isp!=null & !isp.isEmpty())
            {
                networkDetails.isp=isp;
            }
            sendData
                    .getJson()
                    .getRequest()
                    .getData()
                    .setNetworkDetails(networkDetails);
        }
        Call<GetData> call = service.getStringScalar(sendData);
        UtilityClass.appendLogFileByDate(
                "UIAutomator SendStatus Request : " + JsonUtil.toJson(sendData));
        try {
            Response<GetData> response = call.execute();
//      Log.d(
//          GlobalVariables.Tag_Name,
//          "Performance API Response  = " + JsonUtil.toJson(response.body()));
            if (response.isSuccessful()) {
                DataHolder.getInstance().clear();
//        Log.d("", "SendStatus -> " + response.body().toString());
//        Log.d(
//            GlobalVariables.Tag_Name,
//            "Performance API Response  = " + JsonUtil.toJson(response.body()));
                UtilityClass.appendLogFileByDate(
                        "UIAutomator SendStatus Result : " + response.body().toString());
                String resultStatus = null;
                if (response.body() != null) {
                    resultStatus = response.body().getJson().getResponse().getStatusCode();
                }
                if (resultStatus != null && resultStatus.equalsIgnoreCase("0")) {
                    Data data =
                            new Data(
                                    jobId,
                                    deviceId,
                                    testId,
                                    "completed",
                                    false,
                                    Integer.valueOf(orderId),
                                    Integer.valueOf(scriptId),
                                    appVersion,
                                    appName,startDate);
                    Utility.writeLogResultForLaterUse(data, SCRIPT_RESULT);
//                    if (DataHolder.getInstance().isFromIntent()) {
//                        Utility.writeForIntent(data, Intent_Launching_Application_Result,
//                                "Success");
//                    }
//                    Context context = TestApp.getContext();
                    return true;
                }
            }

        } catch (IOException e) {
            Log.e("", "SendStatus failed -> " + e.getMessage());
            UtilityClass.appendLogFileByDate("UIAutomator SendStatus failed : " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

}
