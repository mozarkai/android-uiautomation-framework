package com.mozark.uiautomatorlibrary.utils;


import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mozark.uiautomatorlibrary.mozarkmodel.CellInfoModel;
import com.mozark.uiautomatorlibrary.mozarkmodel.DeviceInfoKpis;
import com.mozark.uiautomatorlibrary.mozarkmodel.SendDataToUIAutomator;
import com.mozark.uiautomatorlibrary.mozarkmodel.WifiInfoModel;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.mozark.uiautomatorlibrary.utils.UtilityClass.getDateString;


public class Data {

    private String lat;
    private String lng;
    private String testDateTime, endDateTime;
    private StartEndDateTime startEndDateTime;
    private int jobId;
    private String deviceId;
    private String testId;
    private String status;
    private boolean liveStreaming;
    private int scriptId;
    private int orderId;
    private String appVersion;
    private String appName;
    private String timeZoneDisplay;
    private String timeZoneId;
    private com.mozark.uiautomatorlibrary.mozarkmodel.Data deviceDetail;
    private DeviceInfoKpis deviceInfoKpis;
    private WifiInfoModel wifiInfoModel;

    @SerializedName("networkDetails")
    @Expose
    private NetworkDetails networkDetails;

    public Data(
            int jobId,
            String deviceId,
            String testId,
            String status,
            boolean liveStreaming,
            int orderId,
            int scriptId,
            String appVersion,
            String appName, String startDateTime) {
        this.jobId = jobId;
        this.deviceId = deviceId;
        this.testId = testId;
        this.status = status;
        this.liveStreaming = liveStreaming;
        this.scriptId = scriptId;
        this.orderId = orderId;
        this.appVersion = appVersion;
        this.appName = appName;
        if (networkDetails == null) {
            networkDetails = new NetworkDetails();
        }

        SendDataToUIAutomator sendDataToUIAutomator = UtilityClass.getDataForUIAutomator();
        this.lat = sendDataToUIAutomator.getLat();
        this.lng = sendDataToUIAutomator.getLng();
        String isp=sendDataToUIAutomator.getIsp();
        deviceInfoKpis=new DeviceInfoKpis();
        this.networkDetails = UtilityClass.get5gMarkData();
        List<CellInfoModel> cellInfoModels= UtilityClass.getCellInfo();
        deviceInfoKpis.setCellInfoModels(cellInfoModels);
        Log.d("Aquamark","The value of cell From cellApp"+UtilityClass.getCellInfo());
        this.testDateTime = startDateTime;
        this.deviceDetail = sendDataToUIAutomator.getDeviceDetail();
        Date varDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
        String date = dateFormat.format(varDate);
        this.endDateTime = date.toUpperCase();
        try {
            Date varDate1 = new Date(varDate.getTime() + (5 * 1000));
            dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
            date = dateFormat.format(varDate1);
            System.out.println("Date :" + date);
            TimeZone tz = TimeZone.getDefault();
            System.out.println(
                    "TimeZone   "
                            + tz.getDisplayName(false, TimeZone.SHORT)
                            + " Timezon id :: "
                            + tz.getID());
            String timeZoneDisplayName = tz.getDisplayName(false, TimeZone.SHORT);
            String timeZoneId = tz.getID();
            this.startEndDateTime =
                    new StartEndDateTime(
                            startDateTime,
                            date.toUpperCase(),
                            DataHolder.getInstance().getpCapStartTime(),
                            DataHolder.getInstance().getpCapEndTime(),
                            DataHolder.getInstance().getVideoStartTime(),
                            DataHolder.getInstance().getVideoEndTime(),
                            timeZoneDisplayName,
                            timeZoneId);
            if (sendDataToUIAutomator.getDeviceInfoKpis() == null) {
                sendDataToUIAutomator.setDeviceInfoKpis(new DeviceInfoKpis());
            }

            if (sendDataToUIAutomator.getWifiInfoModel() != null
                    && !sendDataToUIAutomator.getWifiInfoModel().isEmpty()) {
                List<WifiInfoModel> mWifiInfoList = new ArrayList<>();
                for (WifiInfoModel mInfo : sendDataToUIAutomator.getWifiInfoModel()) {
                    if(networkDetails!=null) {
                        mInfo.downLoadSpeed = networkDetails.downLoadSpeed;
                        mInfo.uploadSpeed = networkDetails.uploadSpeed;
                        mInfo.latency = networkDetails.latency;
                        mInfo.stream = networkDetails.stream;
                        mInfo.web = networkDetails.web;
                        mWifiInfoList.add(mInfo);
                    }
                }
                sendDataToUIAutomator
                        .getDeviceInfoKpis()
                        .setWifiInfoModels(mWifiInfoList);
            } else {
                sendDataToUIAutomator.getDeviceInfoKpis().setWifiInfoModels(null);
            }

            if (UtilityClass.getCellInfo() != null
                    && !UtilityClass.getCellInfo().isEmpty()) {
                List<CellInfoModel> mCellInfoList = new ArrayList<>();
                for (CellInfoModel mInfo : UtilityClass.getCellInfo()) {
                    if (networkDetails!=null)
                    {
                        mInfo.downLoadSpeed = networkDetails.downLoadSpeed;
                        mInfo.uploadSpeed = networkDetails.uploadSpeed;
                        mInfo.latency = networkDetails.latency;
                        mInfo.stream = networkDetails.stream;
                        mInfo.web = networkDetails.web;
                        mInfo.ispname=networkDetails.isp;
                        if(mInfo.ispname==null ||mInfo.ispname.isEmpty())
                        {
                            mInfo.ispname=DataHolder.getInstance().getIsp();
                        }
                        mCellInfoList.add(mInfo);
                    }
                }
                sendDataToUIAutomator
                        .getDeviceInfoKpis()
                        .setCellInfoModels(mCellInfoList);
            } else {
                sendDataToUIAutomator.getDeviceInfoKpis().setCellInfoModels(null);
            }
            this.deviceInfoKpis = sendDataToUIAutomator.getDeviceInfoKpis();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTimeZoneDisplay() {
        return timeZoneDisplay;
    }

    public void setTimeZoneDisplay(String timeZoneDisplay) {
        this.timeZoneDisplay = timeZoneDisplay;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isLiveStreaming() {
        return liveStreaming;
    }

    public void setLiveStreaming(boolean liveStreaming) {
        this.liveStreaming = liveStreaming;
    }

    public int getScriptId() {
        return scriptId;
    }

    public void setScriptId(int scriptId) {
        this.scriptId = scriptId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public NetworkDetails getNetworkDetails() {
        return networkDetails;
    }

    public void setNetworkDetails(NetworkDetails networkDetails) {
        this.networkDetails = networkDetails;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTestDateTime() {
        return testDateTime;
    }

    public void setTestDateTime(String testDateTime) {
        this.testDateTime = testDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public StartEndDateTime getStartEndDateTime() {
        return startEndDateTime;
    }

    public void setStartEndDateTime(StartEndDateTime startEndDateTime) {
        this.startEndDateTime = startEndDateTime;
    }

    public com.mozark.uiautomatorlibrary.mozarkmodel.Data getDeviceDetail() {
        return deviceDetail;
    }

    public void setDeviceDetail(com.mozark.uiautomatorlibrary.mozarkmodel.Data deviceDetail) {
        this.deviceDetail = deviceDetail;
    }
    /*
    public List<CellInfoModel> getCellInfoModelList() {
        return cellInfoModelList;
    }

    public void setCellInfoModelList(List<CellInfoModel> cellInfoModelList) {
        this.cellInfoModelList = cellInfoModelList;
    }*/

    public DeviceInfoKpis getDeviceInfoKpis() {
        return deviceInfoKpis;
    }

    public void setDeviceInfoKpis(DeviceInfoKpis deviceInfoKpis) {
        this.deviceInfoKpis = deviceInfoKpis;
    }

}

class StartEndDateTime {

    @SerializedName("TestStartTime")
    @Expose
    String startdDateTime = "";

    @SerializedName("TestEndTime")
    @Expose
    String endDateTime = "";

    @SerializedName("PCapStartTime")
    @Expose
    String pCapStartDateTime = "";

    @SerializedName("PCapEndTime")
    @Expose
    String pCapEndDateTime = "";

    @SerializedName("VideoStartTime")
    @Expose
    String vCapStartDateTime = "";

    @SerializedName("VideoEndTime")
    @Expose
    String vCapEndDateTime = "";

    private String timeZoneDisplay;
    private String timeZoneId;

    public StartEndDateTime(
            String startdDateTime,
            String endDateTime,
            String pCapStartDateTime,
            String pCapEndDateTime,
            String vCapStartDateTime,
            String vCapEndDateTime,
            String timeZoneDisplay,
            String timeZoneId) {
        this.startdDateTime = startdDateTime;
        this.endDateTime = endDateTime;
        this.pCapStartDateTime = pCapStartDateTime;
        this.pCapEndDateTime = pCapEndDateTime;
        this.vCapStartDateTime = vCapStartDateTime;
        this.vCapEndDateTime = vCapEndDateTime;
        this.timeZoneDisplay = timeZoneDisplay;
        this.timeZoneId = timeZoneId;
    }

    public String getStartdDateTime() {
        return startdDateTime;
    }

    public void setStartdDateTime(String startdDateTime) {
        this.startdDateTime = startdDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getpCapStartDateTime() {
        return pCapStartDateTime;
    }

    public void setpCapStartDateTime(String pCapStartDateTime) {
        this.pCapStartDateTime = pCapStartDateTime;
    }

    public String getpCapEndDateTime() {
        return pCapEndDateTime;
    }

    public void setpCapEndDateTime(String pCapEndDateTime) {
        this.pCapEndDateTime = pCapEndDateTime;
    }

    public String getvCapStartDateTime() {
        return vCapStartDateTime;
    }

    public void setvCapStartDateTime(String vCapStartDateTime) {
        this.vCapStartDateTime = vCapStartDateTime;
    }

    public String getvCapEndDateTime() {
        return vCapEndDateTime;
    }

    public void setvCapEndDateTime(String vCapEndDateTime) {
        this.vCapEndDateTime = vCapEndDateTime;
    }
}
