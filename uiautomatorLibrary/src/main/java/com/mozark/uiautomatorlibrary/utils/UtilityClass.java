package com.mozark.uiautomatorlibrary.utils;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.test.uiautomator.UiDevice;

import com.google.gson.reflect.TypeToken;
import com.mozark.uiautomatorlibrary.mozarkmodel.CellInfoModel;
import com.mozark.uiautomatorlibrary.mozarkmodel.SendDataToUIAutomator;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;

import org.apache.commons.lang3.time.StopWatch;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.mozark.uiautomatorlibrary.utils.CellInfoApp.readCellInfoData;
import static com.mozark.uiautomatorlibrary.utils.aqua5gmark.Aqua5GMark.read5GMarkMeasurement;

public class UtilityClass {

    public static final String TAGS = UtilityClass.class.getName();
    public static String SCRIPT_RESULT = "ScriptResult.txt";
    public static String SCRIPT_RUNNING = "ScriptRunning.txt";
    public static String Intent_Launching_Application_Result = "IntentResult.txt";
    static String root = "/sdcard/QosbeeFiles";

    public static boolean isRootGiven() {

        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"su", "-c", "id"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String output = in.readLine();
            if (output != null && output.toLowerCase().contains("uid=0")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
        return false;
    }

    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                    && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static void showDialogOK(Context ctx, String message,
                                    DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton("Yes", okListener)
                .setNegativeButton("No", okListener)
                .create()
                .show();
    }

    public static void showDialogOKFordisconnect(Context ctx, String message,
                                                 DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    public static void deleteTestFolderWhichSiFailed(String testId) {
        try {
            File folder = new File("/sdcard/Mozark/" + testId);
            if (folder.exists()) {
                File[] fileList = folder.listFiles();
                if (fileList != null) {
                    for (File fileDelete : fileList) {
                        if (fileDelete.exists()) {
                            fileDelete.delete();
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String parseCPUDetailAsPer(String unformatedCpuDetail) {
        String result = "";
        if (unformatedCpuDetail.length() > 0) {
            String[] split = unformatedCpuDetail.split("\n");
            for (String str : split) {
                if (str.contains("Date")) {
                    str = str.replace("Date", "").trim();
                    String[] split1 = str.split(" ");
                    for (String strData : split1) {
                        if (strData.trim().length() > 0) {
                            result += (strData.trim().replace(" ", ""));
                            result += ("|");
                        }
                    }
                    result += ("\n");
                }
            }
        }
        result.replace("Date", "");
        String result1 = "";
        if (result.length() > 0) {
            String[] split = result.split("\n");
            for (String str : split) {
                result1 += str.substring(0, str.lastIndexOf("|"));
                result1 += ("\n");
            }
        }
        result1 = result1.replace("||", "|");
        return result1;
    }

    public static String removeExtraFiles(String outputLogFile) {
        String result = "";
        File yourFile = new File(outputLogFile);
        try {
            //final String FILE_NAME = "example.txt";
            FileInputStream fis = new FileInputStream(new File(String.valueOf(yourFile)));
            //  fis = con.openFileInput(yourFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }
            System.out.println("removeExtraFiles -> the file details is " + sb.toString());
            fis.close();
            String[] splitJson = sb.toString().split("\n");
            for (String json : splitJson) {
                if (!json.contains("1m") || json.split("|").length == 13) {
                    result += json + "\n";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void SendDataToTestApp(Context context, int jobId, String mobileIme, String appName,
                                         String ipAdress, boolean pcap, boolean vcap) {
        System.out.println("inside send data");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        JSONObject Jmain = new JSONObject();
        try {
            Jmain.put("jobid", jobId);
            Jmain.put("mobileimei", mobileIme);
            Jmain.put("appname", appName);
            Jmain.put("ipadress", ipAdress);
            Jmain.put("pcap", pcap);
            Jmain.put("vcap", vcap);

            File gpxfile = new File(file + "/SendDataToServer.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(Jmain.toString());
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void writeLogResultForLaterUse(String sendData, String fileName) {
        System.out.println("inside writing history -> writeLogResultForLaterUse");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        FileWriter writer = null;
        try {
            File gpxfile = new File(file + "/" + fileName);
            writer = new FileWriter(gpxfile);
            writer.append("!@#");
            writer.append(sendData);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static boolean checkFileIfxist(String fileName) {
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File gpxfile = new File(file + "/" + fileName);
            if (gpxfile.exists()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteFileIfxist(String fileName) {
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            File gpxfile = new File(file + "/" + fileName);
            if (gpxfile.exists()) {
                gpxfile.delete();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String readFileResult(File yourFile) {
        try {
            //final String FILE_NAME = "example.txt";
            FileInputStream fis = new FileInputStream(yourFile);
            //  fis = con.openFileInput(yourFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                System.out.println(
                        "the file details text LOGS is " + text + "\ntext.contains(Date) : " + (text
                                .contains("Date")));
            /*    if (text.contains("Date")) {
                    sb.append(text.substring(0, 23)).append("|").append(text.substring(23)).append("\n");
                } else {
                    sb.append(text.trim()).append("\n");
                }*/
                sb.append(text.trim()).append("\n");
                System.out.println("the file details text LOGS is " + sb.toString());
            }
            System.out.println("the file details is " + sb.toString());
            fis.close();
            return sb.toString();
//
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String removeLastOrFileResult(File yourFile) {
        try {
            //final String FILE_NAME = "example.txt";
            FileInputStream fis = new FileInputStream(yourFile);
            //  fis = con.openFileInput(yourFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                String stt = text.trim();
                stt = stt.substring(0, stt.length() - 1);
                sb.append(stt).append("\n");
            }
            System.out.println("the file details is " + sb.toString());
            fis.close();
            return sb.toString();
//
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void printFileSize(File file) {
        try {
            double bytes = file.length();
            double kilobytes = (bytes / 1024);
            double megabytes = (kilobytes / 1024);
            double gigabytes = (megabytes / 1024);
            double terabytes = (gigabytes / 1024);
            double petabytes = (terabytes / 1024);
            double exabytes = (petabytes / 1024);
            double zettabytes = (exabytes / 1024);
            double yottabytes = (zettabytes / 1024);
            Log.d(TAGS, "File Path : " + file.getAbsolutePath());
            Log.d(TAGS, "bytes : " + bytes);
            Log.d(TAGS, "kilobytes : " + kilobytes);
            Log.d(TAGS, "megabytes : " + megabytes);
            Log.d(TAGS, "gigabytes : " + gigabytes);
            Log.d(TAGS, "terabytes : " + terabytes);
            Log.d(TAGS, "petabytes : " + petabytes);
            Log.d(TAGS, "exabytes : " + exabytes);
            Log.d(TAGS, "zettabytes : " + zettabytes);
            Log.d(TAGS, "yottabytes : " + yottabytes);
            UtilityClass.appendLogFileByDate(
                    "UIAutomator" + "File Path : " + file.getAbsolutePath() + "\t  Size : " + kilobytes);
        } catch (Exception e) {
            Log.e("", "" + e.getMessage());
        }
    }

    public static String searchAndGetSpecificLine(String file) {
        StringBuilder result = new StringBuilder();
        Scanner s = null;
        try {
            final String searchKey = "ActivityManager: Displayed";
            s = new Scanner(new BufferedReader(new FileReader(file)));

            while (s.hasNextLine() && (result.length() == 0)) {
                String nextLine = s.nextLine();
                if (nextLine.contains(searchKey)) {
                    // Trim everything from the end, to the beginning character of our searchKey (in this case 'key:')
//                    result.append(nextLine.substring(nextLine.indexOf(searchKey)));
                    result.append(nextLine);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                s.close();
            }
        }
        Log.d("", "searchAndGetSpecificLine : " + result.toString());
        return result.toString();
    }

    public static void appendLogFileByDate(String log) {
        try {
            File fileMkr = new File(root + "/Log/");
            if (!fileMkr.exists()) {
                fileMkr.mkdirs();
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            DateFormat dateFormatTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String dateTime = dateFormat.format(date);
            dateTime = dateTime.replace("/", "-");
            String currentTime = dateFormatTime.format(date);
            File file = new File(root + "/Log/" + dateTime + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            // Open given file in append mode.
            BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
            out.write("\n" + currentTime + "\t");
            out.write(log);
            out.close();

        } catch (IOException e) {
            System.out.println("exception occoured" + e);
        }

    }

    public static NetworkDetails get5gMarkData() {
        File file = new File("/sdcard/QosbeeFiles/aqua5gmark.txt");
        String aqua5GMarkResult = read5GMarkMeasurement(file);
        NetworkDetails networkDetails = (NetworkDetails) JsonUtil.toModel(aqua5GMarkResult, NetworkDetails.class);
        if (networkDetails == null) {
            networkDetails = new NetworkDetails();
        }
        return networkDetails;
    }

        public static List<CellInfoModel> getCellInfo() {
            ArrayList<CellInfoModel> celldataModel = null;
        File file = new File("/sdcard/QosbeeFiles/cellInfo.txt");
        String aqua5GMarkResult = readCellInfoData(file);
            Type collectionType = new TypeToken<List<CellInfoModel>>() {
            }.getType();
                celldataModel = (ArrayList<CellInfoModel>) JsonUtil
                        .toModel(aqua5GMarkResult, collectionType);
        if (celldataModel == null) {
            new CellInfoModel();
        }
        return celldataModel;
    }

    public static SendDataToUIAutomator getDataForUIAutomator() {
        File yourFile = new File("/sdcard/QosbeeFiles/SendDataToServer.txt");
        SendDataToUIAutomator sendDataToUIAutomator = new SendDataToUIAutomator();
        try {

            FileInputStream fis = new FileInputStream(new File(String.valueOf(yourFile)));
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }
            System.out.println("the file details is " + sb.toString());

            fis.close();
            UtilityClass.appendLogFileByDate(
                    "UIAutomator is start method startMainActivityFromHomeScreen App launch info : "
                            + sb.toString());
            String fileContent = sb.toString();
            Log.d(GlobalVariables.Tag_Name, "Mozark Request file : " + fileContent);
            UtilityClass.appendLogFileByDate(
                    "UIAutomator is start method startMainActivityFromHomeScreen  reading file content : "
                            + fileContent);
            sendDataToUIAutomator =
                    (SendDataToUIAutomator) JsonUtil.toModel(fileContent, SendDataToUIAutomator.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendDataToUIAutomator;
    }

    public static String getAppVersion(String appName, Context context) {
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(appName, 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static ScheduledExecutorService startScreenshot(UiDevice device) {
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(() -> {
            try {
                device.executeShellCommand(getScreenshotCommand());
                Log.d(GlobalVariables.Tag_Name, "Captured Screen Successfully");
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in taking Screenshot");
                e.printStackTrace();
            }
        }, 0, 10000, TimeUnit.NANOSECONDS);

        return exec;
    }

    public static String getScreenshotCommand() {
        File file = new File(GlobalVariables.Game_Screenshot);
        if (!file.exists()) {
            file.mkdir();
        }
        return "screencap -p "+ GlobalVariables.Game_Screenshot + new Date().getTime() + ".png";
    }

    public boolean checkDeviceAwake(Context context) {
        Boolean isScreenAwake = false;

        try {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

            isScreenAwake = (Build.VERSION.SDK_INT < 20 ? powerManager.isScreenOn()
                    : powerManager.isInteractive());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isScreenAwake;

    }

    public static String getDateString() {
        String date = "";
        Date varDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
        date = dateFormat.format(varDate);
        System.out.println("Date :" + date);
        return date.toUpperCase();
    }

    public static  String  getJson5gmarkPro(String dowload,String upload,String web,String stream,String latency)
    {
        String result="";
        try {
            JSONObject obj=new JSONObject();
            if(!dowload.equalsIgnoreCase("timeout"))
            {
                obj.put("downLoadSpeed",dowload);
            }

            if(!latency.equalsIgnoreCase("timeout"))
            {
                obj.put("latency",latency);
            }

            if(!upload.equalsIgnoreCase("timeout"))
            {
                obj.put("uploadSpeed",upload);
            }

            obj.put("stream",stream);
            obj.put("web",web);
            result=obj.toString();
            return  result;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return  result;
        }
    }


}
