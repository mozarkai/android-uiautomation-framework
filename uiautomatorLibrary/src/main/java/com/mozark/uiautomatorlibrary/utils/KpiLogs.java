package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;

import java.sql.Timestamp;

public class KpiLogs {

  public static void twoKPI(Timestamp lt, Timestamp st, Timestamp vt) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Two KPI Logs");
    }
  }

  public static void fourKPI(
      Timestamp lt,
      Timestamp st,
      Timestamp et,
      Timestamp sat,
      Timestamp ht,
      Timestamp het,
      Timestamp vt) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Four KPI Logs");
    }
  }

  public static void twitterLogs(
      Timestamp lt,
      Timestamp st,
      Timestamp login_Button_Time,
      Timestamp Elements_LoadTime,
      Timestamp et,
      Timestamp sat,
      Timestamp post_Text,
      Timestamp Text_Tweet,
      Timestamp post_Image,
      Timestamp Image_Tweet) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, "Clicked on First Login Time:" + st);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Login Time:" + login_Button_Time);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + Elements_LoadTime);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Tag_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Tag_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Post Text Tweet Time:" + post_Text);
      Log.d(GlobalVariables.Tag_Name, "Text Tweet Appear Time:" + Text_Tweet);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Post Image Tweet Time:" + post_Image);
      Log.d(GlobalVariables.Tag_Name, "Image Tweet Appear Time:" + Image_Tweet);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, "Clicked on First Login Time:" + st);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Login Time:" + login_Button_Time);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + Elements_LoadTime);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Tag_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Tag_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Post Text Tweet Time:" + post_Text);
      Log.d(GlobalVariables.Tag_Name, "Text Tweet Appear Time:" + Text_Tweet);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Post Image Tweet Time:" + post_Image);
      Log.d(GlobalVariables.Tag_Name, "Image Tweet Appear Time:" + Image_Tweet);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Twitter KPI Logs");
    }
  }

  public static void qatarYouTubeLogs(
      Timestamp lt, Timestamp st, Timestamp et, Timestamp sat, Timestamp vt) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Qatar Youtube KPI Logs");
    }
  }

  public static void qatarClassifiedsLogs(
      Timestamp lt, Timestamp het, Timestamp et, Timestamp sat, Timestamp ht, Timestamp st) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Select Item Time:" + ht);
      Log.d(GlobalVariables.Tag_Name, "First Ad Appear Time:" + st);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, "Clicked on Select Item Time:" + ht);
      Log.d(GlobalVariables.Tag_Name, "First Ad Appear Time:" + st);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra QATAR Classifieds KPI Logs");
    }
  }

  public static void extraLogs(
      Timestamp lt, Timestamp st, Timestamp est, Timestamp srat, Timestamp pvt) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra KPI Logs");
    }
  }

  public static void hoichoiLogs(
      Timestamp lt, Timestamp st, Timestamp et, Timestamp sat, Timestamp vt) {

    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Hoichoi KPI Logs");
    }
  }
}
