package com.mozark.uiautomatorlibrary.utils;


public class DataHolder {
    private static DataHolder dataObject = null;

    private DataHolder() {
    }

    public static DataHolder getInstance() {
        if (dataObject == null)
            dataObject = new DataHolder();
        return dataObject;
    }
    private String pCapStartTime = "";
    private String videoStartTime = "";
    private String pCapEndTime = "";
    private String videoEndTime = "";
    private String failureReason = "";
    private String isp="";
    private boolean isFromIntent=false;

    public static DataHolder getDataObject() {
        return dataObject;
    }

    public static void setDataObject(DataHolder dataObject) {
        DataHolder.dataObject = dataObject;
    }

    public String getpCapStartTime() {
        return pCapStartTime;
    }

    public void setpCapStartTime(String pCapStartTime) {
        this.pCapStartTime = pCapStartTime;
    }

    public String getVideoStartTime() {
        return videoStartTime;
    }

    public void setVideoStartTime(String videoStartTime) {
        this.videoStartTime = videoStartTime;
    }

    public String getpCapEndTime() {
        return pCapEndTime;
    }

    public void setpCapEndTime(String pCapEndTime) {
        this.pCapEndTime = pCapEndTime;
    }

    public String getVideoEndTime() {
        return videoEndTime;
    }

    public void setVideoEndTime(String videoEndTime) {
        this.videoEndTime = videoEndTime;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public boolean isFromIntent() {
        return isFromIntent;
    }

    public void setFromIntent(boolean fromIntent) {
        isFromIntent = fromIntent;
    }

    public void clear(){
        this.pCapStartTime = "";
        this.pCapEndTime = "";
        this.videoStartTime = "";
        this.videoEndTime = "";
        this.failureReason = "";
        this.isp="";
    }
}