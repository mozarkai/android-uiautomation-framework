package com.mozark.uiautomatorlibrary.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;


import java.io.IOException;
import java.io.OutputStream;

public class ExecuteCommand extends AsyncTask<String, String, String> {

  Context mContext = null;
  String statusOfCapture = null;

  public ExecuteCommand(Context _ctx, String captureStatus) {
    mContext = _ctx;
    statusOfCapture = captureStatus;
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
  }

  @TargetApi(Build.VERSION_CODES.O)
  @Override
  protected String doInBackground(String... params) {
    StringBuffer output = new StringBuffer();
    try {

      // p = Runtime.getRuntime().exec(activity.getApplicationInfo().dataDir + "/files/tcpdump.bin
      // -w /mnt/sdcard/0001.cap");
      // p = Runtime.getRuntime().exec("tcpdump -w - | tee /mnt/sdcard/0001.cap | tcpdump -r -");

      /*p = Runtime.getRuntime().exec("tcpdump");

      */
      /*String [] commands = {"/bin/sh", "-c", "echo hello > hello.txt"};
      p = Runtime.getRuntime().exec(commands);*/
      /*
      BufferedReader reader = new BufferedReader(
              new InputStreamReader(p.getInputStream()));
      String line = "";
      while ((line = reader.readLine()) != null) {
          output.append(line + "\n");
          p.waitFor();
      }*/

      if ("start".equals(statusOfCapture)) {

        System.out.println("Thejaswi:ExecuteCommand:statusOfCapture is start");

        /* File file = new File("/sdcard/capturedpcapfile.pcap");
        if(file.exists()){
            file.delete();
            file.createNewFile();
        }*/

        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.
        os.write("su\n".getBytes());
        os.write("cd /system\n".getBytes());
        os.write("mount -o remount,rw /system\n".getBytes());
        os.write("cp -R /sdcard/tcpdump bin/\n".getBytes());
        //  os.write("cp /sdcard/tcpdump /system/bin  /tcpdump\n".getBytes());
        os.write("chmod 6755 /system/bin/tcpdump\n".getBytes());
        os.write("tcpdump -i any -p -s 0 -w /sdcard/capturedpcapfile.pcap\n".getBytes());
        os.flush();

        Log.d("Thejaswi", "end of start.equals(statusOfCapture)");
      } else if ("stop".equals(statusOfCapture)) {

        Process proc = Runtime.getRuntime().exec("kill");

        //                Log.d("Thejaswi","proc.exitValue():"+proc.exitValue());
        //                Log.d("Thejaswi","proc.isAlive():"+proc.isAlive());

        Log.d("Thejaswi", "Kill command excecuted");

        /*Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        //below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        //below code for increase brightness.
        os.write("su\n".getBytes());
        os.write("cp /sdcard/capturedpcapfile.pcap /storage/emulated/0/rooted_pcap\n".getBytes());
        os.flush();*/
      } else if ("splitfile".equals(statusOfCapture)) {

        Runtime.getRuntime()
            .exec("tcpdump -r /sdcard/capturedpcapfile.pcap -w /sdcard/splitpcapfile.pcap -C 25");

      } else if ("start_script".equals(statusOfCapture)) {

        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.
        os.write(" adb logcat -b events | grep am_activity_launch_time".getBytes());
        String finalString = os.toString();
        System.out.println(finalString);
        os.flush();
      } else if ("stopHotstar".equals(statusOfCapture)) {
        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.
        os.write("am kill  in.startv.hotstar".getBytes());
        os.flush();

      } else if ("stopapp".equals(statusOfCapture)) {
        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.
        os.write("am kill  com.example.testapp.test".getBytes());
        os.flush();

      } else if ("closeapp_youtube".equals(statusOfCapture)) {
        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.am force-stop com.google.android.youtube
        os.write("am force-stop com.google.android.youtube".getBytes());
        os.flush();

      } else if ("closeapp_hotstar".equals(statusOfCapture)) {
        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.am force-stop com.google.android.youtube
        os.write("am force-stop in.startv.hotstar".getBytes());
        os.flush();

      } else if ("closeapp_ndtv".equals(statusOfCapture)) {
        Process processcmd = Runtime.getRuntime().exec("su");
        OutputStream os = processcmd.getOutputStream();
        // below code for capture screenshot and placed in internal storage.
        os.write("adb shell\n".getBytes());
        // below code for increase brightness.am force-stop com.google.android.youtube
        os.write("am force-stop com.july.ndtv".getBytes());
        os.flush();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    String response = output.toString();
    return response;
  }
}
