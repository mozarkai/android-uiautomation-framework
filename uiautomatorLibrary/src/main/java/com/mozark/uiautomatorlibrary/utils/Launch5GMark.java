package com.mozark.uiautomatorlibrary.utils;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.aqua5gmark.Aqua5GMark;

import org.json.JSONObject;

public class Launch5GMark {

  public static void LaunchAqua5GMark(
      String device_id, String ipAdress, Context context, UiDevice device, JSONObject jsonObject) {
    try {

//      if (ExampleInstrumentedTest.IS_ACTIVE_AQUA5GMARK) {
        UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
        updateDeviceStatus.update(device_id, ipAdress, "Running");
        UtilityClass.appendLogFileByDate(
            "UIAutomator is start method startMainActivityFromHomeScreen IS_ACTIVE_AQUA5GMARK lauching");
        Aqua5GMark.launchApp(context, device);
//      }
      UtilityClass.appendLogFileByDate("\n\n " + jsonObject.toString());
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Launching Aquamark 5Mark");
      e.printStackTrace();
    }
  }
}
