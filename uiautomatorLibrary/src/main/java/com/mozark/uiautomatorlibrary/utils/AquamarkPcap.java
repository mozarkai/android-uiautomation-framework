package com.mozark.uiautomatorlibrary.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.mozark.uiautomatorlibrary.AquaMarkSharePreferences;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class AquamarkPcap extends AsyncTask<String, String, String> {

    Context mContext = null;
    String statusOfCapture = null;
    private boolean pCap;

    public AquamarkPcap(Context _ctx, String captureStatus, boolean pCap) {
        mContext = _ctx;
        statusOfCapture = captureStatus;
        this.pCap = pCap;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if("start".equals(statusOfCapture)) {
            AquaMarkSharePreferences.getInstance(mContext).setPcapStartTime(UtilityClass.getDateString());
            DataHolder.getInstance().setpCapStartTime(UtilityClass.getDateString());
        }
        else if ("stop".equals(statusOfCapture))
        {
            AquaMarkSharePreferences.getInstance(mContext).setPcapEndTime(UtilityClass.getDateString());
            DataHolder.getInstance().setpCapEndTime(UtilityClass.getDateString());
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected String doInBackground(String... strings) {
        StringBuffer output = new StringBuffer();
        try {

            if ("start".equals(statusOfCapture)) {
                if (!pCap) {
                    return "";
                }
                Utility.deleteFileByPathIfxist("/sdcard/capturedpcapfile.pcap");
                System.out.println("Aquamark:ExecuteCommand:statusOfCapture is start");

                Process processcmd = Runtime.getRuntime().exec("su");
                OutputStream os = processcmd.getOutputStream();
                // below code for capture screenshot and placed in internal storage.
                os.write("adb shell\n".getBytes());
                // below code for increase brightness.
                os.write("su\n".getBytes());
                os.write("cd /system\n".getBytes());
                os.write("mount -o remount,rw /system\n".getBytes());
                os.write("cp -R /sdcard/tcpdump.dms /data/media/\n".getBytes());
                os.write("cp -R /sdcard/tcpdump /data/media/\n".getBytes());
                os.write("chmod 6755 /data/media/tcpdump.dms\n".getBytes());
                os.write("chmod 6755 /data/media/tcpdump\n".getBytes());
                os.write("nohup /data/media/tcpdump.dms -i any -p -s 0 -w /sdcard/capturedpcapfile.pcap &\n"
                                .getBytes());
                os.write("nohup /data/media/tcpdump -i any -p -s 0  -w /sdcard/capturedpcapfile.pcap &\n"
                                .getBytes());
                os.flush();
                // os.close();
                // nohup /data/media/tcpdump -i any -p -s 0 -w /sdcard/capturedpcapfile.pcap &
                //
                Log.d("Aquamark", "end of start.equals(statusOfCapture)");
                UtilityClass.appendLogFileByDate("UIAutomator Start Pcap");
            } else if ("stop".equals(statusOfCapture)) {
                try {
                    if (!pCap) {
                        return "";
                    }
                    String date = "";
                    Date varDate = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    date = dateFormat.format(varDate);
                    System.out.println("Date for pcap end is :" + date);
                    Process processcmd = Runtime.getRuntime().exec("su");
                    BufferedReader bufferedReader =
                            new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

                    OutputStream os = processcmd.getOutputStream();
                    // below code for capture screenshot and placed in internal storage.
                    os.write("adb shell\n".getBytes());
                    // below code for increase brightness.
                    os.write("su \n".getBytes());

                    os.write(
                            "for i in `ps -ef| grep -v grep |grep '/sdcard/capturedpcapfile.pcap'|awk '{print $2}'` \n"
                                    .getBytes());
                    os.write("do \n".getBytes());
                    os.write(" kill $i \n".getBytes());
                    os.write("done \n".getBytes());

                    os.flush();
                    // os.close();
                    Log.d("Aquamark", "the pp is stopped ");
                    UtilityClass.appendLogFileByDate("UIAutomator Stop Pcap");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        String response = output.toString();
        return response;
    }


}
