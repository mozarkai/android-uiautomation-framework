package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;


import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import org.apache.commons.lang3.time.StopWatch;

import java.sql.Timestamp;
import java.util.Date;

public class ResourceUtility {

  int n = 5;
  public static Timestamp hpt;
  public static Timestamp het;
  public static Timestamp st;
  public static Timestamp est;
  public static Timestamp srat;
  public static Timestamp svt;
  public static Timestamp pvt;
  public static Timestamp vst;
  public static Timestamp lbt;
  public static Timestamp hbt;
  public static Timestamp sgt;
  public static Timestamp fbat;
  public static Timestamp bft;
  public static Timestamp glt;

  public boolean loadHomePageWithID(UiDevice device, String homePageID) {
    try {
      UiSelector homePageId = new UiSelector().resourceId(homePageID);
      UiObject homePage = device.findObject(homePageId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (homePage.exists()) {
          hpt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
      return false;
    }
  }

  public boolean loadHomePageWithText(UiDevice device, String homePageID) {
    try {
      UiSelector homePageId = new UiSelector().textContains(homePageID);
      UiObject homePage = device.findObject(homePageId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (homePage.exists()) {
          hpt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
      return false;
    }
  }

  public boolean loadHomePageWithDescription(UiDevice device, String homePageID) {
    try {
      UiSelector homePageId = new UiSelector().descriptionContains(homePageID);
      UiObject homePage = device.findObject(homePageId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (homePage.exists()) {
          hpt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
      return false;
    }
  }

  public boolean loadHomePageWithCollection(String homePageID, int instance) {
    try {
      UiCollection homePageId = new UiCollection(new UiSelector().resourceId(homePageID));
      UiObject homePage = homePageId.getChildByInstance(new UiSelector(), instance);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (homePage.exists()) {
          hpt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
      return false;
    }
  }

  public boolean loadHomePageWithTextMPL(UiDevice device, String homePageID) {
    try {
      UiSelector homePageId = new UiSelector().textContains(homePageID);
      UiObject homePage = device.findObject(homePageId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (homePage.exists()) {
          het = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + het);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
        }
      }
      Log.d(GlobalVariables.Tag_Name, "HomePage Elements Time1:" + hpt);
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
      return false;
    }
  }

  public boolean clickSearchButtonWithID(UiDevice device, String searchButtonID) {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchButtonId = new UiSelector().resourceId(searchButtonID);
          UiObject searchButton = device.findObject(searchButtonId);
          if (searchButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchButton.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Search Button");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Search Button");
      return false;
    }
  }

  public boolean clickSearchButtonWithDescription(UiDevice device, String searchButtonID) {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));
          UiSelector searchButtonId = new UiSelector().descriptionContains(searchButtonID);
          UiObject searchButton = device.findObject(searchButtonId);
          if (searchButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchButton.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Search Button");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Search Button");
      return false;
    }
  }

  public boolean clickSearchButtonWithText(UiDevice device, String searchButtonID) {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));
          UiSelector searchButtonId = new UiSelector().textContains(searchButtonID);
          UiObject searchButton = device.findObject(searchButtonId);
          if (searchButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchButton.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Search Button");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Search Button");
      return false;
    }
  }

  public boolean clickSearchButtonWithCollection(String searchButtonID, int instance) {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));
          UiCollection searchButtonId =
                  new UiCollection(new UiSelector().descriptionContains(searchButtonID));
          UiObject searchButton = searchButtonId.getChildByInstance(new UiSelector(), instance);
          if (searchButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchButton.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Search Button");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Search Button");
      return false;
    }
  }

  public boolean enterSearchTextWithID(UiDevice device, String enterSearchID, String searchText) {
    try {
      boolean enterSearchFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Search with Attempt no." + (i + 1));

          UiSelector enterSearchId = new UiSelector().resourceId(enterSearchID);
          UiObject enterSearch = device.findObject(enterSearchId);
          if (enterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch.click();
            enterSearch.setText(searchText);
            est = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
            enterSearchFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Enter Search");
        }
      }
      return enterSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Text");
      return false;
    }
  }

  public boolean enterSearchTextWithText(UiDevice device, String enterSearchID, String searchText) {
    try {
      boolean enterSearchFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Search with Attempt no." + (i + 1));
          UiSelector enterSearchId = new UiSelector().textContains(enterSearchID);
          UiObject enterSearch = device.findObject(enterSearchId);
          if (enterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch.click();
            enterSearch.setText(searchText);
            est = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
            enterSearchFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Enter Search");
        }
      }
      return enterSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Text");
      return false;
    }
  }

  public boolean enterSearchTextWithDescription(
          UiDevice device, String enterSearchID, String searchText) {
    try {
      boolean enterSearchFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Search with Attempt no." + (i + 1));
          UiSelector enterSearchId = new UiSelector().descriptionContains(enterSearchID);
          UiObject enterSearch = device.findObject(enterSearchId);
          if (enterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch.click();
            enterSearch.setText(searchText);
            est = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
            enterSearchFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Enter Search");
        }
      }
      return enterSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Text");
      return false;
    }
  }

  public boolean enterSearchTextWithCollection(
          String enterSearchID, int instance, String searchText) {
    try {
      boolean enterSearchFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Search with Attempt no." + (i + 1));
          UiCollection enterSearchId =
                  new UiCollection(new UiSelector().descriptionContains(enterSearchID));
          UiObject enterSearch = enterSearchId.getChildByInstance(new UiSelector(), instance);
          if (enterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch.click();
            enterSearch.setText(searchText);
            est = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + est);
            enterSearchFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Enter Search");
        }
      }
      return enterSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Text");
      return false;
    }
  }

  public boolean searchResultsWithID(UiDevice device, String searchResultsID) {
    try {
      UiSelector searchResultsId = new UiSelector().resourceId(searchResultsID);
      UiObject searchResults = device.findObject(searchResultsId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (searchResults.exists()) {
          srat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  public boolean searchResultsWithDescription(UiDevice device, String searchResultsID) {
    try {
      UiSelector searchResultsId = new UiSelector().descriptionContains(searchResultsID);
      UiObject searchResults = device.findObject(searchResultsId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (searchResults.exists()) {
          srat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  public boolean searchResultsWithText(UiDevice device, String searchResultsID) {
    try {
      UiSelector searchResultsId = new UiSelector().textContains(searchResultsID);
      UiObject searchResults = device.findObject(searchResultsId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (searchResults.exists()) {
          srat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  public boolean searchResultsWithCollection(String searchResultsID, int instance) {
    try {
      UiCollection searchResultsId = new UiCollection(new UiSelector().resourceId(searchResultsID));
      UiObject searchResults = searchResultsId.getChildByInstance(new UiSelector(), instance);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (searchResults.exists()) {
          srat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + srat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  public boolean selectVideoWithID(UiDevice device, String selectVideoID) {
    try {
      boolean selectVideoFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find select video with Attempt no." + (i + 1));
          UiSelector selectVideoId = new UiSelector().resourceId(selectVideoID);
          UiObject selectVideo = device.findObject(selectVideoId);
          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            svt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Video_Time + svt);
            selectVideoFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Select Video");
        }
      }
      return selectVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Video");
      return false;
    }
  }

  public boolean selectVideoWithDescription(UiDevice device, String selectVideoID) {
    try {
      boolean selectVideoFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find select video with Attempt no." + (i + 1));
          UiSelector selectVideoId = new UiSelector().descriptionContains(selectVideoID);
          UiObject selectVideo = device.findObject(selectVideoId);
          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            svt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Video_Time + svt);
            selectVideoFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Select Video");
        }
      }
      return selectVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Video");
      return false;
    }
  }

  public boolean selectVideoWithText(UiDevice device, String selectVideoID) {
    try {
      boolean selectVideoFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find select video with Attempt no." + (i + 1));
          UiSelector selectVideoId = new UiSelector().textContains(selectVideoID);
          UiObject selectVideo = device.findObject(selectVideoId);
          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            svt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Video_Time + svt);
            selectVideoFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Select Video");
        }
      }
      return selectVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Video");
      return false;
    }
  }

  public boolean selectVideoWithCollection(String selectVideoID, int instance) {
    try {
      boolean selectVideoFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find select video with Attempt no." + (i + 1));
          UiCollection selectVideoId = new UiCollection(new UiSelector().resourceId(selectVideoID));
          UiObject selectVideo = selectVideoId.getChildByInstance(new UiSelector(), instance);
          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            svt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Video_Time + svt);
            selectVideoFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Select Video");
        }
      }
      return selectVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Video");
      return false;
    }
  }

  public boolean clickPlayButtonWithID(UiDevice device, String playButtonID) {
    try {
      boolean playButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Play button with Attempt no." + (i + 1));

          UiSelector playButtonId = new UiSelector().resourceId(playButtonID);
          UiObject playButton = device.findObject(playButtonId);
          if (playButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            playButton.click();
            pvt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
            playButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Play Button");
        }
      }
      return playButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Play Button");
      return false;
    }
  }

  public boolean clickPlayButtonWithDescription(UiDevice device, String playButtonID) {
    try {
      boolean playButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Play button with Attempt no." + (i + 1));

          UiSelector playButtonId = new UiSelector().descriptionContains(playButtonID);
          UiObject playButton = device.findObject(playButtonId);
          if (playButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            playButton.click();
            pvt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
            playButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Play Button");
        }
      }
      return playButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Play Button");
      return false;
    }
  }

  public boolean clickPlayButtonWithText(UiDevice device, String playButtonID) {
    try {
      boolean playButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Play button with Attempt no." + (i + 1));

          UiSelector playButtonId = new UiSelector().textContains(playButtonID);
          UiObject playButton = device.findObject(playButtonId);
          if (playButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            playButton.click();
            pvt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
            playButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Play Button");
        }
      }
      return playButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Play Button");
      return false;
    }
  }

  public boolean clickPlayButtonWithCollection(String playButtonID, int instance) {

    try {
      boolean playButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Play button with Attempt no." + (i + 1));
          UiCollection playButtonId = new UiCollection(new UiSelector().resourceId(playButtonID));
          UiObject playButton = playButtonId.getChildByInstance(new UiSelector(), instance);
          if (playButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            playButton.click();
            pvt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
            playButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Play Button");
        }
      }
      return playButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Play Button");
      return false;
    }
  }

  public boolean bufferReadWithID(
          String testId, long videoRunTime, UiDevice device, String loaderID) {
    try {

      UiSelector loaderId = new UiSelector().resourceId(loaderID);
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      Timestamp timestamp;

      while (stopWatch.getTime() <= videoRunTime) {
        if (loader.exists()) {
          timestamp = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + timestamp);
        } else {
          timestamp = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + timestamp);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean bufferReadWithDescription(
          String testId, long videoRunTime, UiDevice device, String loaderID) {
    try {

      UiSelector loaderId = new UiSelector().descriptionContains(loaderID);
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= videoRunTime) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean bufferReadWithText(
          String testId, long videoRunTime, UiDevice device, String loaderID) {
    try {

      UiSelector loaderId = new UiSelector().textContains(loaderID);
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= videoRunTime) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean bufferReadWithCollection(
          String testId, long videoRunTime, String loaderID, int instance) {
    try {

      UiCollection loaderId = new UiCollection(new UiSelector().resourceId(loaderID));
      UiObject loader = loaderId.getChildByInstance(new UiSelector(), instance);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= videoRunTime) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean mplBufferReadWithText(
          long videoRunTime, UiDevice device, String loaderID) {
    try {

      UiSelector loaderId = new UiSelector().textContains(loaderID);
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= videoRunTime) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean videoStartedWithText(UiDevice device, String videoStartedID) {
    try {
      UiSelector videoStartedId = new UiSelector().textContains(videoStartedID);
      UiObject videoStarted = device.findObject(videoStartedId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (videoStarted.exists()) {
          vst = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Video_Start_Time + vst);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Video is Loading");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  public boolean clickLiveButtonWithID(UiDevice device, String liveButtonID) {
    try {
      boolean liveButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find live button with Attempt no." + (i + 1));
          UiSelector liveButtonId = new UiSelector().resourceId(liveButtonID);
          UiObject liveButton = device.findObject(liveButtonId);
          if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveButton.click();
            lbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Live_Button_Time + lbt);
            liveButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Live Button");
        }
      }
      return liveButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Live Button");
      return false;
    }
  }

  public boolean clickLiveButtonWithText(UiDevice device, String liveButtonID) {
    try {
      boolean liveButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find live button with Attempt no." + (i + 1));
          UiSelector liveButtonId = new UiSelector().text(liveButtonID);
          UiObject liveButton = device.findObject(liveButtonId);
          if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveButton.click();
            lbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Live_Button_Time + lbt);
            liveButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Live Button");
        }
      }
      return liveButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Live Button");
      return false;
    }
  }

  public boolean clickLiveButtonWithDescription(UiDevice device, String liveButtonID) {
    try {
      boolean liveButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find live button with Attempt no." + (i + 1));
          UiSelector liveButtonId = new UiSelector().descriptionContains(liveButtonID);
          UiObject liveButton = device.findObject(liveButtonId);
          if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveButton.click();
            lbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Live_Button_Time + lbt);
            liveButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Live Button");
        }
      }
      return liveButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Live Button");
      return false;
    }
  }

  public boolean clickLiveButtonWithCollection(String liveButtonID, int instance) {
    try {
      boolean liveButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find live button with Attempt no." + (i + 1));
          UiCollection liveButtonId = new UiCollection(new UiSelector().resourceId(liveButtonID));
          UiObject liveButton = liveButtonId.getChildByInstance(new UiSelector(), instance);
          if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveButton.click();
            lbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Live_Button_Time + lbt);
            liveButtonFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Live Button");
        }
      }
      return liveButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Live Button");
      return false;
    }
  }

  public boolean clickLiveButtonWithTextMPL(UiDevice device, String liveButtonID) {
    try {
      boolean liveButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find live button with Attempt no." + (i + 1));
          UiSelector liveButtonId = new UiSelector().text(liveButtonID);
          UiObject liveButton = device.findObject(liveButtonId);

          UiSelector spinID = new UiSelector().textContains("Spin Today's Wheel");
          UiObject spin = device.findObject(spinID);

          if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveButton.click();
            lbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Live_Button_Time + lbt);
            liveButtonFound = true;
            break;
          } else {
            spin.click();
            Log.d(GlobalVariables.Tag_Name,
                    "Clicked on Spin Button" + new Timestamp(new Date().getTime()));
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Live Button");
        }
      }
      return liveButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Live Button");
      return false;
    }
  }

  public boolean exitVideoWithText(UiDevice device, String exitVideoID) {
    try {
      boolean exitVideoFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to exit Video with Attempt no." + (i + 1));
          UiSelector exitvideoId = new UiSelector().textContains(exitVideoID);
          UiObject exitvideo = device.findObject(exitvideoId);
          device.pressBack();
          Thread.sleep(2000);
          if (exitvideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            exitvideo.click();
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Exit from the Video" + new Timestamp(new Date().getTime()));
            exitVideoFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Exit Video");
        }
      }
      return exitVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Exiting from the Video");
      return false;
    }
  }

  public boolean clickHomeButtonWithText(UiDevice device, String homeButtonID) {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Home button with Attempt no." + (i + 1));

          UiSelector homeButtonId = new UiSelector().textContains(homeButtonID);
          UiObject homeButton = device.findObject(homeButtonId);
          if (homeButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            homeButton.click();
            hbt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + hbt);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Home Button");
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Home Button");
      return false;
    }
  }

  public boolean selectGameWithText(UiDevice device, String selectGameID) {
    try {
      boolean selectGameFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find select Game with Attempt no." + (i + 1));
          UiSelector selectGameId = new UiSelector().textContains(selectGameID);
          UiObject selectGame = device.findObject(selectGameId);
          if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectGame.click();
            sgt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
            selectGameFound = true;
            break;
          } else {
            UiScrollable scroll = new UiScrollable(
                    new UiSelector().className("android.widget.ScrollView"));
            scroll.scrollTextIntoView(selectGameID);
            if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
              selectGame.click();
              sgt = new Timestamp(new Date().getTime());
              Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
              selectGameFound = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Select Game");
        }
      }
      return selectGameFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Game");
      return false;
    }
  }

  public boolean viewFreeButtonWithText(UiDevice device, String freeButtonID) {
    try {
      UiSelector freeButtonId = new UiSelector().textContains(freeButtonID);
      UiObject freeButton = device.findObject(freeButtonId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (freeButton.exists()) {
          fbat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Free_Appear_Time + fbat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Waiting for FREE Text");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting FREE Text");
      return false;
    }
  }

  public boolean clickFreeButtonWithText(UiDevice device, String freeButtonID) {
    try {
      boolean freeButtonFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Free button with Attempt no." + (i + 1));

          UiSelector freeButtonId = new UiSelector().textContains(freeButtonID);
          UiObject freeButton = device.findObject(freeButtonId);
          if (freeButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            freeButton.click();
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Click on Free Button Time:" + new Timestamp(new Date().getTime()));
            freeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Free Button");
        }
      }
      return freeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Free Button");
      return false;
    }
  }

  public boolean clickBattleFreeWithText(UiDevice device, String battleForFreeID) {
    try {
      boolean battleFreeFound = false;
      for (int i = 0; i < n; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Trying to find Battle For Free button with Attempt no." + (i + 1));

          UiSelector battleFreeId = new UiSelector().textContains(battleForFreeID);
          UiObject battleFree = device.findObject(battleFreeId);
          if (battleFree.waitForExists(GlobalVariables.Wait_Timeout)) {
            battleFree.click();
            bft = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Battle_Free_Time + bft);
            battleFreeFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Battle Free Button");
        }
      }
      return battleFreeFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Battle Free Button");
      return false;
    }
  }

  public boolean gameLoaderWithText(UiDevice device, String gameLoaderID) {
    try {
      UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID);
      UiObject gameLoader = device.findObject(gameLoaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (gameLoader.exists()) {
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
        } else {
          glt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
          break;
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
      return false;
    }
  }

  public boolean gameLoader1WithText(UiDevice device, String gameLoaderID1) {
    try {
      UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID1);
      UiObject gameLoader = device.findObject(gameLoaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (gameLoader.exists()) {
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
        } else {
          glt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
          break;
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
      return false;
    }
  }

  public boolean gameLoader2WithText(UiDevice device, String gameLoaderID2) {
    try {
      UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID2);
      UiObject gameLoader = device.findObject(gameLoaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (gameLoader.exists()) {
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
        } else {
          glt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
          break;
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
      return false;
    }
  }
}
