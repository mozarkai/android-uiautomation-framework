package com.mozark.uiautomatorlibrary.utils;

import android.util.Log;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class SendLogsToServer {

  public static final String TAGS = "VideoCapture";

  public void getLogsPath(String testId, File fileName, String ipAdress, String device_id) {

    retrofit(testId, fileName, ipAdress, device_id);
  }

  public void retrofit(String testId, File videoFile, String ipAdress, String device_id) {
    Log.d("Aquamark", "Start sending Log file -> Size : " + videoFile.length());
    JSONObject json = new JSONObject();
    JSONObject requestu = new JSONObject();
    JSONObject data = new JSONObject();
    JSONObject detailsExtra = new JSONObject();
    try {

      detailsExtra.put("deviceId", device_id);
      detailsExtra.put("testId", testId);

      data.put("data", detailsExtra);
      requestu.put("request", data);
      json.put("json", requestu);

    } catch (JSONException e) {
      e.printStackTrace();
    }
    byte[] logo = null;

    try {
      logo = FileUtils.readFileToByteArray(videoFile);
    } catch (IOException e) {
      e.printStackTrace();
    }

    String result = sendPcapWithLegacy(logo, videoFile.getName(), json, ipAdress);

    if (result != null && result.contains("Success")) {
      File sourceM = new File("/sdcard/logM.txt");
      if (sourceM.exists()) {
        File source = new File("/sdcard/log.txt");
        //                if (source.exists()) {
        //                    source.delete();
        //                }
        if (sourceM.exists()) {
          sourceM.delete();
        }
        if (videoFile.exists()) {
          videoFile.delete();
        }
      }
    }
  }

  private String sendPcapWithLegacy(
          byte[] imagebyteArray, String fileName, JSONObject finalJsonStr, String ipAdress) {
    String requestUrl = ipAdress + "performance/uploadmedia?request=";

    String resultString = null;
    String charset = "UTF-8";
    String LINE_FEED = "\r\n";
    try {

      System.out.println("SendPhototoServer: 1");

      String jStrParameters = finalJsonStr.toString();
      System.out.println("PHOTO URL FINAL00 : " + jStrParameters);

      System.out.println("URL FINAL01 : " + requestUrl + jStrParameters);

      String finalURL =
          requestUrl + finalJsonStr; // URLEncoder.encode(jStrParameters.trim(), "UTF-8");
      System.out.println("URL FINAL11 : " + finalURL);

      URL url = new URL(finalURL);
      System.out.println("URL FINAL22 : " + url);

      String boundary = "===" + System.currentTimeMillis() + "===";

      if (imagebyteArray != null) {

        URLConnection connection = url.openConnection();

        HttpURLConnection httpConnection = (HttpURLConnection) connection;
        httpConnection.setRequestMethod("POST");
        httpConnection.setDoOutput(true);
        httpConnection.setConnectTimeout(300000);
        httpConnection.setReadTimeout(300000);
        httpConnection.setRequestProperty(
            "Content-Type", "multipart/form-data; boundary=" + boundary);

        OutputStream outputStream = httpConnection.getOutputStream();

        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);

        String encodedString = "test";

        System.out.println("Encoded String : " + encodedString);

        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"groupzkey\"").append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(encodedString).append(LINE_FEED);
        writer.flush();

        writer.append("--" + boundary).append(LINE_FEED);
        writer
            .append(
                "Content-Disposition: form-data; name=\"document\"; filename=\"" + fileName + "\"")
            .append(LINE_FEED);

        writer
            .append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
            .append(LINE_FEED);
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();
        System.out.println("Send photo in bg ByteArr: " + imagebyteArray);
        outputStream.write(imagebyteArray);

        outputStream.flush();

        writer.append(LINE_FEED);
        writer.flush();
        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        // checks server's status code first
        int status = httpConnection.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
          System.out.println("Photourl innnn");
          StringBuffer res = new StringBuffer();
          BufferedReader in =
              new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
          String inputLine;

          while ((inputLine = in.readLine()) != null) {
            System.out.println("InputLine: " + inputLine + " **-** " + in);
            res.append(inputLine);
          }

          resultString = res.toString();

          System.out.println("PhotoUrl response " + resultString);

          if (resultString.contains("json") == false) {
            resultString = null;
          }
          in.close();

          httpConnection.disconnect();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    System.out.println("PhotoUrl reponse " + resultString);

    return resultString;
  }
}
