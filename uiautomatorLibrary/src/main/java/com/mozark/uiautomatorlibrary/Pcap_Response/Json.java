package com.mozark.uiautomatorlibrary.Pcap_Response;

public class Json {

  private Response response;

  public Json(Response response) {
    this.response = response;
  }

  public Response getResponse() {
    return response;
  }

  public void setResponse(Response response) {
    this.response = response;
  }

  @Override
  public String toString() {
    return "ClassPojo [response = " + response + "]";
  }
}
