package com.mozark.uiautomatorlibrary.Pcap_Response;

public class PcapResponse {

  private Json json;

  public PcapResponse(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
