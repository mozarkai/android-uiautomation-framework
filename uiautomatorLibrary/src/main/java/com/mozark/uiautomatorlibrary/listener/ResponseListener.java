package com.mozark.uiautomatorlibrary.listener;

public interface ResponseListener {

  void onStatus(boolean status, Object result);
}
