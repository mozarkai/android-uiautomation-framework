package com.mozark.uiautomatorlibrary.updatedevicestatus;

public class PutDeviceStatus {

  private Json json;

  public PutDeviceStatus(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
