package com.mozark.uiautomatorlibrary;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.ResourceUtility;
import com.mozark.uiautomatorlibrary.utils.SendStatus;

import java.sql.Timestamp;
import java.util.Date;

public class Base extends ResourceUtility {

  UiDevice device;

  public boolean loadHomePage(UiDevice device, String homePageID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = loadHomePageWithID(device, homePageID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = loadHomePageWithText(device, homePageID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = loadHomePageWithDescription(device, homePageID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = loadHomePageWithCollection(homePageID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean clickSearchButton(
      UiDevice device, String searchButtonID, String idType, int instance) {

    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = clickSearchButtonWithID(device, searchButtonID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = clickSearchButtonWithText(device, searchButtonID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = clickSearchButtonWithDescription(device, searchButtonID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = clickSearchButtonWithCollection(searchButtonID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean enterSearchText(
      UiDevice device, String enterSearchID, String idType, int instance, String searchText) {

    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = enterSearchTextWithID(device, enterSearchID, searchText);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = enterSearchTextWithText(device, enterSearchID, searchText);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = enterSearchTextWithDescription(device, enterSearchID, searchText);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = enterSearchTextWithCollection(enterSearchID, instance, searchText);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean searchResults(
      UiDevice device, String searchResultsID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = searchResultsWithID(device, searchResultsID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = searchResultsWithText(device, searchResultsID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = searchResultsWithDescription(device, searchResultsID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = searchResultsWithCollection(searchResultsID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean selectVideo(UiDevice device, String selectVideoID, String idType, int instance) {

    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = selectVideoWithID(device, selectVideoID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = selectVideoWithText(device, selectVideoID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = selectVideoWithDescription(device, selectVideoID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = selectVideoWithCollection(selectVideoID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean clickPlayButton(
      UiDevice device, String playButtonID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = clickPlayButtonWithID(device, playButtonID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = clickPlayButtonWithText(device, playButtonID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = clickPlayButtonWithDescription(device, playButtonID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = clickPlayButtonWithCollection(playButtonID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean videoStarted(UiDevice device, String videoStartID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = videoStartedWithText(device, videoStartID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean bufferRead(
      String testId,
      long videoRunTime,
      UiDevice device,
      String loaderID,
      String idType,
      int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = bufferReadWithID(testId, videoRunTime, device, loaderID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = bufferReadWithText(testId, videoRunTime, device, loaderID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = bufferReadWithDescription(testId, videoRunTime, device, loaderID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = bufferReadWithCollection(testId, videoRunTime, loaderID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean mplBufferRead(
      long videoRunTime,
      UiDevice device,
      String loaderID,
      String idType,
      int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = mplBufferReadWithText(videoRunTime, device, loaderID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean clickLiveButton(
      UiDevice device, String liveButtonID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("resourceID")) {
        result = clickLiveButtonWithID(device, liveButtonID);
      }
      if (idType.equalsIgnoreCase("text")) {
        result = clickLiveButtonWithText(device, liveButtonID);
      }
      if (idType.equalsIgnoreCase("description")) {
        result = clickLiveButtonWithDescription(device, liveButtonID);
      }
      if (idType.equalsIgnoreCase("collection")) {
        result = clickLiveButtonWithCollection(liveButtonID, instance);
      }

    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean clickLiveButtonMPL(
      UiDevice device, String liveButtonID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = clickLiveButtonWithTextMPL(device, liveButtonID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean exitVideoPage(UiDevice device, String exitVideoID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = exitVideoWithText(device, exitVideoID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean homeButton(UiDevice device, String homeButtonID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = clickHomeButtonWithText(device, homeButtonID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean homeElements(UiDevice device, String homeElementsID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = loadHomePageWithTextMPL(device, homeElementsID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean selectGame(UiDevice device, String selectGameID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = selectGameWithText(device, selectGameID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean freeButton(
      UiDevice device, String freeButtonID, String idType, int instance, String action) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text") && action.equalsIgnoreCase("view")) {
        result = viewFreeButtonWithText(device, freeButtonID);
      }
      if (idType.equalsIgnoreCase("text") && action.equalsIgnoreCase("click")) {
        result = clickFreeButtonWithText(device, freeButtonID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean clickBattleFreeButton(
      UiDevice device, String battleForFreeID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = clickBattleFreeWithText(device, battleForFreeID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean gameLoader(UiDevice device, String gameLoaderID, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = gameLoaderWithText(device, gameLoaderID);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean gameLoader1(UiDevice device, String gameLoaderID1, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = gameLoader1WithText(device, gameLoaderID1);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean gameLoader2(UiDevice device, String gameLoaderID2, String idType, int instance) {
    boolean result = false;
    try {
      if (idType.equalsIgnoreCase("text")) {
        result = gameLoader2WithText(device, gameLoaderID2);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return result;
    }
    return result;
  }

  public boolean kpiCalculation() {
    boolean kpiValue = false;
    try {

      Log.d(GlobalVariables.Tag_Name, "HomePage Elements Time:" + hpt);

      double TLHP = (hpt.getTime() - new Timestamp(new Date().getTime()).getTime()) / 1000.0;
      double TLVP = (srat.getTime() - lbt.getTime()) / 1000.0;
      double TSVP = (vst.getTime() - pvt.getTime()) / 1000.0;
      double TLGP = (fbat.getTime() - sgt.getTime()) / 1000.0;
      double TSG = (glt.getTime() - bft.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TLHP);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Video Page=" + TLVP);
      Log.d(GlobalVariables.Tag_Name, "Time To Start Video Play=" + TSVP);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Game Page=" + TLGP);
      Log.d(GlobalVariables.Tag_Name, "Time To Start a Game=" + TSG);

      kpiValue = true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Base KPI Calculation");
      e.printStackTrace();
      return false;
    }
    return kpiValue;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown(String Package, UiDevice device) {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(Package));
      Log.d(GlobalVariables.Tag_Name, "App Closed Successfully");
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }
}
