package com.mozark.uiautomatorlibrary;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class TestApp extends Application {

    private static Context mContext;
    private Activity mCurrentActivity = null;

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mCurrentActivity != null) {
            setCurrentActivity(mCurrentActivity);
        }
    }
}
