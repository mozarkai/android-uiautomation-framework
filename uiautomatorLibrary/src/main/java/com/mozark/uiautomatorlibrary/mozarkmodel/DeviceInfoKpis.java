package com.mozark.uiautomatorlibrary.mozarkmodel;

import java.util.List;

public class DeviceInfoKpis {

  private List<CellInfoModel> cellInfoModels;
  private List<WifiInfoModel> wifiInfoModels;

  public DeviceInfoKpis() {
  }

  public List<WifiInfoModel> getWifiInfoModels() {
    return wifiInfoModels;
  }

  public void setWifiInfoModels(List<WifiInfoModel> wifiInfoModels) {
    this.wifiInfoModels = wifiInfoModels;
  }

  public List<CellInfoModel> getCellInfoModels() {
    return cellInfoModels;
  }

  public void setCellInfoModels(List<CellInfoModel> cellInfoModels) {
    this.cellInfoModels = cellInfoModels;
  }
}
