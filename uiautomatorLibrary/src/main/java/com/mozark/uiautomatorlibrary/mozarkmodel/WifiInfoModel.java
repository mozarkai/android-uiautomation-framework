package com.mozark.uiautomatorlibrary.mozarkmodel;

public class WifiInfoModel {

  public String latency;
  public String downLoadSpeed;
  public String uploadSpeed;
  public String stream;
  public String web;
  String ConnectedTo;
  String ispname;
  String As_No;
  String SSID;
  String wifi_mac_Address;
  String wifi_signal_Strength;
  String wifi_frequency;
  private String publicIp;

  public WifiInfoModel(
      String ispname,
      String as_No,
      String SSID,
      String wifi_mac_Address,
      String wifi_signal_Strength,
      String wifi_frequency) {
    this.ispname = ispname;
    As_No = as_No;
    this.SSID = SSID;
    this.wifi_mac_Address = wifi_mac_Address;
    this.wifi_signal_Strength = wifi_signal_Strength;
    this.wifi_frequency = wifi_frequency;
  }

  public String getConnectedTo() {
    return ConnectedTo;
  }

  public void setConnectedTo(String connectedTo) {
    ConnectedTo = connectedTo;
  }

  public String getLatency() {
    return latency;
  }

  public void setLatency(String latency) {
    this.latency = latency;
  }

  public String getDownLoadSpeed() {
    return downLoadSpeed;
  }

  public void setDownLoadSpeed(String downLoadSpeed) {
    this.downLoadSpeed = downLoadSpeed;
  }

  public String getUploadSpeed() {
    return uploadSpeed;
  }

  public void setUploadSpeed(String uploadSpeed) {
    this.uploadSpeed = uploadSpeed;
  }

  public String getStream() {
    return stream;
  }

  public void setStream(String stream) {
    this.stream = stream;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getIspname() {
    return ispname;
  }

  public void setIspname(String ispname) {
    this.ispname = ispname;
  }

  public String getAs_No() {
    return As_No;
  }

  public void setAs_No(String as_No) {
    As_No = as_No;
  }

  public String getSSID() {
    return SSID;
  }

  public void setSSID(String SSID) {
    this.SSID = SSID;
  }

  public String getWifi_mac_Address() {
    return wifi_mac_Address;
  }

  public void setWifi_mac_Address(String wifi_mac_Address) {
    this.wifi_mac_Address = wifi_mac_Address;
  }

  public String getWifi_signal_Strength() {
    return wifi_signal_Strength;
  }

  public void setWifi_signal_Strength(String wifi_signal_Strength) {
    this.wifi_signal_Strength = wifi_signal_Strength;
  }

  public String getWifi_frequency() {
    return wifi_frequency;
  }

  public String getPublicIp() {
    return publicIp;
  }

  public void setPublicIp(String publicIp) {
    this.publicIp = publicIp;
  }

  public void setWifi_frequency(String wifi_frequency) {
    this.wifi_frequency = wifi_frequency;
  }
}
