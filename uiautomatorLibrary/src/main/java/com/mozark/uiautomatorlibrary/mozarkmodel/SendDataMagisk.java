package com.mozark.uiautomatorlibrary.mozarkmodel;

public class SendDataMagisk {

  private String flag;
  private String appname;

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public String getAppname() {
    return appname;
  }

  public void setAppname(String appname) {
    this.appname = appname;
  }
}
