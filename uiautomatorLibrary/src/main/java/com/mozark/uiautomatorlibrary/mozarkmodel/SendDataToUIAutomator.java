package com.mozark.uiautomatorlibrary.mozarkmodel;

import java.util.List;

public class SendDataToUIAutomator {

  private int jobid;
  private String mobileimei;
  private String appname;
  private String ipadress;
  private String order;
  private String scripId;
  private boolean pCap;
  private boolean vCap;
  private String lat;
  private String lng;
  private String testId;
  private String isp;
  private String startDate = "";
  private String location;
  private Data deviceDetail;
  private List<CellInfoModel> cellInfoModel;
  private List<WifiInfoModel> wifiInfoModel;
  private DeviceInfoKpis deviceInfoKpis;
  private boolean isFromIntent;
  public String publicIp;


  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public boolean isFromIntent() {
    return isFromIntent;
  }

  public void setFromIntent(boolean fromIntent) {
    isFromIntent = fromIntent;
  }

  public List<WifiInfoModel> getWifiInfoModel() {
    return wifiInfoModel;
  }

  public void setWifiInfoModel(List<WifiInfoModel> wifiInfoModel) {
    this.wifiInfoModel = wifiInfoModel;
  }

  public int getJobid() {
    return jobid;
  }

  public void setJobid(int jobid) {
    this.jobid = jobid;
  }

  public String getMobileimei() {
    return mobileimei;
  }

  public void setMobileimei(String mobileimei) {
    this.mobileimei = mobileimei;
  }

  public String getAppname() {
    return appname;
  }

  public void setAppname(String appname) {
    this.appname = appname;
  }

  public String getIpadress() {
    return ipadress;
  }

  public void setIpadress(String ipadress) {
    this.ipadress = ipadress;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public String getScripId() {
    return scripId;
  }

  public void setScripId(String scripId) {
    this.scripId = scripId;
  }

  public boolean ispCap() {
    return pCap;
  }

  public void setpCap(boolean pCap) {
    this.pCap = pCap;
  }

  public boolean isvCap() {
    return vCap;
  }

  public void setvCap(boolean vCap) {
    this.vCap = vCap;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public String getTestId() {
    return testId;
  }

  public void setTestId(String testId) {
    this.testId = testId;
  }

  public String getIsp() {
    return isp;
  }

  public void setIsp(String isp) {
    this.isp = isp;
  }

  public Data getDeviceDetail() {
    return deviceDetail;
  }

  public void setDeviceDetail(Data deviceDetail) {
    this.deviceDetail = deviceDetail;
  }

  public List<CellInfoModel> getCellInfoModel() {
    return cellInfoModel;
  }

  public void setCellInfoModel(List<CellInfoModel> cellInfoModel) {
    this.cellInfoModel = cellInfoModel;
  }

  public DeviceInfoKpis getDeviceInfoKpis() {
    return deviceInfoKpis;
  }

  public void setDeviceInfoKpis(DeviceInfoKpis deviceInfoKpis) {
    this.deviceInfoKpis = deviceInfoKpis;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getPublicIp() {
    return publicIp;
  }

  public void setPublicIp(String publicIp) {
    this.publicIp = publicIp;
  }

}
