package com.mozark.uiautomatorlibrary.mozarkmodel;

public class Data {

  private String status;
  private String city;
  private String fcmId;
  private String model;
  private String os;
  private String version;
  private String carrier;
  private String operator;
  //    private String all;
  private String isp;
  //    private String cellInfo;
  private String lat;
  private String lng;
  private String timeZoneDisplay;
  private String timeZoneId;
  public String publicIp;

  public Data() {}

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getFcmId() {
    return fcmId;
  }

  public void setFcmId(String fcmId) {
    this.fcmId = fcmId;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }
  /*
  public String getAll() {
      return all;
  }

  public void setAll(String all) {
      this.all = all;
  }*/

  public String getTimeZoneDisplay() {
    return timeZoneDisplay;
  }

  public void setTimeZoneDisplay(String timeZoneDisplay) {
    this.timeZoneDisplay = timeZoneDisplay;
  }

  public String getTimeZoneId() {
    return timeZoneId;
  }

  public void setTimeZoneId(String timeZoneId) {
    this.timeZoneId = timeZoneId;
  }

  public String getIsp() {
    return isp;
  }

  public void setIsp(String isp) {
    this.isp = isp;
  }
  /*
  public String getCellInfo() {
      return cellInfo;
  }

  public void setCellInfo(String cellInfo) {
      this.cellInfo = cellInfo;
  }*/

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public String getPublicIp() {
    return publicIp;
  }

  public void setPublicIp(String publicIp) {
    this.publicIp = publicIp;
  }
}
