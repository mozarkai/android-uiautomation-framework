package com.mozark.uiautomatorlibrary.mozarkmodel;

public class CellInfoModel {

  public String latency;
  public String downLoadSpeed;
  public String uploadSpeed;
  public String stream;
  public String web;
  private String ConnectedTo;
  private String technology;
  private String operator;
  private String MCC;
  private String MNC;
  private String lac;
  private String cellId;
  private String Band;
  private String SignalStrength;
  private String SignalQuality;
  private String SINR;
  private String CQI;
  private String RSSI;
  private String TimingAdvance;
  private int dbm;
  private String publicIp;
  public String ispname;

  public CellInfoModel()
  {}

  public CellInfoModel(
      String cellId, String lac, int dbm, String signalStrength, String signalQuality) {
    this.cellId = cellId;
    this.lac = lac;
    this.dbm = dbm;
    SignalStrength = signalStrength;
    SignalQuality = signalQuality;
  }

  public CellInfoModel(
      String technology,
      String operator,
      String MCC,
      String MNC,
      String lac,
      String cellId,
      String band,
      String signalStrength,
      String signalQuality,
      String SINR,
      String CQI,
      String RSSI,
      String timingAdvance,String ispname) {
    this.technology = technology;
    this.operator = operator;
    this.MCC = MCC;
    this.MNC = MNC;
    this.lac = lac;
    this.cellId = cellId;
    Band = band;
    SignalStrength = signalStrength;
    SignalQuality = signalQuality;
    this.SINR = SINR;
    this.CQI = CQI;
    this.RSSI = RSSI;
    TimingAdvance = timingAdvance;
    this.ispname=ispname;
  }

  public String getIspname() {
    return ispname;
  }

  public void setIspname(String ispname) {
    this.ispname = ispname;
  }

  public String getConnectedTo() {
    return ConnectedTo;
  }

  public void setConnectedTo(String connectedTo) {
    ConnectedTo = connectedTo;
  }

  public String getLatency() {
    return latency;
  }

  public void setLatency(String latency) {
    this.latency = latency;
  }

  public String getDownLoadSpeed() {
    return downLoadSpeed;
  }

  public void setDownLoadSpeed(String downLoadSpeed) {
    this.downLoadSpeed = downLoadSpeed;
  }

  public String getUploadSpeed() {
    return uploadSpeed;
  }

  public void setUploadSpeed(String uploadSpeed) {
    this.uploadSpeed = uploadSpeed;
  }

  public String getStream() {
    return stream;
  }

  public void setStream(String stream) {
    this.stream = stream;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getTechnology() {
    return technology;
  }

  public void setTechnology(String technology) {
    this.technology = technology;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public String getMCC() {
    return MCC;
  }

  public void setMCC(String MCC) {
    this.MCC = MCC;
  }

  public String getMNC() {
    return MNC;
  }

  public void setMNC(String MNC) {
    this.MNC = MNC;
  }

  public String getBand() {
    return Band;
  }

  public void setBand(String band) {
    Band = band;
  }

  public String getSINR() {
    return SINR;
  }

  public void setSINR(String SINR) {
    this.SINR = SINR;
  }

  public String getCQI() {
    return CQI;
  }

  public void setCQI(String CQI) {
    this.CQI = CQI;
  }

  public String getRSSI() {
    return RSSI;
  }

  public void setRSSI(String RSSI) {
    this.RSSI = RSSI;
  }

  public String getTimingAdvance() {
    return TimingAdvance;
  }

  public void setTimingAdvance(String timingAdvance) {
    TimingAdvance = timingAdvance;
  }

  public String getCellId() {
    return cellId;
  }

  public void setCellId(String cellId) {
    this.cellId = cellId;
  }

  public String getLac() {
    return lac;
  }

  public void setLac(String lac) {
    this.lac = lac;
  }

  public int getDbm() {
    return dbm;
  }

  public void setDbm(int dbm) {
    this.dbm = dbm;
  }

  public String getSignalStrength() {
    return SignalStrength;
  }

  public void setSignalStrength(String signalStrength) {
    SignalStrength = signalStrength;
  }

  public String getSignalQuality() {
    return SignalQuality;
  }

  public void setSignalQuality(String signalQuality) {
    SignalQuality = signalQuality;
  }

  public String getPublicIp() {
    return publicIp;
  }

  public void setPublicIp(String publicIp) {
    this.publicIp = publicIp;
  }
}
