package com.mozark.uiautomatorlibrary.senddata;


import com.mozark.uiautomatorlibrary.getdata.GetData;
import com.mozark.uiautomatorlibrary.updatedevicestatus.PutDeviceStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface SendDataApi {

  @Headers("Content-Type: application/json")
  @POST("performance/job/acknowledgement")
  Call<GetData> getStringScalar(@Body SendData body);

  @Headers("Content-Type: application/json")
  @PUT("auth/devices/updatedeviceteststatus/{deviceid}/{Running}")
  Call<PutDeviceStatus> updateDeviceStatus(
          @Path("deviceid") String deviceid, @Path("Running") String running);
}
