package com.mozark.uiautomatorlibrary.senddata;


import com.mozark.uiautomatorlibrary.utils.Data;

public class Request {

  private Data data;

  public Request(Data data) {
    this.data = data;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "ClassPojo [data = " + data + "]";
  }
}
