package com.mozark.uiautomatorlibrary.senddata;

public class SendData {

  private Json json;

  public SendData(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
