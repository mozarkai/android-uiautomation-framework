package com.mozark.uiautomatorlibrary.pcap_request;

public class PcapRequest {

  private Json json;

  public PcapRequest(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }
}
