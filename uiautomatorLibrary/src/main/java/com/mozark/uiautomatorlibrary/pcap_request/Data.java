package com.mozark.uiautomatorlibrary.pcap_request;

public class Data {

  private int jobId;

  private String deviceid;

  public Data(String deviceid, int jobId) {
    this.jobId = jobId;
    this.deviceid = deviceid;
  }

  public int getJobId() {
    return jobId;
  }

  public void setJobId(int jobId) {
    this.jobId = jobId;
  }

  public String getDeviceid() {
    return deviceid;
  }

  public void setDeviceid(String deviceid) {
    this.deviceid = deviceid;
  }
}
