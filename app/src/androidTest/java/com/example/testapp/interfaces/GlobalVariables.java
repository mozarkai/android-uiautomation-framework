package com.example.testapp.interfaces;

public interface GlobalVariables {

    String tagName = "Aquamark";
    String appPackageName = "com.agence3pp";
    String homeElementID = "com.agence3pp:id/id_launch_test_btn";
    String secHomeElementID= "Google Map";
    String indoorID = "INDOOR";
    String resultsID = "Results";
    String speedTestID = "Speed test";
    String outdoorID = "OUTDOOR";
    String favouritesID = "Favourites";
    String backToDashboardID = "Back to dashboard";
    String favouritesElementID = "accountDesc";

    String searchAllBankID = "com.uob.id.digitalbank:id/searchViewText";
    String secSearchAllBankID = "Search all banks";
    String searchID = "com.uob.id.digitalbank:id/searchText";
    String searchFieldID = "android.widget.EditText";
    String searchResultID = "UOB INDONESIA";
    String resultID = "com.uob.id.digitalbank:id/tvTitle";
    String accountNumberFieldID = "com.uob.id.digitalbank:id/accountNumberEdt";
    String secAccountNumberFieldID = "Account number";
    String amountFieldID = "android.widget.EditText";
    String nextButtonID = "com.uob.id.digitalbank:id/transferNext";
    String confirmationElementID = "com.uob.id.digitalbank:id/amount";
    String secConfirmationElementID = "Confirmation";
    String swipeToContinueID = "com.uob.id.digitalbank:id/swiperSliderStartArea";
    String securePinElementID = "com.uob.id.digitalbank:id/forgetPin";
    String secondSecurePinElementID = "android.widget.TextView";
    String successElementID = "com.uob.id.digitalbank:id/textSuccess";
    String secSuccessElementID = "Success";



    String enterSearchID = "com.uob.th.digitalbank:id/searchText";
    String selectBankID = "";
    String enterAccountNumberID = "com.uob.th.digitalbank:id/accountNumberEdt";
    String enterAmount = "amount";
    String loaderID = "com.uob.id.digitalbank:id/pgLoader";


}
