package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.getdata.GetData;
import com.mozark.uiautomatorlibrary.listener.ResponseListener;
import com.mozark.uiautomatorlibrary.senddata.Json;
import com.mozark.uiautomatorlibrary.senddata.Request;
import com.mozark.uiautomatorlibrary.senddata.SendData;
import com.mozark.uiautomatorlibrary.senddata.SendDataApi;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.Data;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.JsonUtil;
import com.mozark.uiautomatorlibrary.utils.PcapSend;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.testapp.utility.UtilityClass.SCRIPT_RESULT;

public class Ola implements AppClass {

    public static String testId;
    long timer, End_time_Video, click_on_video_time, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, price_appear_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    boolean screenRecording, pcapFile;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    private Intent intent;
    private Timestamp lt, dt, rt, pt, before_spinner, Video_end_time, videoStartTime;
    private UiDevice device;
    String startDate;
    public static void SendDataToTestApp(Context context, String start, String stop) {
        System.out.println("inside send data");
        File file = new File("/sdcard/QosbeeFiles");
        if (!file.exists()) {
            file.mkdir();
        }
        JSONObject Jmain = new JSONObject();
        try {
            Jmain.put("start", start);
            Jmain.put("stop", stop);

            File gpxfile = new File(file + "/Vcap.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(Jmain.toString());
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            context = TestApp.getContext();

            if (runTest) {
                runTest = clickDropButton();
            }

            Context context = TestApp.getContext();

            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            if (runTest) {
                runTest = enterDrop();
            }
            RecordScreen recordScreen = new RecordScreen();
            recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);
            if (runTest) {
                selectDrop();
                rideNow();
                priceAppear();
                extraLogs();
//                kpiCalculation();
//                tearDown();
//
//                sendDataAndPcap(pCap, appVersion);
                return 0;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
            return 1;
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Ola_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean clickDropButton() {
        try {
            boolean dropBtnFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to click on drop location with Attempt no." + (i + 1));

                    UiSelector destinationId =
                            new UiSelector().resourceId("com.olacabs.customer:id/location");
                    UiObject destinationBtn = device.findObject(destinationId);
                    if (destinationBtn.exists() && destinationBtn.isEnabled()) {
                        destinationBtn.click();
                        dt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Drop Button time:" + dt);
                        dropBtnFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on drop button failed");
                    Thread.sleep(GlobalVariables.TimeOut);
                }
            }
            return dropBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clicking Drop Button Method");
            return false;
        }
    }

    private boolean enterDrop() {
        try {
            boolean enterDroptext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Drop with Attempt no." + (i + 1));
                    UiSelector enterDropField =
                            new UiSelector().resourceId("com.olacabs.customer:id/searchEdit");
                    UiObject enterDrop_text = device.findObject(enterDropField);

                    if (enterDrop_text.exists() && enterDrop_text.isClickable()) {
                        enterDrop_text.setText("Railway Station");
                        Log.d(
                                GlobalVariables.Tag_Name, "Enter Drop time:" + new Timestamp(new Date().getTime()));
                        enterDroptext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter drop location Failed");
                    Thread.sleep(GlobalVariables.TimeOut);
                }
            }
            return enterDroptext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Drop Location");
            return false;
        }
    }

    private boolean selectDrop() {
        try {
            boolean dropselect = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to select drop with Attempt no." + (i + 1));
                    //                    UiSelector selectDropId = new
                    // UiSelector().resourceId("com.olacabs.customer:xpath//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView[1]");
                    //                    UiObject selectDrop = device.findObject(selectDropId);
                    //                    selectDrop.click();
                    device.executeShellCommand("input tap 500 500");
                    Log.d(
                            GlobalVariables.Tag_Name, "Select drop Time:" + new Timestamp(new Date().getTime()));
                    dropselect = true;
                    break;
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Select Drop location Failed");
                    Thread.sleep(GlobalVariables.TimeOut);
                }
            }
            return dropselect;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Selecting Drop");
            return false;
        }
    }

    private boolean rideNow() {
        try {
            boolean rideNowBtnFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to click on Ride Now with Attempt no." + (i + 1));
                    UiSelector rideNowId =
                            new UiSelector().resourceId("com.olacabs.customer:id/button_ride_now");
                    UiObject rideNowBtn = device.findObject(rideNowId);
                    if (rideNowBtn.exists() && rideNowBtn.isClickable()) {
                        rideNowBtn.click();
                    }

                    //                    device.executeShellCommand("input tap 700 2100");
                    rt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Click Ride Now Button Time:" + rt);
                    rideNowBtnFound = true;
                    break;
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Ride Now Failed");
                    Thread.sleep(GlobalVariables.TimeOut);
                }
            }
            return rideNowBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clicking Ride Now");
            return false;
        }
    }

    private boolean priceAppear() {
        try {
            boolean priceAppear = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to find price " + (i + 1));
                try {
                    boolean checkDouble = true;
                    UiSelector priceId1 = new UiSelector().resourceId("com.olacabs.customer:id/fare_amount");
                    UiObject price1 = device.findObject(priceId1);
                    UiSelector priceId2 = new UiSelector().resourceId("com.olacabs.customer:id/fare");
                    UiObject price2 = device.findObject(priceId2);
                    try {
                        if (price1.exists() || price1.isEnabled()) {
                            Log.d(GlobalVariables.Tag_Name, "Price Appear:Yes");
                            pt = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name, "Price Appear Time:" + pt);
                            priceAppear = true;
                            break;
                        }

                    } catch (Exception e) {
                        Log.d(GlobalVariables.Tag_Name, "Error in getting Single Price");
                    }
                    try {
                        if (price2.exists() || price2.isEnabled()) {
                            Log.d(GlobalVariables.Tag_Name, "Price Appear:Yes");
                            pt = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name, "Price Appear Time:" + pt);
                            priceAppear = true;
                            break;
                        }
                    } catch (Exception e) {
                        Log.d(GlobalVariables.Tag_Name, "Error in getting Two Prices");
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Price Appear:No");
                }
            }
            return priceAppear;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Reading Price");
            return false;
        }
    }

    private void kpiCalculation() {
        try {
            long launch_time = lt.getTime();
            long drop_time = dt.getTime();
            long ride_time = rt.getTime();
            //            long price_time = before_spinner.getTime();
            long price_time = pt.getTime();
            time_Load_Home = ((drop_time - launch_time) / 1000.0);
            price_appear_time = ((price_time - ride_time) / 1000.0);
            Log.d(GlobalVariables.Tag_Name, "Time to Load Home Page:" + time_Load_Home);
            Log.d(GlobalVariables.Tag_Name, "Time to Appear Price:" + price_appear_time);
            //            Log.d(GlobalVariables.Tag_Name, "Total Time:" + total_time);
            //            Log.d(GlobalVariables.Tag_Name, "video End Time:" + Video_end_time);
            //            Log.d(GlobalVariables.Tag_Name, "End Time of Video:" + End_time_Video);
            //            Log.d(GlobalVariables.Tag_Name, "Buffer Count:" + Buffer_Count);
            //            Log.d(GlobalVariables.Tag_Name, "Buffer Time:" + Buffer_percentage_time);
            //            Log.d(GlobalVariables.Tag_Name, "Buffer Percent:" + Buffer_percentage);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI calculation");
        }
    }

//    public void sendData(ResponseListener listener, String appVersion) {
//        try {
//
//            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//            Retrofit retroclient =
//                    new Retrofit.Builder()
//                            .baseUrl(ipAdress)
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .client(client)
//                            .build();
//
//            SendDataApi service = retroclient.create(SendDataApi.class);
//            SendData sendData =
//                    new SendData(
//                            new Json(
//                                    new Request(
//                                            new Data(
//                                                    job_id,
//                                                    device_id,
//                                                    testId,
//                                                    "completed",
//                                                    false,
//                                                    Integer.valueOf(order),
//                                                    Integer.valueOf(script),
//                                                    appVersion,
//                                                    appName))));
//            sendData
//                    .getJson()
//                    .getRequest()
//                    .getData()
//                    .setNetworkDetails(new NetworkDetails());
//            Call<GetData> call = service.getStringScalar(sendData);
//            Log.d("", "SendStatus -> " + JsonUtil.toJson(sendData));
//            call.enqueue(
//                    new Callback<GetData>() {
//                        @Override
//                        public void onResponse(Call<GetData> call, Response<GetData> response) {
//
//                            String status = response.body().getJson().getResponse().getStatusCode();
//                            if (status.equalsIgnoreCase("0")) {
//                                Data data =
//                                        new Data(
//                                                job_id,
//                                                device_id,
//                                                testId,
//                                                "completed",
//                                                false,
//                                                Integer.valueOf(order),
//                                                Integer.valueOf(script),
//                                                "1.0",
//                                                appName);
//                                Utility.writeLogResultForLaterUse(data, SCRIPT_RESULT);
//                                listener.onStatus(true, response.body());
//                            } else {
//                                listener.onStatus(false, response.body());
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<GetData> call, Throwable t) {
//                            listener.onStatus(false, t.getMessage());
//                        }
//                    });
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
//        }
//    }
//
//    private void sendDataAndPcap(boolean pCap, String appVersion) {
//        final boolean[] isSendingPcap = {true};
//        sendData(
//                (status, result) -> {
//                    new Thread(
//                            () -> {
//                                try {
//                                    if (status) {
//                                        Log.d(GlobalVariables.Tag_Name, "PCap Start");
//                                        sendPcap(pCap);
//                                    }
//                                    Log.d(GlobalVariables.Tag_Name, "PCap End");
//                                } catch (Exception e) {
//                                    Log.d(GlobalVariables.Tag_Name, "PCap End with Exception " + e.getMessage());
//                                    e.printStackTrace();
//                                }
//                                isSendingPcap[0] = false;
//                            })
//                            .start();
//                }, appVersion);
//        while (isSendingPcap[0]) {
//            //            Log.d(GlobalVariables.Tag_Name, "Waiting ... ");
//        }
//    }

    public void extraLogs() {
        try {
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
            Log.d(
                    GlobalVariables.Tag_Name,
                    GlobalVariables.Play_Video_Time + new Timestamp(new Date().getTime()));

            Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
            Log.d(
                    GlobalVariables.Tag_Name,
                    GlobalVariables.Play_Video_Time + new Timestamp(new Date().getTime()));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Logs");
        }
    }

    public void sendPcap(boolean pCap) {
        PcapSend pcapSend = new PcapSend();
        pcapSend.compressPcap(testId, storedPcapFilePath, ipAdress, device_id, pCap);
    }
}
