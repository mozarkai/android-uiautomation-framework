package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Hotstar extends Base implements AppClass{

  public static String testId;
  public String log = "";
  public Timestamp lt;
  public UiDevice device;
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  File file;
  String OUTPUT_LOG_FILE = "";
  String appName;
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      String searchButtonID = "in.startv.hotstar:id/action_search";
      String enterSearchID = "in.startv.hotstar:id/search_text";
      String searchText = "Koffee With Karan \n";
      String searchResultsID = "in.startv.hotstar:id/grid_content_list";
      String selectVideoID = "in.startv.hotstar:id/grid_content_list";
      String playButtonID = "in.startv.hotstar:id/play";
      String loaderID = "in.startv.hotstar:id/spinner";

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = clickSearchButton(device, searchButtonID, GlobalVariables.resourceIDType, 0);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
        return 1;
      }

      runTest =
          enterSearchText(device, enterSearchID, GlobalVariables.resourceIDType, 0, searchText);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
        return 1;
      }

      runTest = searchResults(device,searchResultsID,GlobalVariables.resourceIDType,0);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Search Results");
        return 1;
      }

      runTest = selectVideo(device, selectVideoID, GlobalVariables.collectionType, 5);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Select Video");
        return 1;
      }

      runTest = clickPlayButton(device, playButtonID, GlobalVariables.resourceIDType, 0);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Play Button");
        return 1;
      }

      runTest =
          bufferRead(
              testId,
              GlobalVariables.OneMin_Timeout,
              device,
              loaderID,
              GlobalVariables.resourceIDType,
              0);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Play Video");
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twoKPI(lt, st, pvt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown(GlobalVariables.Hotstar_Package, device);

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest =
          sendData(job_id, device_id, testId, "completed", false, order, script, appName, ipAdress,startDate, appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }
}
