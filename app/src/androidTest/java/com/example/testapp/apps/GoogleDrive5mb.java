package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class GoogleDrive5mb implements AppClass {

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String OUTPUT_LOG_FILE = "";
    Timestamp Upload_Loader, Download_Loader;
    String appName;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    private UiDevice device;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.order = order;
            this.startDate=startDate;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;

            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            context = TestApp.getContext();
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = Homepage();
            if (!runTest) {
                return 1;
            }

            runTest = uploadButton();
            if (!runTest) {
                return 1;
            }

            runTest = toolbarButton();
            if (!runTest) {
                return 1;
            }

            runTest = selectLocation();
            if (!runTest) {
                return 1;
            }

            runTest = Seach();
            if (!runTest) {
                return 1;
            }

            runTest = SeachEdit();
            if (!runTest) {
                return 1;
            }

            runTest = selectFolder();
            if (!runTest) {
                return 1;
            }

            runTest = selectsubFolder();
            if (!runTest) {
                return 1;
            }

            runTest = selectFile();
            if (!runTest) {
                return 1;
            }

            runTest = FilesButton();
            if (!runTest) {
                return 1;
            }

            runTest = LoaderUpload();
            if (!runTest) {
                return 1;
            }

            runTest = ClearButton();
            if (!runTest) {
                return 1;
            }

            runTest = moreActionButton();
            if (!runTest) {
                return 1;
            }

            runTest = clickDownload();
            if (!runTest) {
                return 1;
            }

            runTest = Download();
            if (!runTest) {
                return 1;
            }

            runTest = Remove();
            if (!runTest) {
                return 1;
            }

            runTest = moveToBin();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();

            Kpi_Calculation();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            deleteFiles();
            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.GoogleDrivePackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private boolean Homepage() {
        try {
            boolean Homepage = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find homepage button with Attempt no." + (i + 1));

                    UiCollection searchSelector =
                            new UiCollection(
                                    new UiSelector().resourceId("com.google.android.apps.docs:id/branded_fab"));

                    if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {

                        Thread.sleep(3000);
                        searchSelector.click();
                        Homepage = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Homepage Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return Homepage;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Homepage Button Method");
            return false;
        }
    }

    private boolean uploadButton() {
        try {
            boolean uploadButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to click on upload button with Attempt no." + (i + 1));

                    UiSelector homeID = new UiSelector().descriptionContains("Upload");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();

                        uploadButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in upload Button:" + new Timestamp(new Date().getTime()));
                }
            }
            return uploadButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding upload Button Method");
            return false;
        }
    }

    private boolean toolbarButton() {
        try {
            boolean toolbarButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to click on toolbar button with Attempt no." + (i + 1));

                    UiSelector homeID = new UiSelector().descriptionContains("Show roots");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();

                        toolbarButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in toolbar Button:" + new Timestamp(new Date().getTime()));
                }
            }
            return toolbarButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding toolbar Button Method");
            return false;
        }
    }

    private boolean selectLocation() {
        try {
            boolean selectBtnFound = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectBtnFound with Attempt no.:" + (i + 1));
                try {

//          UiCollection playBtnContainerID =
//              new UiCollection(
//                  new UiSelector().resourceId("com.android.documentsui:id/roots_list"));
//
//          UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 30);
//

                    UiSelector playBtnContainerId=new UiSelector().text("Downloads");
                    UiObject playBtn=device.findObject(playBtnContainerId);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();

                        selectBtnFound = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectBtnFound Failed");
                }
            }
            return selectBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return false;
        }
    }

    private boolean Seach() {
        try {
            boolean seacrh = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find seacrh button with Attempt no." + (i + 1));

                    UiCollection searchSelector =
                            new UiCollection(
                                    new UiSelector().resourceId("com.android.documentsui:id/option_menu_search"));

                    if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {

                        searchSelector.click();
                        searchSelector.setText("GoogleDriveFiles");

                        seacrh = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "seacrh Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return seacrh;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding seacrh Button Method");
            return false;
        }
    }

    private boolean SeachEdit() {
        try {
            boolean seacrhedit = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find seacrh button with Attempt no." + (i + 1));

                    UiCollection searchSelector =
                            new UiCollection(new UiSelector().resourceId("android:id/search_src_text"));

                    if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {

                        searchSelector.click();
                        searchSelector.setText("GoogleDriveFiles");
                        device.pressEnter();
                        Thread.sleep(14000);
                        seacrhedit = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "seacrh Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return seacrhedit;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding seacrh Button Method");
            return false;
        }
    }

    private boolean selectFolder() {
        try {
            boolean selectFolder = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectFolder with Attempt no.:" + (i + 1));
                try {

                    UiCollection playBtnContainerID =
                            new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

                    UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 1);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();
                        Thread.sleep(2000);

                        selectFolder = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectFolder Failed");
                }
            }
            return selectFolder;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectFolder");
            return false;
        }
    }

    private boolean selectsubFolder() {
        try {
            boolean selectsubFolder = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectFolder with Attempt no.:" + (i + 1));
                try {

                    UiCollection playBtnContainerID =
                            new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

                    UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 1);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();

                        selectsubFolder = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectsubFolder Failed");
                }
            }
            return selectsubFolder;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectsubFolder");
            return false;
        }
    }

    private boolean selectFile() {
        try {
            boolean selectsubFolder = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectFolder with Attempt no.:" + (i + 1));
                try {

                    UiCollection playBtnContainerID =
                            new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

                    UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 2);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();

                        selectsubFolder = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectsubFolder Failed");
                }
            }
            return selectsubFolder;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectsubFolder");
            return false;
        }
    }

    private boolean FilesButton() {
        try {
            boolean filerButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to click on FilesButtonwith Attempt no." + (i + 1));

                    UiSelector homeID = new UiSelector().descriptionContains("Files");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();
                        vt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.UploadClickTime + vt);
                        Thread.sleep(2400);
                        filerButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in FilesButton :" + new Timestamp(new Date().getTime()));
                }
            }
            return filerButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding FilesButton  Method");
            return false;
        }
    }

    private boolean LoaderUpload() {
        try {

            UiSelector loaderId =
                    new UiSelector().resourceId("com.google.android.apps.docs:id/uploading_view");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Browse_Time) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Uploading + loaderYes);
                } else {

                    Upload_Loader = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Uploaded + Upload_Loader);
                    break;
                }
            }

            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean ClearButton() {
        try {
            boolean clearButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying toClearButton  with Attempt no." + (i + 1));

                    device.swipe(0, 0, 50, 50, 3);
                    device.swipe(0, 150, 150, 150, 1);

                    Thread.sleep(2000);

                    UiSelector homeID = new UiSelector().descriptionContains("Clear,Button");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout) && home.isEnabled()) {
                        home.click();

                        clearButtonFound = true;
                        break;
                    } else {
                        clearButtonFound = true;

                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in ClearButton:" + new Timestamp(new Date().getTime()));
                }
            }
            return clearButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding ClearButton Method");
            return false;
        }
    }

    private boolean moreActionButton() {
        try {
            boolean moreActionButton = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to moreActionButton with Attempt no.:" + (i + 1));
                try {

                    UiCollection playBtnContainerID =
                            new UiCollection(
                                    new UiSelector()
                                            .resourceId("com.google.android.apps.docs:id/more_actions_button"));
                    UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 0);
                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();

                        moreActionButton = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on moreActionButton Failed");
                }
            }
            return moreActionButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectsubFolder");
            return false;
        }
    }

    private boolean clickDownload() {
        try {
            boolean clickDownload = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to clickDownload with Attempt no.:" + (i + 1));
                try {

                    UiScrollable items =
                            new UiScrollable(
                                    new UiSelector()
                                            .resourceId("com.google.android.apps.docs:id/menu_recycler_view"));
                    items.setAsVerticalList();
                    items.scrollToEnd(5, 15);

//          UiCollection playBtnContainerID =
//              new UiCollection(
//                  new UiSelector()
//                      .resourceId("com.google.android.apps.docs:id/menu_recycler_view"));

                    UiSelector playBtnContainerId=new UiSelector().text("Download");
                    UiObject playBtn=device.findObject(playBtnContainerId);
                    //  UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 28);
                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();
                        pt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.DownloadClickTime + pt);

                        device.swipe(0, 0, 50, 50, 3);
                        device.swipe(0, 150, 150, 150, 1);

                        Thread.sleep(2000);
                        clickDownload = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on clickDownload Failed");
                }
            }
            return clickDownload;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectsubFolder");
            return false;
        }
    }

    private boolean Download() {
        try {

            UiSelector loaderId = new UiSelector().text("Google Drive");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            Thread.sleep(2000);
            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Browse_Time) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloading + loaderYes);
                } else {

                    Download_Loader = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloaded + Download_Loader);
                    device.pressBack();

                    break;
                }
            }

            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean Remove() {
        try {
            boolean RemoveButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to RemoveButtonFound  with Attempt no." + (i + 1));

                    UiCollection playBtnContainerID =
                            new UiCollection(
                                    new UiSelector().resourceId("com.google.android.apps.docs:id/document_layout"));

                    UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 0);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout) && playBtn.isEnabled()) {
                        playBtn.longClick();
                        Thread.sleep(2000);
                        UiSelector homeID = new UiSelector().descriptionContains("Remove");
                        UiObject home = device.findObject(homeID);
                        if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                            home.click();

                            RemoveButtonFound = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in RemoveButtonFound:" + new Timestamp(new Date().getTime()));
                }
            }
            return RemoveButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding RemoveButtonFound Method");
            return false;
        }
    }

    private boolean moveToBin() {
        try {
            boolean moveToBin = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to moveToBin  with Attempt no." + (i + 1));

                    UiSelector homeID = new UiSelector().resourceId("com.google.android.apps.docs:id/positive_button");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();
                        moveToBin = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in moveToBin:" + new Timestamp(new Date().getTime()));
                }
            }
            return moveToBin;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding moveToBin Method");
            return false;
        }
    }

    public void deleteFiles()
    {
        try {
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());

            os.write("cd /sdcard/Download && rm -rf !(\"GoogleDriveFiles\")".getBytes());
            os.close();
            os.flush();
        } catch (Exception e) {

        }
    }
    public void Kpi_Calculation() {

        try {
            double TTLH = (st.getTime() - lt.getTime()) / 1000.0;
            double UT = (Upload_Loader.getTime() - vt.getTime()) / 1000.0;
            double DT = (Download_Loader.getTime() - pt.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
            Log.d(GlobalVariables.Tag_Name, "File Uploading Time = " + UT);
            Log.d(GlobalVariables.Tag_Name, "File Downloading Time = " + DT);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

}