package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;

public class BajajFinserv_ETB2 implements AppClass{

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt, timestamp;
    //    HashMap<Timestamp, String> hash_map;
    private UiDevice device;

    public double getTime(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Get KPI Time");
        }
        return timestamp;
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            String enterMPINId = "org.altruist.BajajExperia:id/edt_cust_id";
            String mpin = "2021";
            String continueButtonId = "org.altruist.BajajExperia:id/btn_mpin_login_continue";
            String myEMIcardId = "My EMI Card";
            String homeTabId = "org.altruist.BajajExperia:id/navigation_home";
            String relationsTabId = "org.altruist.BajajExperia:id/navigation_relations";
            String offersTabId = "org.altruist.BajajExperia:id/navigation_offer";
            String emiCardTabId = "org.altruist.BajajExperia:id/navigation_emicard";
            String preApprovedOffersId = "org.altruist.BajajExperia:id/tv_preApprovedOffers_Lable";
            String viewAllId = "View all";
            String availOfferId = "AVAIL OFFER";
            String myEMICardId = "org.altruist.BajajExperia:id/rl_emiCard";
            String linkYourEmiCard = "Link Your EMI Card";
            String dobId = "org.altruist.BajajExperia:id/edt_birth_date";
            String dob = "13/06/1981";
            String continueButtonID = "org.altruist.BajajExperia:id/btn_continue";
            String recentCardTransactionId = "Recent Card Transactions";
//            String recentCardTransactionID = "org.altruist.BajajExperia:id/tv_title_recent_transaction";
            String recentCardTransactionID = "org.altruist.BajajExperia:id/btn_unlock_card_number";
            String hamburgerId = "Navigate up";
            String scrollViewId = "android.widget.ScrollView";
            String logoutId = "org.altruist.BajajExperia:id/tv_logout";
            String logoutText = "Logout";
            String okButtonId = "android:id/button1";

            runTest = enterKeys(device, enterMPINId, mpin, "Enter MPIN", "Entered MPIN Time:");
            if (!runTest) {
                runTest = clickOnContentDescription(device, hamburgerId, "Menu Button", "Clicked On Menu Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = scrollResourceId(device, logoutId, scrollViewId, logoutId, "Logout Button", "Clicked On Logout Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = clickOnResourceID(device, okButtonId, "Ok Button", "Clicked On Ok Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = enterKeys(device, enterMPINId, mpin, "Enter MPIN", "Entered MPIN");
                if (!runTest) {
                    return 1;
                }
            }

            runTest = clickOnResourceID(device, continueButtonId, "Continue Button", "Clicked On Continue Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, myEMIcardId, "My EMI Card", "My EMI Card Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, relationsTabId, "Relations Button", "Clicked On Relations Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, viewAllId, "View All", "View All Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, offersTabId, "Offers Button", "Clicked On Offers Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, availOfferId, "Avail Offer", "Avail Offer Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, emiCardTabId, "EMICard Tab Button", "Clicked On EMICard Tab Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementIdAppear(device, recentCardTransactionID, "Recent Card Transactions", "Recent Card Transactions Appear Time:");
            if (!runTest) {
                runTest = elementAppear(device, linkYourEmiCard, "Link Your Card", "Link Your Card Appear Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = enterKeys(device, dobId, dob, "Enter DOB", "Entered DOB Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = clickOnResourceID(device, continueButtonID, "Continue1 Button", "Clicked On Continue1 Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = clickOnResourceID(device, homeTabId, "Home Tab Button", "Clicked on Home Tab Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = elementAppear(device, myEMIcardId, "My EMI Card 2nd Time", "My EMI Card 2nd Appear Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = clickOnResourceID(device, emiCardTabId, "EMICard Tab Button", "Clicked On EMICard Tab Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = elementIdAppear(device, recentCardTransactionID, "Recent Card Transactions", "Recent Card Transactions Appear Time:");
                if (!runTest) {
                    return 1;
                }
            }

            runTest = clickOnContentDescription(device, hamburgerId, "Menu Button", "Clicked On Menu Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = scrollResourceId(device, logoutId, scrollViewId, logoutId, "Logout Button", "Clicked On Logout Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, okButtonId, "Ok Button", "Clicked On Ok Button Time:");
            if (!runTest) {
                return 1;
            }

            kpiCalculations();

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.BajajFinserv_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean clickOnResourceID(UiDevice device, String resourceID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().resourceId(resourceID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnContentDescription(UiDevice device, String contentDesc, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().descriptionContains(contentDesc);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean enterKeys(UiDevice device, String resourceID, String enterKey, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().resourceId(resourceID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
//                    idObject.click();
                    idObject.setText(enterKey);
//                    device.pressEnter();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementIdAppear(UiDevice device, String resourceId, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().resourceId(resourceId);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (!element.exists()) {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                } else {
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    break;
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean scrollResourceId(UiDevice device, String elementId, String scrollId, String textId, String elementName, String successMsg) {
        try {
            boolean value = false;

            for (int i = 0; i < 12; i++) {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName + i);
                try {

                    UiSelector elementID = new UiSelector().resourceId(elementId);
                    UiObject objectId = device.findObject(elementID);

                    if (objectId.waitForExists(GlobalVariables.TimeOut)) {
                        objectId.click();
                        timestamp = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                        kpis.put(elementName, timestamp);
                        value = true;
                        break;
                    } else {
                        UiScrollable scroll = new UiScrollable(
                                new UiSelector().className(scrollId));
                        scroll.scrollTextIntoView(textId);
                        objectId.click();
                        timestamp = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                        kpis.put(elementName, timestamp);
                        value = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private void kpiCalculations() {
        try {

            double continueButtonTime = getTime("Continue Button");
            double myEmiCardTime = getTime("My EMI Card");
            double relationsButtonTime = getTime("Relations Button");
            double viewAllAppearTime = getTime("View All");
            double offersButtonTime = getTime("Offers Button");
            double availOfferAppearTime = getTime("Avail Offer");
            double emiCardTabButtonTime = getTime("EMICard Tab Button");
            double recentCardTransactionsAppearTime = getTime("Recent Card Transactions");

            double TTLH = myEmiCardTime - continueButtonTime;
            double TTLRP = viewAllAppearTime - relationsButtonTime;
            double TTLPOP = availOfferAppearTime - offersButtonTime;
            double TTLECP = recentCardTransactionsAppearTime - emiCardTabButtonTime;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Relations Page=" + TTLRP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Pre-approved Offers Page=" + TTLPOP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load EMI Card Page=" + TTLECP);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}

