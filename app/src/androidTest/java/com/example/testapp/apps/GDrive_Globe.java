package com.example.testapp.apps;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class GDrive_Globe implements AppClass {
    Timestamp homeElementsAppearTime,uploadFileTime,fileUploadedTime,downloadDocsTime,docsDownloadedTime,imageDownloadedTime,videoDownloadedTime,downloadImageTime,downloadVideoTime;
    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homePage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display Home element");
                return 1;
            }
            runTest = gotoFiles();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to go to Files");
                return 1;
            }
            runTest = clickCreate();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Create");
                return 1;
            }
            runTest = clickUpload();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click upload");
                return 1;
            }
            runTest = clickSearch();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Search");
                return 1;
            }
            runTest = clickFolder();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Folder");
                return 1;
            }
            runTest = clickImage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Image");
                return 1;
            }
            runTest = uploadStatus();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Check Upload Status");
                return 1;
            }
            runTest = clickMoreInUploaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click More");
                return 1;
            }
            runTest = firstClear();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Drag First Clear");
                return 1;
            }
            runTest = clickMoreDownload();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click More in Download");
                return 1;
            }

            runTest = kpiCalculation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI's");
                return 1;
            }
            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.GDrive_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }



    public boolean homePage() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                File downloadFolder = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));

                File[] files = downloadFolder.listFiles((d,s)-> s.contains("DL_"));
                for(int i=0; i<files.length; i++){
                    files[i].delete();
                    Log.d(GlobalVariables.Tag_Name, "Files " + files[i]);
                }
//                File[] files2 = downloadFolder.listFiles((d,s)-> s.equalsIgnoreCase("DL_docs.pdf"));
//                for(int i=0; i<files2.length; i++){
//                    files2[i].delete();
//                    Log.d(GlobalVariables.Tag_Name, "Files " + files2[i]);
//                }
//                File[] files3 = downloadFolder.listFiles((d,s)-> s.equalsIgnoreCase("DL_image.jpg"));
//                for(int i=0; i<files3.length; i++){
//                    files3[i].delete();
//                    Log.d(GlobalVariables.Tag_Name, "Files " + files3[i]);
//                }


                UiSelector secElementSelector = new UiSelector().resourceId("com.google.android.apps.docs:id/file_title");
                UiObject secElementObject = device.findObject(secElementSelector);

                UiSelector thirdElementSelector = new UiSelector().resourceId("com.google.android.apps.docs:id/entry_thumbnail");
                UiObject thirdElementObject = device.findObject(thirdElementSelector);

                if (secElementObject.exists() || thirdElementObject.exists())
                {
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + homeElementsAppearTime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Home Page");
            return false;
        }
    }

    public boolean gotoFiles(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().descriptionContains("File");
                UiObject object = device.findObject(selector);

                if (object.exists())
                {
                    object.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Files Successfully Time:" + unUsableTime);
                    value = true;
                    clickMoreInUploaded();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Files");
        }
        return value;
    }

    public boolean clickCreate(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().resourceId("com.google.android.apps.docs:id/entry_label");
                UiObject object = device.findObject(selector);

                UiSelector uploadBtn = new UiSelector().resourceId("com.google.android.apps.docs:id/extended_fab");
                UiObject uploadBtnObj = device.findObject(uploadBtn);

                if (object.exists())
                {
                    uploadBtnObj.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Create Successfully Time:" + unUsableTime);
                    value = true;
                    break;
                }
                if (uploadBtnObj.exists())
                {
                    uploadBtnObj.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Create Successfully Time:" + unUsableTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Create");
        }
        return value;
    }

    public boolean clickUpload(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstElementSelector = new UiSelector().textContains("Upload");
                UiObject firstElementObject = device.findObject(firstElementSelector);
                Log.d(GlobalVariables.Tag_Name,"Trying to Upload Button");

                if(firstElementObject.exists()){
                    firstElementObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Click Upload Successfully Time:" + unUsableTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Upload");
        }
        return value;
    }

    public boolean clickSearch(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().descriptionContains("Search");
                UiObject object = device.findObject(selector);

                UiSelector selectorSearchTab = new UiSelector().className("android.widget.EditText");
                UiObject objectSearchTab = device.findObject(selectorSearchTab);

                if(object.exists())
                {
                    object.click();
                    objectSearchTab.setText("image5mb");
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Click and input message Successfully Time:" + unUsableTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Search");
        }
        return value;
    }

    public boolean clickFolder(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Thread.sleep(2000);
                UiSelector firstSelector = new UiSelector().textContains("image5mb").resourceId("android:id/title");
                UiObject firstElementObject = device.findObject(firstSelector);
                if(firstElementObject.exists())
                {
                    firstElementObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Click Folder Successfully Time:" + unUsableTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Folder");
        }
        return value;
    }

    public boolean clickImage(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstSelector = new UiSelector().resourceId("com.google.android.documentsui:id/icon_thumb");
                UiObject firstElementObject = device.findObject(firstSelector);

                if(firstElementObject.exists())
                {
                    firstElementObject.click();
                    uploadFileTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Image Successfully" + uploadFileTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error clicking Image");
        }
        return value;
    }

    public boolean uploadStatus(){
        boolean value = false;
        boolean checker = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= 120000) {
                UiSelector firstSelector = new UiSelector().textContains("Uploading");
                UiObject firstElementObject = device.findObject(firstSelector);

                if(firstElementObject.exists())
                {
                    Log.d(GlobalVariables.Tag_Name,"Uploading...");
                    checker = true;
                }
                else
                {
                    if (checker) {
                        fileUploadedTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Uploaded Successfully" + fileUploadedTime);
                        value = true;
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Uploading 5mb");
        }
        return value;
    }
    public boolean clickMoreInUploaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Entered in function");

                Thread.sleep(6000);
                UiSelector firstSelector = new UiSelector().descriptionContains("More actions for Snake");
                UiObject firstElementObject = device.findObject(firstSelector);

                UiSelector removeSel = new UiSelector().textContains("Remove").resourceId("com.google.android.apps.docs:id/label");
                UiObject remove = device.findObject(removeSel);
                Log.d(GlobalVariables.Tag_Name, String.valueOf(firstElementObject.exists()));
                Log.d(GlobalVariables.Tag_Name, String.valueOf(remove.exists()));

                if(firstElementObject.exists() || remove.exists())
                {
                    if (firstElementObject.exists())
                    {
                        firstElementObject.click();
                    }
                    Log.d(GlobalVariables.Tag_Name,"Clicked first element");

                    Thread.sleep(2000);
                    UiSelector removeSelector = new UiSelector().textContains("Remove");
                    UiObject removeObject = device.findObject(removeSelector);
                    if (removeObject.exists())
                    {
                        removeObject.click();
                        Log.d(GlobalVariables.Tag_Name,"Clicked second element");

                        Thread.sleep(2000);
                        UiSelector moveSelector = new UiSelector().textContains("Move to").className("android.widget.Button");
                        UiObject moveObject = device.findObject(moveSelector);
                        if (moveObject.exists())
                        {
                            moveObject.click();
                            Timestamp unUsableTime = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name,"Click Remove Successfully" + unUsableTime);
                            value = true;
                            clickMoreInUploaded();
                            break;
                        }
                    }
                    else
                    {
                        Log.d(GlobalVariables.Tag_Name,"Scroll Down");
                        UiScrollable scroll = new UiScrollable(new UiSelector().resourceId("com.google.android.apps.docs:id/menu_recycler_view"));
                        scroll.scrollForward(5);
                    }
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name,"no more item to delete");
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name, String.valueOf(e));
        }
        return value;
    }

    public boolean firstClear(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector uploadBtn = new UiSelector().resourceId("com.google.android.apps.docs:id/extended_fab");
            UiObject uploadBtnObj = device.findObject(uploadBtn);

            UiSelector clearButton = new UiSelector().descriptionContains("Clear");
            UiObject clear = device.findObject(clearButton);

            UiSelector clearButton2 = new UiSelector().textContains("Clear");
            UiObject clear2 = device.findObject(clearButton2);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout)
            {
                int x = device.getDisplayWidth();
                int y = device.getDisplayHeight();
                double width = x / 100.00;
                double height = y / 100.00;
                int w = (int) (width * 30.00);
                int h = (int) (height * 01.00);
                device.drag(w,h,w,h + 200, 6);

                if (uploadBtnObj.exists() == false)
                {
                    if (clear.exists())
                    {
                        clear.click();
                    }
                    if (clear2.exists())
                    {
                        clear2.click();
                    }
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on Drag down First Clear");
        }
        return value;
    }

    public boolean clickMoreDownload(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Loop Count : " + loopCount);
                UiSelector firstSelector = new UiSelector().descriptionContains("More actions for DL_docs");
                UiObject firstElementObject = device.findObject(firstSelector);

                UiSelector secSelector = new UiSelector().descriptionContains("More actions for DL_image");
                UiObject secElementObject = device.findObject(secSelector);

                UiSelector thirdSelector = new UiSelector().descriptionContains("More actions for DL_video");
                UiObject thirdElementObject = device.findObject(thirdSelector);

                if(firstElementObject.exists() && loopCount == 1)
                {
                    firstElementObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"More on docs Clicked Successfully" + unUsableTime);
                    value = true;
                    download();
                    break;
                }
                else if(secElementObject.exists() && loopCount == 2)
                {
                    secElementObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"More on image Clicked Successfully" + unUsableTime);
                    value = true;
                    download();
                    break;
                }
                else if(thirdElementObject.exists() && loopCount == 3)
                {
                    thirdElementObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"More on video Clicked Successfully" + unUsableTime);
                    value = true;
                    download();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Uploading 5mb");
        }
        return value;
    }

    public boolean download(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstSelector = new UiSelector().textContains("Download");
                UiObject firstElementObject = device.findObject(firstSelector);

                if(firstElementObject.exists() && loopCount == 1)
                {
                    firstElementObject.click();
                    downloadDocsTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Download Clicked for docs Successfully" + downloadDocsTime);
                    value = true;
                    dragDown();
                    break;
                }
                if(firstElementObject.exists() && loopCount == 2)
                {
                    firstElementObject.click();
                    downloadImageTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Download Clicked for image Successfully" + downloadImageTime);
                    value = true;
                    dragDown();
                    break;
                }
                if(firstElementObject.exists() && loopCount == 3)
                {
                    firstElementObject.click();
                    downloadVideoTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Download Clicked for video Successfully" + downloadVideoTime);
                    value = true;
                    dragDown();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Clicking Download");
        }
        return value;
    }

    public boolean dragDown(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector uploadBtn = new UiSelector().resourceId("com.google.android.apps.docs:id/extended_fab");
            UiObject uploadBtnObj = device.findObject(uploadBtn);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout)
            {
                int x = device.getDisplayWidth();
                int y = device.getDisplayHeight();
                double width = x / 100.00;
                double height = y / 100.00;
                int w = (int) (width * 30.00);
                int h = (int) (height * 01.00);
                device.drag(w,h,w,h + 200, 6);

                if (uploadBtnObj.exists() == false)
                {
                    checkIfDownloaded();
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on Drag down");
        }
        return value;
    }

    public boolean checkIfDownloaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= 120000 && loopCount < 4) {
                UiSelector firstSelector = new UiSelector().textContains("Downloaded");
                UiObject firstElementObject = device.findObject(firstSelector);

                switch (loopCount)
                {
                    case 1:
                        if(firstElementObject.exists())
                        {
                            docsDownloadedTime = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name,"Docs Downloaded Successfully" + docsDownloadedTime);
                            value = true;
                            loopCount = 2;
                            clearStatus();
                            break;
                        }
                        else
                        {
                            Log.d(GlobalVariables.Tag_Name,"Downloading Docs...");
                        }
                        break;
                    case 2:
                        if(firstElementObject.exists())
                        {
                            imageDownloadedTime = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name,"Image Downloaded Successfully" + imageDownloadedTime);
                            value = true;
                            loopCount = 3;
                            clearStatus();
                            break;
                        }
                        else
                        {
                            Log.d(GlobalVariables.Tag_Name,"Downloading Image...");
                        }
                        break;
                    case 3:
                        if(firstElementObject.exists())
                        {
                            videoDownloadedTime = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name,"Video Downloaded Successfully" + videoDownloadedTime);
                            value = true;
                            loopCount = 4;
                            clearStatus();
                            break;
                        }
                        else
                        {
                            Log.d(GlobalVariables.Tag_Name,"Downloading Video...");
                        }
                        break;
                    default:
                        Log.d(GlobalVariables.Tag_Name,"Waiting to response!");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Checking if Downloaded");
        }
        return value;
    }

    public boolean clearStatus(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector clearButton = new UiSelector().descriptionContains("Clear");
                UiObject clear = device.findObject(clearButton);

                UiSelector clearButton2 = new UiSelector().textContains("Clear");
                UiObject clear2 = device.findObject(clearButton2);

                if (clear.exists())
                {
                    clear.click();
                }
                if (clear2.exists())
                {
                    clear2.click();
                }
                if (loopCount <= 3)
                {
                    Log.d(GlobalVariables.Tag_Name,"Cleared");
                    value = true;
                    clickMoreDownload();
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name,"Cleared");
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Uploading 5mb");
        }
        return value;
    }

    private boolean kpiCalculation() {
        try {
            Thread.sleep(5000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double TTUF = (fileUploadedTime.getTime() - uploadFileTime.getTime()) / 1000.0;
            double TTDD = (docsDownloadedTime.getTime() - downloadDocsTime.getTime()) / 1000.0;
            double TTDI = (imageDownloadedTime.getTime() - downloadImageTime.getTime()) / 1000.0;
            double TTDV = (videoDownloadedTime.getTime() - downloadVideoTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Send 5MB File=" + TTUF);
            Log.d(GlobalVariables.Tag_Name, "Time To Download 5MB Document=" + TTDD);
            Log.d(GlobalVariables.Tag_Name, "Time To Download 10MB Image=" + TTDI);
            Log.d(GlobalVariables.Tag_Name, "Time To Download 30MB Video=" + TTDV);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }
}
