package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Webex implements AppClass{

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  String startDate;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      runTest = Homepage();
      if (!runTest) {
        return 1;
      }

      runTest = JoinMeeting();
      if (!runTest) {
        return 1;
      }

      runTest = EnterMeetingId();
      if (!runTest) {
        return 1;
      }

      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000,vCap);

      runTest = startMeeting();
      if (!runTest) {
        return 1;
      }

      runTest = VideoOn();
      if (!runTest) {
        return 1;
      }

      runTest = ReadyTojoin();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(2000);
      runTest = EnterPassword();
      if (!runTest) {
        return 1;
      }

      runTest = clickOk();
      if (!runTest) {
        return 1;
      }

      runTest = LoaderAppear(vCap);
      if (!runTest) {
        return 1;
      }

      runTest = EndMeeting();
      if (!runTest) {
        return 1;
      }

      runTest = ConfirmEndMeeting();
      if (!runTest) {
        return 1;
      }

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private boolean Homepage() {
    try {
      boolean Homepage = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find homepage button with Attempt no." + (i + 1));

          UiCollection searchSelector =
              new UiCollection(
                  new UiSelector().resourceId("com.cisco.webex.meetings:id/tv_my_pr_join_meeting"));

          if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);

            Homepage = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Homepage Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return Homepage;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Homepage Button Method");
      return false;
    }
  }

  private boolean JoinMeeting() {
    try {
      boolean joinmeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find JoinMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          //                    UiSelector joinmeet = new
          // UiSelector().text("com.cisco.webex.meetings:id/tv_my_pr_join_meeting");
          //                    UiObject joinButton = device.findObject(joinmeet);
          UiCollection searchSelector =
              new UiCollection(
                  new UiSelector().resourceId("com.cisco.webex.meetings:id/tv_my_pr_join_meeting"));
          //                   // searchSelector.click();
          //                    Log.d(GlobalVariables.Tag_Name, "toolbar is clicked:");
          //                    UiObject search = searchSelector.getChildByInstance(new
          // UiSelector(), 1);

          if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchSelector.click();
            // search.click();

            joinmeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "JoinMeeting Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return joinmeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding JoinMeeting Button Method");
      return false;
    }
  }

  private boolean EnterMeetingId() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find enterMeeting text with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/et_connecting_meeting_num");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterMeetingId.click();
            //                        enterMeetingId.setText("913 669 554 ");
            enterMeetingId.setText("1663831449");
            enterMeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "enterMeeting text Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding enterMeeting Method");
      return false;
    }
  }

  private boolean startMeeting() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find StartMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/menu_join");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
              && enterMeetingId.isClickable()
              && enterMeetingId.isClickable()) {
            enterMeetingId.click();

            enterMeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "StartMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
      return false;
    }
  }

  private boolean VideoOn() {
    boolean videoOn = false;

    try {
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find StartMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/warm_video_btn");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterMeetingId.click();

            videoOn = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "StartMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return videoOn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
    }
    return videoOn;
  }

  private boolean ReadyTojoin() {
    try {
      boolean ReadyTojoin = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find StartMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/warm_join_btn");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
              && enterMeetingId.isClickable()
              && enterMeetingId.isClickable()) {
            enterMeetingId.click();
            //                        vt = new Timestamp(new Date().getTime());
            //                        Log.d(GlobalVariables.Tag_Name,
            // GlobalVariables.JoinMeetingClickTime + vt);
            ReadyTojoin = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "StartMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return ReadyTojoin;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
    }
    return ReadyTojoin();
  }

  private boolean EnterPassword() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find password text with Attempt no." + (i + 1));

          //                    UiCollection searchID = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/edtPassword"));
          //                    UiObject enterSearch_text = searchID.getChildByInstance(new
          // UiSelector(), 1);

          //  us.zoom.videomeetings:id/button1
          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/et_connecting_meeting_pass");
          UiObject enterMeetingId = device.findObject(meetingId);

          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            //                        enterMeetingId.setText("s9mTY58qmjf");
            enterMeetingId.setText("Jbx3JNQMU27");
            enterMeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "password text Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding enterMeeting Method");
      return false;
    }
  }

  private boolean clickOk() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Click ok button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId = new UiSelector().resourceId("com.cisco.webex.meetings:id/button1");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
              && enterMeetingId.isClickable()
              && enterMeetingId.isClickable()) {
            enterMeetingId.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
            enterMeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Click ok  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
      return false;
    }
  }

  private boolean LoaderAppear(boolean vCap) {
    try {

      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000,vCap);

      UiSelector loaderId =
          new UiSelector().resourceId("com.cisco.webex.meetings:id/warm_connect_loading");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
        } else {

          Timestamp loaderNo = new Timestamp(new Date().getTime());

          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.cisco", OUTPUT_LOG_FILE);
          c += GlobalVariables.Delay_Time;
        }
      }

      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean EndMeeting() {
    try {
      boolean endMeet = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find EndMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector()
                  .resourceId("com.cisco.webex.meetings:id/small_toolbar_leave_meeting");

          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
              && enterMeetingId.isClickable()
              && enterMeetingId.isClickable()) {
            enterMeetingId.click();

            endMeet = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "EndMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return endMeet;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding EndMeeting  Method");
      return false;
    }
  }

  private boolean ConfirmEndMeeting() {
    try {
      boolean confirmEnd = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find ConfirmEndMeeting button with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.cisco.webex.meetings:id/btn_leavemeeting");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
              && enterMeetingId.isClickable()) {
            enterMeetingId.click();

            confirmEnd = true;
            break;
          } else {
            UiSelector endMeeting =
                new UiSelector().resourceId("com.cisco.webex.meetings:id/btn_endmeeting");
            UiObject endMeetingBtn = device.findObject(endMeeting);
            if (endMeetingBtn.waitForExists(GlobalVariables.Wait_Timeout)
                && endMeetingBtn.isClickable()) {
              endMeetingBtn.click();

              confirmEnd = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "ConfirmEndMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return confirmEnd;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ConfirmEndMeeting  Method");
      return false;
    }
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.WebexPackage));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
