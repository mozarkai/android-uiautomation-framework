package com.example.testapp.apps;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

public class Shahid implements AppClass {
    private Timestamp HPT,EST,SRT,CVPT,VPD,CVT,VPT;
    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String startDate;
    String OUTPUT_LOG_FILE = "";
    private Timestamp lt, st, et, sat, vt;
    private UiDevice device;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {
            context = TestApp.getContext();

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.startDate=startDate;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);


            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = clickSearchButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Go To Search");
                return 1;
            }
            runTest = clickSearchField();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Search Field");
                return 1;
            }
            runTest = enterSearchText();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                return 1;
            }
            runTest = searchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Search Result");
                return 1;
            }
            runTest = clickSearchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Search Result");
                return 1;
            }
            runTest = pageInfoDisplayed();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Display Page");
                return 1;
            }
            runTest = clickPlayButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Play Button");
                return 1;
            }
            runTest = videoStartPlaying();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Play Videos");
                return 1;
            }
            runTest = bufferRead();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Buffer");
                return 1;
            }





            stopScreenRecording();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            // new LogsChecker(context, "stop").execute();

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
            e.printStackTrace();
        }
        return 0;
    }


    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.Timeout);
            device.executeShellCommand(getCommand(GlobalVariables.ShahidPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }


    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Home Element");

                UiSelector homeElementId = new UiSelector().resourceId("net.mbc.shahid:id/img_watch_arrow");
                UiObject homeElement = device.findObject(homeElementId);

                UiSelector secHomeElementId = new UiSelector().resourceId("net.mbc.shahid:id/logo_title");
                UiObject secHomeElement = device.findObject(secHomeElementId);

                if (homeElement.exists() || secHomeElement.exists()) {
                    st = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
                    Thread.sleep(200);
                }
            }
            return homeElementFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
            return false;
        }
    }

    private boolean clickSearchButton() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking Search Button");

                UiSelector searchAppear = new UiSelector().descriptionContains("Explore");
                UiObject searchAppearObject = device.findObject(searchAppear);

                UiSelector secSearchAppear = new UiSelector().textContains("Explore");
                UiObject secSearchAppearObject = device.findObject(secSearchAppear);
                if(searchAppearObject.exists())
                {
                    searchAppearObject.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Search Button" + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                }
                else if(secSearchAppearObject.exists())
                {
                    secSearchAppearObject.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Search Button" + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name, "Search Button Loading");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Search Button Failed");
            return false;
        }
        return value;
    }

    private boolean clickSearchField() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking Search Field");

                UiSelector searchHome = new UiSelector().resourceId("net.mbc.shahid:id/search_view_container");
                UiObject searchHomeObject = device.findObject(searchHome);

                UiSelector secSearchHome = new UiSelector().textContains("Movies");
                UiObject secSearchHomeObject = device.findObject(secSearchHome);
                if(searchHomeObject.exists())
                {
                    searchHomeObject.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Search Button" + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                }
                else if (secSearchHomeObject.exists())
                {
                    secSearchHomeObject.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Search Button" + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Search Click Method");
            return false;
        }
        return value;
    }

    private boolean enterSearchText() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            Log.d(GlobalVariables.Tag_Name, "Entering Text");

            UiSelector selector = new UiSelector().resourceId("net.mbc.shahid:id/search_view");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.setText("News");
                et = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
                device.pressBack();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
        return value;
    }

    private boolean searchResult() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Search Result");

                UiSelector searchResult = new UiSelector().resourceId("net.mbc.shahid:id/image").className("android.widget.ImageView").enabled(true);
                UiObject searchResultObject = device.findObject(searchResult);

                if (searchResultObject.exists()) {
                    sat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return value;
        }
        return value;
    }

    private boolean clickSearchResult() {
        boolean value = false;
        try {
            Thread.sleep(4000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Click Search Result ");

                UiSelector searchResultsID = new UiSelector().resourceId("net.mbc.shahid:id/image").className("android.widget.ImageView").enabled(true);
                UiObject searchResults = device.findObject(searchResultsID);

                if (searchResults.exists()) {
                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 15.00);
                    int h = (int) (height * 20.00);
                    device.click(w,h);
                    Log.d(GlobalVariables.Tag_Name, "Select Video Time:" + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Search Result");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "select video Failed");
            return value;
        }
    }

    private boolean pageInfoDisplayed() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Load Page info");

                UiSelector searchResultsID = new UiSelector().resourceId("net.mbc.shahid:id/show_description");
                UiObject searchResults = device.findObject(searchResultsID);

                if (searchResults.exists()) {
                    Log.d(GlobalVariables.Tag_Name, "Page info Displayed Time: " + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Page info Displayed");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Page info Displayed");
            return value;
        }
    }

    private boolean clickPlayButton() {
        boolean value = false;
        int index = 1;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Clicking Play Button "+ index);

                UiSelector playButton = new UiSelector().resourceId("net.mbc.shahid:id/watch_button_container").index(index);
                UiObject playButtonObject = device.findObject(playButton);

                UiSelector selector = new UiSelector().resourceId("net.mbc.shahid:id/watch_button_container");
                UiObject object = device.findObject(selector);

                if (playButtonObject.exists()) {
                    playButtonObject.click();
                    if (!object.exists())
                    {
                        vt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
                        value = true;
                        break;
                    }
                    else
                    {
                        Log.d(GlobalVariables.Tag_Name, "Missed Click");
                        ++index;
                    }
                }
                else {
                    if (index == 5)
                    {
                        index = 1;
                    }
                    ++index;
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking Play Button");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return false;
        }
        return value;
    }
    private boolean videoStartPlaying() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Check if Video Playing");

                UiSelector selector = new UiSelector().className("android.widget.ProgressBar");
                UiObject object = device.findObject(selector);

                if (!object.exists()) {
                    Log.d(GlobalVariables.Tag_Name, "Video Playing: " + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Video Loading");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Video Playing");
            return false;
        }
        return value;
    }

    private boolean bufferRead() {
        try {
            Log.d(GlobalVariables.Tag_Name, "Checking if Buffering");

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                UiSelector loaderId = new UiSelector().className("android.widget.ProgressBar");
                UiObject loader = device.findObject(loaderId);

                UiSelector tryAgainButton = new UiSelector().resourceId("net.mbc.shahid:id/btn_retry");
                UiObject tryAgainButtonObject = device.findObject(tryAgainButton);

                if (loader.exists() || tryAgainButtonObject.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
                    if (tryAgainButtonObject.exists())
                    {
                        tryAgainButtonObject.click();
                        Log.d(GlobalVariables.Tag_Name, "Try Again Clicked ");
                    }
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
                    Thread.sleep(200);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}

