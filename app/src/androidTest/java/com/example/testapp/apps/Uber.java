package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.PcapSend;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Uber implements AppClass{

  public static String testId;
  long timer, End_time_Video, click_on_video_time, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, price_appear_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  boolean screenRecording, pcapFile;
  String location;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String order, script;
  long testTimes;
  String appName;
  private Intent intent;
  private Timestamp lt, dt, sdt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  private String OUTPUT_LOG_FILE = "";
  String  startDate;

  public static void SendDataToTestApp(
      Context context, String start, String stop, String testId, String status) {
    System.out.println("inside send data");
    File file = new File("/sdcard/QosbeeFiles");
    if (!file.exists()) {
      file.mkdir();
    }
    JSONObject Jmain = new JSONObject();
    try {
      Jmain.put("start", start);
      Jmain.put("stop", stop);
      Jmain.put("testId", testId);
      Jmain.put("status", status);

      File gpxfile = new File(file + "/Vcap.txt");
      FileWriter writer = new FileWriter(gpxfile);
      writer.append(Jmain.toString());
      writer.flush();
      writer.close();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      context = TestApp.getContext();

      if (runTest) {
        runTest = whereTo();
      }

      Context context = TestApp.getContext();
      if (pcapFile) {
        new AquamarkPcap(context, "stop", pCap).execute();

        Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");
      }

      if (runTest) {
        runTest = clickDropButton();
      }
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000,vCap);

      if (runTest) {
        runTest = enterDrop();
      }
      if (runTest) {
        selectDrop();
        //                priceAppear();
        extraLogs();
        kpiCalculation();
        sendPcap(pCap);
        sendData(appVersion);
        tearDown();

        return 0;
      }

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Uber_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean whereTo() {
    try {
      boolean whereToButtonFound = false;
      for (int i = 0; i < 500; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find WhereTo button with Attempt no." + (i + 1));

          this.device.wait(Until.hasObject(By.text("Where to?")), GlobalVariables.Wait_Timeout);
          UiObject whereToAppear =
              device.findObject(
                  new UiSelector().text("Where to?").className("android.widget.TextView"));
          //                    UiSelector whereToId = new UiSelector().text("Where to?");
          //                    UiObject whereToAppear = device.findObject(whereToId);
          if (whereToAppear.waitForExists(4000)) {

            dt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "whereTo Button Appear Time:" + dt);
            whereToButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "whereTo Button Appear Time:No" + new Timestamp(new Date().getTime()));
        }
      }
      return whereToButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding whereTo Button Method");
      return false;
    }
  }

  private boolean clickDropButton() {
    try {
      boolean dropBtnFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on drop location with Attempt no." + (i + 1));

          UiSelector destinationId =
              new UiSelector().resourceId("com.ubercab:id/ub__internal_text");
          UiObject destinationBtn = device.findObject(destinationId);
          if (destinationBtn.exists() && destinationBtn.isEnabled()) {
            destinationBtn.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Drop Button time:" + new Timestamp(new Date().getTime()));
            dropBtnFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on drop button failed");
          Thread.sleep(GlobalVariables.TimeOut);
        }
      }
      return dropBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in clicking Drop Button Method");
      return false;
    }
  }

  private boolean enterDrop() {
    try {
      boolean enterDroptext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Drop with Attempt no." + (i + 1));
          UiSelector enterDropField =
              new UiSelector()
                  .resourceId("com.ubercab:id/ub__location_edit_search_destination_edit");
          UiObject enterDrop_text = device.findObject(enterDropField);

          if (enterDrop_text.exists() && enterDrop_text.isClickable()) {
            enterDrop_text.setText("Railway Station");
            Log.d(
                GlobalVariables.Tag_Name, "Enter Drop time:" + new Timestamp(new Date().getTime()));
            enterDroptext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter drop location Failed");
          Thread.sleep(GlobalVariables.TimeOut);
        }
      }
      return enterDroptext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Drop Location");
      return false;
    }
  }

  private boolean selectDrop() {
    try {
      boolean dropselect = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to select drop with Attempt no." + (i + 1));
          UiCollection buttonSelector =
              new UiCollection(new UiSelector().resourceId("com.ubercab:id/text_search_list_item"));
          //                   buttonSelector.click();
          //                    Log.d("Mozark", "toolbar is clicked:");
          UiObject uiObject3 = buttonSelector.getChildByInstance(new UiSelector(), 0);
          if (uiObject3.isEnabled() || uiObject3.isClickable()) {
            uiObject3.click();
            priceAppear();
            Log.d(GlobalVariables.Tag_Name, "2nd time executing Price Appear Method");
            dropselect = true;

            break;
          }
          // device.executeShellCommand("input tap 500 500");
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Select Drop location Failed");
          Thread.sleep(GlobalVariables.TimeOut);
        }
      }
      return dropselect;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Drop");
      return false;
    }
  }

  private boolean priceAppear() {
    try {

      sdt = new Timestamp(new Date().getTime());
      Log.d(GlobalVariables.Tag_Name, "Select drop Time:" + sdt);

      boolean priceAppear = false;
      for (int i = 0; i < 1000; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to find price " + (i + 1));
        try {
          boolean checkDouble = true;
          Timestamp testTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "TestTime of One method to Other" + testTime);
          //                    UiSelector price = new
          // UiSelector().resourceId("com.ubercab:id/ub__default_fare_cell_element_view");
          //                    UiObject price1 = device.findObject(price);
          //                    UiObject ubergo = device.findObject(new
          // UiSelector().text("UberGo").className("android.widget.TextView"));
          //
          // device.wait(Until.hasObject(By.res("com.ubercab:id/ub__product_selection")), 2);
          UiCollection buttonSelector =
              new UiCollection(new UiSelector().resourceId("com.ubercab:id/ub__product_selection"));
          //                    UiObject price1 = buttonSelector.getChildByInstance(new
          // UiSelector(), 15);
          //                    String price_value = price1.getText();

          if (buttonSelector.exists()) {
            Log.d(GlobalVariables.Tag_Name, "Price Appear:Yes");
            pt = new Timestamp(new Date().getTime());

            Log.d(GlobalVariables.Tag_Name, "Price Appear Time:" + pt);
            priceAppear = true;
            break;
          } else {
            Log.d(GlobalVariables.Tag_Name, "Price Appear:No");
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Price Appear:No");
        }
      }
      return priceAppear;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Reading Price");
      return false;
    }
  }

  public void extraLogs() {
    try {
      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(
          GlobalVariables.Tag_Name,
          GlobalVariables.Play_Video_Time + new Timestamp(new Date().getTime()));

      Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
      Log.d(
          GlobalVariables.Tag_Name,
          GlobalVariables.Play_Video_Time + new Timestamp(new Date().getTime()));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in printing Extra Logs");
    }
  }

  private void kpiCalculation() {
    try {
      long launch_time = lt.getTime();
      long drop_time = dt.getTime();
      long select_drop_time = sdt.getTime();
      //            long price_time = before_spinner.getTime();
      long price_time = pt.getTime();
      time_Load_Home = ((drop_time - launch_time) / 1000.0);
      price_appear_time = ((price_time - select_drop_time) / 1000.0);
      Log.d(GlobalVariables.Tag_Name, "Time to Load Home Page:" + time_Load_Home);
      Log.d(GlobalVariables.Tag_Name, "Time to Appear Price:" + price_appear_time);
      //            Log.d(GlobalVariables.Tag_Name, "Total Time:" + total_time);
      //            Log.d(GlobalVariables.Tag_Name, "video End Time:" + Video_end_time);
      //            Log.d(GlobalVariables.Tag_Name, "End Time of Video:" + End_time_Video);
      //            Log.d(GlobalVariables.Tag_Name, "Buffer Count:" + Buffer_Count);
      //            Log.d(GlobalVariables.Tag_Name, "Buffer Time:" + Buffer_percentage_time);
      //            Log.d(GlobalVariables.Tag_Name, "Buffer Percent:" + Buffer_percentage);

    } catch (Exception e) {
      e.printStackTrace();
      Log.d(GlobalVariables.Tag_Name, "Error in KPI calculation" + e.getStackTrace());
    }
  }

  public boolean sendData(String appVersion) {
    try {

      SendStatus sendStatus = new SendStatus();
      boolean check =
          sendStatus.status(
              job_id,
              device_id,
              testId,
              "completed",
              false,
              order,
              script,
                  appVersion,
              appName,
              ipAdress,startDate);
      if (check) {
        logFileGeneration();

        SendDataToTestApp(context, "", "    ", testId, "completed");
      }

      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }

  public void sendPcap(boolean pCap) {
    PcapSend pcapSend = new PcapSend();
    pcapSend.compressPcap(testId, storedPcapFilePath, ipAdress, device_id, pCap);
  }

  public void logFileGeneration() {
    try {

      File file = new File("/sdcard/Mozark");
      if (!file.exists()) {
        file.mkdir();
      }
      File file1 = new File(file + "/" + testId);
      // String mLogFileName = "/storage/emulated/0/rooted_pcap/" + fileName;
      if (!file1.exists()) {
        file1.mkdir();
      }

      File source = new File("/sdcard/log.txt");
      File sourceM = new File("/sdcard/logM.txt");
      OUTPUT_LOG_FILE = file1 + "/" + testId + ".txt";
      FileOutputStream logFile = new FileOutputStream(OUTPUT_LOG_FILE);
      //            FileInputStream in = new FileInputStream(source);
      //            FileInputStream ins=new FileInputStream(sourceM);
      //
      //
      //
      //            int lens;
      //            while ((lens = ins.read()) > 0 ) {
      //                logFile.write(lens);
      //            }
      //            ins.close();
      //
      //            int len;
      //            while ((len = in.read()) > 0 ) {
      //                logFile.write(len);
      //            }
      // in.close();

      PrintWriter pw = new PrintWriter(logFile);

      // BufferedReader object for file1.txt
      BufferedReader br = new BufferedReader(new FileReader(sourceM));

      String line = br.readLine();

      // loop to copy each line of
      // file1.txt to  file3.txt
      while (line != null) {
        pw.println(line);
        line = br.readLine();
      }

      br = new BufferedReader(new FileReader(source));

      line = br.readLine();

      // loop to copy each line of
      // file2.txt to  file3.txt
      while (line != null) {
        pw.println(line);
        line = br.readLine();
      }

      pw.flush();

      // closing resources
      br.close();
      pw.close();

      logFile.close();

    } catch (Exception e) {

    }
  }
}
