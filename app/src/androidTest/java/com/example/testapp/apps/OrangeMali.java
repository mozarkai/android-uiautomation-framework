package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class OrangeMali implements AppClass {
    //    static boolean checker = false;
    public static String testId;
    public String log = "";
    long counter = 0;
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    Timestamp homeElementsAppearTime, clickSearchTime, enterSearchTime, searchResultsAppearTime;

    @Override
    public int testRun(int job_id, String device_id, String ipAdress, UiDevice device, Timestamp lt, String order, String script, String location, String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homePage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display Home element");
                return 1;
            }
            runTest = clickOrangeShop();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to click OrangeShop  element");
                return 1;
            }
            runTest = productLoadPage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display productLoadPage element");
                return 1;
            }
            runTest = kpiCalculation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI's");
                return 1;
            }

            stopScreenRecording();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            device.executeShellCommand(getCommand(GlobalVariables.OrangeMaliPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }


    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    public boolean homePage(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("Boutique Orange");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                homeElementsAppearTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Home Elements Appear Time:" + homeElementsAppearTime);
                value = true;
            }
            else
            {
                UiSelector selector1 = new UiSelector().textContains("Orange Shop");
                UiObject object1 = device.findObject(selector1);

                if(object1.waitForExists(GlobalVariables.OneMin_Timeout)){
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Home Elements Appear Time:" + homeElementsAppearTime);
                    value = true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in finding search results");
        }
        return  value;
    }

    public boolean clickOrangeShop(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("Boutique Orange");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickSearchTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked On Search Successfully Time:" + clickSearchTime);
                value = true;
            }
            else
            {
                UiSelector selector1 = new UiSelector().textContains("Orange Shop");
                UiObject object1 = device.findObject(selector1);

                if(object1.waitForExists(GlobalVariables.OneMin_Timeout)){
                    object1.click();
                    clickSearchTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked On Search Successfully Time:" + clickSearchTime);
                    value = true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in finding search results");
        }
        return  value;
    }

    public boolean productLoadPage(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("CFA");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                searchResultsAppearTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Search Results Appear Time:" + searchResultsAppearTime);
                value = true;
            }
            else
            {
                UiSelector selector1 = new UiSelector().textContains("XOF");
                UiObject object1 = device.findObject(selector1);

                if(object1.waitForExists(GlobalVariables.OneMin_Timeout)){
                    object1.click();
                    searchResultsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Search Results Appear Time:" + searchResultsAppearTime);
                    value = true;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in finding search results");
        }
        return  value;
    }
    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double SRT = (searchResultsAppearTime.getTime() - clickSearchTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Product Page=" + SRT);

            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }
}