package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class PureGold implements AppClass{

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String startDate;
  File file, file1;
  String OUTPUT_LOG_FILE = "";
  String appName;
  private Timestamp lt,
      DeliveryTime,
      vt,
      et,
      ht,
      it,
      het,
      sat,
      ClickCartTime,
      TimeToLoad,
      ClickCheckoutTime,
      CheckoutAppearTime;
  private UiDevice device;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      runTest = ClickDelivery();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Delievery element");
        return 1;
      }

      runTest = AcceptTerms();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display AcceptTerms element");
        return 1;
      }

      runTest = SelectAddress();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Select Address element");
        return 1;
      }

      runTest = SelectStore();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Select Store element");
        return 1;
      }

      runTest = SearchProduct();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Search Product element");
        return 1;
      }

      runTest = SearchProductToolbar();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Search Product element");
        return 1;
      }

      runTest = ClickOnAddToCart();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Add To Cart element");
        return 1;
      }

      runTest = ClickBackButton();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Back Button  element");
        return 1;
      }

      runTest = CartItemsCheck();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Cart Items Check element");
        return 1;
      }
      runTest = AddItem();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Add Item element");
        return 1;
      }

      runTest = ClickOnChekout();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display CheckOut element");
        return 1;
      }

      runTest = TotalPayment();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Total Payment element");
        return 1;
      }

      runTest = ClickBackButtonAfter();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Back Button After payment element");
        return 1;
      }

      runTest = ClearCart();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable to display Clear cart element");
        return 1;
      }

      stopScreenRecording();

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.PURE_GOLD_PACKAGE));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean ClickDelivery() {
    try {
      boolean clickDelvery = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find clickDelvery button with Attempt no." + (i + 1));

          UiSelector deliveryId =
              new UiSelector().resourceId("com.grocery.puregold:id/home_card_delivery");
          UiObject deliveryAppear = device.findObject(deliveryId);
          if (deliveryAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            deliveryAppear.click();
            DeliveryTime = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + DeliveryTime);
            clickDelvery = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in clickDelvery" + new Timestamp(new Date().getTime()));
        }
      }
      return clickDelvery;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding clickDelvery Button Method");
      return false;
    }
  }


  private boolean AcceptTerms() {
    try {
      boolean AcceptTerms = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find AcceptTerms button with Attempt no." + (i + 1));

          UiSelector AcceptTermsId =
              new UiSelector().resourceId("com.grocery.puregold:id/terms_conditions_accept");
          UiSelector selector =
                  new UiSelector().resourceId("com.grocery.puregold:id/dialog_delivery_ok");
          UiObject AcceptTermsAppear = device.findObject(AcceptTermsId);
          UiObject object = device.findObject(selector);
          if (AcceptTermsAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            AcceptTermsAppear.click();
            AcceptTerms = true;
            break;
          }
          else
          {
            if (object.waitForExists(GlobalVariables.Wait_Timeout)) {
              object.click();
            }
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in AcceptTerms" + new Timestamp(new Date().getTime()));
        }
      }
      return AcceptTerms;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding AcceptTerms Button Method");
      return false;
    }
  }

  private boolean SelectAddress() {
    try {
      boolean SelectAddress = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find SelectAddress button with Attempt no." + (i + 1));

          UiSelector SelectAddressId =
              new UiSelector().resourceId("com.grocery.puregold:id/address_book_record_swipe");
          UiObject SelectAddressAppear = device.findObject(SelectAddressId);
          if (SelectAddressAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SelectAddressAppear.click();
            SelectAddress = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in SelectAddress" + new Timestamp(new Date().getTime()));
        }
      }
      return SelectAddress;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding SelectAddress Button Method");
      return false;
    }
  }

  private boolean SelectStore() {
    try {
      boolean SelectStore = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find SelectStore button with Attempt no." + (i + 1));

          UiCollection SelectStoreId =
              new UiCollection(new UiSelector().resourceId("com.grocery.puregold:id/store_list"));
          UiObject SelectStoreAppear = SelectStoreId.getChildByInstance(new UiSelector(), 1);

          //                    UiSelector SelectStoreId = new
          // UiSelector().resourceId("com.grocery.puregold:id/address_book_record_swipe");
          //                    UiObject SelectStoreAppear = device.findObject(SelectStoreId);
          if (SelectStoreAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SelectStoreAppear.click();
            SelectStore = true;
            break;
          }

        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in SelectStore" + new Timestamp(new Date().getTime()));
        }
      }
      return SelectStore;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding SelectStore Button Method");
      return false;
    }
  }

  private boolean SearchProduct() {
    try {
      boolean SearchProduct = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find SearchProduct button with Attempt no." + (i + 1));

//          UiSelector SearchProductId =
//              new UiSelector().resourceId("com.grocery.puregold:id/menu_search");

          UiSelector SearchProductId =
                  new UiSelector().resourceId("com.grocery.puregold:id/catalog_shop_search_text");

          UiObject SearchProductAppear = device.findObject(SearchProductId);

          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchProductAppear.click();

            SearchProduct = true;
            break;
          } else {
            okGotIt();
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in SearchProduct" + new Timestamp(new Date().getTime()));
        }
      }
      return SearchProduct;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding SearchProduct Button Method");
      return false;
    }
  }

  private boolean SearchProductToolbar() {
    try {
      boolean SearchProductToolbar = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find SearchProductToolbar button with Attempt no." + (i + 1));

          UiSelector SearchProductId =
              new UiSelector().resourceId("com.grocery.puregold:id/toolbar_searchbar");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchProductAppear.click();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Product_Enter_Search_Time + et);
            SearchProductAppear.setText("cappuccino ");
            Thread.sleep(4000);
            int x = device.getDisplayHeight();
            int y = device.getDisplayWidth();
            Log.d("Aquamark", "the value of width" + y);
            Log.d("Aquamark", "the value of Height" + x);

            double s = x / 100.00;
            double n = y / 100.00;
            int p = (int) (s * 99.7);
            int q = (int) (n * 92);

            Log.d("Aquamark", "the value of 90% of width" + q);
            Log.d("Aquamark", "the value of  90% of Height" + p);

            device.click(q, p);

            SearchProductToolbar = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in SearchProductToolbar" + new Timestamp(new Date().getTime()));
        }
      }
      return SearchProductToolbar;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding SearchProductToolbar Button Method");
      return false;
    }
  }

  private boolean ClickOnAddToCart() {
    try {
      boolean ClickOnAddToCart = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find ClickOnAddToCart button with Attempt no." + (i + 1));

          UiSelector SearchProductId =
              new UiSelector()
                  .resourceId("com.grocery.puregold:id/search_item_add_to_cart_status_layout");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            sat = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Product_Search_Result_Time + sat);

            SearchProductAppear.click();
            ClickCartTime = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Click_On_Cart_Time + ClickCartTime);

            ClickOnAddToCart = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in ClickOnAddToCart" + new Timestamp(new Date().getTime()));
        }
      }
      return ClickOnAddToCart;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ClickOnAddToCart Button Method");
      return false;
    }
  }

  private boolean ClickBackButton() {
    try {
      boolean ClickBackButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find ClickBackButton button with Attempt no." + (i + 1));

          UiSelector SearchProductId = new UiSelector().descriptionContains("Navigate up");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchProductAppear.click();
            ClickBackButton = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in ClickBackButton" + new Timestamp(new Date().getTime()));
        }
      }
      return ClickBackButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ClickBackButton Button Method");
      return false;
    }
  }

  private boolean CartItemsCheck() {
    try {
      boolean CartItemsCheck = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find CartItemsCheck button with Attempt no." + (i + 1));


//          UiSelector CartItemsCheckId =
//              new UiSelector().resourceId("com.grocery.puregold:id/cart_quantity_add");
          UiSelector CartItemsCheckId =
                  new UiSelector().resourceId("com.grocery.puregold:id/nav_cart");
          UiObject CartItemsCheckAppear = device.findObject(CartItemsCheckId);
          if (CartItemsCheckAppear.waitForExists(GlobalVariables.Wait_Timeout)
              && CartItemsCheckAppear.isEnabled()) {
            CartItemsCheckAppear.click();
            TimeToLoad = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Cart_items_appear_time + TimeToLoad);

            CartItemsCheck = true;
            break;
          }

        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in CartItemsCheck" + new Timestamp(new Date().getTime()));
        }
      }
      return CartItemsCheck;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding CartItemsCheck Button Method");
      return false;
    }
  }

  private boolean AddItem() {
    try {
      boolean AddItem = false;

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      while (stopWatch.getTime() < GlobalVariables.Video_Run_TimeMid) {
        try {

          UiSelector SearchProductId =
              new UiSelector().resourceId("com.grocery.puregold:id/cart_quantity_add");
          UiObject SearchProductAppear = device.findObject(SearchProductId);

          UiSelector checkOutId =
              new UiSelector().resourceId("com.grocery.puregold:id/cart_checkout");
          UiObject checkOutSelect = device.findObject(checkOutId);

          if (checkOutSelect.exists() && checkOutSelect.isEnabled()) {
            AddItem = true;
            break;
          } else {
            if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
              SearchProductAppear.click();
              Thread.sleep(6000);
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in AddItem" + new Timestamp(new Date().getTime()));
        }
      }
      return AddItem;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding AddItem Button Method");
      return false;
    }
  }

  private boolean ClickOnChekout() {
    try {
      boolean ClickOnChekout = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find ClickOnChekout button with Attempt no." + (i + 1));

          UiSelector SearchProductId =
              new UiSelector().resourceId("com.grocery.puregold:id/cart_checkout");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchProductAppear.click();
            ClickCheckoutTime = new Timestamp(new Date().getTime());
            Log.d(
                GlobalVariables.Tag_Name,
                GlobalVariables.Click_On_CheckOut_time + ClickCheckoutTime);

            ClickOnChekout = true;

            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in ClickOnChekout" + new Timestamp(new Date().getTime()));
        }
      }
      return ClickOnChekout;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ClickOnChekout Button Method");
      return false;
    }
  }

  private boolean TotalPayment() {
    try {
      boolean TotalPayment = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find TotalPayment button with Attempt no." + (i + 1));

          UiSelector TotalPaymentId = new UiSelector().textContains("Subtotal");
          UiObject TotalPaymentAppear = device.findObject(TotalPaymentId);
          if (TotalPaymentAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            CheckoutAppearTime = new Timestamp(new Date().getTime());
            Log.d(
                GlobalVariables.Tag_Name,
                GlobalVariables.Checkout_page_appear_time + CheckoutAppearTime);

            TotalPayment = true;

            break;
          }
          else {
            UiScrollable scroll = new UiScrollable(
                    new UiSelector().className("android.widget.ScrollView"));
            scroll.scrollTextIntoView(String.valueOf(TotalPaymentId));
            if (TotalPaymentAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
              CheckoutAppearTime = new Timestamp(new Date().getTime());
              Log.d(
                      GlobalVariables.Tag_Name,
                      GlobalVariables.Checkout_page_appear_time + CheckoutAppearTime);

              TotalPayment = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in TotalPayment" + new Timestamp(new Date().getTime()));
        }
      }
      return TotalPayment;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding TotalPayment Button Method");
      return false;
    }
  }

  private boolean ClickBackButtonAfter() {
    try {
      boolean ClickBackButtonAfter = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find ClickBackButtonAfter button with Attempt no." + (i + 1));

          UiSelector SearchProductId = new UiSelector().descriptionContains("Navigate up");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchProductAppear.click();

            ClickBackButtonAfter = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in ClickBackButtonAfter" + new Timestamp(new Date().getTime()));
        }
      }
      return ClickBackButtonAfter;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ClickBackButtonAfter Button Method");
      return false;
    }
  }

  private boolean ClearCart() {
    try {
      boolean ClearCart = false;

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      while (stopWatch.getTime() < GlobalVariables.Video_Run_TimeMid) {
        try {

          UiSelector SearchProductId =
              new UiSelector().resourceId("com.grocery.puregold:id/cart_quantity_sub");
          UiObject SearchProductAppear = device.findObject(SearchProductId);
//          UiSelector SearchProductId1 =
//              new UiSelector().resourceId("com.grocery.puregold:id/bNegative");

          UiSelector SearchProductId1 =
              new UiSelector().resourceId("com.grocery.puregold:id/bPositive");

                  UiObject SearchProductAppear1 = device.findObject(SearchProductId1);

          if (SearchProductAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            SearchProductAppear.click();

          } else {
            if (SearchProductAppear1.waitForExists(GlobalVariables.Wait_Timeout)) {

              SearchProductAppear1.click();
              ClearCart = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name, "Error in ClearCart" + new Timestamp(new Date().getTime()));
        }
      }
      return ClearCart;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ClearCart Button Method");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (DeliveryTime.getTime() - lt.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double TLC = (TimeToLoad.getTime() - ClickCartTime.getTime()) / 1000.0;
      double Payment = (CheckoutAppearTime.getTime() - ClickCheckoutTime.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Cart=" + TLC);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Page=" + Payment);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
      e.printStackTrace();
    }
  }

  private boolean okGotIt() {
    try {
      boolean okGotItButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click on Ok Got It Button with Attempt no." + (i + 1));
          UiSelector gotitId = new UiSelector().resourceId("com.grocery.puregold:id/bPositive");
          UiObject gotit = device.findObject(gotitId);

          UiSelector confirmationId = new UiSelector().resourceId("");
          UiObject confirmationIdAppear = device.findObject(confirmationId);

          if (gotit.waitForExists(GlobalVariables.Wait_Timeout)) {
            gotit.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Ok Got It Popup:" + new Timestamp(new Date().getTime()));
            okGotItButton = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Ok Got It popup Failed");
        }
      }
      return okGotItButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Ok Got it Popup Method");
      return false;
    }
  }
}
