package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.*;
import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Paytm implements AppClass {

    private Timestamp HPT, click_on_search_contact_time, SCT, click_on_pay_Time, T_T_G_P_S, click_on_check_balance_time, T_T_C_B;
    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, heat, sqct,mdat,pbt,psat,bayt,bant,enterUPI_Balance,enterUPI_Payment;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = clickScanAnyQR();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click On Scan Any QR");
                return 1;
            }
            runTest = clickOnScanFromGallery();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click on Scan From Gallery");
                return 1;
            }

            runTest = clickOnSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click on Search");
                return 1;
            }

            runTest = enterSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Element");
                return 1;
            }

            runTest = selectQRCode();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Select QR Code");
                return 1;
            }

            runTest = merchantDetailsAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Get Merchant Details");
                return 1;
            }

            runTest = clickOnCheckBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click On Check Balance");
                return 1;
            }

            runTest = enterUpiPin();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter UPI Pin");
                return 1;
            }

            runTest = clickOnEnterForBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click On Enter Element for Balance");
                return 1;
            }

            runTest = checkBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Balance");
                return 1;
            }

            runTest = enterAmount();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To  Enter Amount");
                return 1;
            }

            runTest = clickOnPay();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Click On Pay Element");
                return 1;
            }

            runTest = enterUpiPin();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Upi Pin");
                return 1;
            }

            runTest = clickOnEnterForPayment();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click On Enter Element for Payment");
                return 1;
            }

            runTest = checkPaymentStatus();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Payment Status");
                return 1;
            }

            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check KPI's");
                return 1;
            }

            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Paytm_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private boolean homeElement() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().textContains("To Mobile");
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                if (object.exists()) {
                    heat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + heat);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
        }
        return value;
    }

    public boolean clickScanAnyQR() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().resourceId("net.one97.paytm:id/scan_btn");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.Wait_Timeout)) {
                object.click();
                Log.d(GlobalVariables.Tag_Name, "Click on Scan Any QR Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Scan Any QR Button");
        }
        return value;
    }

    public boolean clickOnScanFromGallery() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().resourceId("net.one97.paytm:id/galley_scan");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.Wait_Timeout)) {
                object.click();
                Log.d(GlobalVariables.Tag_Name, "Click on Scan From Gallery Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Scan From Gallery Button");
        }
        return value;
    }

    private boolean clickOnSearch() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().descriptionContains("Search");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked On Search Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Click On Search Button Failed");
        }
        return value;
    }

    private boolean enterSearch() {
        boolean value = false;
        try {
            UiCollection element =
                    new UiCollection(new UiSelector().text("Search…"));

            if (element.waitForExists(GlobalVariables.Wait_Timeout)) {
                element.click();
                element.setText("PaytmQRCodeScanner");
                device.pressEnter();
                Log.d(GlobalVariables.Tag_Name, "Entered Search Text Time:" + new Timestamp(new Date().getTime()));
                Thread.sleep(5000);
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Enter Search Button Failed");
        }
        return value;
    }

    private boolean selectQRCode() {
        boolean value = false;
        try {

            UiCollection elementID1 =
                    new UiCollection(new UiSelector().resourceId("com.google.android.documentsui:id/dir_list"));

            UiObject element1 = elementID1.getChildByInstance(new UiSelector(), 2);

            UiCollection elementID2 =
                    new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

            UiObject element2 = elementID2.getChildByInstance(new UiSelector(), 2);

            if (element1.waitForExists(GlobalVariables.HalfMin_Timout)) {
                element1.click();
                sqct = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Selected QR Code Time:" + sqct);
                value = true;
            } else {
                element2.click();
                sqct = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Selected QR Code Time:" + sqct);
                value = true;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on Select QR Code Failed");
        }
        return value;
    }

    private boolean merchantDetailsAppear() {
        boolean value = false;
        try {

            UiSelector selector =
                    new UiSelector().textContains("Add");
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (object.exists()) {
                    mdat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Merchant Details Appear Time:" + mdat);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Merchant Details");
        }
        return value;
    }

    public boolean clickOnCheckBalance() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().textContains("Check Balance");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.Wait_Timeout)) {
                object.click();
                click_on_check_balance_time = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On Check Balance Time:" + click_on_check_balance_time);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Check Balance Button");
        }
        return value;
    }

    public boolean clickOnEnterForBalance() {
        boolean value = false;
        try {

            UiSelector selector = new UiSelector().className("android.widget.ImageView").index(2).clickable(true).focusable(true);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                enterUPI_Balance = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click on Enter UPI Payment Time for Balance:" + enterUPI_Balance);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in click On Enter UPI for Balance");
        }
        return value;
    }

    private boolean checkBalance() {
        boolean value = false;
        try {

            UiSelector viewersSelector = new UiSelector().textContains("Available Balance");
            UiObject viewersObject = device.findObject(viewersSelector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= (GlobalVariables.OneMin_Timeout)) {
                if (viewersObject.exists()) {
                    bayt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Balance Appear:YES Time: " + bayt);
                    value = true;
                    break;
                } else {
                    bant = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Balance Appear:NO Time: " + bant);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Check Balance");
        }
        return value;
    }

    private boolean enterAmount() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("net.one97.paytm:id/et_amount");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                object.setText("1");
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Entering Amount");
            return false;
        }
        return value;
    }

    private boolean clickOnPay() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("net.one97.paytm:id/payment_proceed_pay_btn_prefill_amount");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                pbt = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On Pay Time:" + pbt);
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in click on Pay Button");
            return false;
        }
        return value;
    }


    private boolean enterUpiPin() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("net.one97.paytm:id/form_item_input");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
//                device.pressKeyCode(KeyEvent.KEYCODE_0);
//                device.pressKeyCode(KeyEvent.KEYCODE_0);
//                device.pressKeyCode(KeyEvent.KEYCODE_5);
//                device.pressKeyCode(KeyEvent.KEYCODE_5);
//                device.pressKeyCode(KeyEvent.KEYCODE_9);
//                device.pressKeyCode(KeyEvent.KEYCODE_9);

                device.pressKeyCode(KeyEvent.KEYCODE_5);
                device.pressKeyCode(KeyEvent.KEYCODE_3);
                device.pressKeyCode(KeyEvent.KEYCODE_7);
                device.pressKeyCode(KeyEvent.KEYCODE_4);
                Thread.sleep(2000);

                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in enterUpiPin Drawer");
        }
        return value;
    }


    public boolean clickOnEnterForPayment() {
        boolean value = false;
        try {

            UiSelector selector = new UiSelector().className("android.widget.ImageView").index(2).clickable(true).focusable(true);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                enterUPI_Payment = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click on Enter UPI Payment Time:" + enterUPI_Payment);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in click On Enter UPI for Payment");
        }
        return value;
    }

    private boolean checkPaymentStatus() {
        boolean value = false;
        try {

            UiSelector selector = new UiSelector().textContains("PAYMENT SUCCESSFUL");
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= (GlobalVariables.TwoMins_Timeout)) {
                if (object.exists()) {
                    Timestamp loading = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Payment Status Appear:YES Time: " + loading);
                    psat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Payment Status Appear Time:" + psat);
                    value = true;
                    break;
                } else {
                    Timestamp loading = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "payment_status Appear:NO Time: " + loading);

                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in checking Payment Status");
        }
        return value;
    }

    private boolean kpiCalculation() {
        try {
            double TTLH = (heat.getTime() - lt.getTime()) / 1000.0;
            double TTMD = (mdat.getTime() - sqct.getTime()) / 1000.0;
            double TTCPT = (psat.getTime() - enterUPI_Payment.getTime()) / 1000.0;
            double TTCB = (bayt.getTime() - enterUPI_Balance.getTime()) / 1000.0;


            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Merchant Details From QR Code=" + TTMD);
            Log.d(GlobalVariables.Tag_Name, "Time To Complete Payment Transaction=" + TTCPT);
            Log.d(GlobalVariables.Tag_Name, "Time To Check Balance=" + TTCB);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }
}
