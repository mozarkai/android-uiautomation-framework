package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Vc_GoogleMeet implements AppClass{

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String OUTPUT_LOG_FILE = "";
  String appName;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  String startDate;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      Log.d("Aquamark", "");
      context = TestApp.getContext();
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);
      runTest = Homepage();
      if (!runTest) {
        return 1;
      }

      runTest = micOff();
      if (!runTest) {
        return 1;
      }

      runTest = videoOff();
      if (!runTest) {
        return 1;
      }

      runTest = clickMeetingCode();
      if (!runTest) {
        return 1;
      }

      runTest = EnterMeetingId();
      if (!runTest) {
        return 1;
      }

      runTest = JoinMeeting();
      if (!runTest) {
        return 1;
      }

      //
      //            runTest=AskJoin();
      //            if(!runTest)
      //            {
      //
      //
      //
      //
      //            }

      //            runTest = startMeeting();
      //
      runTest = LoaderAppear(vCap);
      if (!runTest) {
        return 1;
      }

      runTest = EndMeeting();
      if (!runTest) {
        return 1;
      }

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private boolean Homepage() {
    try {
      boolean Homepage = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find homepage button with Attempt no." + (i + 1));

          UiCollection searchSelector =
              new UiCollection(
                  new UiSelector()
                      .resourceId(
                          "com.google.android.apps.meetings:id/audio_switch_button_container"));

          if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            Thread.sleep(3000);
            Homepage = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Homepage Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return Homepage;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Homepage Button Method");
      return false;
    }
  }

  private boolean clickMeetingCode() {
    try {
      boolean clickMeetingCode = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find clickMeetingCode button with Attempt no." + (i + 1));

          UiSelector joinmeeting =
              new UiSelector()
                  .resourceId("com.google.android.apps.meetings:id/enter_meeting_code_button");
          UiObject clickMeeting = device.findObject(joinmeeting);

          if (clickMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            clickMeeting.click();
            clickMeetingCode = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "clickMeetingCode Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return clickMeetingCode;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in clickMeetingCode  Button Method");
      return false;
    }
  }

  private boolean EnterMeetingId() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find enterMeeting text with Attempt no." + (i + 1));

          //  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector()
                  .resourceId("com.google.android.apps.meetings:id/join_meeting_edittext");
          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterMeetingId.click();
//            enterMeetingId.setText("mee-bbog-brn");
            enterMeetingId.setText("dtv-shxk-pxo");
            enterMeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "enterMeeting text Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding enterMeeting Method");
      return false;
    }
  }

  private boolean JoinMeeting() {
    try {
      boolean joinMeeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find JoinMeeting button with Attempt no." + (i + 1));

          UiSelector joinmeeting =
              new UiSelector()
                  .resourceId("com.google.android.apps.meetings:id/join_meeting_positive_button");
          UiObject clickOnJoin = device.findObject(joinmeeting);

          if (clickOnJoin.waitForExists(GlobalVariables.Wait_Timeout)) {
            clickOnJoin.click();
            // search.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);

            joinMeeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "JoinMeeting Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return joinMeeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding JoinMeeting Button Method");
      return false;
    }
  }

  private boolean micOff() {
    boolean micof = false;

    try {
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find micof button with Attempt no." + (i + 1));
          UiSelector micOffID =
              new UiSelector().resourceId("com.google.android.apps.meetings:id/mic_button");
          UiObject mic = device.findObject(micOffID);
          if (mic.waitForExists(GlobalVariables.GoogleTime)) {
            mic.click();

            micof = true;

            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "micof Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return micof;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in micof Method");
    }
    return micof;
  }

  private boolean videoOff() {
    boolean videoOf = false;

    try {
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find videoOff button with Attempt no." + (i + 1));
          UiSelector videoOffID =
              new UiSelector().resourceId("com.google.android.apps.meetings:id/videocam_button");
          UiObject video = device.findObject(videoOffID);
          if (video.waitForExists(GlobalVariables.GoogleTime)) {
            video.click();

            videoOf = true;

            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "videoOff Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return videoOf;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in videoOff Method");
    }
    return videoOf;
  }

  private boolean LoaderAppear(boolean vCap) {
    try {
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_TimeMid / 1000, vCap);

      UiSelector loaderId =
          new UiSelector().resourceId("com.google.android.apps.meetings:id/progress_bar");

      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_TimeMid) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
        } else {

          Timestamp loaderNo = new Timestamp(new Date().getTime());

          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.googlemeet", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean EndMeeting() {
    try {
      boolean endMeet = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find EndMeeting button with Attempt no." + (i + 1));
          device.click(500, 500);
          // \  UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
          UiSelector meetingId =
              new UiSelector().resourceId("com.google.android.apps.meetings:id/leave_button");

          UiObject enterMeetingId = device.findObject(meetingId);
          if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterMeetingId.click();

            endMeet = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "EndMeeting  Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return endMeet;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding EndMeeting  Method");
      return false;
    }
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.GoogleMeetPackage));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
