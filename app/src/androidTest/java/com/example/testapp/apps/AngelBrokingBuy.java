package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;


public class AngelBrokingBuy implements AppClass {

    public static String testId;
    public String log = "";

    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    private Timestamp lt, het, cct, lsct, cbc, dt;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){
    boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

            runTest = homeElement();
            if (!runTest) {
                return 1;
            }

            runTest = loadSensex();
            if (!runTest) {
                return 1;
            }

            runTest = buyStock();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            kpiCalculation();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate, ipAdress);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;

    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.AngelBroking_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject homeElement = device.findObject(new UiSelector().textContains("Watchlist"));
                if (homeElement.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    private boolean loadSensex() {
        boolean loadSensexFound = false;

        try {

            UiSelector searchOption = new UiSelector().resourceId("com.msf.angelmobile:id/floating_search_1");
            UiObject searchAppear = device.findObject(searchOption);
            searchAppear.click();
            UiSelector searchBox = new UiSelector().resourceId("com.msf.angelmobile:id/symbol_searchView");
            UiObject searchField = device.findObject(searchBox);
            searchField.waitForExists(GlobalVariables.Wait_Timeout);
            searchField.click();
            searchField.setText("NIFTYBEES");
            UiObject niftyBees = device.findObject(new UiSelector().resourceId("com.msf.angelmobile:id/symbol_name").index(0));
            niftyBees.waitForExists(GlobalVariables.Wait_Timeout);
            niftyBees.click();
            UiObject niftyBeesOption = device.findObject(new UiSelector().text("NSE"));
            niftyBeesOption.click();

            UiObject charts = device.findObject(new UiSelector().resourceId("com.msf.angelmobile:id/charts"));
            charts.click();
            cct = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On Chart Option:" + cct);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject sensexChart = device.findObject(new UiSelector().resourceId("angelChart"));
                if (sensexChart.exists() && sensexChart.isEnabled()) {
                    lsct = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Sensex Chart Appear Time:" + lsct);
                    loadSensexFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Sensex Chart");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Sensex Chart");
            return loadSensexFound;
        }
        return loadSensexFound;
    }

    private boolean buyStock() {
        boolean buyStockFound = false;
        try {

            UiObject buyOption = device.findObject(new UiSelector().text("BUY"));
            buyOption.click();
            UiObject quantityOption = device.findObject(new UiSelector().resourceId("com.msf.angelmobile:id/edt_qty"));
            quantityOption.setText("1");
            UiObject marketOption = device.findObject(new UiSelector().text("Market"));
            marketOption.click();
            UiObject deliveryOption = device.findObject(new UiSelector().text("Delivery"));
            deliveryOption.click();
            UiObject dayOption = device.findObject(new UiSelector().text("Day"));
            dayOption.click();
            UiObject niftyBeesBuy = device.findObject(new UiSelector().resourceId("com.msf.angelmobile:id/buyBtnLayout"));
            niftyBeesBuy.click();

            UiObject confirmBuy = device.findObject(new UiSelector().resourceId("com.msf.angelmobile:id/buyBtnLayout"));
            confirmBuy.click();
            cbc = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Confirm Buy:" + cbc);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject positionsOption = device.findObject(new UiSelector().text("Delivery"));
                if (positionsOption.exists()) {
                    dt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Buy Stock Appear Time:" + dt);
                    buyStockFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Buy Stock Page");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Buy Stock page");
            return buyStockFound;
        }
        return buyStockFound;
    }

    private void kpiCalculation() {
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double TTLSC = (lsct.getTime() - cct.getTime()) / 1000.0;
            double TTBY = (dt.getTime() - cbc.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Stock Chart=" + TTLSC);
            Log.d(GlobalVariables.Tag_Name, "Time To Buy Stock=" + TTBY);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean sendData(String appVersion, String startDate, String ipAdress) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(job_id, device_id, testId, "completed", false, order, script, appVersion, appName,ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}
