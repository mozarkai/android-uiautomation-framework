package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class UpstoxProSell implements AppClass {public static String testId;
    public String log = "";

    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    private Timestamp lt, het, cct, lsct, sp, ohp;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,pCap);

            runTest = homeElement();
            if (!runTest) {
                return 1;
            }

            runTest = loadSensex();
            if (!runTest) {
                return 1;
            }

            runTest = sellStock();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            kpiCalculation();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;

    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.UpstoxPro_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector homeElementId =
                        new UiSelector().textContains("Watchlist");
                UiObject homeElement = device.findObject(homeElementId);
                if (homeElement.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    private boolean loadSensex() {
        boolean loadSensexFound = false;

        try {
            UiObject watchlistOption = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/bottomNavigationWatchlist"));
            watchlistOption.click();
            UiObject addOption = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/action_add"));
            addOption.click();
            UiObject searchText = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/search_text"));
            searchText.setText("nippon india nifty bees");
            UiObject addButton = device.findObject(new UiSelector().text("ADD"));
            addButton.waitForExists(GlobalVariables.Wait_Timeout);
            if(addButton.exists()){
                addButton.click();
            }
            UiObject backButton = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/back_button"));
            backButton.click();
            UiObject niftyBees = device.findObject(new UiSelector().text("NIFTYBEES"));
            niftyBees.click();
            UiObject charts = device.findObject(new UiSelector().text("Charts"));
            charts.click();
            UiObject monthOption = device.findObject(new UiSelector().text("1M"));
            monthOption.click();
            cct = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On 1M Option:" + cct);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject loaderImage = device.findObject(new UiSelector().className("stx-show"));
//                Boolean chart = doc.body().className().contains("stx-show");

//                doc.body().html("cq-loader") doc.body().tagName("cq-loader");
//                UiObject chart = device.findObject(new UiSelector().className("android.view.View").index(3)
//                        .className("android.view.View").index(2).className("android.view.View"));
                if (!loaderImage.exists()) {
                    lsct = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Sensex Chart Appear Time:" + lsct);
                    loadSensexFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Sensex Chat");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Sensex Chart");
            return loadSensexFound;
        }
        return loadSensexFound;
    }

    private boolean sellStock() {
        boolean sellStockFound = false;
        try {

            UiObject sellOption = device.findObject(new UiSelector().text("Sell"));
            sellOption.click();
            UiObject quantityOption = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/quantity_value"));
            quantityOption.setText("1");
            UiObject orderComplexity = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/v1_icon"));
            orderComplexity.click();
            UiObject simpleOption = device.findObject(new UiSelector().text("SIMPLE"));
            simpleOption.click();
            UiObject orderType = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/icon"));
            orderType.click();
            UiObject marketOption = device.findObject(new UiSelector().text("MARKET"));
            marketOption.click();
            UiObject reviewOption = device.findObject(new UiSelector().text("REVIEW"));
            reviewOption.click();
            UiObject sellButton = device.findObject(new UiSelector().text("SELL"));
            sellButton.click();
            sp = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Sell:" + sp);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject doneOption = device.findObject(new UiSelector().text("SELL"));
                if (doneOption.exists()) {
                    ohp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Sell Stock Appear Time:" + ohp);
                    sellStockFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Sell Stock Page");
                }
            }

            UiObject watchlist = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/bottomNavigationWatchlist"));
            watchlist.click();
            UiObject addOption1 = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/action_add"));
            addOption1.click();
            UiObject searchText1 = device.findObject(new UiSelector().resourceId("in.upstox.pro:id/search_text"));
            searchText1.setText("nippon india nifty bees");
            UiObject addButton1 = device.findObject(new UiSelector().text("REMOVE"));
            addButton1.click();

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in loading Sell Stock page");
            return sellStockFound;
        }
        return sellStockFound;

    }

    private void kpiCalculation() {
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double TTLSC = (lsct.getTime() - cct.getTime()) / 1000.0;
            double TTSS = (ohp.getTime() - sp.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Stock Chart=" + TTLSC);
            Log.d(GlobalVariables.Tag_Name, "Time To Sell Stock=" + TTSS);


        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
