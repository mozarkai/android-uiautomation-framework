package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;


public class MPL_Money_1 implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt;
    private UiDevice device;

    public double getTime(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Get KPI Time");
        }
        return timestamp;
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            String homePageId = "All Games";
            String walletIconId = "";
            String walletXpath = "//android.view.ViewGroup[1]//android.view.ViewGroup[1]//android.view.ViewGroup[4]";
            //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]
            int walletindex = 3;
            String cashBalanceId = "Cash Balance";
            String addCashButtonId = "Add Cash";
            String addDepositCashId = "Add Deposit Cash";
            String yourBalanceId = "Your Balance";
            String proceedToAddcashId = "Proceed To Add Cash";
            String paymentOptionId = "Payment Options";
            String chooseYourBankId = "Choose Your Bank";
            String allBanksId = "ALL BANKS";
            String iciciBankId = "ICICI Netbanking";
            String welcomePageId = "Welcome to Razorpay";
            String successButtonId = "Success";
            String depositStatusText = "Deposit Status";
            String paymentSuccessId = "Payment Successful";
            String scratchcardsId = "Scratch Cards";
            String offersPageId = "Offers";
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.view.ViewGroup
//            /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[2]/android.view.ViewGroup

//            /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup
//            /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup
            String firstScratchCardId = "//android.view.ViewGroup[1]//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup";
            String highlightedScratchCardId = "//android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup";
            String youWonId = "You won";
            String CongratulationsText = "Congratulations!!";
            String DepositedtoBonuscashText = "Deposited to Bonus cash";

            String availableOnId = "Available on";
            String availableDateId = "17th Feb | 7:45 pm";
            String earnedFromId = "Earned from:";
            String scratchReadyId = "Scratch back and forth to reveal.";
            String rupeedId = "₹";
//            Middle Square Bounds = [277,502][456,681]
//            FullCard Bounds = [145,437][575,868]

            runTest = elementAppear(device, homePageId, "Home Elements", "Home Page Elements Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, walletIconId, "Wallet Button", "Clicked On Wallet Button Time:");
            if (!runTest) {
                return 1;
            }

//            clickOnText(device, okayButtonId, "Okay Button", "Clicked On Okay Button Time:");

            runTest = elementAppear(device, cashBalanceId, "Cash Balance Appear", "Cash Balance Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, addCashButtonId, "Add Cash Button", "Clicked On Add Cash Button Time:");
            if (!runTest) {
                return 1;
            }

//            clickOnText(device, okayButtonId, "Okay Button", "Clicked On Okay Button Time:");

            runTest = elementAppear(device, addDepositCashId, "Add Deposit Cash Appear", "Add Deposit Cash Appear Time:");
            if (!runTest) {
                return 1;
            }

            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);

            runTest = clickOnOnlyText(device, proceedToAddcashId, "Proceed To Add Cash", "Clicked on Proceed To Add Cash Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, paymentOptionId, "Payment Option", "Payment Option Page Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, chooseYourBankId, "Choose Your Bank", "Clicked on Choose Your Bank Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, allBanksId, "All Banks", "All Banks Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, iciciBankId, "Select ICICI Bank", "Selected ICICI Bank Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, welcomePageId, "Razorpay Page", "Razorpay Page Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnOnlyText(device, successButtonId, "Success Button", "Clicked On Success Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementOnlyTextAppear(device, paymentSuccessId, "Payment Successful", "Payment Successful Appear Time:");
            if (!runTest) {
                return 1;
            }

//            runTest = clickOnText(device, scratchcardsId, "Scratch Cards", "Clicked On Scratch Cards Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, offersPageId, "Offers Page", "Offers Page Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnCollections(device, "Open Scratch Card", "Clicked On View Scratch Card Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, earnedFromId, "Earned From", "Earned From Page Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, scratchReadyId, "Scratch Ready", "Scratch Ready Card Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = swiping("Swiping", "Swiping Scratch Card Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, youWonId, "You Won", "You Won Appear Time:");
//            if (!runTest) {
//                return 1;
//            }

            kpiCalculations();

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion, startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.MPL_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean clickOnText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnOnlyText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().text(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean enterKeysText(UiDevice device, String textID, String enterKey, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    idObject.setText(enterKey);
                    device.pressEnter();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    return elementFound;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementOnlyTextAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().text(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    return elementFound;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnCollections(UiDevice device, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

//                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(3).className("android.view.ViewGroup").className("android.widget.ImageView"));
//                idObject.click();

                UiCollection collection = new UiCollection(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(3).className("android.view.ViewGroup").className("android.widget.ImageView"));
                UiObject idObject = collection.getChildByInstance(new UiSelector(), 0);

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean swiping(String elementName, String successMsg) {
        try {
            Log.d(GlobalVariables.Tag_Name, "Started Swiping");
            device.swipe(160, 450, 560, 850, 50);
            device.swipe(560, 450, 160, 850, 50);
            device.swipe(150, 630, 570, 630, 50);
            device.swipe(360, 440, 360, 840, 50);
            Timestamp timestamp = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
            kpis.put(elementName, timestamp);
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Swiping Scratch Card");
            return false;
        }
    }

    private void kpiCalculations() {
        try {

            double homeElementsAppearTime = getTime("Home Elements");
            double walletButtonTime = getTime("Wallet Button");
            double cashBalanceAppearTime = getTime("Cash Balance Appear");
            double proceedToAddCashTime = getTime("Proceed To Add Cash");
            double paymentOptionAppearTime = getTime("Payment Option");
            double chooseYourBankTime = getTime("Choose Your Bank");
            double allBanksAppearTime = getTime("All Banks");
            double selectIciciBankTime = getTime("Select ICICI Bank");
            double razorpayPageAppearTime = getTime("Razorpay Page");
            double successButtonTime = getTime("Success Button");
            double paymentSuccessfulAppearTime = getTime("Payment Successful");
//            double scratchCardsTime = getTime("Scratch Cards");
//            double offersPageAppearTime = getTime("Offers Page");
//            double openScratchCardTime = getTime("Open Scratch Card");
//            double earnedFromAppearTime = getTime("Earned From");
//            double swipingTime = getTime("Swiping");
//            double youWonAppearTime = getTime("You Won");

            double TTLH = homeElementsAppearTime - (ExampleInstrumentedTest.lt.getTime() / 1000.0);
            double TTLWP = cashBalanceAppearTime - walletButtonTime;
            double TTLPOP = paymentOptionAppearTime - proceedToAddCashTime;
            double TTLSBP = allBanksAppearTime - chooseYourBankTime;
            double TTLSB = razorpayPageAppearTime - selectIciciBankTime;
            double TTLPSP = paymentSuccessfulAppearTime - successButtonTime;
//            double TTLOP = offersPageAppearTime - scratchCardsTime;
//            double TTLSC = earnedFromAppearTime - openScratchCardTime;
//            double TTLSCRT = youWonAppearTime - swipingTime;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Wallet Page=" + TTLWP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Options Page=" + TTLPOP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Select a Bank Page=" + TTLSBP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Success Button=" + TTLSB);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Success Page=" + TTLPSP);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Offers Page=" + TTLOP);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Scratch Card=" + TTLSC);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Scratch Card Result Time=" + TTLSCRT);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
