package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;
import org.json.JSONObject;

public class Magisk {

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  private Timestamp lt, st, et, sat, vt;
  private UiDevice device;

  public static void sendDataToWebApp(String flag, String appName) {
    System.out.println("inside send data");
    File file = new File("/sdcard/QosbeeFiles");
    if (!file.exists()) {
      file.mkdir();
    }
    JSONObject Jmain = new JSONObject();
    try {
      Jmain.put("flag", flag);
      Jmain.put("appname", appName);

      File gpxfile = new File(file + "/Magisk.txt");
      FileWriter writer = new FileWriter(gpxfile);
      writer.append(Jmain.toString());
      writer.flush();
      writer.close();

    } catch (Exception e) {
      Log.e("", "" + e.getMessage());
    }
  }

  public int testRun(UiDevice device, String appName, String flagM) {
    boolean runTest = true;
    try {
      context = TestApp.getContext();

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;

      this.lt = lt;

      this.location = location;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;

      sendDataToWebApp("stop", "Magis");

      runTest = navigation();
      if (!runTest) {
        sendDataToWebApp("stop", "Magis");
        return 1;
      }

      sendDataToWebApp("stop", "Magis");
      runTest = modules();
      if (!runTest) {
        sendDataToWebApp("stop", "Magis");
        return 1;
      }

      sendDataToWebApp("stop", "Magis");
      runTest = moreOptions();
      if (!runTest) {
        sendDataToWebApp("stop", "Magis");
        return 1;
      }

      sendDataToWebApp("stop", "Magis");
      runTest = reboot();
      if (!runTest) {
        return 1;
      }

      this.appName = appName;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;
  }

  private boolean navigation() {
    try {
      boolean Navigation = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Navigation button with Attempt no." + (i + 1));

          UiSelector browseID = new UiSelector().descriptionContains("Navigate up");
          UiObject browse = device.findObject(browseID);
          if (browse.waitForExists(GlobalVariables.Wait_Timeout)) {
            browse.click();
            Navigation = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Navigation Button");
        }
      }
      return Navigation;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Navigation Button Method");
      return false;
    }
  }

  private boolean modules() {
    try {
      boolean Modules = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Modules button with Attempt no." + (i + 1));

          UiSelector browseID = new UiSelector().textContains("Modules");
          UiObject browse = device.findObject(browseID);
          if (browse.waitForExists(GlobalVariables.Wait_Timeout)) {
            browse.click();
            Modules = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Modules Button");
        }
      }
      return Modules;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Modules Button Method");
      return false;
    }
  }

  private boolean moreOptions() {
    try {
      boolean MoreOptions = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on MoreOptions button with Attempt no." + (i + 1));

          UiSelector browseID = new UiSelector().descriptionContains("More options");
          UiObject browse = device.findObject(browseID);
          if (browse.waitForExists(GlobalVariables.Wait_Timeout)) {
            browse.click();
            MoreOptions = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in MoreOptions Button");
        }
      }
      return MoreOptions;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in MoreOptions Button Method");
      return false;
    }
  }

  private boolean reboot() {
    try {
      boolean Reboot = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Reboot button with Attempt no." + (i + 1));

          UiSelector browseID = new UiSelector().textContains("Reboot");
          UiObject browse = device.findObject(browseID);
          if (browse.waitForExists(GlobalVariables.Wait_Timeout)) {
            browse.click();
            Reboot = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Reboot Button");
        }
      }
      return Reboot;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Reboot Button Method");
      return false;
    }
  }
}
