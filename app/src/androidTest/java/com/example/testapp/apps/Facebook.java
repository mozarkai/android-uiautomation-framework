package com.example.testapp.apps;

import android.content.Context;
import android.graphics.Rect;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Facebook implements AppClass {

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }

            runTest = watchVideos();
            if (!runTest) {
                runTest = searchAppear();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
                    return 1;
                }

                runTest = enterSearch();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                    return 1;
                }

                runTest = searchResults1();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Search Results");
                    return 1;
                }

                runTest = seeAll();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find See All Button");
                    return 1;
                }

                runTest = videoAppear1();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Videos");
                    return 1;
                }

                runTest = liveButton();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Live Button");
                    return 1;
                }

                runTest = liveVideoAppear1();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Live Videos");
                    return 1;
                }

                runTest = play2();
                if (!runTest) {
                    runTest = play1();
                    if (!runTest) {
                        runTest = play3();
                        if (!runTest) {
                            runTest = play4();
                            if (!runTest) {
                                DataHolder.getInstance().setFailureReason("Unable To Find Play Button");
                                return 1;
                            }
                        }
                    }
                }

                runTest = videoStartedWithText();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Start Video");
                    return 1;
                }

                runTest = bufferRead();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Play Video");
                    return 1;
                }
            } else {
                runTest = videoAppear();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Videos");
                    return 1;
                }

                runTest = searchAppear();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
                    return 1;
                }

                runTest = enterSearch();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                    return 1;
                }

                runTest = searchResults();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Search Results");
                    return 1;
                }

                runTest = liveButton();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Live Button");
                    return 1;
                }

                runTest = liveVideoAppear();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Find Live Videos");
                    return 1;
                }

                runTest = play1();
                if (!runTest) {
                    runTest = play2();
                    if (!runTest) {
                        runTest = play3();
                        if (!runTest) {
                            runTest = play4();
                            if (!runTest) {
                                DataHolder.getInstance().setFailureReason("Unable To Find Play Button");
                                return 1;
                            }
                        }
                    }
                }

                runTest = videoStartedWithText();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Start Video");
                    return 1;
                }

                runTest = bufferRead();
                if (!runTest) {
                    DataHolder.getInstance().setFailureReason("Unable To Play Video");
                    return 1;
                }
            }

            stopScreenRecording();
            runTest = kpiCalculation();
            if (!runTest) {
                return 1;
            }

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Facebook_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector homeElementId =
                        new UiSelector().descriptionContains("Photo");
                UiObject homeElement = device.findObject(homeElementId);
                if (homeElement.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return homeElementFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return false;
        }
    }

    private boolean watchVideos() {
        try {
            boolean watchVideoFound = false;
            for (int i = 0; i < 2; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Watch Videos button with Attempt no." + (i + 1));
                    UiSelector watchVideoButtonId = new UiSelector().descriptionContains("Watch, tab");
                    UiObject watchVideoButton = device.findObject(watchVideoButtonId);

                    if (watchVideoButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        watchVideoButton.click();
                        wvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Watched Videos Time:" + wvt);
                        watchVideoFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Watch Videos");
                }
            }
            return watchVideoFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Watch Videos Method");
            return false;
        }
    }

    private boolean videoAppear() {
        boolean videoAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector videosId =
                        new UiSelector().descriptionContains("More options");
                UiObject videoAppear = device.findObject(videosId);
                if (videoAppear.exists()) {
                    vat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Videos Appear Time:" + vat);
                    videoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Videos");
                    Thread.sleep(200);
                }
            }
            return videoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Videos");
            return false;
        }
    }


    private boolean searchAppear() {
        try {
            boolean searchButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

                    UiSelector searchId = new UiSelector().descriptionContains("Search");
                    UiObject searchAppear = device.findObject(searchId);

                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
                        searchAppear.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
                        searchButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in search Appear");
                }
            }
            return searchButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
            return false;
        }
    }

    private boolean enterSearch() {
        try {
            boolean enterSearchtext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));
                    UiSelector enterSearchField =
                            new UiSelector()
                                    .textContains("Search");
                    UiObject enterSearch_text = device.findObject(enterSearchField);

                    if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterSearch_text.click();
                        enterSearch_text.setText("live videos \n");
//                        enterSearch_text.setText("live video \n");
                        device.pressEnter();
                        est = new Timestamp(new Date().getTime());
                        Log.d(
                                GlobalVariables.Tag_Name,
                                GlobalVariables.Enter_Search_Time + est);
                        enterSearchtext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
                }
            }
            return enterSearchtext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
    }

    private boolean searchResults() {
        boolean value = false;
        try {
            UiSelector searchResultsID =
                    new UiSelector().descriptionContains("Play current video");
            UiObject searchResults = device.findObject(searchResultsID);

            UiSelector searchResultsID1 =
                    new UiSelector().descriptionContains("views");
            UiObject searchResults1 = device.findObject(searchResultsID1);

            UiSelector searchResultsID2 = new UiSelector().descriptionContains("SORT BY");
            UiObject searchResults2 = device.findObject(searchResultsID2);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (searchResults.exists() || searchResults1.exists() || searchResults2.exists()) {
                    sat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
                    value = true;
                    break;
                } else {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Loading Search Results");
                    Thread.sleep(200);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return value;
        }
    }

    private boolean liveButton() {
        try {
            boolean liveButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Live button with Attempt no." + (i + 1));
                    UiSelector liveButtonId = new UiSelector().description("LIVE");
                    UiObject liveButton = device.findObject(liveButtonId);

                    if (liveButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        liveButton.click();
                        lbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Live Button Time:" + lbt);
                        liveButtonFound = true;
                        break;
                    } else {
                        UiScrollable scroll = new UiScrollable(new UiSelector().className("androidx.recyclerview.widget.RecyclerView"));
                        scroll.setAsHorizontalList().scrollDescriptionIntoView("LIVE");
                        liveButton.click();
                        lbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Live Button Time:" + lbt);
                        liveButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Live Button");
                }
            }
            return liveButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Live Button Method");
            return false;
        }
    }

    private boolean liveVideoAppear() {
        boolean liveVideoAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector liveVideosId =
                        new UiSelector().descriptionContains("Play current video");
                UiObject liveVideoAppear = device.findObject(liveVideosId);

                UiSelector liveVideosId1 =
                        new UiSelector().descriptionContains("Video");
                UiObject liveVideoAppear1 = device.findObject(liveVideosId1);

                if (liveVideoAppear1.exists() || liveVideoAppear.exists()) {
                    lvat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Live Videos Appear Time:" + lvat);
                    liveVideoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Live Videos");
                    Thread.sleep(200);
                }
            }
            return liveVideoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Live Videos");
            return false;
        }
    }

    private boolean seeAll() {
        try {
            boolean seeAllButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find See All button with Attempt no." + (i + 1));

                    UiSelector seeAllID = new UiSelector().descriptionContains("See All");
                    UiObject seeAll = device.findObject(seeAllID);

                    if (seeAll.waitForExists(GlobalVariables.Wait_Timeout)) {
                        seeAll.click();
                        wvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on See All Button Time:" + wvt);
                        seeAllButtonFound = true;
                        break;
                    }
                    else {
//                        UiScrollable scroll = new UiScrollable(
//                                new UiSelector().className("androidx.recyclerview.widget.RecyclerView"));
//                        scroll.setAsVerticalList();
//                        scroll.scrollDescriptionIntoView("See All");

//                        UiScrollable scroll = new UiScrollable(new UiSelector().className("android.widget.FrameLayout").instance(0).className("android.widget.LinearLayout").index(0).className("android.widget.LinearLayout").className("android.widget.FrameLayout").className("android.view.ViewGroup").className("android.view.ViewGroup").className("androidx.recyclerview.widget.RecyclerView").index(1));
                        UiScrollable scroll = new UiScrollable(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout").className("android.widget.FrameLayout").className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout").className("android.view.ViewGroup").className("android.view.ViewGroup").className("androidx.recyclerview.widget.RecyclerView").instance(1));
                        scroll.setAsVerticalList();
                        scroll.scrollDescriptionIntoView("See All");

                        if (seeAll.waitForExists(GlobalVariables.Wait_Timeout)) {
                            seeAll.click();
                            wvt = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name, "Clicked on See All Button Time:" + wvt);
                            seeAllButtonFound = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in See All");
                }
            }
            return seeAllButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding See All Button Method");
            return false;
        }
    }

    private boolean searchResults1() {
        boolean value = false;
        try {
            UiSelector searchResultsID =
                    new UiSelector().descriptionContains("See All");
            UiObject searchResults = device.findObject(searchResultsID);

            UiSelector searchResultsID1 =
                    new UiSelector().descriptionContains("All search results");
            UiObject searchResults1 = device.findObject(searchResultsID1);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (searchResults.exists() || searchResults1.exists()) {
                    sat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results1");
                    Thread.sleep(200);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results1");
            return value;
        }
    }

    private boolean videoAppear1() {
        boolean videoAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {

                UiSelector videosId1 =
                        new UiSelector().descriptionContains("watching");
                UiObject videoAppear1 = device.findObject(videosId1);

                UiSelector videosId2 =
                        new UiSelector().descriptionContains("views");
                UiObject videoAppear2 = device.findObject(videosId2);

                UiSelector videosId3 =
                        new UiSelector().descriptionContains("ago");
                UiObject videoAppear3 = device.findObject(videosId3);

                if (videoAppear1.exists() || videoAppear2.exists() || videoAppear3.exists()) {
                    vat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Videos Appear Time:" + vat);
                    videoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Videos1");
                    Thread.sleep(200);
                }
            }
            return videoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Videos1");
            return false;
        }
    }

    private boolean liveVideoAppear1() {
        boolean liveVideoAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector liveVideosId =
                        new UiSelector().descriptionContains("watching");
                UiObject liveVideoAppear = device.findObject(liveVideosId);

                UiSelector liveVideosId1 =
                        new UiSelector().descriptionContains("Video");
                UiObject liveVideoAppear1 = device.findObject(liveVideosId1);

                if (liveVideoAppear1.exists() || liveVideoAppear.exists()) {
                    lvat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Live Videos Appear Time:" + lvat);
                    liveVideoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Live Videos1");
                    Thread.sleep(200);
                }
            }
            return liveVideoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Live Videos1");
            return liveVideoAppearFound;
        }
    }

    private boolean play() {
        boolean playBtnFound = false;
        try {
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
                try {

                    UiSelector playId1 =
                            new UiSelector()
                                    .descriptionContains("Video");
                    UiObject playBtn1 = device.findObject(playId1);

                    Rect playBtn1VisibleBounds = playBtn1.getVisibleBounds();

                    UiSelector playId2 =
                            new UiSelector()
                                    .descriptionContains("Play current video");
                    UiObject playBtn2 = device.findObject(playId2);

                    Rect playBtn2VisibleBounds = playBtn2.getVisibleBounds();
                    UiSelector playId3 =
                            new UiSelector()
                                    .descriptionContains("watching");
                    UiObject playBtn3 = device.findObject(playId3);

                    Rect playBtn3VisibleBounds = playBtn3.getVisibleBounds();

                    // 417,364

                    if (playBtn1.waitForExists(GlobalVariables.Wait_Timeout)) {
//                        playBtn2.click();
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn1VisibleBounds.centerX() + " Value of Y is :" + playBtn1VisibleBounds.centerY());
                        device.click(playBtn1VisibleBounds.centerX(), playBtn1VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 1st play:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }

                    else if (playBtn2.waitForExists(GlobalVariables.Wait_Timeout)) {
                        Log.d(GlobalVariables.Tag_Name,"Checking Play Button with Second Condition");
//                        playBtn2.click();
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn2VisibleBounds.centerX() + " Value of Y is :" + playBtn2VisibleBounds.centerY());
                        device.click(playBtn2VisibleBounds.centerX(), playBtn2VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 2nd play:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }
                    else {
//                        playBtn.click();
                        Log.d(GlobalVariables.Tag_Name,"Checking Play Button with Third Condition");
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn3VisibleBounds.centerX() + " Value of Y is :" + playBtn3VisibleBounds.centerY());
                        device.click(playBtn3VisibleBounds.centerX(), playBtn3VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 3rd play:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                    e.printStackTrace();
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return playBtnFound;
        }
    }

    private boolean play1() {
        boolean playBtnFound = false;
        try {
            for (int i = 0; i < 3; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to play1 with Attempt no.:" + (i + 1));
                try {

                    UiSelector playId1 =
                            new UiSelector()
                                    .descriptionContains("Video");
                    UiObject playBtn1 = device.findObject(playId1);

                    Rect playBtn1VisibleBounds = playBtn1.getVisibleBounds();

                    if (playBtn1.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn1.click();
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 1st play1:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }

                    else {
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn1VisibleBounds.centerX() + " Value of Y is :" + playBtn1VisibleBounds.centerY());
                        device.click(playBtn1VisibleBounds.centerX(), playBtn1VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 1st play2:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                    e.printStackTrace();
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return playBtnFound;
        }
    }

    private boolean play2() {
        boolean playBtnFound = false;
        try {
            for (int i = 0; i < 3; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to play2 with Attempt no.:" + (i + 1));
                try {

                    UiSelector playId2 =
                            new UiSelector()
                                    .descriptionContains("watching");
                    UiObject playBtn2 = device.findObject(playId2);

                    Rect playBtn2VisibleBounds = playBtn2.getVisibleBounds();
                    if(playBtn2.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn2.click();
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn2VisibleBounds.centerX() + " Value of Y is :" + playBtn2VisibleBounds.centerY());
                        device.click(playBtn2VisibleBounds.centerX(), playBtn2VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 2nd play1:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }
                    else {
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn2VisibleBounds.centerX() + " Value of Y is :" + playBtn2VisibleBounds.centerY());
                        device.click(playBtn2VisibleBounds.centerX(), playBtn2VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 2nd play2:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                    e.printStackTrace();
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return playBtnFound;
        }
    }

    private boolean play3() {
        boolean playBtnFound = false;
        try {
            for (int i = 0; i < 3; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to play3 with Attempt no.:" + (i + 1));
                try {

                    UiSelector playId3 =
                            new UiSelector()
                                    .descriptionContains("Play current video");
                    UiObject playBtn3 = device.findObject(playId3);

                    Rect playBtn3VisibleBounds = playBtn3.getVisibleBounds();

                    if (playBtn3.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn3.click();
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn3VisibleBounds.centerX() + " Value of Y is :" + playBtn3VisibleBounds.centerY());
                        device.click(playBtn3VisibleBounds.centerX(), playBtn3VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 3rd play1:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }

                    else  {
                        Log.d(GlobalVariables.Tag_Name,"Checking Play Button with Second Condition");
                        Log.d(GlobalVariables.Tag_Name, "Value of X is :" + playBtn3VisibleBounds.centerX() + " Value of Y is :" + playBtn3VisibleBounds.centerY());
                        device.click(playBtn3VisibleBounds.centerX(), playBtn3VisibleBounds.centerY());
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 3rd play2:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                    e.printStackTrace();
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return playBtnFound;
        }
    }

    private boolean play4() {
        boolean playBtnFound = false;
                Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (1));
                try {
                        device.click(350,550);
                        pvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click on 4th play:" + pvt);
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + pvt);
                        playBtnFound = true;
                        return playBtnFound;

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                    e.printStackTrace();
                    return playBtnFound;
                }
    }

    private boolean videoStartedWithText() {

        boolean value = false;
        try {
            UiSelector videoStartedId = new UiSelector().text("LIVE");
            UiObject videoStarted = device.findObject(videoStartedId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (videoStarted.exists()) {
                    vst = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Video_Start_Time + vst);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Video is Loading");
                    device.click(150,150);
                    Thread.sleep(200);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Video Started");
            return value;
        }
    }

    private boolean bufferRead() {
        try {

            UiSelector loaderId =
                    new UiSelector().textContains("Couldn't play video");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
                    Thread.sleep(200);
                }

                time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
                if (time > c) {
                    Utility.cpuUsage("com.facebook", OUTPUT_LOG_FILE_TEMP);
                    c += GlobalVariables.Delay_Time;
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean kpiCalculation() {
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double TTLV = (vat.getTime() - wvt.getTime()) / 1000.0;
            double SRT = (sat.getTime() - est.getTime()) / 1000.0;
            double TTLLV = (lvat.getTime() - lbt.getTime()) / 1000.0;
            double PST = (vst.getTime() - pvt.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Video Page=" + TTLV);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Live Video Page=" + TTLLV);
            Log.d(GlobalVariables.Tag_Name, "Play Start Time=" + PST);

           
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}