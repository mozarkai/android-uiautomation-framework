package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Zoom_180 implements AppClass {

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    String OUTPUT_LOG_FILE = "";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    private UiDevice device;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            Utility.permission(GlobalVariables.ZoomPackage);

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                Log.d("Aquamark", "");
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);
            context = TestApp.getContext();

            runTest = continueClickSplash();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Continue Button");
                return 1;
            }

            runTest = homepage();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }

            runTest = joinMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Join Meeting Button");
                return 1;
            }

            RecordScreen recordScreen = new RecordScreen();
            recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000,vCap);

            runTest = enterMeetingId();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Meeting ID");
                return 1;
            }

            runTest = searchGo();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Go Button");
                return 1;
            }

            runTest = startMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Start Meeting Button");
                return 1;
            }

            Thread.sleep(2000);
            runTest = enterPassword();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Meeting Password");
                return 1;
            }

            runTest = clickOk();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find OK Button");
                return 1;
            }

            runTest = loaderAppear(vCap);
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Join Meeting");
                return 1;
            }

            runTest = endMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find End Meeting Button");
                return 1;
            }

            runTest = confirmEndMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Confirm End Meeting Button");
                return 1;
            }

            KpiLogs.twoKPI(lt, st, vt);

            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private boolean homepage() {
        try {
            boolean Homepage = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find continue button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector searchId = new UiSelector().resourceId("us.zoom.videomeetings:id/btnStart");
                    UiObject searchAppear = device.findObject(searchId);
                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);

                        Homepage = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Homepage Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return Homepage;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Homepage Button Method");
            return false;
        }
    }

    private boolean joinMeeting() {
        try {
            boolean joinmeeting = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find JoinMeeting button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector joinmeet = new UiSelector().resourceId("us.zoom.videomeetings:id/btnJoin");
                    UiObject joinButton = device.findObject(joinmeet);
                    if (joinButton.waitForExists(GlobalVariables.Wait_Timeout) && joinButton.isEnabled()) {
                        joinButton.click();

                        joinmeeting = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "JoinMeeting Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return joinmeeting;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding JoinMeeting Button Method");
            return false;
        }
    }

    private boolean continueClickSplash() {
        try {
            boolean clickContinue = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find continue button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector searchId =
                            new UiSelector()
                                    .resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn");
                    UiObject searchAppear = device.findObject(searchId);
                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
                        searchAppear.click();

                        clickContinue = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "continue Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return clickContinue;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding continue Button Method");
            return false;
        }
    }

    private boolean enterMeetingId() {
        try {
            boolean enterMeeting = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find enterMeeting text with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId =
                            new UiSelector().resourceId("us.zoom.videomeetings:id/edtConfNumber");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterMeetingId.click();
//                        enterMeetingId.setText("971 8885 2595");
                        enterMeetingId.setText("939 469 9386");
                        enterMeeting = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "enterMeeting text Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return enterMeeting;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding enterMeeting Method");
            return false;
        }
    }

    private boolean searchGo() {
        try {
            boolean searchGo = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to remove the keyboard  with Attempt no." + (i + 1));

                    device.pressBack();
                    device.removeWatcher("GO");
                    searchGo = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "keyboard Remove Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return searchGo;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding keyboard Remove Method");
            return false;
        }
    }

    private boolean startMeeting() {
        try {
            boolean enterMeeting = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find StartMeeting button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId = new UiSelector().resourceId("us.zoom.videomeetings:id/btnJoin");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
                            && enterMeetingId.isClickable()) {
                        enterMeetingId.click();
                        //                        vt = new Timestamp(new Date().getTime());
                        //                        Log.d(GlobalVariables.Tag_Name,
                        // GlobalVariables.JoinMeetingClickTime + vt);
                        enterMeeting = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "StartMeeting  Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return enterMeeting;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
            return false;
        }
    }

    private boolean enterPassword() {
        try {
            boolean enterMeeting = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find password text with Attempt no." + (i + 1));

                    //                    UiCollection searchID = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/edtPassword"));
                    //                    UiObject enterSearch_text = searchID.getChildByInstance(new
                    // UiSelector(), 1);

                    //  us.zoom.videomeetings:id/button1
                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId =
                            new UiSelector().resourceId("us.zoom.videomeetings:id/edtPassword");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    enterMeetingId.click();
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterMeetingId.click();
//                        enterMeetingId.setText("8Qku8n");
                        enterMeetingId.setText("Mozark");

                        enterMeeting = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "password text Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return enterMeeting;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding enterMeeting Method");
            return false;
        }
    }

    private boolean clickOk() {
        try {
            boolean enterMeeting = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Click ok button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId = new UiSelector().resourceId("us.zoom.videomeetings:id/button1");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
                            && enterMeetingId.isClickable()) {
                        enterMeetingId.click();
                        vt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
                        enterMeeting = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Click ok  Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return enterMeeting;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
            return false;
        }
    }

    private boolean loaderAppear(boolean vCap) {
        try {

            RecordScreen recordScreen = new RecordScreen();
            recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_TimeMid / 1000, vCap);

            UiSelector loaderId = new UiSelector().resourceId("us.zoom.videomeetings:id/txtConnecting");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_TimeMid) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
                }

                time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
                if (time > c) {
                    Utility.cpuUsage("us.zoom", OUTPUT_LOG_FILE_TEMP);
                    c += GlobalVariables.Delay_Time;
                }
            }

            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean endMeeting() {
        try {
            boolean endMeet = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find EndMeeting button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId = new UiSelector().resourceId("us.zoom.videomeetings:id/btnLeave");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
                            && enterMeetingId.isClickable()) {
                        enterMeetingId.click();

                        endMeet = true;
                        break;
                    }
                    else {
                        device.click(300, 700);
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "End Meeting Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return endMeet;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding End Meeting  Method");
            return false;
        }
    }

    private boolean confirmEndMeeting() {
        try {
            boolean confirmEnd = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find ConfirmEndMeeting button with Attempt no." + (i + 1));

                    //  UiCollection searchSelector = new UiCollection(new
                    // UiSelector().resourceId("us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn"));
                    UiSelector meetingId =
                            new UiSelector().resourceId("us.zoom.videomeetings:id/btnLeaveMeeting");
                    UiObject enterMeetingId = device.findObject(meetingId);
                    if (enterMeetingId.waitForExists(GlobalVariables.Wait_Timeout)
                            && enterMeetingId.isClickable()) {
                        enterMeetingId.click();

                        confirmEnd = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "ConfirmEndMeeting  Button Failed" + new Timestamp(new Date().getTime()));
                }
            }
            return confirmEnd;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding ConfirmEndMeeting  Method");
            return false;
        }
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.ZoomPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}

