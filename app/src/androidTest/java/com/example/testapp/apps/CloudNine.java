package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class CloudNine implements AppClass {

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String OUTPUT_LOG_FILE = "";
    String appName;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    private UiDevice device;
    private boolean vCap = false;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.vCap = vCap;
            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            context = TestApp.getContext();
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);
            runTest = Homepage();
            if (!runTest) {
                return 1;
            }

            runTest = SelectStartVideo();
            if (!runTest) {
                return 1;
            }

            runTest = SelectDocAdvice();
            if (!runTest) {
                return 1;
            }

            runTest = SelectVideoCall();
            if (!runTest) {
                return 1;
            }

            runTest = SelectVideoCall();
            if (!runTest) {
                return 1;
            }
            runTest = VideoLoader();
            if (!runTest) {
                return 1;
            }

            runTest = VideoComplete();
            if (!runTest) {
                return 1;
            }

            //            runTest = selectDoctor();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectContinue();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectVideo();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = loader();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectCalendarTime();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectContinueWith();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectCorporate();
            //            if (!runTest) {
            //                return 1;
            //            }
            //
            //            runTest = selectCorporateEmail();
            //            if (!runTest) {
            //                return 1;
            //            }

            //            runTest = selectCorporateconfirm();
            //            if (!runTest) {
            //                return 1;
            //            }

            //  KpiLogs.twoKPI(lt, st, vt);

            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.CLOUD_NINE_PACKAGE));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private boolean Homepage() {
        try {
            boolean homepage = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to clickDownload with Attempt no.:" + (i + 1));
                try {

                    UiSelector loaderId = new UiSelector().text("START VIDEO CONSULTATION");
                    UiObject loader = device.findObject(loaderId);

                    if (loader.waitForExists(GlobalVariables.Wait_Timeout)) {
                        loader.click();

                        homepage = true;
                        break;
                    } else {
                        UiSelector loadersId = new UiSelector().text("OK. GOT IT");
                        UiObject loaders = device.findObject(loadersId);

                        if (loaders.waitForExists(GlobalVariables.Wait_Timeout)) {
                            loaders.click();

                            UiSelector loaderMId = new UiSelector().text("START VIDEO CONSULTATION");
                            UiObject loaderM = device.findObject(loaderMId);

                            if (loaderM.waitForExists(GlobalVariables.Wait_Timeout)) {
                                loaderM.click();

                                homepage = true;
                                break;
                            }
                        }
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on homepage Failed");
                }
            }
            return homepage;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in homepage");
            return false;
        }
    }

    private boolean SelectStartVideo() {
        try {
            boolean selectUser = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to START VIDEO CHAT with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectId = new UiSelector().text("START VIDEO CHAT");
                    UiObject select = device.findObject(selectId);

                    if (select.waitForExists(GlobalVariables.Delay_Time)) {
                        select.click();

                        selectUser = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on START VIDEO CHAT Failed");
                }
            }
            return selectUser;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in START VIDEO CHAT");
            return false;
        }
    }

    private boolean SelectDocAdvice() {
        try {
            boolean selectcity = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Video Call with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectCityId = new UiSelector().text("Doc's Advice");
                    UiObject select = device.findObject(selectCityId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();

                        selectcity = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on Video Call Failed");
                }
            }
            return selectcity;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Video Call");
            return false;
        }
    }

    private boolean SelectVideoCall() {
        try {
            boolean selectcity = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Video Call with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectCityId = new UiSelector().text("Video Call");
                    UiObject select = device.findObject(selectCityId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();

                        selectcity = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on Video Call Failed");
                }
            }
            return selectcity;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Video Call");
            return false;
        }
    }

    private boolean VideoLoader() {
        try {
            RecordScreen recordScreen = new RecordScreen();
            recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {

                Log.d(GlobalVariables.Tag_Name, "The Meeting has started Successfully");
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean VideoComplete() {
        try {
            boolean selectType = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Disconnect with Attempt no.:" + (i + 1));
                try {

                    UiSelector selecttypeId = new UiSelector().text("Disconnect");
                    UiObject select = device.findObject(selecttypeId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();
                        Thread.sleep(2000);
                        selectType = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on Disconnect Failed");
                }
            }
            return selectType;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Disconnect");
            return false;
        }
    }

    private boolean selectDoctor() {
        try {
            boolean selectDoctor = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectDoctor with Attempt no.:" + (i + 1));
                try {

                    device.swipe(600, 900, 0, 100, 20);
                    device.swipe(600, 900, 0, 100, 20);
                    device.swipe(600, 900, 0, 100, 20);

                    UiSelector selectDocId = new UiSelector().text("Testobg Oar");
                    UiObject select = device.findObject(selectDocId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();

                        selectDoctor = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectDoctor Failed");
                }
            }
            return selectDoctor;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectDoctor");
            return false;
        }
    }

    private boolean selectContinue() {
        try {
            boolean selectContinue = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectContinue with Attempt no.:" + (i + 1));
                try {

                    //   device.swipe(200, 1200, 1300, 1200, 20);
                    UiSelector selectconId = new UiSelector().text("CONTINUE");
                    UiObject select = device.findObject(selectconId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();

                        selectContinue = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectContinue Failed");
                }
            }
            return selectContinue;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectContinue");
            return false;
        }
    }

    private boolean selectVideo() {
        try {
            boolean selectVideo = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectVideo with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectvideoId = new UiSelector().text("VIDEO/ TELE CONSULT");
                    UiObject select = device.findObject(selectvideoId);

                    if (select.waitForExists(GlobalVariables.Wait_Timeout)) {
                        select.click();

                        selectVideo = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectVideo Failed");
                }
            }
            return selectVideo;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectVideo");
            return false;
        }
    }

    private boolean loader() {
        try {
            boolean loader_appear = false;

            try {
                //
                //                    UiSelector selectvideoId = new
                // UiSelector().descriptionContains("ButtonText");
                //                    UiObject loader = device.findObject(selectvideoId);

                UiCollection playBtnContainerID =
                        new UiCollection(new UiSelector().descriptionContains("ButtonText"));

                UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 0);

                StopWatch stopWatch = new StopWatch();
                stopWatch.start();

                int time = 0;
                int c = GlobalVariables.Delay_Time;

                while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                    if (playBtn.exists()) {
                        Timestamp loaderYes = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloading + loaderYes);
                        device.click(569, 1110);

                        loader_appear = true;
                        break;
                    } else {
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloading + "hello");
                    }
                }

            } catch (Exception e) {

            }
            return loader_appear;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in loader");
            return false;
        }
    }

    private boolean selectCalendarTime() {
        try {
            boolean selectCalendar = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to selectCalendar with Attempt no.:" + (i + 1));
                try {

                    device.click(691, 391);
                    Thread.sleep(7000);

                    selectCalendar = true;
                    break;

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on selectCalendar Failed");
                }
            }
            return selectCalendar;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectCalendar");
            return false;
        }
    }

    private boolean selectContinueWith() {
        try {
            boolean continuewith = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to continuewith with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectvideoId = new UiSelector().text("CONITNUE WITH");
                    UiObject select = device.findObject(selectvideoId);
                    Thread.sleep(2000);
                    device.click(640, 1423);

                    continuewith = true;
                    break;

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on continuewith Failed");
                }
            }
            return continuewith;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in continuewith");
            return false;
        }
    }

    private boolean selectCorporate() {
        try {
            boolean corporate = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to corporate with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectcorporateId = new UiSelector().text("CORPORATE CUSTOMER?");
                    UiObject selectCorporate = device.findObject(selectcorporateId);

                    if (selectCorporate.waitForExists(GlobalVariables.Wait_Timeout)) {
                        selectCorporate.click();

                        corporate = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on corporate Failed");
                }
            }
            return corporate;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in corporate");
            return false;
        }
    }

    private boolean selectCorporateEmail() {
        try {
            boolean corporate = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to corporate with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectcorporateId = new UiSelector().descriptionContains("InputBox");
                    UiObject selectCorporate = device.findObject(selectcorporateId);

                    if (selectCorporate.waitForExists(GlobalVariables.Wait_Timeout)) {
                        selectCorporate.click();
                        selectCorporate.setText("testobg@cloudninecare.com");
                        corporate = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on corporate Failed");
                }
            }
            return corporate;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in corporate");
            return false;
        }
    }

    private boolean selectCorporateconfirm() {
        try {
            boolean corporate = false;

            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to corporate with Attempt no.:" + (i + 1));
                try {

                    UiSelector selectcorporateId = new UiSelector().descriptionContains("Forward");
                    UiObject selectCorporate = device.findObject(selectcorporateId);

                    if (selectCorporate.waitForExists(GlobalVariables.Wait_Timeout)) {
                        selectCorporate.click();
                        corporate = true;
                        break;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on corporate Failed");
                }
            }
            return corporate;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in corporate");
            return false;
        }
    }
}
