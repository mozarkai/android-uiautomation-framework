package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Myntra implements AppClass {

    private Timestamp DeliveryTime,
            vt,
            et,
            ht,
            it,
            het,
            sat,
            ClickCartTime,
            TimeToLoad,
            ClickCheckoutTime,
            CheckoutAppearTime;

    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, heat, sqct, mdat, pbt, psat, bayt, bant, enterUPI_Balance, enterUPI_Payment;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }


            runTest = clickOnSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Search  Element");
                return 1;
            }
            runTest = clickOnEnterSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Search Element");
                return 1;
            }
            runTest = selectProduct();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find select Product Element");
                return 1;
            }
            runTest = clickOnAddToBag();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Add To Bag Element");
                return 1;
            }
            runTest = clickOnViewBag();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find View Bag Element");
                return 1;
            }
            runTest = clickOnProceedToBuy();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Proceed To Buy Element");
                return 1;
            }
            runTest = clickOnContinue();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Continue Element");
                return 1;
            }
            runTest = paymentDetailsAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Payment Details Element");
                return 1;
            }

//            runTest = clickOnBack();
//            if (!runTest) {
//                DataHolder.getInstance().setFailureReason("Unable To Find Back Element");
//                return 1;
//            }
//            runTest = clickOnBack();
//            if (!runTest) {
//                DataHolder.getInstance().setFailureReason("Unable To Find Back Element");
//                return 1;
//            }
//            runTest = clickOnRemove();
//            if (!runTest) {
//                DataHolder.getInstance().setFailureReason("Unable To Find Remove Element");
//                return 1;
//            }
            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check KPI's");
                return 1;
            }
            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Myntra_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private boolean homeElement() {
        boolean value = false;
        try {

            UiSelector selector =
                    new UiSelector().descriptionContains("search");
            UiObject object = device.findObject(selector);

            UiSelector selector1 =
                    new UiSelector().className("android.widget.ProgressBar");
            UiObject object1 = device.findObject(selector1);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                heat = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + heat);
                value = true;
            }

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (object1.exists()) {
                    Log.d(GlobalVariables.Tag_Name, " Progress Bar  Appear Time:" + new Timestamp(new Date().getTime()));
                } else {
                    if (object.exists()) {
                        heat = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + heat);
                        value = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on home Element   Failed");
        }
        return value;
    }

    private boolean selectCatagory() {
        boolean value = false;
        try {

            UiCollection elementID1 =
                    new UiCollection(new UiSelector().resourceId("com.fsn.nds:id/recyclerView"));
            UiObject element1 = elementID1.getChildByInstance(new UiSelector(), 10);

            if (element1.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element1.click();
                value = true;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on select Catagory Element Failed");
        }
        return value;
    }


    public boolean clickOnSearch() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().descriptionContains("search");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Search Button");
        }
        return value;
    }

    public boolean clickOnEnterSearch() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().descriptionContains("search_default_search_text_input");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                object.setText("Wallets For men");
                et = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Product_Enter_Search_Time + et);
                device.pressEnter();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Enter Seacrh Button");
        }
        return value;
    }


    private boolean selectProduct() {
        boolean value = false;
        try {

//            UiCollection elementID1 =
//                    new UiCollection(new UiSelector().descriptionContains("touchable_image_container"));
//
//            UiObject element1 = elementID1.getChildByInstance(new UiSelector(), 1);

            UiSelector selector =
                    new UiSelector().descriptionContains("touchable_image_container");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                sat = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Product_Search_Result_Time + sat);
                object.click();
                value = true;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on select Product Failed");
        }
        return value;
    }

    public boolean clickOnAddToBag() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().descriptionContains("buy_button");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
               object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Add To Bag Button");
        }
        return value;
    }

    public boolean clickOnViewBag() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().descriptionContains("bag");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                ClickCartTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Click_On_Cart_Time + ClickCartTime);
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on View Bag Button");
        }
        return value;
    }
    public boolean clickOnProceedToBuy() {
        boolean value = false;
        try {
//            UiSelector selector =
//                    new UiSelector().resourceId("placeOrderButton");
//            UiObject object = device.findObject(selector);
//            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
//                TimeToLoad = new Timestamp(new Date().getTime());
//                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Cart_items_appear_time + TimeToLoad);
//                object.click();
//                value = true;
//            }
            UiSelector selector = new UiSelector().resourceId("placeOrderButton");
            UiObject object = device.findObject(selector);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (object.exists()) {
                    TimeToLoad = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Cart_items_appear_time + TimeToLoad);
                    object.click();
                    value= true;
                    break;
                } else {
                    UiScrollable scroll = new UiScrollable(
                            new UiSelector().className("android.webkit.WebView"));
                    scroll.scrollTextIntoView("PLACE ORDER");
                    if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {

                    }
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Proceed To Buy Button");
        }
        return value;
    }
    public boolean clickOnContinue() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().resourceId("placeOrderButton");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                ClickCheckoutTime = new Timestamp(new Date().getTime());
                Log.d(
                        GlobalVariables.Tag_Name,
                        GlobalVariables.Click_On_CheckOut_time + ClickCheckoutTime);
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Proceed To Buy Button");
        }
        return value;
    }

    private boolean paymentDetailsAppear() {
        boolean value = false;
        try {

            UiSelector selector = new UiSelector().textContains("Total Amount");
            UiObject object = device.findObject(selector);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (object.exists()) {
                    CheckoutAppearTime = new Timestamp(new Date().getTime());
                    Log.d(
                            GlobalVariables.Tag_Name,
                            GlobalVariables.Checkout_page_appear_time + CheckoutAppearTime);
                    value= true;
                    break;
                } else {
                    UiScrollable scroll = new UiScrollable(
                            new UiSelector().className("android.webkit.WebView"));
                    scroll.scrollTextIntoView("Total Amount");
                    if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                        CheckoutAppearTime = new Timestamp(new Date().getTime());
                        Log.d(
                                GlobalVariables.Tag_Name,
                                GlobalVariables.Checkout_page_appear_time + CheckoutAppearTime);
                        value= true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Payment Details");
        }
        return value;
    }

    public boolean clickOnBack() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().descriptionContains("Navigate up");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking On Back  Button");
        }
        return value;
    }

    public boolean clickOnRemove() {
        boolean value = false;
        try {
            UiSelector selector =
                    new UiSelector().resourceId("com.fsn.nds:id/btn_remove");
            UiObject object = device.findObject(selector);
            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking On Remove Button");
        }
        return value;
    }
    private boolean kpiCalculation() {
        try {

            double TTLH = (heat.getTime() - lt.getTime()) / 1000.0;
            double SRT = (sat.getTime() - et.getTime()) / 1000.0;
            double TLC = (TimeToLoad.getTime() - ClickCartTime.getTime()) / 1000.0;
            double Payment = (CheckoutAppearTime.getTime() - ClickCheckoutTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Cart=" + TLC);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Page=" + Payment);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }


}
