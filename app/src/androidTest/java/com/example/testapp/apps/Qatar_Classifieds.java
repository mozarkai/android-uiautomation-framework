package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Qatar_Classifieds implements AppClass {

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  File file, file1;
  String  startDate;
  String OUTPUT_LOG_FILE = "";
  String appName;
  private Timestamp lt,
      st,
      DeliveryTime,
      vt,
      et,
      ht,
      it,
      het,
      sat,
      ClickCartTime,
      TimeToLoad,
      ClickCheckoutTime,
      CheckoutAppearTime;
  private UiDevice device;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements1();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements2();
      if (!runTest) {
        return 1;
      }

      runTest = menuButton();
      if (!runTest) {
        return 1;
      }

      runTest = browseButton();
      if (!runTest) {
        return 1;
      }

      runTest = selectVehicles();
      if (!runTest) {
        return 1;
      }

      runTest = selectCar();
      if (!runTest) {
        return 1;
      }

      runTest = clickSearch();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults1();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults2();
      if (!runTest) {
        return 1;
      }

      runTest = selectItem();
      if (!runTest) {
        return 1;
      }

      runTest = progressBar1();
      if (!runTest) {
        return 1;
      }

      runTest = firstAdAppear();
      if (!runTest) {
        return 1;
      }
      runTest = progressBar2();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.qatarClassifiedsLogs(lt, het, et, sat, ht, st);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Qatar_Classifieds_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean homeElements() {
    try {
      boolean homeElements = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Home Elements with Attempt no." + (i + 1));

          UiSelector deliveryId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/ad_image_drawee");
          UiObject deliveryAppear = device.findObject(deliveryId);
          if (deliveryAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            Log.d(
                GlobalVariables.Tag_Name,
                "Home Elements Image View Appear Time:" + new Timestamp(new Date().getTime()));
            homeElements = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Home Elements");
        }
      }
      return homeElements;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Elements Method");
      return false;
    }
  }

  private boolean homeElements1() {
    try {

      Thread.sleep(1000);
      UiSelector loaderId =
          new UiSelector().resourceId("com.qatarliving.classifieds:id/loadingImageView");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Log.d(
              GlobalVariables.Tag_Name,
              "First Home Elements Loader No Time:" + new Timestamp(new Date().getTime()));
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Home Elements1 Method");
      return false;
    }
  }

  private boolean homeElements2() {
    try {

      Thread.sleep(1000);
      UiSelector loaderId =
          new UiSelector().resourceId("com.qatarliving.classifieds:id/image_loading_progress");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          het = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Home Elements2 Method");
      return false;
    }
  }

  private boolean menuButton() {
    try {
      boolean menuButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Menu button with Attempt no." + (i + 1));

          UiSelector menuID =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/btn_toggle");
          UiObject menu = device.findObject(menuID);
          if (menu.waitForExists(GlobalVariables.Wait_Timeout)) {
            menu.click();
            menuButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Menu Button");
        }
      }
      return menuButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Menu Button Method");
      return false;
    }
  }

  private boolean browseButton() {
    try {
      boolean browseButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Browse button with Attempt no." + (i + 1));

          UiSelector BrowseId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/item_categories");
          UiObject Browse = device.findObject(BrowseId);
          if (Browse.waitForExists(GlobalVariables.Wait_Timeout)) {
            Browse.click();
            browseButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in clicking Browse Button");
        }
      }
      return browseButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Browse Button Method");
      return false;
    }
  }

  private boolean selectVehicles() {
    try {
      boolean selectVehiclesFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Selecting Vehicles button with Attempt no." + (i + 1));
          Thread.sleep(4000);
          UiCollection selectVehicleId =
              new UiCollection(new UiSelector().textContains("Vehicles"));
          UiObject selectVehicleBtn = selectVehicleId.getChildByInstance(new UiSelector(), 0);
          if (selectVehicleBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVehicleBtn.click();
            selectVehiclesFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Selecting Vehicles");
        }
      }
      return selectVehiclesFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Select Vehicles Method");
      return false;
    }
  }

  private boolean selectCar() {
    try {
      boolean selectCarFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Selecting Car button with Attempt no." + (i + 1));

          UiSelector carID = new UiSelector().textContains("Car/Sedan");
          UiObject car = device.findObject(carID);
          if (car.waitForExists(GlobalVariables.Wait_Timeout)) {
            car.click();
            selectCarFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Selecting Car");
        }
      }
      return selectCarFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Selecting Car Method");
      return false;
    }
  }

  private boolean clickSearch() {
    try {
      boolean clickSearchFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Search button with Attempt no." + (i + 1));

          UiSelector SearchId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/btn_search");
          UiObject SearchAppear = device.findObject(SearchId);
          if (SearchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            SearchAppear.click();
            clickSearchFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Clicking Search");
        }
      }
      return clickSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Search with Attempt no." + (i + 1));

          UiSelector EnterSearchId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/edit_search");
          UiObject EnterSearch = device.findObject(EnterSearchId);
          if (EnterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            EnterSearch.click();
            EnterSearch.setText("BMW \n");
            device.pressSearch();
            device.pressEnter();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            enterSearchFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Entering Search");
        }
      }
      return enterSearchFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Method");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      boolean searchResults = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Search Results with Attempt no." + (i + 1));

          UiSelector deliveryId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/ad_image_drawee");
          UiObject deliveryAppear = device.findObject(deliveryId);
          if (deliveryAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            Log.d(
                GlobalVariables.Tag_Name,
                "Search Results Image View Appear Time:" + new Timestamp(new Date().getTime()));
            searchResults = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Search Results");
        }
      }
      return searchResults;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Search Results Method");
      return false;
    }
  }

  private boolean searchResults1() {
    try {
      UiSelector loaderId = new UiSelector().textContains("Loading…");
      //            UiSelector loaderId = new
      // UiSelector().resourceId("com.qatarliving.classifieds:id/messageTv");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Log.d(
              GlobalVariables.Tag_Name,
              "First Search Results Loader No Time:" + new Timestamp(new Date().getTime()));
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Search Results1 Methods");
      return false;
    }
  }

  private boolean searchResults2() {
    try {

      UiSelector loaderId =
          new UiSelector().resourceId("com.qatarliving.classifieds:id/image_loading_progress");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Search Results2 Method");
      return false;
    }
  }

  private boolean selectItem() {
    try {
      boolean selectItemFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Select Item with Attempt no." + (i + 1));

          UiCollection selectItemId =
              new UiCollection(
                  new UiSelector()
                      .resourceId("com.qatarliving.classifieds:id/customItemContainer"));
          UiObject selectVehicleBtn = selectItemId.getChildByInstance(new UiSelector(), 0);
          if (selectVehicleBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVehicleBtn.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Select Item Time:" + ht);
            selectItemFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Selecting Item");
        }
      }
      return selectItemFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Select Item Method");
      return false;
    }
  }

  private boolean progressBar1() {
    try {

      Thread.sleep(1000);
      UiSelector loaderId = new UiSelector().textContains("Loading…");
      //            UiSelector loaderId = new
      // UiSelector().resourceId("com.qatarliving.classifieds:id/messageTv");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Log.d(
              GlobalVariables.Tag_Name,
              "First Progress Loader No Time:" + new Timestamp(new Date().getTime()));
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Progress Bar1 Method");
      return false;
    }
  }

  private boolean firstAdAppear() {
    try {
      boolean firstAdAppear = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find firstAdAppear with Attempt no." + (i + 1));

          UiSelector deliveryId =
              new UiSelector().resourceId("com.qatarliving.classifieds:id/page_image");
          UiObject deliveryAppear = device.findObject(deliveryId);
          if (deliveryAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "First Ad Appear Time:" + st);
            firstAdAppear = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in firstAdAppear");
        }
      }
      return firstAdAppear;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding firstAdAppear Method");
      return false;
    }
  }

  private boolean progressBar2() {
    try {

      Thread.sleep(1000);
      UiSelector loaderId =
          new UiSelector().resourceId("com.qatarliving.classifieds:id/image_loading_progress");
      //            UiSelector loaderId = new UiSelector().textContains("0.0");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          st = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "First Ad Appear Time:" + st);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Progress Bar2 Method");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double TLP = (st.getTime() - ht.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Load First Add = " + TLP);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
