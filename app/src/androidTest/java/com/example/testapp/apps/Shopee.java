package com.example.testapp.apps;


import android.content.Context;
import android.util.Log;


import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;
import static org.junit.Assert.assertEquals;

public class Shopee implements AppClass{
//    static boolean checker = false;
public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    Timestamp homeElementsAppearTime,clickSearchTime,enterSearchTime,searchResultsAppearTime,clickItemTime,viewItemTime,clickFeedTime,feedResultTime,
            clickFeedProfileTime,displayFeedProfileTime,clickMydayTime,viewMydayTime,clickShopeeLiveTime,displayLiveTime,clickLiveTime,displayLiveViwerTime;

    @Override
    public int testRun(int job_id, String device_id, String ipAdress, UiDevice device, Timestamp lt, String order, String script, String location, String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homePage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to find Home Element");
                return 1;
            }
            runTest = checkAddsOnHome();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to find Home Element");
                return 1;
            }
            runTest = clickSearchButton();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to click Search Button");
                return 1;
            }
            runTest = enterSearchText();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to enter Search text Button");
                return 1;
            }
            runTest = searchResults();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Search result");
                return 1;
            }
            runTest = clickOnItem();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click item");
                return 1;
            }
            runTest = viewItem();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Display item");
                return 1;
            }
            runTest = gotoFeed();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to go feed");
                return 1;
            }
            runTest = viewFeedItem();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to view feed");
                return 1;
            }
//            runTest = gotoFeedProfile();
//            if(!runTest){
//                DataHolder.getInstance().setFailureReason("Unable to go feed profile");
//                return 1;
//            }
//            runTest = displayFeedProfile();
//            if(!runTest){
//                DataHolder.getInstance().setFailureReason("Unable to display profile");
//                return 1;
//            }
//            runTest = gotoMyDay();
//            if(!runTest){
//                DataHolder.getInstance().setFailureReason("Unable to go myday");
//                return 1;
//            }
//            runTest = viewMyday();
//            if(!runTest){
//                DataHolder.getInstance().setFailureReason("Unable to view myday");
//                return 1;
//            }
            runTest = gotoShopeeLive();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to go shopee live");
                return 1;
            }
            runTest = displayLive();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display live");
                return 1;
            }
            runTest = clickLive();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to click live");
                return 1;
            }
            runTest = displayLiveViewer();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display live");
                return 1;
            }
            runTest = kpiCalculation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI");
                return 1;
            }



            stopScreenRecording();


            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Shopee_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean homePage() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().className("android.widget.ImageView");
                UiObject object = device.findObject(selector);
                if (object.exists()) {
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + homeElementsAppearTime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Home Page");
            return value;
        }
    }
    public boolean checkAddsOnHome() {
        boolean value = false;
        try {
            //get adds element in home page
            UiSelector addsSelector = new UiSelector().className("android.view.ViewGroup").index(2).clickable(true).focusable(true);
            UiObject addsObject = device.findObject(addsSelector);
            if(addsObject.waitForExists(20000)){
                //remove adds in home page
                Log.d(GlobalVariables.Tag_Name, "Home adds detected");
                Thread.sleep(200);
                addsObject.click();
                value = true;
            } else {
                Log.d(GlobalVariables.Tag_Name, "no Home adds detected");
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Home Page");
        }
        return value;
    }

    public boolean clickSearchButton() {
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().resourceId("com.shopee.ph:id/text_search");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickSearchTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked On Search Successfully Time:" + clickSearchTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in clicking Search Button");
        }
        return value;
    }

    public boolean enterSearchText(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                object.setText("Globe Lte");
                device.pressEnter();
                enterSearchTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Entered Search Text Successfully Time:" + enterSearchTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error while entering search text");
        }
        return value;
    }

    public boolean searchResults(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("sold");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                searchResultsAppearTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Search Results Appear Time:" + searchResultsAppearTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in finding search results");
        }
        return  value;
    }

    public boolean clickOnItem(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("sold");
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickItemTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked on item successfully Time:" + clickItemTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in clicking on item");
        }
        return  value;
    }

    public boolean viewItem(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("sold");
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                viewItemTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"View Item Appeared Time:" + viewItemTime);
                value = true;
                Thread.sleep(3000);
                device.pressBack();
                Thread.sleep(3000);
                device.pressBack();
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Item");
        }
        return  value;
    }


    public boolean gotoFeed(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("Feed");
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickFeedTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked on Feed successfully Time:" + clickFeedTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in clicking on Feed");
        }
        return  value;
    }

    public boolean viewFeedItem(){
        boolean value = false;
        try{
            //UiSelector selector = new UiSelector().className("android.widget.ImageView").index(2);
            UiSelector selector = new UiSelector().className("android.widget.ImageView").index(0);
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                feedResultTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Feed Item Appeared Time:" + feedResultTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Feed");
        }
        return  value;
    }

//    public boolean gotoFeedProfile(){
//        boolean value = false;
//        try{
//            Thread.sleep(2000);
//            UiSelector selector = new UiSelector().textContains("shopeeph").index(1).className("android.widget.TextView");
//            UiObject object = device.findObject(selector);
//            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
//                object.click();
//                clickFeedProfileTime = new Timestamp(new Date().getTime());
//                Log.d(GlobalVariables.Tag_Name,"Clicked on FeedProfile successfully Time:" + clickFeedProfileTime);
//                value = true;
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.d(GlobalVariables.Tag_Name,"Error in clicking on FeedProfile");
//        }
//        return  value;
//    }
//
//    public boolean displayFeedProfile(){
//        boolean value = false;
//        try{
//            UiSelector selector = new UiSelector().className("android.view.ViewGroup");
//            UiObject object = device.findObject(selector);
//            UiScrollable scroll = new UiScrollable(new UiSelector().className("androidx.viewpager.widget.ViewPager").scrollable(true));
//                if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
//                    displayFeedProfileTime = new Timestamp(new Date().getTime());
//                    Log.d(GlobalVariables.Tag_Name, "Feed profile Appeared Time:" + displayFeedProfileTime);
//                    scroll.scrollBackward();
//                    value = true;
//                }
//
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.d(GlobalVariables.Tag_Name,"Error in View Feed profile");
//        }
//        return  value;
//    }
//
//    public boolean gotoMyDay(){
//        boolean value = false;
//        try{
//
//            UiSelector selector = new UiSelector().className("android.view.ViewGroup").index(0).clickable(true).
//                    enabled(true).checkable(false).checked(false).focusable(true);
//            UiObject object = device.findObject(selector);
//            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
//                object.click();
//                clickMydayTime = new Timestamp(new Date().getTime());
//                Log.d(GlobalVariables.Tag_Name,"Clicked on Myday successfully Time:" + clickMydayTime);
//                value = true;
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.d(GlobalVariables.Tag_Name,"Error in clicking on Myday");
//        }
//        return  value;
//    }
//
//    public boolean viewMyday(){
//        boolean value = false;
//        try{
//            UiSelector selector = new UiSelector().resourceId("com.shopee.ph:id/function_btn_layout");
//            UiObject object = device.findObject(selector);
//            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
//                viewMydayTime = new Timestamp(new Date().getTime());
//                Log.d(GlobalVariables.Tag_Name,"Myday Appeared Time:" + viewMydayTime);
//                value = true;
//                Thread.sleep(3000);
//                device.pressBack();
//                Thread.sleep(3000);
//                device.pressBack();
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.d(GlobalVariables.Tag_Name,"Error in View Myday");
//        }
//        return  value;
//    }

    public boolean gotoShopeeLive(){
        boolean value = false;
        try{

            // UiSelector selector = new UiSelector().textContains("Shopee Live");
            UiSelector selector = new UiSelector().textContains("Live");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickShopeeLiveTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked on Shopee Live successfully Time:" + clickShopeeLiveTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in clicking on Shopee Live");
        }
        return  value;
    }

    public boolean displayLive(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("LIVE").className("android.widget.TextView").index(2);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                displayLiveTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Shopee Live Appeared Time:" + displayLiveTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Shopee Live");
        }
        return  value;
    }

    public boolean clickLive(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().className("android.widget.TextView").textContains("LIVE").index(2);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                clickLiveTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked on Live successfully Time:" + clickLiveTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in clicking on Live");
        }
        return  value;
    }

    public boolean displayLiveViewer(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().resourceId("com.shopee.ph:id/tv_live_title");
            UiObject object = device.findObject(selector);
            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                displayLiveViwerTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Viewer Appeared Time:" + displayLiveViwerTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Viewer");
        }
        return  value;
    }


    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double SRT = (searchResultsAppearTime.getTime() - enterSearchTime.getTime()) / 1000.0;
            double TTLSI = (viewItemTime.getTime() - clickItemTime.getTime()) / 1000.0;
//            double TTLM = (viewMydayTime.getTime() - clickMydayTime.getTime())/ 1000.0;
            double TTLM = (feedResultTime.getTime() - clickFeedTime.getTime())/ 1000.0;
            double TTLL = (displayLiveViwerTime.getTime() - clickLiveTime.getTime())/ 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Selected Item=" + TTLSI);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Myday=" + TTLM);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Live Video Page=" + TTLL);


            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }


}

