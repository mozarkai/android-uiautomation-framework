package com.example.testapp.apps;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class OTVProd implements AppClass {
    public static String testId;
    public String log = "";

    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    private Timestamp lt, het, lp, eet, ep, llt, wo, vpst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            context= TestApp.getContext();

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            // copyFromAsset(context,testId);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

            runTest = homeElement();
            if (!runTest) {
                return 1;
            }

            runTest = loadEPG();
            if (!runTest) {
                return 1;
            }

            runTest = loadLiveTVDetail();
            if (!runTest) {
                return 1;
            }

            runTest = loadVideo();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            kpiCalculation();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate, ipAdress);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;

    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.AngelBroking_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiObject homeElement = device.findObject(new UiSelector().resourceId("com.telekom.onetv.hu.prodDebug:id/viewPagerItem_image1"));
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (homeElement.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    private boolean loadEPG() {
        boolean EPGElementFound = false;

        try {

            UiObject EPGPage = device.findObject(new UiSelector().textMatches("EPG"));
            EPGPage.click();
            ep = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On EPG Option:" + ep);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiObject loadEPG = device.findObject(new UiSelector().resourceId("com.telekom.onetv.hu.prodDebug:id/channel_lyt"));
            loadEPG.click();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (loadEPG.exists()) {
                    eet = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "EPG Elements Appear Time:" + eet);
                    EPGElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading EPG Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding EPG Element");
            return EPGElementFound;
        }
        return EPGElementFound;
    }

    private boolean loadLiveTVDetail() {
        boolean LiveTVDetailFound = false;

        try {

            UiObject liveTV = device.findObject(new UiSelector().className("android.widget.FrameLayout").instance(1).className("androidx.recyclerview.widget.RecyclerView").instance(2).className("android.view.ViewGroup").instance(1).className("android.widget.RelativeLayout").className("android.view.ViewGroup"));
            //android.widget.FrameLayout[1]//androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[1]//android.widget.RelativeLayout/android.view.ViewGroup
            liveTV.click();
            lp = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On live TV channel:" + lp);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiObject loadLiveTV = device.findObject(new UiSelector().textContains("Watch"));
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (loadLiveTV.exists()) {
                    llt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Live TV Details Appear Time:" + llt);
                    LiveTVDetailFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Live TV Detail Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Live TV Detail Page");
            return LiveTVDetailFound;
        }
        return LiveTVDetailFound;
    }

    private boolean loadVideo() {
        boolean loadVideoFound = false;

        try {
            UiObject watchOption = device.findObject(new UiSelector().textStartsWith("Watch"));
            watchOption.click();
            wo = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On live TV Option:" + wo);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject videoBar = device.findObject(new UiSelector().resourceId("com.telekom.onetv.hu.prodDebug:id/video_progress"));
                if (videoBar.exists()) {
                    vpst = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Video Play Start Time:" + vpst);
                    loadVideoFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Video");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in loading video");
            return loadVideoFound;
        }
        return loadVideoFound;
    }

    private void kpiCalculation() {
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double TTLEP = (eet.getTime() - ep.getTime()) / 1000.0;
            double TTLLTDP = (llt.getTime() - lp.getTime()) / 1000.0;
            double TTLV = (vpst.getTime() - wo.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load EPG Page=" + TTLEP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Live TV Detail Page=" + TTLLTDP);
            Log.d(GlobalVariables.Tag_Name, "Play Start Time=" + TTLV);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean sendData(String appVersion, String startDate, String ipAdress) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(job_id, device_id, testId, "completed", false, order, script, appVersion, appName,ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private static void copyFromAsset(Context context,String path)
    {
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        InputStream in = null;
        OutputStream out = null;
        String filename = "pcap.pcap";
        try
        {
            String pcapName=Utility.fileName(testId) + ".pcap.gz";
            in = assetManager.open(filename);
            out = new FileOutputStream(pcapName);
            File file=new File(pcapName);
            if(!file.exists())
            {
                file.createNewFile();
            }
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }
        catch(IOException e)
        {
            Log.e("tag", "Failed to copy asset file: " + filename, e);
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }
}
