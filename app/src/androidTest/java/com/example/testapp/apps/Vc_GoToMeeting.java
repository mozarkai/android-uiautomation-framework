package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Vc_GoToMeeting implements AppClass{

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String startDate;
  String OUTPUT_LOG_FILE = "";
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      Utility.permission(GlobalVariables.GoToMeeting_Package);

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      runTest = enterMeetingId();
      if (!runTest) {
        return 1;
      }

      runTest = loaderAppear(vCap);
      if (!runTest) {
        return 1;
      }

      runTest = endMeeting();
      if (!runTest) {
        return 1;
      }

      runTest = confirmEndMeeting();
      if (!runTest) {
        return 1;
      }

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private boolean homeElements() {
    try {
      boolean homeElements = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Home Elements with Attempt no." + (i + 1));

          UiSelector homeID = new UiSelector().resourceId("com.gotomeeting:id/button_join_meeting");
          UiObject home = device.findObject(homeID);
          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + st);
            homeElements = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Home Elements");
        }
      }
      return homeElements;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Elements Method");
      return false;
    }
  }

  private boolean enterMeetingId() {
    try {
      boolean EnterMeetingId = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Meeting ID with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().resourceId("com.gotomeeting:id/autoCompleteTextView");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            joinMeeting.click();
            // Akanksha                       joinMeeting.setText("703539005");
            joinMeeting.setText("237634085");
            device.pressEnter();
            vt = new Timestamp(new Date().getTime());
            Log.d(
                GlobalVariables.Tag_Name,
                "Entered Meeting ID:" + new Timestamp(new Date().getTime()));
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
            EnterMeetingId = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Meeting ID Failed");
        }
      }
      return EnterMeetingId;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Meeting ID Method");
      return false;
    }
  }

  private boolean clickJoin() {
    try {
      boolean clickJoin = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Join button with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().text("com.gotomeeting:id/button_join_meeting");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            joinMeeting.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
            clickJoin = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Join Button Failed");
        }
      }
      return clickJoin;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Join Button Method");
      return false;
    }
  }

  private boolean loaderAppear(boolean vCap) {
    try {

      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_TimeMid / 1000,vCap);

      UiSelector loaderId = new UiSelector().textContains("Joining…");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_TimeMid) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
        } else {

          Timestamp loaderNo = new Timestamp(new Date().getTime());

          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
        }
      }

      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean endMeeting() {
    try {
      boolean endMeet = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find End Meeting Button with Attempt no." + (i + 1));

          UiSelector endMeetingID = new UiSelector().resourceId("com.gotomeeting:id/end_session");
          UiObject endMeeting = device.findObject(endMeetingID);

          device.click(100, 100);

          if (endMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            endMeeting.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked End Meeting Button Time:" + new Timestamp(new Date().getTime()));
            endMeet = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click On End Meeting Button Failed");
        }
      }
      return endMeet;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding End Meeting Method");
      return false;
    }
  }

  private boolean confirmEndMeeting() {
    try {
      boolean cEndMeet = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Confirm End Meeting Button with Attempt no." + (i + 1));

          UiSelector endMeetingID = new UiSelector().textContains("LEAVE MEETING");
          UiObject endMeeting = device.findObject(endMeetingID);

          if (endMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            endMeeting.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked Confirm End Meeting Button Time:" + new Timestamp(new Date().getTime()));
            cEndMeet = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click On Confirm End Meeting Button Failed");
        }
      }
      return cEndMeet;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Confirm End Meeting Method");
      return false;
    }
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.GoToMeeting_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
