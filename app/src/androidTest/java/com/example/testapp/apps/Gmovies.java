package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Gmovies implements AppClass {
    Timestamp homeElementsAppearTime, clickOnSearchButtonTime, searchResultsTime, clickOnPayButtonTime,paymentDetailPageTime, clickOnConfirmPaymentTime, totalAmountTime, sendMoneyTime;
    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homePageElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display HomePage element");
                return 1;
            }

            runTest = clickSearchButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display SearchButton element");
                return 1;
            }
            runTest = enterSearchText();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display HomePage element");
                return 1;
            }
            runTest = searchResultsWithText();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display searchResultsText element");
                return 1;
            }
            Thread.sleep(5000);
            runTest = clickOnSearchedItem();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display SearchedItem element");
                return 1;
            }
            Thread.sleep(5000);
            runTest = clickOnPayButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display PayButton element");
                return 1;
            }
            runTest = paymentOption();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display paymentOption element");
                return 1;
            }
            runTest = confirmPayment();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display confirmPayment element");
                return 1;
            }
            runTest = totalAmount();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to display totalAmount element");
                return 1;
            }

            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI's");
                return 1;
            }

            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.GmoviePackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }


    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double SRT = (searchResultsTime.getTime() - clickOnSearchButtonTime.getTime()) / 1000.0;
            double TTLPDP = (paymentDetailPageTime.getTime() - clickOnPayButtonTime.getTime()) / 1000.0;
            double TTLPP = (totalAmountTime.getTime() - clickOnConfirmPaymentTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Detail Page=" + TTLPDP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Payment Page=" + TTLPP);


            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    public boolean homePageElement () {
        try {
            UiSelector homePageId = new UiSelector().textContains("Movies on UPSTREAM");
            UiObject homePage = device.findObject(homePageId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (homePage.exists()) {
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + homeElementsAppearTime);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
            return false;
        }
    }

    public boolean clickSearchButton() {
        try {
            UiSelector clickSearchButtonId = new UiSelector().descriptionContains("Search");
            UiObject clickSearchButton = device.findObject(clickSearchButtonId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (clickSearchButton.exists()) {
                    clickSearchButton.click();
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading clickSearchButton Results");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return false;
        }
    }

    public boolean enterSearchText() {
        try {
            UiSelector enterSearchTextId = new UiSelector().resourceId("com.globereel:id/et_search");
            UiObject enterSearchText = device.findObject(enterSearchTextId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (enterSearchText.exists()) {
                    enterSearchText.click();
                    enterSearchText.setText("fast");

                    clickOnSearchButtonTime = new Timestamp(new Date().getTime());
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return false;
        }

    }
    public boolean searchResultsWithText() {
        try {
            UiSelector searchResultsId = new UiSelector().textContains("Searching for: \"fast\"");
            UiObject searchResults = device.findObject(searchResultsId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (searchResults.exists()) {
                    searchResultsTime = new Timestamp(new Date().getTime());
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return false;
        }
    }


    public boolean clickOnSearchedItem() {
        try {
            UiCollection clickOnSearchedItemId = new UiCollection(new UiSelector().resourceId("com.globereel:id/rv_search"));
            UiObject clickOnSearchedItem = clickOnSearchedItemId.getChildByInstance(new UiSelector(), 0);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (clickOnSearchedItem.exists()) {
                    clickOnSearchedItem.click();
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Searched Item");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Searched Item");
            return false;
        }
    }

    public boolean clickOnPayButton() {
        try {
            UiSelector clickOnPayButtonId = new UiSelector().resourceId("com.globereel:id/button_buy");
            UiObject clickOnPayButton = device.findObject(clickOnPayButtonId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (clickOnPayButton.exists()) {
                    clickOnPayButton.click();
                    clickOnPayButtonTime = new Timestamp(new Date().getTime());
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading clickOnPayButton ");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting clickOnPayButton Results");
            return false;
        }
    }

    private boolean paymentOption() {
        try {
            boolean paymentOptionFlag = false;
            for (int i = 0; i < 5; i++) {
                try {
                    UiCollection paymentOptionId = new UiCollection(new UiSelector().resourceId("com.globereel:id/rv_payment_option"));
                    UiObject paymentOption = paymentOptionId.getChildByInstance(new UiSelector(), 1);

                    UiScrollable scroll = new UiScrollable(
                                new UiSelector().className("android.widget.ScrollView"));
                        scroll.scrollIntoView(paymentOption);
                        if (paymentOption.waitForExists(GlobalVariables.Wait_Timeout)) {
                            paymentOption.click();
                            paymentDetailPageTime = new Timestamp(new Date().getTime());
                            paymentOptionFlag = true;
                            break;
                        }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in paymentOption" + new Timestamp(new Date().getTime()));
                }
            }
            return paymentOptionFlag;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding paymentOption Button Method");
            return false;
        }

    }

    private boolean confirmPayment() {
        try {
            boolean confirmPaymentFlag = false;
            for (int i = 0; i < 5; i++) {
                try {

                    UiSelector confirmPaymentId = new UiSelector().resourceId("com.globereel:id/btn_confirm_payment");
                    UiObject confirmPayment = device.findObject(confirmPaymentId);

                    UiScrollable scroll = new UiScrollable(
                            new UiSelector().className("android.widget.ScrollView"));
                    scroll.scrollIntoView(confirmPayment);
                    if (confirmPayment.waitForExists(GlobalVariables.Wait_Timeout)) {
                        confirmPayment.click();
                        clickOnConfirmPaymentTime = new Timestamp(new Date().getTime());
                        confirmPaymentFlag = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in confirmPayment" + new Timestamp(new Date().getTime()));
                }
            }
            return confirmPaymentFlag;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding confirmPayment Button Method");
            return false;
        }

    }

    public boolean totalAmount () {
        try {
            UiSelector totalAmountId = new UiSelector().textContains("Total Amount");
            UiObject totalAmount = device.findObject(totalAmountId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (totalAmount.exists()) {
                    totalAmountTime = new Timestamp(new Date().getTime());
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading totalAmount");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading totalAmount");
            return false;
        }
    }

}
