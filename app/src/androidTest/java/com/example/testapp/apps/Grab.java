package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Grab implements AppClass {
    Timestamp homeElementsAppearTime,foodPageClickTime,foodPageLoadTime,martPageClickTime,martPageLoadTime,carPageClickTime,carPageLoadTime,faireLoadTime,placeLoadTime,clickConfirmTime
            ,searchPlaceTime;
    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homePage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display Home element");
                return 1;
            }
            runTest = gotoFood();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Go To Food");
                return 1;
            }
            runTest = foodPageLoaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Load Food Page");
                return 1;
            }
            runTest = gotoMart();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Mart Page");
                return 1;
            }
            runTest = martPageLoaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Load Mart Page");
                return 1;
            }
            runTest = gotoCar();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Car");
                return 1;
            }
            runTest = carPageLoaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Load Car Page");
                return 1;
            }
            runTest = clickWhereTo();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Where To");
                return 1;
            }
            runTest = clickPlace();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Place");
                return 1;
            }
            runTest = placeLoaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Load Place");
                return 1;
            }
            runTest = confirm();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Confirm");
                return 1;
            }
            runTest = fareLoaded();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Load Fare");
                return 1;
            }
            runTest = kpiCalculation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI's");
                return 1;
            }

            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Grab_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    public boolean homePage() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstElementSelector = new UiSelector().textContains("Points");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                UiSelector secElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/title");
                UiObject secElementObject = device.findObject(secElementSelector);

                UiSelector thirdElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/tile_icon");
                UiObject thirdElementObject = device.findObject(thirdElementSelector);

                if (firstElementObject.exists()||secElementObject.exists() || thirdElementObject.exists())
                {
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + homeElementsAppearTime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Home Page");
            return false;
        }
    }

    public boolean gotoFood() {
        boolean value = false;
        try
        {
            UiSelector selector = new UiSelector().textContains("Food");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout))
            {
                object.click();
                foodPageClickTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked on Food Successfully Time:" + foodPageClickTime);
                value = true;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on clicking Food");
        }
        return value;
    }

    public boolean foodPageLoaded() {
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Trying to load food page");

                UiSelector firstElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/iv_advertise");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                UiSelector secondElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/name");
                UiObject secondElementObject = device.findObject(secondElementSelector);

                UiSelector thirdElementSelector = new UiSelector().textContains("Dismiss");
                UiObject thirdElementObject = device.findObject(thirdElementSelector);

                UiSelector fourthElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/gf_food_mca_dialog_img");
                UiObject fourthElementObject = device.findObject(fourthElementSelector);

                UiSelector fifthElementSelector = new UiSelector().textContains("Mart").resourceId("com.grabtaxi.passenger:id/tile_text");
                UiObject fifthElementObject = device.findObject(fifthElementSelector);

                if(firstElementObject.exists() || secondElementObject.exists() || thirdElementObject.exists() || fourthElementObject.exists() || fifthElementObject.exists()){
                    if(fifthElementObject.exists() == false)
                    {
                        foodPageLoadTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Food Page Displayed Successfully Time:" + foodPageLoadTime);
                        value = true;
                        Log.d(GlobalVariables.Tag_Name, String.valueOf(fifthElementObject.exists()));
                        device.pressBack();
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on Displaying Food Page");
        }
        return value;
    }
    public boolean gotoMart() {
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("Mart");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                martPageClickTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked Mart Page Successfully Time:" + martPageClickTime);
                value = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on Clicking Mart Page");
        }
        return value;
    }

    public boolean martPageLoaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Find mart page");

                UiSelector firstElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/icon");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                UiSelector secondElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/name");
                UiObject secondElementObject = device.findObject(secondElementSelector);

                UiSelector selector = new UiSelector().textContains("Food").resourceId("com.grabtaxi.passenger:id/tile_text");
                UiObject object = device.findObject(selector);

                if(firstElementObject.exists() || secondElementObject.exists() || object.exists()){
                    Log.d(GlobalVariables.Tag_Name, String.valueOf(object.exists()));
                    if (object.exists() == false)
                    {
                        martPageLoadTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Mart Page Displayed Successfully Time:" + martPageLoadTime);
                        value = true;
                        device.pressBack();
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Displaying Mart");
        }
        return value;
    }

    public boolean gotoCar(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("Car").resourceId("com.grabtaxi.passenger:id/tile_text");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                carPageClickTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked Car Page Successfully Time:" + carPageClickTime);
                value = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error on Clicking Car Page");
        }
        return value;
    }

    public boolean carPageLoaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                UiSelector firstElementSelector = new UiSelector().descriptionContains("Google Map");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                UiSelector secondElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/recycler_drop_off_suggestion");
                UiObject secondElementObject = device.findObject(secondElementSelector);

                UiSelector acceptElementSelector = new UiSelector().textContains("I Accept");
                UiObject acceptElementObject = device.findObject(acceptElementSelector);

                UiSelector dismissElementSelector = new UiSelector().textContains("Dismiss");
                UiObject dismissElementObject = device.findObject(dismissElementSelector);

                UiSelector laterElementSelector = new UiSelector().textContains("Maybe Later");
                UiObject laterElementObject = device.findObject(laterElementSelector);

                UiSelector bannerElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/ivBanner");
                UiObject bannerElementObject = device.findObject(bannerElementSelector);

                if (bannerElementObject.exists())
                {
                    device.pressBack();
                }
                else if (acceptElementObject.exists())
                {
                    acceptElementObject.click();
                }
                else if (dismissElementObject.exists())
                {
                    dismissElementObject.click();
                }
                else if (laterElementObject.exists())
                {
                    laterElementObject.click();
                }
                else if(firstElementObject.exists() || secondElementObject.exists())
                {
                    carPageLoadTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Car Page Displayed Successfully Time:" + carPageLoadTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Displaying Car");
        }
        return value;
    }

    public boolean clickWhereTo(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstElementSelector = new UiSelector().textContains("Where to?");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                Log.d(GlobalVariables.Tag_Name,"Trying to Click Where to");
                if(firstElementObject.exists()){
                    firstElementObject.click();

                    UiSelector secElementSelector = new UiSelector().descriptionContains("Select location from map");
                    UiObject secElementObject = device.findObject(secElementSelector);

                    UiSelector thirdElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/parent_search_location");
                    UiObject thirdElementObject = device.findObject(thirdElementSelector);

                    UiSelector fourthElementSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/cv_saved_places");
                    UiObject fourthElementObject = device.findObject(fourthElementSelector);


                    if (secElementObject.exists() || thirdElementObject.exists() || fourthElementObject.exists())
                    {
                        firstElementObject.setText("The Globe Tower");
                        Timestamp unUsableTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Enter Text Successfully Time:" + unUsableTime);
                        value = true;
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Displaying Car");
        }
        return value;
    }

    public boolean clickPlace(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstElementSelector = new UiSelector().textContains("The Globe Tower").resourceId("com.grabtaxi.passenger:id/poi_item_title");
                UiObject firstElementObject = device.findObject(firstElementSelector);

                if(firstElementObject.exists())
                {
                    firstElementObject.click();
                    searchPlaceTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Place Successfully Time:" + searchPlaceTime);
                    value = true;

                    if (firstElementObject.exists() == false)
                    {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Displaying Car");
        }
        return value;
    }

    public boolean placeLoaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstSelector = new UiSelector().descriptionContains("Google Map");
                UiObject firstElementObject = device.findObject(firstSelector);
                if(firstElementObject.exists())
                {
                    placeLoadTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Place Loaded Successfully" + placeLoadTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Loading Place");
        }
        return value;
    }

    public boolean confirm(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstSelector = new UiSelector().textContains("Confirm Pick-Up");
                UiObject firstElementObject = device.findObject(firstSelector);

                Thread.sleep(2000);
                if(firstElementObject.exists())
                {
                    firstElementObject.click();
                    clickConfirmTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Confirm Successfully" + clickConfirmTime);
                    value = true;
                    if (firstElementObject.exists() == false)
                    {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Loading Place");
        }
        return value;
    }

    public boolean fareLoaded(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector firstSelector = new UiSelector().resourceId("com.grabtaxi.passenger:id/serviceTypeItemView");
                UiObject firstElementObject = device.findObject(firstSelector);

                if(firstElementObject.exists())
                {
                    faireLoadTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Fare Loaded Successfully" + faireLoadTime);
                    value = true;
                    device.pressBack();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Loading Place");
        }
        return value;
    }

    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double TTLFP = (foodPageLoadTime.getTime() - foodPageClickTime.getTime()) / 1000.0;
            double TTLMP = (martPageLoadTime.getTime() - martPageClickTime.getTime()) / 1000.0;
            double TTLBP = (carPageLoadTime.getTime() - carPageClickTime.getTime()) / 1000.0;
            double TTLBF = (faireLoadTime.getTime() - clickConfirmTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Food Page=" + TTLFP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Mart Page=" + TTLMP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Booking Page=" + TTLBP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Booking Fare=" + TTLBF);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }
}
