package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Voice5GmarkPro implements AppClass{

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  private Timestamp lt, st, vt;
  private UiDevice device;
  private String OUTPUT_LOG_FILE = "";
  String startDate;

  @Override
  public int testRun(int job_id, String device_id, String ipAdress, UiDevice device, Timestamp lt, String order, String script, String location, String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {
      context = TestApp.getContext();

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.startDate=startDate;
      this.lt = lt;
      this.location = location;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      int id = android.os.Process.myPid();

      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();

        File file = new File("/sdcard/MozarkUtility/");
        if (!file.exists()) {
          file.mkdir();
        }
        File file1 = new File(file + "/" + testId);
        if (!file1.exists()) {
          file1.mkdirs();
        }
        //                Process processcmd = Runtime.getRuntime().exec("logcat | grep
        // --line-buffered '5gmarktrace' > /sdcard/logdd.txt");
        Process install = Runtime.getRuntime().exec("su\n");
        DataOutputStream os = new DataOutputStream(install.getOutputStream());
        os.writeBytes("adb shell\n");
        os.writeBytes("logcat  > " + file1 + "/" + testId + ".txt" + "\n");
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");
      bufferRead(vCap);
      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);
      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }
      // new LogsChecker(context, "stop").execute();
      Utility.logFileGeneration(testId, log, ipAdress, device_id);
      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
      e.printStackTrace();
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.TimeOut);
      device.executeShellCommand(getCommand(GlobalVariables.AirtelTV_Package));
    } catch (Exception e) {
        Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiCollection searchID =
              new UiCollection(
                  new UiSelector().resourceId("tv.accedo.airtel.wynk:id/iv_bottom_tab_icon"));
          UiObject search = searchID.getChildByInstance(new UiSelector(), 1);

          if (search.waitForExists(GlobalVariables.TimeOut)) {
            search.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Search Button Failed" + new Timestamp(new Date().getTime()));
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));
          UiCollection searchID =
              new UiCollection(
                  new UiSelector().resourceId("tv.accedo.airtel.wynk:id/iv_bottom_tab_icon"));
          UiObject enterSearch_text = searchID.getChildByInstance(new UiSelector(), 2);
          //
          //                    UiSelector enterSearchField = new
          // UiSelector().resourceId("tv.accedo.airtel.wynk:id/iv_bottom_tab_icon");
          //                    UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.TimeOut)) {
            enterSearch_text.setText("kumkum bhagya \n ");
            device.pressEnter();
            Log.d(
                GlobalVariables.Tag_Name,
                "Enter Search time:" + new Timestamp(new Date().getTime()));
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search");
      return false;
    }
  }

  private boolean selectVideo() {
    try {
      boolean selectVideoFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Select Video with Attempt no.:" + (i + 1));
        try {
          device.wait(Until.hasObject(By.res("tv.accedo.airtel.wynk:id/poster_view")), 5);

          UiCollection selectVideoID =
              new UiCollection(new UiSelector().resourceId("tv.accedo.airtel.wynk:id/poster_view"));
          UiObject selectVideo = selectVideoID.getChildByInstance(new UiSelector(), 0);

          if (selectVideo.waitForExists(GlobalVariables.TimeOut)) {
            selectVideo.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            selectVideoFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Select Video to Play Failed");
        }
      }
      return selectVideoFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Video");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          device.wait(Until.hasObject(By.res("tv.accedo.airtel.wynk:id/play_icon")), 5);

          UiCollection playBtnContainerID =
              new UiCollection(new UiSelector().resourceId("tv.accedo.airtel.wynk:id/play_icon"));
          UiObject playBtn = playBtnContainerID.getChildByInstance(new UiSelector(), 7);

          if (playBtn.waitForExists(GlobalVariables.TimeOut)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playBtnFound = true;
            break;
          }

        } catch (Exception e) {

          Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead(boolean vCap) {
    try {
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000,vCap);
      UiSelector loaderId = new UiSelector().resourceId("tv.accedo.airtel.wynk:id/playerloading");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= (GlobalVariables.Video_Run_Time - 25000)) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("tv.accedo", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
