package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;

public class MPL_Fantasy_1 implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt, sbat, sbt, sdbt, bnbt, vt, opat, plat, tpat, svt, tutt;
    //    HashMap<Timestamp, String> hash_map;
    private UiDevice device;

    public double getTime(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Get KPI Time");
        }
        return timestamp;
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            String homePageId = "All Games";
            String fantasyId = "Fantasy";
            String okayButtonId = "Okay";
            String mplFantasyPageId = "MPL Fantasy";
            String firstMatchCardId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup";
            String indexFirstMatchCardId = "0";
            String firstMatchCardId1 = "Hr Mins";
            String firstMatchCardId2 = "Day Hrs";
            String firstMatchCardId3 = "Days";
            String allContestId = "All Contests";
            String entryFeeId = "Entry Fee";
            String prizePoolId = "Prize Pool";
            String contestcardId = "android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]";
            //                      /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]

            String indexContestId = "3";
            String joinContestButton = "//android.widget.ScrollView//android.view.ViewGroup[2]//android.view.ViewGroup[1]";
            String addTeamId = "Add Team";
            String teamsId = "Teams";
            String selById = "Sel by";
            String wkId = "WK (0)";
            String creditsId = "Credits"; // (click 2 times for increasing order)
            String firstPlayersId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup";
            //android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup
            // android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup
            // android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup

            //  sellby_3rd = /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[2]
            String secondPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup";
            String thirdPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup";
            String index = "0";
            String batId = "BAT (0)";
            String arId = "AR (0)";
            String bowlerId = "BOW (0)";
            String fourthPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup";
            String elevenOutOfElevenId = "11/11";
            String nextButtonId = "Next";
            String chooseCaptainPageId = "Choose Captain(C) And Vice Captain(VC)";
            String captainId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]";
            String captianIndex = "4";
            String vcId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup[3]";
            String vcIndex = "5";
            String saveTeamId = "Save Team";
            String registerTeamIdText = "Register";
            String registerTeamId = "& Register";
            String joinedId = "Joined";
            String editTeamsId = "Edit Teams";

            runTest = elementAppear(device, homePageId, "Home Elements", "Home Page Elements Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, fantasyId, "Fantasy Button", "Clicked On Fantasy Button Time:");
            if (!runTest) {
                return 1;
            }

//            clickOnText(device, okayButtonId, "Okay Button", "Clicked On Okay Button Time:");


            runTest = elementAppear(device, mplFantasyPageId, "MPL Fantasy Page", "MPL Fantasy Page Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnMultipleText(device, firstMatchCardId1, firstMatchCardId2, firstMatchCardId3, "First Match Card", "Clicked on First Match Card Time:");
            if (!runTest) {
                runTest = clickButtonWithCollections(device, firstMatchCardId, "First Match Card", "Clicked on First Match Card Time:");
                if (!runTest) {
                    return 1;
                }
            }

            runTest = elementAppear(device, allContestId, "All Contests", "All Contests Page Appear Time:");
            if (!runTest) {
                return 1;
            }

//            runTest = clickOnContestCard(device, joinContestButton, "First Contest Card", "Clicked on First Contest Card Time:");
//            if (!runTest) {
//                return 1;
//            }

            runTest = clickOnOnlyText(device, addTeamId, "Add Team", "Clicked on Add Team Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, selById, "Sel By", "Sel By Appear Time:");
            if (!runTest) {
                return 1;
            }

//            runTest = clickOnText(device, wkId, "WK", "Clicked on WK Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 1, "Select First Player", "Selected First Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 22, "Select Second Player", "Selected Second Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnText(device, batId, "Bat", "Clicked on Bat Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 1, "Select First Player", "Selected First Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 22, "Select Second Player", "Selected Second Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 28, "Select Third Player", "Selected Third Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 56, "Select Fourth Player", "Selected Fourth Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnText(device, arId, "All-Rounder", "Clicked on All-rounder Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 1, "Select First Player", "Selected First Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnText(device, bowlerId, "Bowl", "Clicked on Bowl Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, creditsId, "Credits", "Clicked on Credits Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 1, "Select First Player", "Selected First Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 22, "Select Second Player", "Selected Second Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 28, "Select Third Player", "Selected Third Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectPlayersBasedonIndex(device, 56, "Select Fourth Player", "Selected Fourth Player Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, elevenOutOfElevenId, "Selected Team Members Count", "Selected 11 Members Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, nextButtonId, "Next Button", "Clicked on Next button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, chooseCaptainPageId, "Choose C and VC", "Choose C and VC Page Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectCaptainOrViceCaptain(device, 5, 0, 4, "Select Captain", "Selected Captain Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectCaptainOrViceCaptain(device, 2, 3, 5, "Select Vice Captain", "Selected Vice Captain Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnOnlyText(device, saveTeamId, "Save Team", "Clicked on Save Team Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, registerTeamIdText, "Register Team Appear", "Register Team Button Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickOnText(device, registerTeamId, "Register Team", "Clicked on Register Team Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, editTeamsId, "Edit Team", "Edit Team Button Appear Time:");
//            if (!runTest) {
//                return 1;
//            }


            kpiCalculations();

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion, startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.MPL_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean clickOnText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnMultipleText(UiDevice device, String textID1, String textID2, String textID3, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector1 = new UiSelector().textContains(textID1);
                UiObject idObject1 = device.findObject(selector1);
                UiSelector selector2 = new UiSelector().textContains(textID2);
                UiObject idObject2 = device.findObject(selector2);
                UiSelector selector3 = new UiSelector().textContains(textID3);
                UiObject idObject3 = device.findObject(selector3);

                if (idObject1.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject1.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                } else if (idObject2.exists()) {
                    idObject2.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                } else if (idObject3.exists()) {
                    idObject3.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnOnlyText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().text(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    return elementFound;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickButtonWithCollections(UiDevice device, String collection, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(2).className("android.view.ViewGroup").className("android.view.ViewGroup").index(1));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnContestCard(UiDevice device, String collection, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {
//android.widget.ScrollView//android.view.ViewGroup[2]//android.view.ViewGroup[1]";
//                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(2).className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").index(3));
//               for (int i=0; i<20; i++) {
                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").instance(8).className("android.view.ViewGroup").index(1));
                Log.d(GlobalVariables.Tag_Name, "Value of i is ");
//                   idObject.click();
//               }

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean selectPlayersBasedonIndex(UiDevice device, int index, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                UiObject idObject3 = new UiObject(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").index(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").index(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").index(index).className("android.view.ViewGroup").index(0));

                UiObject idObject4 = new UiObject(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").instance(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup")).getChild(new UiSelector().instance(index).className("android.view.ViewGroup").index(0));


                UiCollection btnId = new UiCollection(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").instance(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(index).className("android.view.ViewGroup"));

                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(index).className("android.view.ViewGroup"));

                UiObject idObject2 = btnId.getChildByInstance(new UiSelector(), 0);
                int count = btnId.getChildCount();

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    //idObject.click();

                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean selectCaptainOrViceCaptain(UiDevice device, int instance1, int instance2, int index, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;

            try {
                UiObject idObject1 = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").instance(instance1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").instance(instance2).index(index));
                UiObject idObject = null;
                idObject = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(instance1).className("android.view.ViewGroup").instance(instance2).index(index));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private void kpiCalculations() {
        try {

            double homeElementsAppearTime = getTime("Home Elements");
            double fantasyButtonTime = getTime("Fantasy Button");
            double fantasyPageAppearTime = getTime("MPL Fantasy Page");
            double firstMatchCardTime = getTime("First Match Card");
            double allContestsAppearTime = getTime("All Contests");
//            double firstContestCardTime = getTime("First Contest Card");
            double addTeamTime = getTime("Add Team");
            double selByAppearTime = getTime("Sel By");
//            double nextButtonTime = getTime("Next Button");

//            double saveTeamTime = getTime("Save Team");
//            double registerTeamAppearTime = getTime("Register Team Appear");
//            double registerTeamTime = getTime("Register Team");
//            double editTeamTime = getTime("Edit Team");


            double TTLH = homeElementsAppearTime - (ExampleInstrumentedTest.lt.getTime() / 1000.0);
            double TTLSMP = fantasyPageAppearTime - fantasyButtonTime;
            double TTLSCP = allContestsAppearTime - firstMatchCardTime;
            double TTLSPP = selByAppearTime - addTeamTime;
//            double TTLTRSP = registerTeamAppearTime - saveTeamTime;
//            double TTLST = editTeamTime - registerTeamTime;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Select Match Page=" + TTLSMP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Select Contest Page=" + TTLSCP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Select Players Page=" + TTLSPP);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Team Registration Success Page=" + TTLTRSP);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Save Team Page=" + TTLST);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
