package com.example.testapp.apps;

import static org.junit.Assert.assertEquals;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.interfaces.GlobalVariables;
import com.example.testapp.utility.JsonUtil;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

@RunWith(AndroidJUnit4.class)
public class fiveGMark {

    static long OneMin_Timeout = 60000;
    static long TwoMin_Timeout = 120000;
    static long ThreeMin_Timeout = 180000;
    static long FiveMin_Timeout = 300000;

    Context context = ApplicationProvider.getApplicationContext();
    private UiDevice device = null;
//    ClearCache clearCache = new ClearCache();

    public fiveGMark(Context context, UiDevice device) {
        this.context = context;
        this.device = device;
    }

    @Test
    public void fiveGMarkSpeedTest() {
        String testCaseName = "FiveGMark Speed Test Outdoor";
        try {
            assertEquals("Launching App Failed", true, launchApp(testCaseName));
            assertEquals("Home Screen Element Loading Failed", true, fiveGMarkCheckingForHomeScreen(testCaseName));
            assertEquals("Click Speed Test Button Failed", true, fiveGMarkCheckAndClickSpeedTest(testCaseName));
            assertEquals("Click Test Button Failed", true, fiveGMarkCheckAndClickTest(testCaseName));
            assertEquals("Click Indoor Button Failed", true, fiveGMarkCheckAndClickOutDoor(testCaseName));
            assertEquals("Results Element Loading Failed", true, fiveGMarkCheckResults(testCaseName));
            resultInSpeedTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fiveGMarkFullTest() {
        String testCaseName = "FiveGMark Full Test Indoor";
        try {
            Thread.sleep(2000);
            assertEquals("Launching App Failed", true, launchApp(testCaseName));
            assertEquals("Home Screen Element Loading Failed", true, fiveGMarkCheckingForHomeScreen(testCaseName));
            assertEquals("Click Test Button Failed", true, fiveGMarkCheckAndClickTest(testCaseName));
            assertEquals("Click Outdoor Button Failed", true, fiveGMarkCheckAndClickIndoor(testCaseName));
            assertEquals("Results Element Loading Failed", true, fiveGMarkCheckResults(testCaseName));
            resultInFullTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean launchApp(String testCaseName) {
        boolean value = false;
        System.out.println("Launching the App Activity");
        try {
//            device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
//            final String launcherPackage = device.getLauncherPackageName();
//            device.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), 0);
//            final Intent intent = context.getPackageManager().getLaunchIntentForPackage(GlobalVariables.appPackageName);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            context.startActivity(intent);
            device.executeShellCommand("monkey -p " + GlobalVariables.appPackageName + " -c android.intent.category.LAUNCHER 1");
            Timestamp alt = new Timestamp(new Date().getTime());
            System.out.println("App launch time" + alt);
            //           device.hasObject(By.pkg(GlobalVariables.appPackageName));
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "launchApp");
            captureEventAttributes.put("appLaunch", "Success");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("appLaunch");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            value = true;
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "launchApp");
            captureEventAttributes.put("appLaunch", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("appLaunch");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    @After
    public void closeAppOld() {
        try {
            device.executeShellCommand("am force-stop " + GlobalVariables.appPackageName);
            Log.d(GlobalVariables.tagName, "Closing the application");
        } catch (Exception e) {
            System.out.println("Error in closing the app");
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void closeApp() {
        try {
            Runtime.getRuntime().exec("am force-stop " + GlobalVariables.appPackageName);
            Log.d(GlobalVariables.tagName, "Closing the application");
        } catch (Exception e) {
            Log.d(GlobalVariables.tagName, "Error in closing the app");
            e.printStackTrace();
        }
    }

    private boolean fiveGMarkCheckingForHomeScreen(String testCaseName) {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId(GlobalVariables.homeElementID);
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().descriptionContains(GlobalVariables.secHomeElementID);
            UiObject secObject = device.findObject(secSelector);

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists() || secObject.exists()) {
                    HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                    captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                    captureEventAttributes.put("testCaseName", testCaseName);
                    captureEventAttributes.put("eventType", "appear");
                    captureEventAttributes.put("homeElementsAppear", "Yes");
                    captureEventAttributes.put("customer", "FiveGMark");

//                    event.setEventName("homeElementsAppear");
//                    event.setEventAttributes(captureEventAttributes);
//                    eventInitilizer.sendSingleEvent(event);
                    Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                    value = true;
                    break;
                } else {
                    if (stopWatch.getTime() >= OneMin_Timeout) {
                        HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                        captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                        captureEventAttributes.put("testCaseName", testCaseName);
                        captureEventAttributes.put("eventType", "appear");
                        captureEventAttributes.put("homeElementsAppear", "No");
                        captureEventAttributes.put("customer", "FiveGMark");

//                        event.setEventName("homeElementsAppear");
//                        event.setEventAttributes(captureEventAttributes);
//                        eventInitilizer.sendSingleEvent(event);
                        Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                    }
                }
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "appear");
            captureEventAttributes.put("homeElementsAppear", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");
//
//            event.setEventName("homeElementsAppear");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    private boolean fiveGMarkCheckAndClickTest(String testCaseName) {
        boolean value = false;
        try {
            Thread.sleep(2000);
            UiSelector selector = new UiSelector().resourceId(GlobalVariables.homeElementID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnTestButton", "Yes");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnTestButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                value = true;
            } else {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnTestButton", "No");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnTestButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "click");
            captureEventAttributes.put("clickOnTestButton", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("clickOnTestButton");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    private boolean fiveGMarkCheckAndClickIndoor(String testCaseName) {
        boolean value = false;
        try {
            Thread.sleep(2000);
            UiSelector selector = new UiSelector().textContains(GlobalVariables.indoorID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnIndoorButton", "Yes");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnIndoorButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                value = true;
            } else {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnIndoorButton", "No");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnIndoorButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "click");
            captureEventAttributes.put("clickOnIndoorButton", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("clickOnIndoorButton");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    private boolean fiveGMarkCheckResults(String testCaseName) {
        boolean value = false;
        try {
            Thread.sleep(2000);
            UiSelector selector = new UiSelector().descriptionContains(GlobalVariables.resultsID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(FiveMin_Timeout)) {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "appear");
                captureEventAttributes.put("resultsAppear", "Yes");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("resultsAppear");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                value = true;
            } else {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "appear");
                captureEventAttributes.put("resultsAppear", "No");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("resultsAppear");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "appear");
            captureEventAttributes.put("resultsAppear", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("resultsAppear");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    //Second Test Case Here

    private boolean fiveGMarkCheckAndClickSpeedTest(String testCaseName) {
        boolean value = false;
        try {
            Thread.sleep(2000);
            UiSelector selector = new UiSelector().textContains(GlobalVariables.speedTestID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickSpeedTestButton", "Yes");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickSpeedTestButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                value = true;
            } else {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickSpeedTestButton", "No");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickSpeedTestButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "click");
            captureEventAttributes.put("clickSpeedTestButton", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("clickSpeedTestButton");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }

    private boolean fiveGMarkCheckAndClickOutDoor(String testCaseName) {
        boolean value = false;
        try {
            Thread.sleep(2000);
            UiSelector selector = new UiSelector().textContains(GlobalVariables.outdoorID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnOutdoorButton", "Yes");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnOutdoorButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
                value = true;
            } else {
                HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
                captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                captureEventAttributes.put("testCaseName", testCaseName);
                captureEventAttributes.put("eventType", "click");
                captureEventAttributes.put("clickOnOutdoorButton", "No");
                captureEventAttributes.put("customer", "FiveGMark");

//                event.setEventName("clickOnOutdoorButton");
//                event.setEventAttributes(captureEventAttributes);
//                eventInitilizer.sendSingleEvent(event);
                Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            }
        } catch (Exception e) {
            HashMap<String, String> captureEventAttributes = new HashMap<String, String>();
            captureEventAttributes.put("dateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            captureEventAttributes.put("testCaseName", testCaseName);
            captureEventAttributes.put("eventType", "click");
            captureEventAttributes.put("clickOnOutdoorButton", "Failed");
            captureEventAttributes.put("customer", "FiveGMark");

//            event.setEventName("clickOnOutdoorButton");
//            event.setEventAttributes(captureEventAttributes);
//            eventInitilizer.sendSingleEvent(event);
            Log.d(GlobalVariables.tagName, String.valueOf(captureEventAttributes));
            e.printStackTrace();
        }
        return value;
    }


    public void resultInSpeedTest() {
        try {
//        File outDir = new File("/sdcard/QosbeeFiles");
            File outDir = new File(Environment.getExternalStorageDirectory() + File.separator + "QosbeeFiles");

            UiSelector result1 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_1");
            UiObject object1 = device.findObject(result1);

            UiSelector result2 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_value_1");
            UiObject object2 = device.findObject(result2);

            UiSelector result4 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_mini_techno_tv");
            UiObject object4 = device.findObject(result4);

            UiSelector result3 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_value_2");
            UiObject object3 = device.findObject(result3);

            String Latency = object1.exists() ? object1.getText() : "";
            String Download = object2.exists() ? object2.getText() : "";
            String Upload = object3.exists() ? object3.getText() : "";
            String Network = object4.exists() ? object4.getText() : "";

            if (!outDir.exists()) {
                outDir.mkdir();
            }
            String string = "Latency=" + Latency + "\n" + "Download=" + Download + "\n" + "Upload=" + Upload + "\n" + "Network=" + Network;
            if (object1.waitForExists(OneMin_Timeout)) {
                NetworkDetails networkDetails = new NetworkDetails();
                networkDetails.setLatency(Latency);
                networkDetails.setDownLoadSpeed(Download);
                networkDetails.setUploadSpeed(Upload);
                networkDetails.setIsp(Network);
                Thread.sleep(2000);
                FileOutputStream write = new FileOutputStream(new File(outDir, "aqua5gmark.txt"));
                write.write(Objects.requireNonNull(JsonUtil.toJson(networkDetails)).getBytes());
                write.close();
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.tagName, "Error is here...");
            Log.d(GlobalVariables.tagName, String.valueOf(e));
            e.printStackTrace();
        }
    }

    public void resultInFullTest() {
        try {
//        File outDir = new File("/sdcard/QosbeeFiles");
            File outDir = new File(Environment.getExternalStorageDirectory() + File.separator + "QosbeeFiles");
            Log.d(GlobalVariables.tagName, String.valueOf(outDir));

            UiSelector result1 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_1");
            UiObject object1 = device.findObject(result1);

            UiSelector result2 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_2");
            UiObject object2 = device.findObject(result2);

            UiSelector result4 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_4");
            UiObject object4 = device.findObject(result4);

            UiSelector result3 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_3");
            UiObject object3 = device.findObject(result3);

            UiSelector result5 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_icon_tv_5");
            UiObject object5 = device.findObject(result5);

            UiSelector result6 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_value_1");
            UiObject object6 = device.findObject(result6);

            UiSelector result7 = new UiSelector().resourceId("com.agence3pp:id/id_test_result_mini_techno_tv");
            UiObject object7 = device.findObject(result7);

            String Latency = object1.exists() ? object1.getText() : "";
            String Download = object2.exists() ? object2.getText() : "";
            String Upload = object3.exists() ? object3.getText() : "";
            String Stream = object4.exists() ? object4.getText() : "";
            String Web = object5.exists() ? object5.getText() : "";
            String Points = object6.exists() ? object6.getText() : "";
            String Network = object7.exists() ? object7.getText() : "";

            if (!outDir.exists()) {
                outDir.mkdir();
            }
            String string = "Latency=" + Latency + "\n" + "Download=" + Download + "\n" + "Upload=" + Upload + "\n" + "Stream=" + Stream + "\n"
                    + "Web=" + Web + "\n" + "Points=" + Points + "\n" + "Network=" + Network;
            if (object1.waitForExists(OneMin_Timeout)) {
                Thread.sleep(2000);
                FileOutputStream write = new FileOutputStream(new File(outDir, "aqua5gmarkFullTest.txt"));
                write.write(string.getBytes());
                write.close();
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.tagName, "Error is here...");
            Log.d(GlobalVariables.tagName, String.valueOf(e));
            e.printStackTrace();
        }
    }
}
