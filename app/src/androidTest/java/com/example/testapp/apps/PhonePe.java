package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class PhonePe implements AppClass {
    private Timestamp HPT, click_on_QR_time, MDT, click_on_pay_Time, T_T_G_P_S, click_on_check_balance_time, T_T_C_B;
    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = clickToQR();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find To QR Element");
                return 1;
            }
            runTest = clickOnUploadImage();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Click To Upload Image Element");
                return 1;
            }

            runTest = clickToSearchQR();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Search QR Element");
                return 1;
            }

            runTest = enterSearchQR();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Search QR ");
                return 1;
            }

            runTest = selectQrForPayment();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Select Qr For Payment  ");
                return 1;
            }

            runTest = merchantDetailsAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Merchant Details ");
                return 1;
            }

            runTest = enterAmount();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Amount ");
                return 1;
            }
            runTest = clickOnPay();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Click On Pay Element");
                return 1;
            }

            runTest = enterUpiPin();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Upi Pin");
                return 1;
            }

            runTest = clickOnEnter();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find On Enter Element ");
                return 1;
            }
            runTest = checkPaymentStatus();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Check Payment Status");
                return 1;
            }

            runTest = clickOnCheckBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find On Check Balance ");
                return 1;
            }

            runTest = enterUpiPin();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Upi Pin");
                return 1;
            }

            runTest = clickOnEnterForBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find On Enter Element for Balance");
                return 1;
            }

            runTest = checkBalance();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Check Balance");
                return 1;
            }

            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find KPI's");
                return 1;
            }

            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.PhonePe_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector homeElementId =
                    new UiSelector().textContains("To Mobile");
            UiObject homeElement = device.findObject(homeElementId);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (homeElement.exists()) {
                    HPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + HPT);
                    homeElementFound = true;
                    break;
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    public boolean clickToQR() {
        try {
            boolean toQRFound = false;
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector homeElementId =
                    new UiSelector().resourceId("com.phonepe.app:id/id_icon_pay_at_store");
            UiObject toMobile = device.findObject(homeElementId);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                if (toMobile.waitForExists(GlobalVariables.Wait_Timeout)) {
                    toMobile.click();
                    toQRFound = true;
                    break;
                }
        }
            return toQRFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on QR Button");
            return false;
        }
    }

    private boolean clickOnUploadImage() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("com.phonepe.app:id/pick_image");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clickOnUploadImage Drawer");
            return false;
        }
        return value;
    }

    public boolean clickToSearchQR() {
        try {
            boolean toSearchQRFound = false;
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector homeElementId =
                    new UiSelector().descriptionContains("Search");
            UiObject toMobile = device.findObject(homeElementId);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                if (toMobile.waitForExists(GlobalVariables.Wait_Timeout)) {
                    toMobile.click();
                    toSearchQRFound = true;
                    break;
                }
            }
            return toSearchQRFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on clickToSearchQR Button");
            return false;
        }
    }


    private boolean enterSearchQR() {
        boolean value = false;
        try {
           UiSelector selector = new UiSelector().text("Search…");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.setText("PhonePeQRCodeScanner");
                device.pressEnter();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in enterSearchQR element");
            return false;
        }
        return value;
    }

    private boolean selectQrForPayment() {
        boolean value = false;
        try {
            UiCollection elementID1 =
                    new UiCollection(new UiSelector().resourceId("com.google.android.documentsui:id/dir_list"));

            UiObject element1 = elementID1.getChildByInstance(new UiSelector(), 2);

            UiCollection elementID2 =
                    new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

            UiObject element2 = elementID2.getChildByInstance(new UiSelector(), 2);

            if (element1.waitForExists(GlobalVariables.HalfMin_Timout)) {
                click_on_QR_time = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On select QR Button Time:" + click_on_QR_time);
                element1.click();
                value = true;
            }else {
                click_on_QR_time = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On select QR Button Time:" + click_on_QR_time);
                element2.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in selectQrForPayment button");
        }
        return value;
    }

    private boolean merchantDetailsAppear() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().textContains("Send");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                MDT = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Merchant Details Time:" + MDT);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in merchantDetailsAppear text");
            return false;
        }
        return value;
    }

    private boolean enterAmount() {
        boolean value = false;
        try {
             UiSelector selector = new UiSelector().resourceId("com.phonepe.app:id/et_amount");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                object.setText("1");
                device.pressEnter();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in enterAmount Drawer");
            return false;
        }
        return value;
    }

    private boolean clickOnPay() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("com.phonepe.app:id/tv_action");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clickOnPay Drawer");
            return false;
        }
        return value;
    }


    private boolean enterUpiPin() {
        boolean value = false;
        try {
            UiSelector selector = new UiSelector().resourceId("com.phonepe.app:id/form_item_input");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.click();
//                device.pressKeyCode(KeyEvent.KEYCODE_0);
//                device.pressKeyCode(KeyEvent.KEYCODE_0);
//                device.pressKeyCode(KeyEvent.KEYCODE_5);
//                device.pressKeyCode(KeyEvent.KEYCODE_5);
//                device.pressKeyCode(KeyEvent.KEYCODE_9);
//                device.pressKeyCode(KeyEvent.KEYCODE_9);

                device.pressKeyCode(KeyEvent.KEYCODE_5);
                device.pressKeyCode(KeyEvent.KEYCODE_3);
                device.pressKeyCode(KeyEvent.KEYCODE_7);
                device.pressKeyCode(KeyEvent.KEYCODE_4);
                Thread.sleep(2000);

                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in enterUpiPin Drawer");
            return false;
        }
        return value;
    }


    public boolean clickOnEnter() {
        boolean value = false;
        try {
            //get adds element in home page
            UiSelector addsSelector = new UiSelector().className("android.widget.ImageView").index(2).clickable(true).focusable(true);
            UiObject addsObject = device.findObject(addsSelector);
            if(addsObject.waitForExists(GlobalVariables.OneMin_Timeout)){
                //remove adds in home page
                click_on_pay_Time = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On Pay Time:" + click_on_pay_Time);
                addsObject.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clickOnEnter Page");
        }
        return value;
    }

    private boolean checkPaymentStatus() {
        try {

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector viewersSelector = new UiSelector().textContains("successful.");
            UiObject viewersObject = device.findObject(viewersSelector);

            while (stopWatch.getTime() <= (GlobalVariables.TwoMins_Timeout)) {

                if (viewersObject.exists())
                {
                        Timestamp loading = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "payment_status Appear:YES Time: "  + loading);
                        T_T_G_P_S = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Payment Status Time:" + T_T_G_P_S);
                        return true;

                }
                else
                {
                    Timestamp loading = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "payment_status Appear:NO Time: "  + loading);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in checkPaymentStatus Reading");
            return false;
        }
    }

    public boolean clickOnCheckBalance() {
        try {
            boolean clickOnCheckBalance = false;
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector homeElementId =
                    new UiSelector().textContains("CHECK BALANCE");
            UiObject toMobile = device.findObject(homeElementId);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (toMobile.waitForExists(GlobalVariables.Wait_Timeout)) {
                    toMobile.click();

                    clickOnCheckBalance = true;
                    break;
                }
            }
            return clickOnCheckBalance;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on clickToMobile Button");
            return false;
        }
    }

    public boolean clickOnEnterForBalance() {
        boolean value = false;
        try {
            //get adds element in home page
            UiSelector addsSelector = new UiSelector().className("android.widget.ImageView").index(2).clickable(true).focusable(true);
            UiObject addsObject = device.findObject(addsSelector);
            if(addsObject.waitForExists(GlobalVariables.OneMin_Timeout)){
                //remove adds in home page
                click_on_check_balance_time = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On Check Balance Time:" + click_on_check_balance_time);
                addsObject.click();
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in clickOnEnter Page");
        }
        return value;
    }

    private boolean checkBalance() {
        boolean value = false;
        try {

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            UiSelector viewersSelector = new UiSelector().resourceId("com.phonepe.app:id/tv_bank_balance");
            UiObject viewersObject = device.findObject(viewersSelector);

            while (stopWatch.getTime() <= (GlobalVariables.OneMin_Timeout)) {
                if (viewersObject.exists()) {
                    T_T_C_B = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Check Balance Time:" + T_T_C_B);
                    Timestamp loading = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "checkBalance Appear:YES Time: "  + loading);
                    value = true;
                    break;
                }
                else {
                    Timestamp loading = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "checkBalance Appear:NO Time: "  + loading);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
        }
        return value;
    }


    private boolean kpiCalculation() {
        try {
            double TTLH = (HPT.getTime() - lt.getTime()) / 1000.0;
            double TTSC = (MDT.getTime() - click_on_QR_time.getTime()) / 1000.0;
            double TTGPS = (T_T_G_P_S.getTime() - click_on_pay_Time.getTime()) / 1000.0;
            double TTCB = (T_T_C_B.getTime() - click_on_check_balance_time.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Merchant Details From QR Code=" + TTSC);
            Log.d(GlobalVariables.Tag_Name, "Time To Complete Payment Transaction=" + TTGPS);
            Log.d(GlobalVariables.Tag_Name, "Time To Check Balance=" + TTCB);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }

}