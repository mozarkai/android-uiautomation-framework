package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class TimesNow implements AppClass {

  public static String testId;
  public String log = "";
  long timer, End_time_Video, click_on_video_time, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  boolean screenRecording, pcapFile;
  String location;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  private Intent intent;
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      runTest = shareAppear();
      if (!runTest) {
        return 1;
      }

      runTest = liveTV();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
      return 1;
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.TimesNow_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean shareAppear() {
    try {
      boolean shareButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find share button with Attempt no." + (i + 1));

          UiSelector shareId =
              new UiSelector().resourceId("com.timesnowmobile.TimesNow:id/iv_share");
          UiObject shareAppear = device.findObject(shareId);
          if (shareAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Share Button Appear Time:" + st);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            shareButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Share Button Appear Time:No");
        }
      }
      return shareButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Share Button Method");
      return false;
    }
  }

  private boolean liveTV() {
    try {
      boolean liveBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click Live TV with Attempt no.:" + (i + 1));
        try {

          UiSelector liveTvId =
              new UiSelector().resourceId("com.timesnowmobile.TimesNow:id/livetv");
          UiObject liveTvBtn = device.findObject(liveTvId);
          if (liveTvBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveTvBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on LiveTv:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            liveBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return liveBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in LiveTV");
      return false;
    }
  }

  private boolean bufferRead() {
    try {

      UiSelector loaderId =
          new UiSelector().resourceId("com.timesnowmobile.TimesNow:id/video_progress");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.timesnowmobile", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
