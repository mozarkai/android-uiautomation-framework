package com.example.testapp.apps;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.example.testapp.utility.UtilityClass;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class MPL_Tournament_1 implements AppClass {

    public static String testId;
    public static Timestamp hpt, hbt, het, sgt, gcat, gct, pbt, got, lbt;
    public String log = "";
    public Timestamp lt;
    public UiDevice device;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    File file;
    String OUTPUT_LOG_FILE = "";
    String appName;
    int n = 5;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String  startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            Log.d(GlobalVariables.Tag_Name, "Before Video Recording Start Time:" + new Timestamp(new Date().getTime()));
            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            Log.d(GlobalVariables.Tag_Name, "After Video Recording Start Time:" + new Timestamp(new Date().getTime()));

            String spinID = "Spin the Wheel!";
            String okayID = "Okay";
            String homePageID = "All Games";
            String selectGameID = "Ice Jump";
            String gameCardID = "Mozark Free Tournament";
            String playID = "Play";
//            String gameOverID = "Did you like playing";
            String gameOverID = "Rank";
            String leaderBoradID = "LEADERBOARD";
            String freeButtonID = "FREE";

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

            copyFromAsset(context,testId);

            runTest = loadHomePageWithText(device, homePageID, spinID, okayID);
            if (!runTest) {
                return 1;
            }

            runTest = selectGameWithText(device, selectGameID);
            if (!runTest) {
                return 1;
            }

            runTest = loadGameCardWithText(device, gameCardID);
            if (!runTest) {
                return 1;
            }

            Thread.sleep(5000);

            runTest = clickGameCard(device, gameCardID);
            if (!runTest) {
                return 1;
            }

            Thread.sleep(5000);

            runTest = clickPlayButtonWithText(device, playID);
            if (!runTest) {
                return 1;
            }

            runTest = gameOverWithText(device, gameOverID);
            if (!runTest) {
                return 1;
            }

            runTest = leaderboard(device, leaderBoradID);
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();

            runTest = kpiCalculation();
            if (!runTest) {
                return 1;
            }

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();

            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown(GlobalVariables.MPL_Package, device);

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest =
                    sendData(job_id, device_id, testId, "completed", false, order, script, appName, ipAdress, appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown(String Package, UiDevice device) {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(Package));
            Log.d(GlobalVariables.Tag_Name, "App Closed Successfully");
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(
            int job_id,
            String device_id,
            String testId,
            String status,
            boolean liveStream,
            String order,
            String script,
            String appName,
            String ipAdress, String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, status, liveStream, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    public boolean loadHomePageWithText(UiDevice device, String homePageID, String spinID, String okayID) {
        try {
            UiSelector homePageId = new UiSelector().textContains(homePageID);
            UiObject homePage = device.findObject(homePageId);

            UiSelector spinWheelId = new UiSelector().textContains(spinID);
            UiObject spinWheel = device.findObject(spinWheelId);

            UiSelector okayId = new UiSelector().textContains(okayID);
            UiObject okay = device.findObject(okayId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (homePage.exists()) {
                    hpt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
            return false;
        }
    }

    public boolean selectGameWithText(UiDevice device, String selectGameID) {
        try {
            boolean selectGameFound = false;
            for (int i = 0; i < n; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find select Game with Attempt no." + (i + 1));
                    UiSelector selectGameId = new UiSelector().textContains(selectGameID);
                    UiObject selectGame = device.findObject(selectGameId);
                    if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
                        selectGame.click();
                        sgt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
                        selectGameFound = true;
                        break;
                    } else {
                        UiScrollable scroll = new UiScrollable(
                                new UiSelector().className("android.widget.ScrollView"));
                        scroll.scrollTextIntoView(selectGameID);
                        if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
                            selectGame.click();
                            sgt = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
                            selectGameFound = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in Select Game");
                }
            }
            return selectGameFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Selecting Game");
            return false;
        }
    }

    public boolean loadGameCardWithText(UiDevice device, String gameCardID) {
        try {
            UiSelector gameCardId = new UiSelector().textContains(gameCardID);
            UiObject gameCard = device.findObject(gameCardId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (gameCard.exists()) {
                    gcat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Game Card Appear Time:" + gcat);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Game Card");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Game Card");
            return false;
        }
    }

    public boolean clickGameCard(UiDevice device, String gameCardID) {
        try {
            boolean gamecardFound = false;
            for (int i = 0; i < n; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find Game Card with Attempt no." + (i + 1));

                    UiSelector gameCardId = new UiSelector().textContains(gameCardID);
                    UiObject gameCard = device.findObject(gameCardId);
                    if (gameCard.waitForExists(GlobalVariables.Wait_Timeout)) {
                        gameCard.click();
                        gct = new Timestamp(new Date().getTime());
                        Log.d(
                                GlobalVariables.Tag_Name,
                                "Click on Game Card Time:" + gct);
                        gamecardFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in Game Card");
                }
            }
            return gamecardFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Game Card");
            return false;
        }
    }

    public boolean clickPlayButtonWithText(UiDevice device, String playButtonID) {
        try {
            boolean playButtonFound = false;
            for (int i = 0; i < n; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Play with Attempt no." + (i + 1));

                    UiSelector playButtonId = new UiSelector().text(playButtonID);
                    UiObject playButton = device.findObject(playButtonId);
                    if (playButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playButton.click();
                        pbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked On Play Button Time:" + pbt);
                        playButtonFound = true;
                        break;
                    } else {
                        UiObject freeButton = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(3).className("android.view.ViewGroup").index(2));
                        freeButton.click();
                        pbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked On Play Button Time:" + pbt);
                        playButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in Play Button");
                    e.printStackTrace();
                }
            }
            return playButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Play Button");
            return false;
        }
    }

    public boolean gameOverWithText(UiDevice device, String gameOverID) {
        try {
            UiSelector gameOverId = new UiSelector().textContains(gameOverID);
            UiObject gameOver = device.findObject(gameOverId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (gameOver.exists()) {
                    got = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Game Over:YES Time: " + got);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Game Over:NO");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Game Over");
            return false;
        }
    }

    public boolean leaderboard(UiDevice device, String leaderBoardID) {
        try {
            UiSelector leaderboardId = new UiSelector().textContains(leaderBoardID);
            UiObject leaderboard = device.findObject(leaderboardId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (leaderboard.exists()) {
                    lbt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Leaderboard Appear:YES Time:" + lbt);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Leaderboard Appear:NO");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Leadrboard");
            return false;
        }
    }

    public boolean kpiCalculation() {
        boolean kpiValue = false;
        try {

            double TLHP = (hpt.getTime() - ExampleInstrumentedTest.lt.getTime()) / 1000.0;
            double TLGP = (gcat.getTime() - sgt.getTime()) / 1000.0;
            double TLLP = (lbt.getTime() - got.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TLHP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Game Page=" + TLGP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Leaderboard Page=" + TLLP);

            kpiValue = true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Base KPI Calculation");
            e.printStackTrace();
            return false;
        }
        return kpiValue;
    }

    private static void copyFromAsset(Context context,String path)
    {
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        InputStream in = null;
        OutputStream out = null;
        String filename = "pcap.pcap";
        try
        {
            String pcapName=Utility.fileName(testId) + ".pcap.gz";
            in = assetManager.open(filename);
            out = new FileOutputStream(pcapName);
            File file=new File(pcapName);
            if(!file.exists())
            {
                file.createNewFile();
            }
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }
        catch(IOException e)
        {
            Log.e("tag", "Failed to copy asset file: " + filename, e);
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }
}
