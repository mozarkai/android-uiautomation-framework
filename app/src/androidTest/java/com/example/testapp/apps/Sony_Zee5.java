package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Sony_Zee5 implements AppClass {

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String OUTPUT_LOG_FILE = "";
  private Timestamp lt, st, vt, et, ht, it, het, sat;
  private UiDevice device;
  String startDate;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
                job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      runTest = homePageElements();
      if (!runTest) {
        return 1;
      }

      runTest = searchAppear();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = homeButton();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      runTest = selectVideo();
      if (!runTest) {
        return 1;
      }

      runTest = play();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.fourKPI(lt, st, et, sat, ht, het, vt);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Zee5_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean homePageElements() {
    try {
      boolean value = false;

//      UiSelector homeElementsID = new UiSelector().textContains("Play");
      UiSelector homeElementsID = new UiSelector().resourceId("com.graymatrix.did:id/cell_top_container");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          st = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
          value = true;
          break;
        } else {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Loading Home Page Elements");
        }
      }
      return value;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean searchAppear() {
    try {
      boolean value = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().resourceId("com.graymatrix.did:id/home_toolbar_search_icon");
          UiObject searchAppear = device.findObject(searchId);

          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Click on Search Appear Button Time:" + new Timestamp(new Date().getTime()));
            value = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in search Appear");
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean value = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));

          UiSelector enterSearchField =
                  new UiSelector().resourceId("com.graymatrix.did:id/searchBarText");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.setText("Kumkum Bhagya \n ");
            device.pressSearch();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            value = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      boolean value = false;

      UiSelector searchResultsID = new UiSelector().textContains("Shows");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          value = true;
          break;
        } else {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Loading Search Results");
        }
      }
      return value;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean homeButton() {
    try {
      boolean value = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Trying to click on Home button with Attempt no." + (i + 1));

          UiSelector homeID =
                  new UiSelector()
                          .resourceId("com.graymatrix.did:id/navigation_home");
          UiObject home = device.findObject(homeID);

          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            home.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
            value = true;
            break;
          }

        } catch (Exception e) {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Error in Home Button:" + new Timestamp(new Date().getTime()));
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
      return false;
    }
  }

  private boolean homeElements() {
    try {
      boolean value = false;

//      UiSelector homeElementsID = new UiSelector().textContains("Play");
      UiSelector homeElementsID = new UiSelector().resourceId("com.graymatrix.did:id/cell_top_container");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          het = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
          value = true;
          break;
        } else {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Loading Home Elements");
        }
      }
      return value;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Elements");
      return false;
    }
  }

  private boolean selectVideo() {
    try {
      boolean value = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to select video with Attempt no.:" + (i + 1));
        try {

          UiSelector videoId = new UiSelector().textContains("Movies");
          UiObject selectVideo = device.findObject(videoId);

          if (selectVideo.waitForExists(GlobalVariables.OneMin_Timeout)) {
            selectVideo.click();
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Select Video Time:" + new Timestamp(new Date().getTime()));
            value = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "select video Failed");
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in selecting video");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean value = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          UiSelector playId =
                  new UiSelector().resourceId("com.graymatrix.did:id/outlinedButton");
          UiObject playBtn = device.findObject(playId);

          if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            value = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead() {

    try {

//      UiSelector loaderId =
//          new UiSelector().resourceId("com.graymatrix.did:id/player_loading_progress");
      UiSelector loaderId =
              new UiSelector().resourceId("com.graymatrix.did:id/playerLoadingProgressBar");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      Timestamp loaderYes, loaderNo;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (st.getTime() - lt.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double TLP = (het.getTime() - ht.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Page = " + TLP);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
              job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
