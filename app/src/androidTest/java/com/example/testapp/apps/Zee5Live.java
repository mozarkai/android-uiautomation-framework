package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Zee5Live implements AppClass{

  // This is a New Zee5 Script

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String OUTPUT_LOG_FILE = "";
  private Timestamp lt, st, vt, et, ht, it, het, sat;
  private UiDevice device;
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", true, order, script, appVersion, appName, ipAdress,startDate);
      }
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      runTest = initialLoader();
      if (!runTest) {
        return 1;
      }
      runTest = homePageLoader();
      if (!runTest) {
        return 1;
      }
      runTest = searchAppear();
      if (!runTest) {
        return 1;
      }

      runTest = clickSearch();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = homeButton();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      //            Thread.sleep(3000);
      //            runTest = menuButton();
      //            if (!runTest) {
      //                return 1;
      //            }

      runTest = liveTV();
      if (!runTest) {
        return 1;
      }

      //            runTest = selectVideo();
      //            if (!runTest) {
      //                return 1;
      //            }

      runTest = play();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.fourKPI(lt, st, et, sat, ht, het, vt);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest =  sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Zee5_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean initialLoader() {
    boolean initial_loader = false;

    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        UiSelector loaderId = new UiSelector().resourceId("com.graymatrix.did:id/progress_bar");
        UiObject loader = device.findObject(loaderId);
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Initial Loader:YES " + loaderYes);
          Thread.sleep(500);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Initial Loader:NO " + loaderNo);
          initial_loader = true;
          break;
        }
      }

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in finding Initial Loader");
      return initial_loader;
    }
    return initial_loader;
  }

  private boolean homePageLoader() {
    boolean home_page_loader = false;
    try {

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        UiSelector loaderId =
            new UiSelector().resourceId("com.graymatrix.did:id/compoiste_progress_bar");
        UiObject loader = device.findObject(loaderId);
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "HomePage Loader:YES " + loaderYes);
          Thread.sleep(500);
        } else {
          st = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "HomePage Loader:NO " + st);
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
          home_page_loader = true;
          break;
        }
      }

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in finding Home Page Loader");
    }
    return home_page_loader;
  }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          //                    UiSelector searchId = new
          // UiSelector().resourceId("com.graymatrix.did:id/action_bar_search");
          //                    UiObject searchAppear = device.findObject(searchId);

          UiCollection searchId =
              new UiCollection(new UiSelector().resourceId("com.graymatrix.did:id/toolbar"));
          UiObject searchAppear = searchId.getChildByInstance(new UiSelector(), 4);

          //                    UiSelector searchId = new
          // UiSelector().className("android.widget.ImageView");
          //                    UiObject searchAppear = device.findObject(searchId);

          //                    for (int j=0; j<20;j++){
          //                        searchAppear.click();
          //                        Log.d(GlobalVariables.Tag_Name,"J value is"+ j);
          //                    }

          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)
              && searchAppear.isEnabled()
              && searchAppear.isFocusable()
              && searchAppear.isClickable()) {
            searchAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Click on SearchAppear:" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in search Appear");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean clickSearch() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to Click search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().resourceId("com.graymatrix.did:id/searchView");
          UiObject searchClick = device.findObject(searchId);

          //                    UiCollection searchId = new UiCollection(new
          // UiSelector().resourceId("com.graymatrix.did:id/searchView"));
          //                    UiObject searchAppear = searchId.getChildByInstance(new
          // UiSelector(),0);

          if (searchClick.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchClick.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Click on 1st Search" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in 1st Search Click");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking on 1st Search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));

          UiSelector enterSearchField =
              new UiSelector().resourceId("com.graymatrix.did:id/searchBar");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          //                    UiSelector enterSearchField = new
          // UiSelector().textContains("Trending Searches");
          //                    UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.setText("Kumkum Bhagya \n ");
            device.pressBack();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      UiSelector searchResultsID = new UiSelector().resourceId("com.graymatrix.did:id/list");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Search Results:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean homeButton() {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Home button with Attempt no." + (i + 1));

          //                    UiSelector homeID = new UiSelector().text("Home");
          //                    UiObject home = device.findObject(homeID);

          UiCollection homeID =
              new UiCollection(
                  new UiSelector()
                      .resourceId("com.graymatrix.did:id/bb_bottom_bar_item_container"));
          UiObject home = homeID.getChildByInstance(new UiSelector(), 1);

          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            home.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in Home Button:" + new Timestamp(new Date().getTime()));
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
      return false;
    }
  }

  private boolean homeElements() {
    try {
      UiSelector homeElementsID = new UiSelector().resourceId("com.graymatrix.did:id/pager");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          het = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean menuButton() {
    try {
      boolean menuButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Menu button with Attempt no." + (i + 1));

          //                    UiCollection menuID = new UiCollection(new
          // UiSelector().resourceId("com.graymatrix.did:id/action_menu"));
          //                    UiObject menu = menuID.getChildByInstance(new UiSelector(), 0);
          //
          //                    if (menu.waitForExists(GlobalVariables.TimeOut)) {
          //                        menu.click();
          //                        Log.d(GlobalVariables.Tag_Name, "Click on Menu Button:" + new
          // Timestamp(new Date().getTime()));
          //                        menuButtonFound = true;
          //                        break;
          //                    }

          //  items.scrollToEnd(3);
          // items.scrollForward(20);

          //                    items.scrollToEnd(10,20);
          //
          //                    items.scrollToEnd(10,20);
          //
          //                    items.scrollToEnd(10,20);
          //                    //  items.scrollToEnd(5,20);

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Finding Menu Button");
        }
      }
      return menuButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Menu Button Method");
      return false;
    }
  }

  private boolean liveTV() {
    try {
      boolean liveBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click Live TV with Attempt no.:" + (i + 1));
        try {

          UiScrollable items =
              new UiScrollable(new UiSelector().resourceId("com.graymatrix.did:id/tabLayout"));
          items.setAsHorizontalList();
          items.scrollToEnd(10, 20);
          UiObject liveTvBtn = device.findObject(new UiSelector().textContains("Live TV"));

          if (liveTvBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveTvBtn.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Click on Live Tv:" + new Timestamp(new Date().getTime()));
            liveBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return liveBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in LiveTV");
      return false;
    }
  }

  private boolean selectVideo() {
    try {
      boolean selectVideoBtn = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to select video with Attempt no.:" + (i + 1));
        try {

          UiCollection videoId =
              new UiCollection(new UiSelector().resourceId("com.graymatrix.did:id/recyclerView"));
          UiObject selectVideo = videoId.getChildByInstance(new UiSelector(), 1);

          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Select Video Time:" + new Timestamp(new Date().getTime()));
            selectVideoBtn = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "select video Failed");
        }
      }
      return selectVideoBtn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in selecting video");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          //                    UiCollection playId = new UiCollection(new
          // UiSelector().resourceId("com.graymatrix.did:id/item_image"));
          //                    UiObject playBtn = playId.getChildByInstance(new UiSelector(), 5);

          UiSelector playId = new UiSelector().text("Zee News");
          UiObject playBtn = device.findObject(playId);

          //                    for (int j=0; j<20;j++){
          //
          //                        playBtn.click();
          //                        Log.d(GlobalVariables.Tag_Name,"J value is"+ j);
          //                    }

          if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead() {

    try {

      UiSelector loaderId =
          new UiSelector().resourceId("com.graymatrix.did:id/player_loading_progress");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.graymatrix", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }

      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (st.getTime() - lt.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double TLP = (het.getTime() - ht.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Page = " + TLP);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", true, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
