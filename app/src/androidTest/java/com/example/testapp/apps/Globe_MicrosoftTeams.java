package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Globe_MicrosoftTeams implements AppClass {
    Timestamp homeElementsAppearTime,clickSearchTime,enterSearchTime,messageDeliveredTime,messageSendTime,
            deliveredImageTime,clickSendImageTime,deliveredLocationTime ,clickCurrentLocationTime ;
    boolean sendLocation = false;
    boolean sendImage = false;

    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, het, mbt, wvt, vat, lbt, lvat, st, est, sat, pvt, vst;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homePage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to display Home element");
                return 1;
            }
            runTest = checkExistingConvo();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to check existing convo");
                return 1;
            }
            runTest = searchContact();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Check Search Contact");
                return 1;
            }
            runTest = selectContact();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Check Select Contact");
                return 1;
            }
            runTest = clickMessageIcon();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Message Icon");
                return 1;
            }
            runTest = startMessage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Start Message");
                return 1;
            }
            runTest = enterMessageText();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Enter Message");
                return 1;
            }
            runTest = sendMessage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Send Message");
                return 1;
            }
            runTest = checkMessageSent();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Check Message");
                return 1;
            }
            runTest = attach();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Attach");
                return 1;
            }
            runTest = clickMedia();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Media");
                return 1;
            }
            runTest = openLibrary();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Open Gallery");
                return 1;
            }
            runTest = gotoGallery();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to go to Gallery");
                return 1;
            }
            runTest = gotoDownloads();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to go Downloads");
                return 1;
            }
            runTest = clickDownloads();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to click Downloads");
                return 1;
            }
            runTest = searchFoler();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to search Folder");
                return 1;
            }
            runTest = Image5mbDisplayed();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Display Image5mb");
                return 1;
            }
            runTest = clickImage5mbDisplayed();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Click Image5mb");
                return 1;
            }
            runTest = sendImage();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Send Image");
                return 1;
            }
            runTest = deliveredPhotoDisplayed();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Delivered Photo");
                return 1;
            }
            runTest = clickLocation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to click sticker Button");
                return 1;
            }
            runTest = sendLocation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to send location");
                return 1;
            }
            runTest = deliveredLocation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to delivered location");
                return 1;
            }
            runTest = kpiCalculation();
            if(!runTest){
                DataHolder.getInstance().setFailureReason("Unable to Calculate KPI's");
                return 1;
            }

            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.MicrosoftTeams_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }


    public boolean homePage() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Loading Home Screen");
                UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/chat_item_container");
                UiObject object = device.findObject(selector);

                UiSelector secSelector = new UiSelector().resourceId("com.microsoft.teams:id/action_bar_search_action");
                UiObject secObject = device.findObject(secSelector);
                if (object.exists() || secObject.exists()) {
                    homeElementsAppearTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + homeElementsAppearTime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Home Page");
            return value;
        }
    }

    public boolean checkExistingConvo(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Checking..");
                UiSelector selectorSec = new UiSelector().textContains("Globe Aquamark");
                UiObject objectSec = device.findObject(selectorSec);

                UiSelector deleteSelector = new UiSelector().textContains("Delete");
                UiObject deleteObject = device.findObject(deleteSelector);

                UiSelector okSelector = new UiSelector().resourceId("android:id/button1");
                UiObject okObject = device.findObject(okSelector);

                if(objectSec.exists()){
                    objectSec.dragTo(objectSec,1);

                    Thread.sleep(2000);
                    if (deleteObject.exists())
                    {
                        deleteObject.click();

                        Thread.sleep(2000);
                        if (okObject.exists())
                        {
                            okObject.click();
                            Timestamp unUsableTime = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name,"Long Press Successfully Time:" + unUsableTime);
                            Thread.sleep(5000);
                            if (stopWatch.getTime() > 50000)
                            {
                                value = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in long pressing");
        }
        return value;
    }

    public boolean searchContact(){
        boolean value = false;
        try{
            Log.d(GlobalVariables.Tag_Name, "Enter Contact");
            UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/action_bar_search_action");
            UiObject object = device.findObject(selector);

            UiSelector selectorSec = new UiSelector().className("android.widget.EditText");
            UiObject objectSec = device.findObject(selectorSec);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                objectSec.setText("Globe Aquamark");
                enterSearchTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Entered Contact Successfully Time:" + enterSearchTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error Entering Contact text");
        }
        return value;
    }

    public boolean selectContact(){
        boolean value = false;
        try{
            Log.d(GlobalVariables.Tag_Name, "Selecting Contact");
            UiSelector selector = new UiSelector().descriptionContains("Globe Aquamark");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                enterSearchTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Select Contact Successfully Time:" + enterSearchTime);
                value = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error In Selecting Contact");
        }
        return value;
    }

    public boolean clickMessageIcon(){
        boolean value = false;
        try{
            Log.d(GlobalVariables.Tag_Name, "Clicking Message Icon");
            UiSelector selector = new UiSelector().descriptionContains("Open private chat");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                Timestamp contactSelectedTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Clicked icon successfully:" + contactSelectedTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Contact Selected");
        }
        return  value;
    }

    public boolean startMessage(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                Timestamp contactSelectedTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Contact Selected Appeared Time:" + contactSelectedTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in View Contact Selected");
        }
        return  value;
    }

    public boolean enterMessageText(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                object.setText("Sending Microsoft Teams Text Message");
                Timestamp enterTextTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Enter Text Message Successfully Time:" + enterTextTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Entering Text Message");
        }
        return  value;
    }

    public boolean sendMessage(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/message_area_send_btn");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                if (sendImage == false)
                {
                    messageSendTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Message send Successfully Time:" + messageSendTime);
                    value = true;
                }
                else
                {
                    clickSendImageTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Click Send Successfully Time:" + clickSendImageTime);
                    value = true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Sending Message");
        }
        return  value;
    }

    public boolean checkMessageSent(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector messageSent = new UiSelector().descriptionContains("Sent").resourceId("com.microsoft.teams:id/chat_message_sending_indicator");
                UiObject objectSent = device.findObject(messageSent);

                UiSelector messageLoading = new UiSelector().textContains("Loading");
                UiObject objectLoading = device.findObject(messageLoading);

                if(objectSent.exists() && objectLoading.exists() == false){
                    messageDeliveredTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Message Sent successfully Time:" + messageDeliveredTime);
                    value = true;
                    if (sendImage == false)
                    {
                        holdMessage();
                    }
                    break;
                }
            }
            return  value;

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Delivering Message");
        }
        return  value;
    }

    public boolean holdMessage(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector messageSent = new UiSelector().descriptionContains("Sending Microsoft Teams Text Message");
                UiObject objectSent = device.findObject(messageSent);

                UiSelector deleteIconSelector = new UiSelector().textContains("Delete");
                UiObject deleteIconObject = device.findObject(deleteIconSelector);

                UiSelector DELETESelector = new UiSelector().textContains("DELETE").resourceId("android:id/button1");
                UiObject DELETEObject = device.findObject(DELETESelector);

                objectSent.dragTo(objectSent,1);

                if(deleteIconObject.exists()){
                    deleteIconObject.click();
                    Thread.sleep(2000);

                    if (DELETEObject.exists())
                    {
                        DELETEObject.click();
                        Timestamp unUsableTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Delete successfully Time:" + unUsableTime);
                        value = true;
                        break;
                    }
                }
            }
            return  value;

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Delivering Message");
        }
        return  value;
    }

    public boolean attach(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/message_area_plus_btn");
                UiObject object = device.findObject(selector);

                UiSelector mediaSelector = new UiSelector().textContains("Media");
                UiObject mediaObject = device.findObject(mediaSelector);

                if(object.exists()){
                    object.click();
                    Timestamp ClickAttachTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Click Attach Button Successfully Time:" + ClickAttachTime);
                    value = true;

                    if (mediaObject.exists())
                    {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Attach Button");
        }
        return  value;
    }
    public boolean clickMedia(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector messageSent = new UiSelector().textContains("Media");
                UiObject objectSent = device.findObject(messageSent);

                if(objectSent.exists()){
                    objectSent.click();
                    Timestamp galleryDisplayedTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Media successfully Time:" + galleryDisplayedTime);
                    value = true;
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Clicking Media");
        }
        return  value;
    }

    public boolean openLibrary(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().descriptionContains("library");
                UiObject object = device.findObject(selector);

                if(object.exists()){
                    object.click();
                    Timestamp galleryDisplayedTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Library successfully Time:" + galleryDisplayedTime);
                    value = true;
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Clicking Library");
        }
        return  value;
    }

    public boolean gotoGallery(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().descriptionContains("Gallery");
                UiObject object = device.findObject(selector);

                if(object.exists()){
                    object.click();
                    Timestamp galleryDisplayedTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Gallery successfully Time:" + galleryDisplayedTime);
                    value = true;
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Clicking Gallery");
        }
        return  value;
    }

    public boolean gotoDownloads(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().descriptionContains("Show roots");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                Timestamp ClickFolderTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click Roots Successfully Time:" + ClickFolderTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Roots");
        }
        return  value;
    }

    public boolean clickDownloads(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("Downloads").resourceId("android:id/title").index(0);
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                Timestamp ClickFolderTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click Downloads Successfully Time:" + ClickFolderTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Downloads");
        }
        return  value;
    }

    public boolean searchFoler(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().descriptionContains("Search");
            UiObject object = device.findObject(selector);

            UiSelector textFieldSelector = new UiSelector().className("android.widget.EditText");
            UiObject textFieldObject = device.findObject(textFieldSelector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                textFieldObject.setText("image5mb");
                device.pressEnter();
                Timestamp ClickFolderTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click Search Successfully Time:" + ClickFolderTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Folder");
        }
        return  value;
    }
    public boolean Image5mbDisplayed(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().textContains("image5mb").className("android.widget.TextView");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                Thread.sleep(2000);
                object.click();
                Timestamp ClickFolderTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Image5mb Displayed Successfully Time:" + ClickFolderTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Folder");
        }
        return  value;
    }
    public boolean clickImage5mbDisplayed(){
        boolean value = false;
        try{

            UiSelector selector = new UiSelector().resourceId("com.google.android.documentsui:id/icon_thumb");
            UiObject object = device.findObject(selector);

            if(object.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                Timestamp ClickFolderTime = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click Image Successfully Time:" + ClickFolderTime);
                value = true;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Image");
        }
        return  value;
    }
    public boolean sendImage(){
        boolean value = false;
        try{
            UiSelector selector = new UiSelector().textContains("Done");
            UiObject object = device.findObject(selector);

            UiSelector selectorSec = new UiSelector().resourceId("com.microsoft.teams:id/gpuImageView");
            UiObject objectSec = device.findObject(selectorSec);

            if(objectSec.waitForExists(GlobalVariables.OneMin_Timeout)){
                object.click();
                value = true;
                sendImage = true;
                sendMessage();
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Click Image");
        }
        return  value;
    }
    public boolean deliveredPhotoDisplayed(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector messageSent = new UiSelector().descriptionContains("Sent").resourceId("com.microsoft.teams:id/chat_message_sending_indicator");
                UiObject objectSent = device.findObject(messageSent);
                if(objectSent.exists()){
                    deliveredImageTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Delivered Photo successfully Time:" + deliveredImageTime);
                    value = true;
                    attach();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Delivering Photo");
        }
        return  value;
    }
    public boolean clickLocation(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                UiSelector selector = new UiSelector().textContains("Location");
                UiObject object = device.findObject(selector);

                if(object.exists()){
                    object.click();
                    Timestamp stickerTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Location button clicked successfully Time:" + stickerTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Clicking Location button");
        }
        return  value;
    }

    public boolean sendLocation(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/share_nearby_place_location");
                UiObject object = device.findObject(selector);

                UiSelector selectorSec = new UiSelector().textContains("Send your current location");
                UiObject objectSec= device.findObject(selectorSec);
                if(object.exists()){
                    objectSec.click();
                    clickCurrentLocationTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Location send successfully Time:" + clickCurrentLocationTime);
                    value = true;
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in sending Location button");
        }
        return  value;
    }

    public boolean deliveredLocation(){
        boolean value = false;
        try{
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector selector = new UiSelector().resourceId("com.microsoft.teams:id/info");
                UiObject object = device.findObject(selector);

                UiSelector messageSent = new UiSelector().descriptionContains("Sent").resourceId("com.microsoft.teams:id/chat_message_sending_indicator");
                UiObject objectSent = device.findObject(messageSent);
                if(object.exists()){
                    if (objectSent.exists())
                    {
                        deliveredLocationTime = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Delivered Location successfully Time:" + deliveredLocationTime);
                        value = true;
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d(GlobalVariables.Tag_Name,"Error in Delivering Photo");
        }
        return  value;
    }

    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (homeElementsAppearTime.getTime() - lt.getTime()) / 1000.0;
            double MST = (messageDeliveredTime.getTime() - messageSendTime.getTime()) / 1000.0;
            double IST = (deliveredImageTime.getTime() - clickSendImageTime.getTime()) / 1000.0;
            double LST = (deliveredLocationTime.getTime() - clickCurrentLocationTime.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Send Message=" + MST);
            Log.d(GlobalVariables.Tag_Name, "Time To Send 5Mb File=" + IST);
            Log.d(GlobalVariables.Tag_Name, "Time To Send Location=" + LST);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }

}