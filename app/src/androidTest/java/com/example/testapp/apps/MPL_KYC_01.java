package com.example.testapp.apps;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;


import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_KYC_01 extends MPL_BaseTest {
    private int n = 5;

    public int testRun(UiDevice device, Timestamp lt) {
        boolean runTest = false;

        //kyc
        String walletIcon = "";
        String WalletPageText = "Wallet";
        String withdrawText = "Withdraw";
        String goToKYCButton = "GO TO KYC";
        String kycVerificationText = "KYC Verification";
        String govtIdProofText = "Government ID Proof";
        String uploadIdProofYText = "UPLOAD ID PROOF";
        String selectIdType = "Select ID Type";
        String idTypeOkayButton = "OKAY";
        String idProofNumberTextBox = "ID Proof Number";
        String firstNameTextBox = "First Name";
        String lastNameTextBox = "Last Name";
        String selectYourStateDropDownText = "Select Your State";
        String selectStateOkayButton = "OKAY";
        String DOBText = "Date of Birth";
        String DOBDate = "01-01-1990";
        String year = "1990";
        String year_id = "android:id/date_picker_header_year";
        String year_xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]";
        int index = 0;
        String year1992 = "1992";
        String okText = "OK";
        String FrontSidePhotoId = "Front Side Photo of ID Proof";

        Log.d(GlobalVariables.Tag_Name, "Executing kyc tests");

        runTest = clickButtonWithText(device,walletIcon,"Wallet icon","Successfully clicked on Wallet icon","Unable to click on Wallet icon");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,WalletPageText,"WalletPageText","Successfully Wallet page is loaded","Unable to load Wallet page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, withdrawText,  "withdrawTextLink", "Successfully clicked on withdrawTextLink ", "Unable to click on withdrawTextLink");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, goToKYCButton,  "goToKYCButton", "Successfully clicked on goToKYCButton ", "Unable to click on goToKYCButton");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,kycVerificationText,"kycVerificationText","Successfully kycVerification page is loaded","Unable to load kycVerification page");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,govtIdProofText,"govtIdProofText","Successfully loaded Govt Id Proof page","Unable to load Govt Id Proof page");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device, uploadIdProofYText,  "uploadIdProofYText", "Successfully loaded on uploadIdProof page ", "Unable to load on uploadIdProof page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, selectIdType,  "selectIdType", "Successfully clicked on selectIdType ", "Unable to click on selectIdType");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, idTypeOkayButton,  "idTypeOkayButton", "Successfully clicked on idTypeOkayButton ", "Unable to click on idTypeOkayButton");
        if (!runTest) {
            return 1;
        }

        runTest = typeTextWithText(device,idProofNumberTextBox,"","idProofNumberTextBox","Successfully entered id proof number","Unable to enter id proof numberUnable to enter id proof number");
        if (!runTest) {
            return 1;
        }

        runTest = typeTextWithText(device,firstNameTextBox,"xyz","firstNameTextBox","Successfully entered first name","Unable to enter first name");
        if (!runTest) {
            return 1;
        }
        runTest = typeTextWithText(device,lastNameTextBox,"xyz","lastNameTextBox","Successfully entered last name","Unable to enter last name");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, selectYourStateDropDownText,  "selectYourStateDropDownText", "Successfully clicked on selectYourStateDropDownText ", "Unable to click on selectYourStateDropDownText");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, selectStateOkayButton,  "selectStateOkayButton", "Successfully clicked on selectStateOkayButton ", "Unable to click on selectStateOkayButton");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, FrontSidePhotoId,  "FrontSidePhotoId", "Successfully clicked on FrontSidePhotoId ", "Unable to click on FrontSidePhotoId");
        if (!runTest) {
            return 1;
        }
        return 0;
    }

}
