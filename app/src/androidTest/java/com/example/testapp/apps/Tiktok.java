package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;


public class Tiktok implements AppClass {

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String  startDate;
    String appName;
    //    private Intent intent;
    private Timestamp lt, heat, wvt, vat, hbt, hvat, st, est, sat, pvt, vst, ht, bbt, fbt, tct;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }

            runTest = searchAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
                return 1;
            }

            runTest = enterSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                return 1;
            }

            runTest = searchResults();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Search Results");
                return 1;
            }

            Thread.sleep(5000);

            runTest = hashTag();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find HashTag Button");
                return 1;
            }

            runTest = hashtagAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find HashTag Results");
                return 1;
            }

            Thread.sleep(5000);

            runTest = watchVideos();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Videos Button");
                return 1;
            }

            runTest = videoAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Videos");
                return 1;
            }

            runTest = backButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Back Button");
                return 1;
            }

            runTest = homeButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Button");
                return 1;
            }

            runTest = following();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Following Button");
                return 1;
            }

            runTest = trendingCreator();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Trending Creators");
                return 1;
            }


            stopScreenRecording();
            runTest = kpiCalculation();
            if (!runTest) {
                return 1;
            }

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Tiktok_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector homeElementId =
                        new UiSelector().textContains("For You");
                UiObject homeElement = device.findObject(homeElementId);
                if (homeElement.exists()) {
                    heat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + heat);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    private boolean searchAppear() {
        try {
            boolean searchButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));
                    UiSelector searchId = new UiSelector().textContains("Discover");
                    UiObject searchAppear = device.findObject(searchId);

                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
                        searchAppear.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Search Button Appear Time" + st);
                        searchButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in search Appear");
                }
            }
            return searchButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
            return false;
        }
    }

    private boolean enterSearch() {
        try {
            boolean enterSearchtext = false;
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no.");
                    UiSelector enterSearchField = new UiSelector().resourceId("com.ss.android.ugc.trill:id/eq7");
                    UiObject enterSearch_text = device.findObject(enterSearchField);

                    UiSelector enterText = new UiSelector().className("android.widget.EditText");
                    UiObject objectEnterText = device.findObject(enterText);

                    UiSelector clickSearch = new UiSelector().className("android.widget.TextSwitcher");
                    UiObject clickObjectSearch = device.findObject(clickSearch);

                    UiSelector searchSelector = new UiSelector().textContains("Search");
                    UiObject searchObject = device.findObject(searchSelector);

                    if (enterSearch_text.exists()) {
                        enterSearch_text.click();
                        Log.d(GlobalVariables.Tag_Name, "Entered one");
                        objectEnterText.setText("Football");
                        Thread.sleep(2000);
                        searchObject.click();
                        est = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Enter Search Time" + est);
                        enterSearchtext = true;
                        break;
                    }
                    else if (clickObjectSearch.exists())
                    {
                        clickObjectSearch.click();
                        Log.d(GlobalVariables.Tag_Name, "Entered two");
                        objectEnterText.setText("Football");
                        Thread.sleep(2000);
                        searchObject.click();
                        est = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name,"Enter Search Time" + est);
                        enterSearchtext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed"+ " "+ e.getMessage());
                }
            }
            return enterSearchtext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
    }

    private boolean searchResults() {
        boolean value = false;
        try {
            UiSelector searchResultsID =
                    new UiSelector().resourceId("com.ss.android.ugc.trill:id/bmb");
            UiObject searchResults = device.findObject(searchResultsID);

            UiSelector searchResultsID1 =
                    new UiSelector().textContains("K");
            UiObject searchResults1 = device.findObject(searchResultsID1);

            UiSelector searchResultsID2 =
                    new UiSelector().textContains("M");
            UiObject searchResults2 = device.findObject(searchResultsID2);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (searchResults.exists() || searchResults1.exists() || searchResults2.exists()) {
                    sat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search #Tag Result Time" + sat);
                    value = true;
                    break;
                } else {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Loading Search Results");
                    Thread.sleep(200);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return value;
        }
    }

    private boolean watchVideos() {
        try {
            boolean watchVideoFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Watch Videos button with Attempt no." + (i + 1));
                    UiSelector watchVideoId = new UiSelector().textContains("VIDEOS");
                    UiObject watchVideo = device.findObject(watchVideoId);

                    if (watchVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
                        watchVideo.click();
                        wvt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Watched Videos Time:" + wvt);
                        watchVideoFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Watch Videos");
                }
            }
            return watchVideoFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Watch Videos Method");
            return false;
        }
    }

    private boolean videoAppear() {
        boolean videoAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector videosId =
                        new UiSelector().resourceId("com.ss.android.ugc.trill:id/bmb");
                UiObject videoAppear = device.findObject(videosId);

                UiSelector videosID1 =
                        new UiSelector().textContains("K");
                UiObject videoAppear1 = device.findObject(videosID1);

                UiSelector videosID2 =
                        new UiSelector().textContains("M");
                UiObject videoAppear2 = device.findObject(videosID2);

                if (videoAppear.exists() || videoAppear1.exists() || videoAppear2.exists()) {
                    vat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Videos Appear Time:" + vat);
                    videoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Videos");
                    Thread.sleep(200);
                }
            }
            return videoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Videos");
            return false;
        }
    }

    private boolean hashTag() {
        try {
            boolean hashtagButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Hashtag button with Attempt no." + (i + 1));
                    UiSelector hashtagButtonId = new UiSelector().textContains("HASHTAGS");
                    UiObject hashtagButton = device.findObject(hashtagButtonId);

                    if (hashtagButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        hashtagButton.click();
                        hbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Hashtag Button Time:" + hbt);
                        hashtagButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Hashtag Button");
                }
            }
            return hashtagButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Hashtag Button Method");
            return false;
        }
    }

    private boolean hashtagAppear() {
        boolean hashtagAppearFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                UiSelector hashtagViewsId =
                        new UiSelector().textContains("views");
                UiObject hashViews = device.findObject(hashtagViewsId);
                if (hashViews.exists()) {
                    hvat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Hashtags Views Appear Time:" + hvat);
                    hashtagAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading HashTags");
                    Thread.sleep(200);
                }
            }
            return hashtagAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding HashTag Results");
            return false;
        }
    }

    private boolean backButton() {
        try {
            boolean backButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name,"Trying to click on Back button with Attempt no." + (i + 1));
                    device.pressBack();
                    Thread.sleep(2000);
                    device.pressBack();
                    Thread.sleep(2000);
                    device.pressBack();
                    bbt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Back Button:" + bbt);
                    backButtonFound = true;
                    break;
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click Back Button Failed");
                }
            }
            return backButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Back Button Method");
            return false;
        }
    }

    private boolean homeButton() {
        try {
            boolean homeButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to click on Home button with Attempt no." + (i + 1));
                    UiSelector homeID = new UiSelector().textContains("Home");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();
                        ht = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Click Home Button Time" + ht);
                        homeButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Home Button");
                }
            }
            return homeButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
            return false;
        }
    }

    private boolean following() {
        try {
            boolean followingButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to click on Following button with Attempt no." + (i + 1));
                    UiSelector followingId = new UiSelector().textContains("Following");
                    UiObject following = device.findObject(followingId);
                    if (following.waitForExists(GlobalVariables.Wait_Timeout)) {
                        following.click();
                        fbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Following Button Time:" + fbt);
                        followingButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Following Button");
                }
            }
            return followingButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Following Button Method");
            return false;
        }
    }

    private boolean trendingCreator() {
        boolean value = false;
        try {
            UiSelector trendingCreatorID = new UiSelector().resourceId("com.ss.android.ugc.trill:id/e0n");
            UiObject trendingCreator = device.findObject(trendingCreatorID);
            UiSelector trendingCreatorID1 = new UiSelector().text("Follow");
            UiObject trendingCreator1 = device.findObject(trendingCreatorID1);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (trendingCreator.exists() || trendingCreator1.exists()) {
                    tct = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Trending Creators Appear Time:" + tct);
                    value = true;
                    break;
                } else {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Loading Trending Creators");
                    Thread.sleep(200);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Trending Creators");
            return value;
        }
    }

    private boolean kpiCalculation() {
        try {
            double TTLH = (heat.getTime() - lt.getTime()) / 1000.0;
            double SRT = (sat.getTime() - est.getTime()) / 1000.0;
            double TTLV = (vat.getTime() - wvt.getTime()) / 1000.0;
            double TTHT = (hvat.getTime() - hbt.getTime()) / 1000.0;
            double TCT = (tct.getTime() - fbt.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Video Page=" + TTLV);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Hashtag List=" + TTHT);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Trending Creators Page=" + TCT);

            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
