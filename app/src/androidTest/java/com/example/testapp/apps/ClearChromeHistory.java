package com.example.testapp.apps;

import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;
import java.util.Date;

public class ClearChromeHistory {

  UiDevice device;
  Timestamp Enter_URLTime, ht;

  public int chromeTestRun(UiDevice device) {

    this.device = device;

    boolean runTest = true;
    try {

      Thread.sleep(GlobalVariables.TimeOut);

      homeButton();

      runTest = moreOptions();
      if (!runTest) {
        return 1;
      }

      runTest = newTab();
      if (!runTest) {
        return 1;
      }

      runTest = switchTabs();
      if (!runTest) {
        return 1;
      }

      runTest = moreOptions();
      if (!runTest) {
        return 1;
      }

      runTest = closeAllTabs();
      if (!runTest) {
        return 1;
      }

      runTest = moreOptions();
      if (!runTest) {
        return 1;
      }

      runTest = newTab();
      if (!runTest) {
        return 1;
      }

      runTest = moreOptions();
      if (!runTest) {
        return 1;
      }

      runTest = history();
      if (!runTest) {
        return 1;
      }

      runTest = clearBrowseData();
      if (!runTest) {
        return 1;
      }

      runTest = selectTimeRange();
      if (!runTest) {
        return 1;
      }

      runTest = selectAllTimeData();
      if (!runTest) {
        return 1;
      }

      runTest = clearData();
      if (!runTest) {
        return 1;
      }

      runTest = closeButton();
      if (!runTest) {
        return 1;
      }

      //            runTest = moreOptions();
      //            if (!runTest) {
      //                return 1;
      //            }
      //
      //            runTest = newTab();
      //            if (!runTest) {
      //                return 1;
      //            }

      runTest = searchURL();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearchURL();
      if (!runTest) {
        return 1;
      }

      return 0;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Chrome Test Run");
    }
    return 0;
  }

  private boolean homeButton() {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Home button with Attempt no." + (i + 1));

          UiSelector homeID = new UiSelector().resourceId("com.android.chrome:id/home_button");
          UiObject home = device.findObject(homeID);
          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            home.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click Home Button Failed");
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
      return false;
    }
  }

  private boolean switchTabs() {
    try {
      boolean switchTabsButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Switch Tabs button with Attempt no." + (i + 1));

          //                    UiSelector moreId = new UiSelector().descriptionContains("More
          // options");
          //                    UiObject moreOptionsAppear = device.findObject(moreId);

          //                    UiCollection moreId = new UiCollection(new
          // UiSelector().resourceId("com.android.chrome:id/tab_switcher_button"));
          //                    UiObject moreOptionsAppear = moreId.getChildByInstance(new
          // UiSelector(),2);

          UiSelector moreId =
              new UiSelector().resourceId("com.android.chrome:id/tab_switcher_button");
          UiObject moreOptionsAppear = device.findObject(moreId);

          if (moreOptionsAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            moreOptionsAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Switch Tabs:" + new Timestamp(new Date().getTime()));
            switchTabsButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click Switch Tabs Failed");
        }
      }
      return switchTabsButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Switch Tabs Method");
      return false;
    }
  }

  private boolean moreOptions() {
    try {
      boolean moreOptionsButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find More options button with Attempt no." + (i + 1));

          UiSelector moreId = new UiSelector().descriptionContains("More options");
          UiObject moreOptionsAppear = device.findObject(moreId);

          //                    UiCollection moreId = new UiCollection(new
          // UiSelector().resourceId("com.android.chrome:id/menu_button"));
          //                    UiObject moreOptionsAppear = moreId.getChildByInstance(new
          // UiSelector(),2);

          if (moreOptionsAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            moreOptionsAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on More Options:" + new Timestamp(new Date().getTime()));
            moreOptionsButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click More Options Failed");
        }
      }
      return moreOptionsButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding More Options Method");
      return false;
    }
  }

  private boolean closeAllTabs() {
    try {
      boolean closeAllTabsButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Close All Tabs button with Attempt no." + (i + 1));

          UiSelector moreId = new UiSelector().descriptionContains("Close all tabs");
          UiObject moreOptionsAppear = device.findObject(moreId);

          if (moreOptionsAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            moreOptionsAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Close all tabs:" + new Timestamp(new Date().getTime()));
            closeAllTabsButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Close all tabs Failed");
        }
      }
      return closeAllTabsButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Close all tabs Method");
      return false;
    }
  }

  private boolean history() {
    try {
      boolean histroyButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click History Button with Attempt no." + (i + 1));
          UiSelector historyId = new UiSelector().descriptionContains("History");
          UiObject history = device.findObject(historyId);

          if (history.waitForExists(GlobalVariables.Wait_Timeout)) {
            history.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on History Button:" + new Timestamp(new Date().getTime()));
            histroyButton = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click History Button Failed");
        }
      }
      return histroyButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in History Method");
      return false;
    }
  }

  private boolean clearBrowseData() {
    try {
      boolean clearBrowseData = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click Clear Browsing Data Button with Attempt no." + (i + 1));
          UiSelector cbdID =
              new UiSelector().resourceId("com.android.chrome:id/clear_browsing_data_button");
          UiObject cbd = device.findObject(cbdID);

          if (cbd.waitForExists(GlobalVariables.Wait_Timeout)) {
            cbd.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Clear Browsing Button:" + new Timestamp(new Date().getTime()));
            clearBrowseData = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Clear Browse Button Failed");
        }
      }
      return clearBrowseData;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clear Browse Button Method");
      return false;
    }
  }

  private boolean selectTimeRange() {
    try {
      boolean timeRange = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Select Time Range Button with Attempt no." + (i + 1));
          UiSelector timeRangeId = new UiSelector().resourceId("com.android.chrome:id/spinner");
          UiObject selectTimeRange = device.findObject(timeRangeId);

          if (selectTimeRange.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectTimeRange.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Time Range Button:" + new Timestamp(new Date().getTime()));
            timeRange = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Select Time Range Button Failed");
        }
      }
      return timeRange;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting Time Range Button Method");
      return false;
    }
  }

  private boolean selectAllTimeData() {
    try {
      boolean allTimeData = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click All Time Data Button with Attempt no." + (i + 1));
          UiSelector allTimeID = new UiSelector().text("All time");
          UiObject allTime = device.findObject(allTimeID);

          if (allTime.waitForExists(GlobalVariables.Wait_Timeout)) {
            allTime.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on All Time Data Button:" + new Timestamp(new Date().getTime()));
            allTimeData = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on All Time Button Failed");
        }
      }
      return allTimeData;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Selecting All Time Button Method");
      return false;
    }
  }

  private boolean clearData() {
    try {
      boolean clearDataButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click Clear Data Button with Attempt no." + (i + 1));
          UiSelector clearDataId =
              new UiSelector().resourceId("com.android.chrome:id/clear_button");
          UiObject clearData = device.findObject(clearDataId);

          if (clearData.waitForExists(GlobalVariables.Wait_Timeout)) {
            clearData.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Clear Data Button:" + new Timestamp(new Date().getTime()));
            clearDataButton = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Clear Data Button Failed");
        }
      }
      return clearDataButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clear Data Button Method");
      return false;
    }
  }

  private boolean closeButton() {
    try {
      boolean closeButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click on Close Button with Attempt no." + (i + 1));
          UiSelector closeId = new UiSelector().resourceId("com.android.chrome:id/close_menu_id");
          UiObject close = device.findObject(closeId);

          if (close.waitForExists(GlobalVariables.Wait_Timeout)) {
            close.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Close Button:" + new Timestamp(new Date().getTime()));
            closeButton = true;
            break;
          } else {
            okGotIt();
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Close Button Failed");
        }
      }
      return closeButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Close Button Method");
      return false;
    }
  }

  private boolean okGotIt() {
    try {
      boolean okGotItButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to Click on Ok Got It Button with Attempt no." + (i + 1));
          UiSelector gotitId = new UiSelector().textContains("OK, got it");
          UiObject gotit = device.findObject(gotitId);

          if (gotit.waitForExists(GlobalVariables.Wait_Timeout)) {
            gotit.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Ok Got It Popup:" + new Timestamp(new Date().getTime()));
            okGotItButton = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Ok Got It popup Failed");
        }
      }
      return okGotItButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Ok Got it Popup Method");
      return false;
    }
  }

  private boolean newTab() {
    try {
      boolean newTabButton = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Click on New Tab with Attempt no." + (i + 1));
          UiSelector newTabId = new UiSelector().descriptionContains("New tab");
          UiObject newTab = device.findObject(newTabId);

          if (newTab.waitForExists(GlobalVariables.Wait_Timeout)) {
            newTab.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on New Tab:" + new Timestamp(new Date().getTime()));
            newTabButton = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on New Tab Failed");
          okGotIt();
        }
      }
      return newTabButton;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in New Tab Method");
      return false;
    }
  }

  private boolean searchURL() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Search Button with Attempt no." + (i + 1));

          UiSelector searchId =
              new UiSelector().resourceId("com.android.chrome:id/search_box_text");
          UiObject searchAppear = device.findObject(searchId);
          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Search URL:" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Search Failed");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Search Button Method");
      return false;
    }
  }

  private boolean enterSearchURL() {
    try {
      boolean enterSearchURL = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search URL with Attempt no." + (i + 1));
          UiSelector enterSearchField =
              new UiSelector().resourceId("com.android.chrome:id/url_bar");
          UiObject enterSearch = device.findObject(enterSearchField);

          if (enterSearch.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch.setText("voot.com \n");
            device.pressEnter();
            Enter_URLTime = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Enter Search URL:" + Enter_URLTime);
            enterSearchURL = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search URL Failed");
        }
      }
      return enterSearchURL;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search URL Method");
      return false;
    }
  }
}
