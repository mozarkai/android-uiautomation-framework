package com.example.testapp.apps;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;


public class BajajFinserv_ETB1 implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt, sbat, sbt, sdbt, bnbt, vt, opat, plat, tpat, svt, tutt;
    //    HashMap<Timestamp, String> hash_map;
    private UiDevice device;

    public double getTime(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Get KPI Time");
        }
        return timestamp;
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            File sdcard = Environment.getExternalStorageDirectory();

            try {
                File file = new File(sdcard, "file.txt");
                Scanner reader = new Scanner(file);
                while (reader.hasNextLine()) {
                    mobileNumber = reader.nextLine();
                    Log.d(GlobalVariables.Tag_Name, "Mobile Number is:" + mobileNumber);
                }
                reader.close();
            } catch (FileNotFoundException e) {
                Log.d(GlobalVariables.Tag_Name, "Error in finding mobile number from file");
                e.printStackTrace();
            }

            String enterMPINId = "org.altruist.BajajExperia:id/edt_cust_id";
            String mpin = "2021";
            String continueButtonId = "org.altruist.BajajExperia:id/btn_mpin_login_continue";
            String myEMIcardId = "My EMI Card";
            String mobileId = "org.altruist.BajajExperia:id/cl_mobile";
            String enterMobileNumberId = "Enter Number or Name";
            String postpaidId = "Postpaid";
            String mobileNumber = "9168925925";
            String selectOperatorId = "Vi Postpaid";
            String fetchBillId = "FETCH BILL";
            String proceedToPayId = "PROCEED TO PAY";
            String bajajFinservHealthId = "org.altruist.BajajExperia:id/ll_health_rx";
            String bajajFineservHealthText = "BAJAJ FINSERV HEALTH";
            String moreServicesId = "Explore More Services";
            String webViewId = "root";
            String carePlansId = "Care Plans";
            String findCarePlansId = "Find Care Plans";
            String diabetesId = "Diabetes";
            String diabetesCarePlansId = "Diabetes Care Plans";
            String proceedButtonId = "android:id/button1";
            String relationsId = "org.altruist.BajajExperia:id/navigation_relations";
            String preApprovedOffersId = "org.altruist.BajajExperia:id/tv_preApprovedOffers_Lable";
            String viewAllId = "";
            String myEMICardId = "org.altruist.BajajExperia:id/rl_emiCard";
            String dobId = "org.altruist.BajajExperia:id/edt_birth_date";
            String dob = "13061981";
            String continueButtonID = "org.altruist.BajajExperia:id/btn_continue";
            String recentCardTransactionId = "org.altruist.BajajExperia:id/tv_title_recent_transaction";
            String hamburgerId = "Navigate up";
            String scrollViewId = "android.widget.ScrollView";
            String logoutId = "org.altruist.BajajExperia:id/tv_logout";
            String logoutText = "Logout";
            String okButtonId = "android:id/button1";

            runTest = enterKeys(device, enterMPINId, mpin, "Enter MPIN", "Entered MPIN");
            if (!runTest) {
                runTest = clickOnContentDescription(device, hamburgerId, "Menu Button", "Clicked On Menu Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = scrollResourceId(device, logoutId, scrollViewId, logoutId, "Logout Button", "Clicked On Logout Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = clickOnResourceID(device, okButtonId, "Ok Button", "Clicked On Ok Button Time:");
                if (!runTest) {
                    return 1;
                }

                runTest = enterKeys(device, enterMPINId, mpin, "Enter MPIN", "Entered MPIN");
                if (!runTest) {
                    return 1;
                }
            }

//            runTest = elementAppear(device, enterMPINId, "Enter MPIN Appear", "Enter MPIN Appear Time:");
//            if (!runTest) {
//                return 1;
//            }

            runTest = clickOnResourceID(device, continueButtonId, "Continue Button", "Clicked On Continue Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, myEMIcardId, "My EMI Card", "My EMI Card Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, mobileId, "Mobile Button", "Clicked On Mobile Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, enterMobileNumberId, "Enter Mobile Number Appear", "Enter Mobile Number Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, postpaidId, "Postpaid Button", "Clicked On Postpaid Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = enterKeysText(device, enterMobileNumberId, mobileNumber, "Enter Mobile Number", "Entered Mobile Number Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, selectOperatorId, "Select Operator", "Selected Operator Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, fetchBillId, "Fetch Bill", "Fetch Bill Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, fetchBillId, "Fetch Bill Button", "Clicked On Fetch Bill Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, proceedToPayId, "Proceed To Pay", "Proceed To Pay Appear Time:");
            if (!runTest) {
                return 1;
            }

//            runTest = clickOnText(device, proceedToPayId, "Proceed To Pay Button", "Clicked On Proceed To Pay Button Time:");
//            if (!runTest) {
//                return 1;
//            }

            Thread.sleep(2000);
            device.pressBack();
            Thread.sleep(2000);
            device.pressBack();
            Thread.sleep(2000);
            device.pressBack();
            Thread.sleep(2000);

            runTest = elementAppear(device, myEMIcardId, "Home Elements", "Home Elements Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = scrollResourceId(device, bajajFinservHealthId, scrollViewId, bajajFineservHealthText, "BajajFinserv Health Button", "Clicked On BajajFinserv Health Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, moreServicesId, "Explore More Services", "Explore More Services Appear Time:");
            if (!runTest) {
                return 1;
            }

            elementAppear(device, carePlansId, "Care Plans", "Care Plans Appear Time:");

            Thread.sleep(5000);

            runTest = clickOnOnlyText(device, carePlansId, "Care Plans Button", "Clicked On Care Plans Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, findCarePlansId, "Find Care Plans", "Find Care Plans Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnText(device, diabetesId, "Diabetes Button", "Clicked On Diabetes Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, diabetesCarePlansId, "Diabetes Care Plans", "Diabetes Care Plans Appear Time:");
            if (!runTest) {
                return 1;
            }

            device.pressBack();

            runTest = clickOnResourceID(device, okButtonId, "Proceed Button", "Clicked On Proceed Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnContentDescription(device, hamburgerId, "Menu Button", "Clicked On Menu Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = scrollResourceId(device, logoutId, scrollViewId, logoutId, "Logout Button", "Clicked On Logout Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnResourceID(device, okButtonId, "Ok Button", "Clicked On Ok Button Time:");
            if (!runTest) {
                return 1;
            }

            kpiCalculations();

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.BajajFinserv_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean clickOnResourceID(UiDevice device, String resourceID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().resourceId(resourceID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnContentDescription(UiDevice device, String contentDesc, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().descriptionContains(contentDesc);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnOnlyText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().text(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean enterKeys(UiDevice device, String resourceID, String enterKey, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().resourceId(resourceID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
//                    idObject.click();
                    idObject.setText(enterKey);
//                    device.pressEnter();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean enterKeysText(UiDevice device, String textID, String enterKey, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    idObject.setText(enterKey);
                    device.pressEnter();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    return elementFound;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean scrollResourceId(UiDevice device, String elementId, String scrollId, String textId, String elementName, String successMsg) {
        try {
            boolean value = false;

            for (int i = 0; i < 5; i++) {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);
                try {

                    UiSelector elementID = new UiSelector().resourceId(elementId);
                    UiObject objectId = device.findObject(elementID);

                    if (objectId.waitForExists(GlobalVariables.Wait_Timeout)) {
                        objectId.click();
                        Timestamp timestamp = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                        kpis.put(elementName, timestamp);
                        value = true;
                        break;
                    } else {
                        UiScrollable scroll = new UiScrollable(
                                new UiSelector().className(scrollId));
                        scroll.scrollTextIntoView(textId);
                        objectId.click();
                        Timestamp timestamp = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                        kpis.put(elementName, timestamp);
                        value = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean scrollText(UiDevice device, String elementId, String scrollId, String elementName, String successMsg) {
        try {
            boolean value = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);
            try {

                UiSelector elementID = new UiSelector().textContains(elementId);
                UiObject objectId = device.findObject(elementID);

                if (objectId.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    objectId.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    value = true;
                } else {
                    UiScrollable scroll = new UiScrollable(
                            new UiSelector().className(scrollId));
                    scroll.scrollTextIntoView(elementId);
                    objectId.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                }

            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private void kpiCalculations() {
        try {

            double continueButtonTime = getTime("Continue Button");
            double myEmiCardTime = getTime("My EMI Card");
            double mobileButtonTime = getTime("Mobile Button");
            double mobileNumberEnterAppearTime = getTime("Enter Mobile Number Appear");
            double enterMobileNumberTime = getTime("Enter Mobile Number");
            double fetchBillAppearTime = getTime("Fetch Bill");
            double fetchBillButtonTime = getTime("Fetch Bill Button");
            double proceedToPayAppearTime = getTime("Proceed To Pay");
//            double proceedToPayButtonTime = getTime("Proceed To Pay Button");
            double bajajFineservHealthButtonTime = getTime("BajajFinserv Health Button");
            double exploreMoreServicesAppearTime = getTime("Explore More Services");
            double carePlansButtonTime = getTime("Care Plans Button");
            double findCarePlansAppearTime = getTime("Find Care Plans");
            double diabetesButtonTime = getTime("Diabetes Button");
            double diabetesCarePlansAppearTime = getTime("Diabetes Care Plans");

            double TTLH = myEmiCardTime - continueButtonTime;
            double TTLMPP = mobileNumberEnterAppearTime - mobileButtonTime;
            double TTLSOP = fetchBillAppearTime - enterMobileNumberTime;
            double TTLRBP = proceedToPayAppearTime - fetchBillButtonTime;
            double TTLHP = exploreMoreServicesAppearTime - bajajFineservHealthButtonTime;
            double TTLFCPP = findCarePlansAppearTime - carePlansButtonTime;
            double TTLDCPP = diabetesCarePlansAppearTime - diabetesButtonTime;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Mobile Payment Page=" + TTLMPP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Select Operator Page=" + TTLSOP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Review Bill Page=" + TTLRBP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Health Page=" + TTLHP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Find Care Plans Page=" + TTLFCPP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Diabetes Care Plans Page=" + TTLDCPP);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}
