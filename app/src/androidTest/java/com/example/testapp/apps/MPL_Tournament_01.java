package com.example.testapp.apps;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_Tournament_01 extends MPL_BaseTest {

    public int testRun(UiDevice device, Timestamp lt) {
        boolean runTest = false;

        String spinID = "Spin the Wheel!";
        String okayID = "Okay";
        String homePageID = "All Games";
        String scrollId = "android.widget.ScrollView";
        String selectGameID = "Ice Jump";
        String gameCardID = "Mozark Free Tournament";
        String playID = "Play";
        String gameOverID = "Calculating your rank";
        String gameOverID1 = "Calculating";
        String gameOverID2 = "Your High Score!";
        String leaderBoradID = "LEADERBOARD";

        Log.d(GlobalVariables.Tag_Name, "Executing Tournament tests");

        runTest = checkForElementText(device, homePageID,  "AllGamesText", "Successfully loaded HomePage ", "unable to load Home Page");
        if (!runTest) {
            return 1;
        }

        runTest = scroll(device,selectGameID,scrollId,"Unable to scroll to Ice jump game","Successfully scrolled to Ice jump game","IceJumpGameText");
        if (!runTest) {
            return 1;
        }
        runTest = checkForElementText(device, gameCardID,  "IceJumpPage", "Successfully loaded Ice jump page ", "unable to load Ice jump Page");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, gameCardID,  "GameCard", "Successfully clicked on Mozark game card", "Unable to click on Mozark game card");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, playID,  "playButton", "Successfully clicked on play button", "Unable to click on play button");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device, gameOverID,  "GameOverPage", "Successfully loaded GameOverPage ", "unable to load GameOverPage");
        if (!runTest) {
            return 1;
        }
        runTest = checkForElementText(device, leaderBoradID,  "LeaderBoardPage", "Successfully loaded LeaderBoard page ", "unable to load LeaderBoard Page");
        if (!runTest) {
            return 1;
        }
        return 0;
    }

}
