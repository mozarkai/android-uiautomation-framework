package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.DataOutputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MPL_BaseTest {

    private int n = 5;
    /// public UiDevice device;
    String AppName;
    public static Timestamp lt;
    Context context = ApplicationProvider.getApplicationContext();
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();


    public void startAppActivity(UiDevice device, boolean clearCache, boolean clearData) {

        try {
            Log.d(GlobalVariables.Tag_Name, "Launching the App Activity");

            if (clearCache)
                Utility.clearCache(GlobalVariables.MPL_Package);

            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(GlobalVariables.MPL_Package);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            lt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
            device.hasObject(By.pkg(GlobalVariables.MPL_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Launching the App Activity");
            e.printStackTrace();
        }
    }

    public void runApp(UiDevice device, String appPackage) {
        try {

//             MPL_Tournament_01 tournament = new MPL_Tournament_01();
//            tournament.testRun(device,lt);
            //  MPL_Login_01 mpl = new MPL_Login_01();
            //  mpl.testRun(device, lt,"7996695253");

            MPL_Fantasy_01 fantasy = new MPL_Fantasy_01();
            fantasy.testRun(device, lt);

//            MPL_MoneyIn_01 moneyIn = new MPL_MoneyIn_01();
//            moneyIn.testRun(device, lt);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in running Apps");
            e.printStackTrace();
        }
    }

    public void tearDown(String Package, UiDevice device) {
        try {
            Thread.sleep(GlobalVariables.Timeout);
            device.executeShellCommand(getCommand(Package));
            Log.d(GlobalVariables.Tag_Name, "App Closed Successfully");
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }


    //It will work only for text Contain
    public boolean checkForElementText(UiDevice device, String text, String elementName, String successMsg, String errorMsg) {
        boolean value = false;
        try {
            UiSelector idSelector = new UiSelector().textContains(text);
            UiObject idObject = device.findObject(idSelector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (idObject.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, elementName);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    //It will work only for text
    public boolean checkForElementWithSameText(UiDevice device, String text, String elementName, String successMsg, String errorMsg) {
        boolean value = false;
        try {
            UiSelector idSelector = new UiSelector().text(text);
            UiObject idObject = device.findObject(idSelector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (idObject.exists()) {
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, elementName);
                }
            }
            return value;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }


    public boolean clickButtonWithText(UiDevice device, String text, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
                UiSelector idSelector = new UiSelector().text(text);
                UiObject idObject = device.findObject(idSelector);
                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean clickButtonWithTextContains(UiDevice device, String textContains, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
                UiSelector idSelector = new UiSelector().textContains(textContains);
                UiObject idObject = device.findObject(idSelector);
                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean clickButtonWithTextContainsAndIndex(UiDevice device, String textContains, int index, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
                UiSelector idSelector = new UiSelector().textContains(textContains).index(index);
                UiObject idObject = device.findObject(idSelector);
                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }


    // send values to text box
    public boolean typeTextWithText(UiDevice device, String text, String textValue, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
                UiSelector idSelector = new UiSelector().textContains(text);
                UiObject idObject = device.findObject(idSelector);
                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.clearTextField();
                    idObject.setText(textValue);
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public boolean scroll(UiDevice device, String elementId, String scrollId, String errorMsg, String successMsg, String elementName) {
        try {
            boolean value = false;
            for (int i = 0; i < n; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find select Game with Attempt no." + (i + 1));
                    UiSelector elementID = new UiSelector().textContains(elementId);
                    UiObject objectId = device.findObject(elementID);
                    if (objectId.waitForExists(GlobalVariables.Wait_Timeout)) {
                        objectId.click();
                        Timestamp timestamp = new Timestamp(new Date().getTime());
                        kpis.put(elementName, timestamp);
                        Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                        value = true;
                        break;
                    } else {
                        UiScrollable scroll = new UiScrollable(
                                new UiSelector().className(scrollId));
                        scroll.scrollTextIntoView(elementId);
                        if (objectId.waitForExists(GlobalVariables.Wait_Timeout)) {
                            objectId.click();
                            Timestamp timestamp = new Timestamp(new Date().getTime());
                            Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                            value = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, elementName);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    private boolean backButton(UiDevice device, String errorMsg) {
        try {
            boolean value = false;
            try {
                device.pressBack();
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, errorMsg);
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public static void androidPermissions(String packageName, String successMsg, String errorMsg) {
        try {
            Process install = Runtime.getRuntime().exec("su\n");
            DataOutputStream os = new DataOutputStream(install.getOutputStream());
            os.writeBytes("adb shell\n");
            {
                os.writeBytes("pm grant " + packageName + " android.permission.ACCESS_COARSE_LOCATION\n");
                os.writeBytes("pm grant " + packageName + " android.permission.ACCESS_FINE_LOCATION\n");
                os.writeBytes("pm grant " + packageName + " android.permission.WRITE_EXTERNAL_STORAGE\n");
                os.writeBytes("pm grant " + packageName + " android.permission.READ_EXTERNAL_STORAGE\n");
                os.writeBytes("pm grant " + packageName + " android.permission.CAMERA\n");
                os.writeBytes("pm grant " + packageName + " android.permission.RECORD_AUDIO\n");
                os.writeBytes("pm grant " + packageName + " android.permission.CALL_PHONE\n");
            }
            os.writeBytes("exit\n");
            os.flush();
            os.close();
            Log.d(GlobalVariables.Tag_Name, successMsg);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            e.printStackTrace();
        }
    }

    public boolean clickButtonWithCollections(UiDevice device, String collection, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {

                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(2).className("android.view.ViewGroup").className("android.view.ViewGroup").index(1));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean clickOnContestCard(UiDevice device, String collection, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {

                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(2).className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").index(3));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean selectPlayersBasedonIndex(UiDevice device, int index, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {

                UiObject idObject3 = new UiObject(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").index(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").index(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").index(index).className("android.view.ViewGroup").index(0));

                UiObject idObject4 = new UiObject(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").instance(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup")).getChild(new UiSelector().instance(index).className("android.view.ViewGroup").index(0));


                UiCollection btnId = new UiCollection(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout")
                        .className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.FrameLayout").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").instance(3).className("androidx.viewpager.widget.ViewPager")
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup")
                        .className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(index).className("android.view.ViewGroup"));

                UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(index).className("android.view.ViewGroup"));

                // 56 4th pl
                //index 68,69,70 is for 5th player
                // 70 as 6th player
             /* UiObject  idObject = null;
                for(int i=55;i<68;i++){

                     idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(i).className("android.view.ViewGroup"));
                Log.d(GlobalVariables.Tag_Name,String.valueOf(i));
                    idObject.click();

                }*/

                UiObject idObject2 = btnId.getChildByInstance(new UiSelector(), 0);
                int count = btnId.getChildCount();

                //  UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(index).textContains("Sel by").index(2));
                //   UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").index(index).className("android.view.ViewGroup").childSelector(new UiSelector().textContains("Sel by")));
                //  UiSelector lt= new UiSelector().textContains("Sel by");
                //   UiObject idObject = new UiObject(new UiSelector().textContains("Sel by"));
                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    //idObject.click();

                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean selectCaptainOrViceCaptain(UiDevice device, int instance1, int instance2, int index, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;

            //android.view.ViewGroup[5]//android.view.ViewGroup[2]

            try {
                UiObject idObject1 = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").instance(instance1).className("android.view.ViewGroup")
                        .className("android.view.ViewGroup").instance(instance2).index(index));
                UiObject idObject = null;
               /* for(int i =0;i<10;i++){
                     idObject = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(instance1).className("android.view.ViewGroup").instance(instance2).index(index));
                     Log.d(GlobalVariables.Tag_Name,String.valueOf(i));
                     idObject.click();
                     Thread.sleep(2000);
                }*/
                idObject = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(instance1).className("android.view.ViewGroup").instance(instance2).index(index));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean clickOnWalletIcon(UiDevice device, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            //android.widget.LinearLayout//android.widget.FrameLayout/android.view.ViewGroup[1]//android.view.ViewGroup[1]/android.view.ViewGroup[4]
//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]
//hierarchy///////[1]///[1]/[4]

            try {
                UiObject idObject1 = new UiObject(new UiSelector().className("android.widget.FrameLayout").className("android.widget.LinearLayout").className("android.widget.FrameLayout")
                        .className("android.widget.LinearLayout").className("android.widget.FrameLayout").className("android.widget.FrameLayout").className("android.view.ViewGroup").instance(1)
                        .className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(1).index(2));

                UiObject idObject = new UiObject(new UiSelector()
                        .className("android.view.ViewGroup").instance(1)
                        .className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(4).index(3));


                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }

    public boolean clickOn50RupeeButton(UiDevice device, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
                //android.view.ViewGroup[1]//android.view.ViewGroup[1]//android.widget.HorizontalScrollView//android.view.ViewGroup[4]/android.view.ViewGroup";
                UiObject idObject = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(1).className("android.widget.HorizontalScrollView").className("android.view.ViewGroup").instance(4).className("android.view.ViewGroup"));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    Timestamp timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName, timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }


    /*public UiObject getIdObjectWithCollection(UiDevice device,String xPath,int index) {

        String[] classNames = xPath.split("/");
        String instanceIndex;
        String extendedIdObject = ".className(\""+classNames[0]+"\")";

        for (int i = 1; i < classNames.length; i++) {
            if (classNames[i].contains("[")) {
                instanceIndex = StringUtils.getDigits(classNames[i]);
                classNames[i] = classNames[i].replace("[", "").replace("]", "").replace(instanceIndex, "");
                extendedIdObject = extendedIdObject + ".className(\"" + classNames[i] + "\").instance(" + instanceIndex + ")";
            } else
                extendedIdObject = extendedIdObject + ".className(\"" + classNames[i] + "\")";
        }
        extendedIdObject = extendedIdObject+".index("+index+")";

        Log.d(GlobalVariables.Tag_Name,extendedIdObject.toString());

        //UiSelector idSelector = (UiSelector) ((Object)(new UiSelector()+extendedIdObject));

       // Log.d(GlobalVariables.Tag_Name,idSelector.toString());

        UiSelector selector = new UiSelector();
        device.findObject((selector.+extendedIdObject));
      //  UiObject idObject = device.findObject(selector.toString()+extendedIdObject);

      //  UiObject idObject = new UiObject((UiSelector)extendedIdObject);

        return  idObject;
    }


    public boolean clickOnElementsWithXpath(UiDevice device, String xPath,int index, Timestamp timestamp, String elementName, String successMsg, String errorMsg) {
        try {
            boolean buttonFound = false;
            try {
              //  UiObject idObject2 = new UiObject(new UiSelector().extendedIdObject);
                UiObject idObject = getIdObjectWithCollection(xPath,index);
               // UiObject idObject = new UiObject(new UiSelector().className("android.widget.ScrollView").className("android.view.ViewGroup").className("android.view.ViewGroup").instance(2).className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").className("android.view.ViewGroup").index(3));

                if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    kpis.put(elementName,timestamp);
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(GlobalVariables.Tag_Name, elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, errorMsg);
            return false;
        }
    }*/


//    public boolean selectGameWithText(UiDevice device, String selectGameID, String className) {
//        try {
//            boolean selectGameFound = false;
//            for (int i = 0; i < n; i++) {
//                try {
//                    Log.d(GlobalVariables.Tag_Name, "Trying to find select Game with Attempt no." + (i + 1));
//                    UiSelector selectGameId = new UiSelector().textContains(selectGameID);
//                    UiObject selectGame = device.findObject(selectGameId);
//                    if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
//                        selectGame.click();
//                        sgt = new Timestamp(new Date().getTime());
//     kpis.put(elementName,timestamp);
//                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
//                        selectGameFound = true;
//                        break;
//                    } else {
//                        UiScrollable scroll = new UiScrollable(
//                                new UiSelector().className(className));
//                        scroll.scrollTextIntoView(selectGameID);
//                        if (selectGame.waitForExists(GlobalVariables.Wait_Timeout)) {
//                            selectGame.click();
//                            sgt = new Timestamp(new Date().getTime());
//                            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Select_Game_Time + sgt);
//                            selectGameFound = true;
//                            break;
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.d(GlobalVariables.Tag_Name, "Error in Select Game");
//                }
//            }
//            return selectGameFound;
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Selecting Game");
//            return false;
//        }
//    }
//
//    public boolean loadGameCardWithText(UiDevice device, String gameCardID) {
//        try {
//            UiSelector gameCardId = new UiSelector().textContains(gameCardID);
//            UiObject gameCard = device.findObject(gameCardId);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.start();
//
//            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
//                if (gameCard.exists()) {
//                    lgct = new Timestamp(new Date().getTime());
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Game_Card_Time + lgct);
//                    break;
//                } else {
//                    Log.d(GlobalVariables.Tag_Name, "Loading Game Card");
//                }
//            }
//            return true;
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Loading Game Card");
//            return false;
//        }
//    }
//
//    public boolean clickFreeButtonWithText(UiDevice device, String freeButtonID) {
//        try {
//            boolean freeButtonFound = false;
//            for (int i = 0; i < n; i++) {
//                try {
//                    Log.d(GlobalVariables.Tag_Name, "Trying to find Free button with Attempt no." + (i + 1));
//
//                    UiSelector freeButtonId = new UiSelector().textContains(freeButtonID);
//                    UiObject freeButton = device.findObject(freeButtonId);
//                    if (freeButton.waitForExists(GlobalVariables.Wait_Timeout)) {
//                        freeButton.click();
//                        cfbt = new Timestamp(new Date().getTime());
//                        Log.d(
//                                GlobalVariables.Tag_Name,
//                                GlobalVariables.Free_Button_Time + cfbt);
//                        freeButtonFound = true;
//                        break;
//                    }
//                } catch (Exception e) {
//                    Log.d(GlobalVariables.Tag_Name, "Error in Free Button");
//                }
//            }
//            return freeButtonFound;
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Free Button");
//            return false;
//        }
//    }
//
//    public boolean clickBattleFreeWithText(UiDevice device, String battleForFreeID) {
//        try {
//            boolean battleFreeFound = false;
//            for (int i = 0; i < n; i++) {
//                try {
//                    Log.d(
//                            GlobalVariables.Tag_Name,
//                            "Trying to find Battle For Free button with Attempt no." + (i + 1));
//
//                    UiSelector battleFreeId = new UiSelector().textContains(battleForFreeID);
//                    UiObject battleFree = device.findObject(battleFreeId);
//                    if (battleFree.waitForExists(GlobalVariables.Wait_Timeout)) {
//                        battleFree.click();
//                        cbft = new Timestamp(new Date().getTime());
//                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Battle_Free_Time + cbft);
//                        battleFreeFound = true;
//                        break;
//                    }
//                } catch (Exception e) {
//                    Log.d(GlobalVariables.Tag_Name, "Error in Battle Free Button");
//                }
//            }
//            return battleFreeFound;
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Clicking on Battle Free Button");
//            return false;
//        }
//    }
//
//    public boolean gameLoaderWithText(UiDevice device, String gameLoaderID) {
//        try {
//            UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID);
//            UiObject gameLoader = device.findObject(gameLoaderId);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.start();
//
//            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
//                if (gameLoader.exists()) {
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
//                } else {
//                    glt = new Timestamp(new Date().getTime());
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
//                    break;
//                }
//            }
//            return true;
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
//            return false;
//        }
//    }
//
//    public boolean gameLoader1WithText(UiDevice device, String gameLoaderID1) {
//        try {
//            UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID1);
//            UiObject gameLoader = device.findObject(gameLoaderId);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.start();
//
//            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
//                if (gameLoader.exists()) {
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
//                } else {
//                    glt = new Timestamp(new Date().getTime());
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
//                    break;
//                }
//            }
//            return true;
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
//            return false;
//        }
//    }
//
//    public boolean gameLoader2WithText(UiDevice device, String gameLoaderID2) {
//        try {
//            UiSelector gameLoaderId = new UiSelector().textContains(gameLoaderID2);
//            UiObject gameLoader = device.findObject(gameLoaderId);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.start();
//
//            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
//                if (gameLoader.exists()) {
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_Yes);
//                } else {
//                    glt = new Timestamp(new Date().getTime());
//                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.GameLoading_No + glt);
//                    break;
//                }
//            }
//            return true;
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in Game Loader");
//            return false;
//        }
//    }
//

    public double getTimeFromKpi(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Unable to fetch the timestamp from kpis");
        }
        return timestamp;
    }

    /*public boolean kpiCalculation() {
        boolean kpiValue = false;
        try {

      //     Timestamp timestampForFantasyTab = getTimeFromKpi("FantasyTab");

            double TLHP = (lhpt.getTime() - ExampleInstrumentedTest.lt.getTime()) / 1000.0;
            double TLGP = (cfbt.getTime() - sgt.getTime()) / 1000.0;
            double TSG = (glt.getTime() - cbft.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TLHP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Game Page=" + TLGP);
            Log.d(GlobalVariables.Tag_Name, "Time To Start a Game=" + TSG);

            kpiValue = true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Base KPI Calculation");
            e.printStackTrace();
            return false;
        }
        return kpiValue;
    }*/
//

}

