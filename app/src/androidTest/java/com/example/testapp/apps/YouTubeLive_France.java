package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class YouTubeLive_France implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    String OUTPUT_LOG_FILE = "";
    private Timestamp lt, st, vt, et, ht, het, sat;
    private UiDevice device;
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", true, order, script, appVersion, appName, ipAdress,startDate);
            }

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

            runTest = searchAppear();
            if (!runTest) {
                return 1;
            }

            runTest = enterLiveSearch();
            if (!runTest) {
                return 1;
            }

            runTest = play();
            if (!runTest) {
                return 1;
            }

            runTest = bufferRead();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();

            KpiLogs.fourKPI(lt, st, et, sat, ht, het, vt);

            kpiCalculation();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();

            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.YouTube_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean searchAppear() {
        try {
            boolean searchButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

                    UiSelector searchId = new UiSelector().descriptionContains("Search");
                    UiObject searchAppear = device.findObject(searchId);

                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
                        searchAppear.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
                        searchButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in search Appear:" + new Timestamp(new Date().getTime()));
                }
            }
            return searchButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
            return false;
        }
    }

    private boolean enterSearch() {
        try {
            boolean enterSearchtext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));

                    UiSelector enterSearchField =
                            new UiSelector().resourceId("com.google.android.youtube:id/search_edit_text");
                    UiObject enterSearch_text = device.findObject(enterSearchField);

                    if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterSearch_text.setText("coldplay concert 2019 full \n ");
                        device.pressEnter();
                        et = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
                        enterSearchtext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
                }
            }
            return enterSearchtext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
    }

    private boolean searchResults() {
        try {
            UiSelector searchResultsID =
                    new UiSelector().resourceId("com.google.android.youtube:id/results");
            UiObject searchResults = device.findObject(searchResultsID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (searchResults.exists()) {
                    sat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
                    break;
                } else {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Loading Search Results:" + new Timestamp(new Date().getTime()));
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return false;
        }
    }

    private boolean homeButton() {
        try {
            boolean homeButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to click on Home button with Attempt no." + (i + 1));

                    UiSelector homeID = new UiSelector().descriptionContains("Navigate up");
                    UiObject home = device.findObject(homeID);
                    if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
                        home.click();
                        ht = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
                        homeButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Home Button:" + new Timestamp(new Date().getTime()));
                }
            }
            return homeButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
            return false;
        }
    }

    private boolean homeElements() {
        try {
            UiSelector homeElementsID =
                    new UiSelector().resourceId("com.google.android.youtube:id/results");
            UiObject homeElements = device.findObject(homeElementsID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (homeElements.exists() && homeElements.isEnabled()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
                    break;
                } else {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
            return false;
        }
    }

    private boolean clickSearch() {
        try {
            boolean searchButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

                    UiSelector searchId = new UiSelector().descriptionContains("Search");
                    UiObject searchAppear = device.findObject(searchId);

                    if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
                        searchAppear.click();
                        Log.d(
                                GlobalVariables.Tag_Name, "Click on Search" + new Timestamp(new Date().getTime()));
                        searchButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click on Search:" + new Timestamp(new Date().getTime()));
                }
            }
            return searchButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Search Click Method");
            return false;
        }
    }

    private boolean enterLiveSearch() {
        try {
            boolean enterSearchtext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));

                    UiSelector enterSearchField =
                            new UiSelector().resourceId("com.google.android.youtube:id/search_edit_text");
                    UiObject enterSearch_text = device.findObject(enterSearchField);

                    if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterSearch_text.setText("france 24 live \n ");
                        device.pressEnter();
                        Log.d(
                                GlobalVariables.Tag_Name,
                                "Enter Live Search" + new Timestamp(new Date().getTime()));
                        enterSearchtext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter Live Search Text Failed");
                }
            }
            return enterSearchtext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Live Search Text");
            return false;
        }
    }

    private boolean play() {
        try {
            boolean playBtnFound = false;
            for (int i = 0; i < 5; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
                try {

//                    UiCollection playId =
//                            new UiCollection(
//                                    new UiSelector().resourceId("com.google.android.youtube:id/results"));
//                    UiObject playBtn = playId.getChildByInstance(new UiSelector(), 5);

                    UiSelector playId = new UiSelector().descriptionContains("watching");
                    UiObject playBtn = device.findObject(playId);

                    if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                        playBtn.click();
                        vt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
                        playBtnFound = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in play");
            return false;
        }
    }

    private boolean bufferRead() {
        try {

            UiSelector loaderId =
                    new UiSelector().resourceId("com.google.android.youtube:id/player_loading_view_thin");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            int time = 0;
            int c = GlobalVariables.Delay_Time;

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
                    Thread.sleep(200);
                }

                time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
                if (time > c) {
                    Utility.cpuUsage("com.google", OUTPUT_LOG_FILE_TEMP);
                    c += GlobalVariables.Delay_Time;
                }
            }

            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    public void kpiCalculation() {
        try {
            double TTLH = (st.getTime() - lt.getTime()) / 1000.0;
//            double SRT = (sat.getTime() - et.getTime()) / 1000.0;
//            double TLP = (het.getTime() - ht.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
//            Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Page = " + TLP);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", true, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}
