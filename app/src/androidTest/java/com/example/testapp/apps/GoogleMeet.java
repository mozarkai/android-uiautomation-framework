package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;


import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class GoogleMeet implements AppClass {

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String OUTPUT_LOG_FILE = "";
    String appName;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    private UiDevice device;
    private boolean vCap = false;
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.vCap = vCap;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            context = TestApp.getContext();
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);
            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = clickJoinWithCode();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Join With Code");
                return 1;
            }
            runTest = joinWithCodePageLoaded();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Join With Code Page");
                return 1;
            }
            runTest = enterMeetingCode();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Meeting Code");
                return 1;
            }
            runTest = joinMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Join Meetin");
                return 1;
            }
            runTest = micOff();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Off Mic");
                return 1;
            }
            runTest = videoOff();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Off Video");
                return 1;
            }
            runTest = askToJoinMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Ask To Join");
                return 1;
            }
            runTest = enteredMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check If Entered Meeting");
                return 1;
            }
            runTest = loaderAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Loader Element");
                return 1;
            }
            runTest = endMeeting();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click End Meeting Button");
                return 1;
            }


            KpiLogs.twoKPI(lt, st, vt);

            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector homeElementId = new UiSelector().resourceId("com.google.android.apps.meetings:id/calls_list");
            UiObject homeElement = device.findObject(homeElementId);

            UiSelector secHomeElementId = new UiSelector().resourceId("com.google.android.apps.meetings:id/join_meeting");
            UiObject secHomeElement = device.findObject(secHomeElementId);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Home Element");
                if (homeElement.exists() || secHomeElement.exists()) {
                    st = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return homeElementFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return false;
        }
    }

    private boolean clickJoinWithCode() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector joinWithCodeSelector = new UiSelector().resourceId("com.google.android.apps.meetings:id/join_meeting");
            UiObject joinWithCodeObject = device.findObject(joinWithCodeSelector);

            UiSelector secJoinWithCodeSelector = new UiSelector().textContains("Join with a code");
            UiObject secJoinWithCodeObject = device.findObject(secJoinWithCodeSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying Join With Code Button");
                if(joinWithCodeObject.exists())
                {
                    joinWithCodeObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Join With A Code Button Clicked Successfully time: " + unUsableTime);
                    value = true;
                    break;
                }
                else if(secJoinWithCodeObject.exists())
                {
                    secJoinWithCodeObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Join With A Code Button Clicked Successfully time: " + unUsableTime);
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name, "Join With A Code Button Loading");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Join With A Code Button");
            return false;
        }
        return value;
    }

    private boolean joinWithCodePageLoaded() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Load Join With Code Page");
                if (object.exists()) {
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Join With Code Page Loaded " + unUsable);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Join With Code");
            return false;
        }
        return value;
    }

    private boolean enterMeetingCode() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/meeting_code_entry");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().className("android.widget.EditText");
            UiObject secObject = device.findObject(secSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Entering Code");
                if (object.exists()) {
                    object.setText("omo-tufc-fki");
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Enter Code Time: " + unUsable);
                    value = true;
                    break;
                }
                if (secObject.exists()) {
                    secObject.setText("omo-tufc-fki");
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Enter Code Time: " + unUsable);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Code");
            return false;
        }
        return value;
    }

    private boolean joinMeeting() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector joinMeeting = new UiSelector().resourceId("com.google.android.apps.meetings:id/next_button");
            UiObject joinMeetingObject = device.findObject(joinMeeting);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying To Click Join Button");
                if (joinMeetingObject.exists()) {
                    joinMeetingObject.click();
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Join Meeting Result Time: " + unUsable);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Join Meeting Button");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Join Meeting");
            return value;
        }
        return value;
    }

    private boolean micOff() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/audio_input");
            UiObject object = device.findObject(selector);

            UiSelector checkIfMicOff = new UiSelector().descriptionContains("Turn microphone off");
            UiObject checkIfMicOffObject = device.findObject(checkIfMicOff);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying To Click Mic Off ");
                if (object.exists()) {
                    object.click();
                    Thread.sleep(2000);
                    if (!checkIfMicOffObject.exists())
                    {
                        Timestamp unUsable = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked Mic Off Result Time: " + unUsable);
                        value = true;
                        break;
                    }
                    else
                    {
                        Log.d(GlobalVariables.Tag_Name,"Mic Still On");
                        Thread.sleep(200);
                    }
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking Mic Off");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Mic Off");
            return value;
        }
    }

    private boolean videoOff() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/video_input");
            UiObject object = device.findObject(selector);

            UiSelector checkIfVideoOff = new UiSelector().descriptionContains("Turn camera off");
            UiObject checkIfVideoOffObject = device.findObject(checkIfVideoOff);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying To Click Video Off ");
                if (object.exists()) {
                    object.click();
                    Thread.sleep(2000);
                    if (!checkIfVideoOffObject.exists())
                    {
                        Timestamp unUsable = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked Video Off Result Time: " + unUsable);
                        value = true;
                        break;
                    }
                    else
                    {
                        Log.d(GlobalVariables.Tag_Name,"Video Still On");
                        Thread.sleep(200);
                    }
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Video Off");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Video Off");
            return value;
        }
    }

    private boolean askToJoinMeeting() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/join_button");
            UiObject object = device.findObject(selector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying To Click Ask to Join ");
                if (object.exists()) {
                    object.click();
                    vt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking Ask to Join");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Ask to Join");
            return value;
        }
    }

    private boolean enteredMeeting() {
        boolean value = false;
        try {
            RecordScreen recordScreen = new RecordScreen();
            recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/pip_participant");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().resourceId("com.google.android.apps.meetings:id/more_controls");
            UiObject secObject = device.findObject(secSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Checking if Entered The Meeting");
                if (object.exists() || secObject.exists()) {
                    Log.d(GlobalVariables.Tag_Name, "Entered Successfully Time: " + new Timestamp(new Date().getTime()));
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Checking Loader");
            value = false;
        }
        return value;
    }

    private boolean loaderAppear() {
        try {
            UiSelector loaderId = new UiSelector().className("android.widget.ProgressBar");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean endMeeting() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("com.google.android.apps.meetings:id/leave_call");
            UiObject object = device.findObject(selector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying To Click End Meeting");
                if (object.exists()) {
                    object.click();
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked End Successfully Time: " + unUsable);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking End Meeting");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking End Meeting");
            return value;
        }
        return value;
    }


    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.GoogleMeetPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}
