package com.example.testapp.apps;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_Referal_01 extends MPL_BaseTest {

    private int n = 5;

    public int testRun(UiDevice device, Timestamp lt) {
        boolean runTest = false;

        // refer and earn
        String referEarnText = "Refer & Earn";
        String referEarnPage = "Refer & Earn";
        String rewardText = "Rewards";
        String referFriendsText = "Refer Friends & Earn Cash";
        String whatsAppImage = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]";
        int index = 12;
        String instagramImage="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[6]";
        int index2 = 13;
        String shareImage = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[7]";
        int index3 = 14;
        String whatsApp = "WhatsApp";

        Log.d(GlobalVariables.Tag_Name, "Executing Referral tests");

        runTest = checkForElementText(device,referEarnText,"HomePage","Successfully Home page is loaded","Unable to load Home page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device,referEarnText,"referEarnText","Successfully clicked on referEarnText icon","Unable to click on referEarnText icon");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,referEarnPage,"referEarnPage","Successfully Refer Earn page is loaded","Unable to load Refer Earn page");
        if (!runTest) {
            return 1;
        }



        return 0;
    }
}