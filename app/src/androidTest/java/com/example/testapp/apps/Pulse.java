package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.testapp.ExampleInstrumentedTest.sendDataToUIAutomator;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Pulse implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    Map<String, Timestamp> kpis = new HashMap<String, Timestamp>();
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt, timestamp;
    private UiDevice device;

    public double getTime(String elementName) {
        double timestamp = 0;
        try {
            timestamp = kpis.get(elementName).getTime() / 1000.0;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Get KPI Time");
        }
        return timestamp;
    }

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, sendDataToUIAutomator.isvCap());
            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            String homeElementsId = "android.widget.HorizontalScrollView";
            String homeId = "Home";
            String communitiesId = "Communities";
            String collectionsID1 = "android.widget.FrameLayout";
            String collectionsID2 = "android.view.ViewGroup";
            int profileIndex = 0;
            String profileId = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]";
            String editProfileId = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]";
            String backButtonId = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]";
            String accountsId = " My Settings";
            int editProfileIndex = 5;
            String editProfileText = "\uF040";
            String myProfilePageId = "First Name / Given Name";
            String scrollViewId = "android.widget.ScrollView";
            String addressId = "Address";
            String textId = "Indonesia";
            String updateId = "Update";
            String backButtonText = "\uDB80\uDC4D";
            String healthId = "Health";
            String healthCheckId = "Healthcheck";
            String getStartedId = "Get Started";
            String fullAssessmentId = "Full Assessment";
            String okButtonId = "Ok";
//            String chatbotMessageId = "Hi, it's nice to meet you too";
            String chatbotMessageId = "nice to meet you";

            runTest = elementAppear(device, communitiesId, "My Communities", "My Communities Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppearWithClassName(device, homeElementsId, "Home Element", "Home Elements Appear Time:");
            if (!runTest) {
                return 1;
            }

//            runTest = clickOnCollections(device, collectionsID1,collectionsID2,profileIndex, "Accounts", "Clicked On Accounts Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(5000);
//
//            runTest = clickOnProfile(device, "Accounts", "Clicked On Accounts Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(5000);
//
//            runTest = elementAppear(device, accountsId, "My Account", "My Account Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
////            runTest = clickOnCollections(device, collectionsID1,collectionsID2,editProfileIndex, "Edit Profile", "Clicked On Edit Profile Button Time:");
////            if (!runTest) {
////                return 1;
////            }
//
//            Thread.sleep(5000);
//
//            runTest = clickOnEditProfile(device, "Edit Profile", "Clicked On Edit Profile Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(5000);
//
//            runTest = elementAppear(device, myProfilePageId, "My Profile", "My Profile Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = scrollResourceId(device, addressId, scrollViewId, textId,"Enter Address", "Entered Address Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = scrollText(device,updateId,scrollViewId,"Update Button","Clicked on Update Button Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = elementAppear(device, accountsId, "My Updated Account", "My Updated Account Appear Time:");
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(3000);
//            device.pressBack();
//            Thread.sleep(3000);

            runTest = clickOnOnlyText(device,homeId,"Home","Clicked on Home Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnOnlyText(device,healthId,"Health","Clicked on Health Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, healthCheckId, "HealthCheck Appear", "HealthCheck Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnOnlyText(device,healthCheckId,"HealthCheck","Clicked on HealthCheck Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppear(device, getStartedId, "Get Started Appear", "Get Started Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = clickOnOnlyText(device,getStartedId,"Get Started","Clicked on Get Started Button Time:");
            if (!runTest) {
                return 1;
            }

            Thread.sleep(5000);

            runTest = clickOnFullAssessment(device,fullAssessmentId,"Full Assessment","Clicked on Full Assessment Button Time:");
            if (!runTest) {
                return 1;
            }

            runTest = elementAppearElseClick(device, chatbotMessageId,okButtonId, "ChatBot Message", "ChatBot Message Appear Time:");
            if (!runTest) {
                return 1;
            }

            runTest = kpiCalculations();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Pulse_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean clickOnText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().textContains(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnFullAssessment(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().text(textID);
                UiObject idObject = device.findObject(selector);

                UiSelector about15minsID = new UiSelector().text(textID);
                UiObject about15mins = device.findObject(about15minsID);


                if (about15mins.waitForExists(GlobalVariables.Wait_Timeout)){
                    about15mins.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
                else if (idObject.waitForExists(GlobalVariables.Wait_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    public boolean clickOnOnlyText(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean buttonFound = false;
            try {

                Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

                UiSelector selector = new UiSelector().text(textID);
                UiObject idObject = device.findObject(selector);

                if (idObject.waitForExists(GlobalVariables.OneMin_Timeout)) {
                    idObject.click();
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    buttonFound = true;
                }
            } catch (Exception e) {
                Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            }
            return buttonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppear(UiDevice device, String textID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppearElseClick(UiDevice device, String textID,String textID2, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().textContains(textID);
            UiObject element = device.findObject(elementId);

            UiSelector elementId2 = new UiSelector().textContains(textID2);
            UiObject element2 = device.findObject(elementId2);

            UiSelector ofId = new UiSelector().text("of");
            UiObject of = device.findObject(ofId);

            UiSelector theId = new UiSelector().text("the");
            UiObject the = device.findObject(theId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists() || of.exists() || the.exists()) {
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    break;
                } else if(element2.exists()){
                    element2.click();
                    Log.d(GlobalVariables.Tag_Name,"Clicked on Ok Button Time" + new Timestamp(new Date().getTime()));
                }
                else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean elementAppearWithClassName(UiDevice device, String homeElementID, String elementName, String successMsg) {
        try {
            boolean elementFound = false;

            Log.d(GlobalVariables.Tag_Name, "Trying To Execute " + elementName);

            UiSelector elementId = new UiSelector().className(homeElementID);
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists() && element.isEnabled() && element.isFocusable()) {
                    timestamp = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, successMsg + timestamp);
                    kpis.put(elementName, timestamp);
                    elementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading " + elementName);
                }
            }
            return elementFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in " + elementName);
            return false;
        }
    }

    private boolean kpiCalculations() {
        try {

            double homeElementTime = getTime("Home Element");
            double  healthTime = getTime("Health");
            double healthCheckAppearTime = getTime("HealthCheck Appear");
            double healthCheckTime = getTime("HealthCheck");
            double getStartedAppearTime = getTime("Get Started Appear");
            double fullAssessmentTime = getTime("Full Assessment");
            double chatBotMessageTime = getTime("ChatBot Message");

            double TTLH = homeElementTime - (ExampleInstrumentedTest.lt.getTime() / 1000.0);
            double TTLHP = healthCheckAppearTime - healthTime;
            double TTLGSP = getStartedAppearTime - healthCheckTime;
            double TTSHA = chatBotMessageTime - fullAssessmentTime;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Health Page=" + TTLHP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Get Started Page=" + TTLGSP);
            Log.d(GlobalVariables.Tag_Name, "Time To Start Health Assessment=" + TTSHA);

            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            return false;
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }


}
