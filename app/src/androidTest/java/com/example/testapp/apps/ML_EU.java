package com.example.testapp.apps;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;

import static com.mozark.uiautomatorlibrary.utils.FpsUtility.*;

public class ML_EU implements AppClass {

    public static String testId;
    public String log = "";
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    boolean screenRecording, pcapFile;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String startDate;
    String appName;
    public static UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    private Intent intent;
    private Timestamp lt;
    static long T12;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            ML_EU.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            startFpsCapture(device,context);
            ScheduledExecutorService exec = UtilityClass.startScreenshot(device);
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);


            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            Log.d(GlobalVariables.Tag_Name, "Before Sleeping");
            Thread.sleep(10000);
            Log.d(GlobalVariables.Tag_Name, "After Sleeping");

//            runTest = clickBackButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(10000);
//
//            runTest = clickCancelButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(10000);

            runTest = clickAgreeButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Agree Button");
                return 1;
            }

            Thread.sleep(10000);

            runTest = clickClassicButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Classic Button");
                return 1;
            }

            Thread.sleep(10000);

            runTest = clickStartGameButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Start Game Button");
                return 1;
            }

            Thread.sleep(10000);

            runTest = clickEnterButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Enter Button");
                return 1;
            }

            Thread.sleep(10000);

            runTest = clickChooseHero();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Choose Hero Button");
                return 1;
            }

            sleep();

            runTest = playGame();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Play Game");
                return 1;
            }

//            Thread.sleep(10000);

            exec.shutdown();
            Log.d(GlobalVariables.Tag_Name, "Stopped Taking Screenshots");

//            stopScreenRecording();
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            stopFpsCapture(false);
            sendFPSBroadcast(context);
            copyFpsFile(Utility.fileName(testId),testId);
            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private void sendFPSBroadcast(Context context) {
        Intent intent = new Intent();
        intent.setAction(GlobalVariables.AGENT_FPS_ACTION);
        intent.putExtra("T1", "MOBILE\nLEGENDS");
        intent.putExtra("T2", "CLASSIC");
        intent.putExtra("T3", "ENTER");
        intent.putExtra("T4", "Recall");
        intent.putExtra("testId", testId);
        intent.putExtra("T11","Agree");
        intent.putExtra("T12",String.valueOf(T12));
        Log.d(GlobalVariables.Tag_Name,"T12"+String.valueOf(T12));




// NOTE :- KPI for Mobile Legends France
//        TTLH = ((T11-T1) + (T2-T12));

        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setComponent(
                new ComponentName(GlobalVariables.AGENT_NAME, GlobalVariables.AGENT_FPS_RECEIVER_CLASS));
        context.sendBroadcast(intent);
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.MobileLegends_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean clickBackButton() {
        try {
            boolean backButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Back button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 8.43);
                    int h = (int) (height * 5.55);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 8.43% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 5.55% is " + h);

                    device.click(w, h);
//                    device.click(120,40);
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Back Button Time:" + new Timestamp(new Date().getTime()));
                    backButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Back");
                }
            }
            return backButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Back Button Method");
            return false;
        }
    }

    private boolean clickCancelButton() {
        try {
            boolean cancelButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Cancel button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 91.71);
                    int h = (int) (height * 15.97);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 91.71% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 15.97% is " + h);

                    device.click(w, h);
//                    device.click(1305,115);
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Cancel Button Time:" + new Timestamp(new Date().getTime()));
                    cancelButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Cancel");
                }
            }
            return cancelButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Cancel Button Method");
            return false;
        }
    }

     boolean clickAgreeButton() {
        try {
            boolean AgreeButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Agree button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 77.43);
                    int h = (int) (height * 78.98);

//                    int w = (int) (width * 78.98);
//                    int h = (int) (height * 70.20);
                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 77.43% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 78.98% is " + h);

                    device.click(w, h);
                    T12 = new Date().getTime();
//                    A71
//                    s21 resolution - 2,400x1,080 pixels
//                  device.click(1685,853);
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Agree Button Time:" + T12);
                    AgreeButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name,
                            "Error in Click Agree"+e.getMessage());
                }
            }
            return AgreeButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Agree Button Method");
            return false;
        }
    }

    private boolean clickClassicButton() {
        try {
            boolean classicButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Classic button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 53.76);
                    int h = (int) (height * 76.38);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 53.76% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 76.38% is " + h);

                    device.click(w, h);
                    //                    m10
//                    device.click(765,550);
//                    A71
//                    device.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Classic Button Time:" + new Timestamp(new Date().getTime()));
                    classicButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Classic");
                }
            }
            return classicButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Classic Button Method");
            return false;
        }
    }

    private boolean clickStartGameButton() {
        try {
            boolean startGameButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Start Game button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 46.24);
                    int h = (int) (height * 84.44);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 46.24% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 84.44% is " + h);

                    device.click(w, h);
//                    m10
//                    device.click(658,608);
//                    A71
//                    device.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Start Game Button Time:" + new Timestamp(new Date().getTime()));
                    startGameButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Start Game");
                }
            }
            return startGameButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Start Game Button Method");
            return false;
        }
    }

    private boolean clickEnterButton() {
        try {
            boolean enterButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Enter button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 56.92);
                    int h = (int) (height * 87.5);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 56.92% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 87.5% is " + h);

                    device.click(w, h);
//                    m10
//                    device.click(810,630);
//                    A71
//                    device.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Enter Button Time:" + new Timestamp(new Date().getTime()));
                    enterButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Enter");
                }
            }
            return enterButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Enter Button Method");
            return false;
        }
    }

    private boolean clickChooseHero() {
        try {
            boolean chooseHeroButton = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to find Choose Hero button with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;
                    int w = (int) (width * 90.30);
                    int h = (int) (height * 77.77);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 90.35% is " + w);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 77.77% is " + h);

                    device.click(1455, h);
//                    m10
//                    device.click(1455,560);
//                    A71
//                    device.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Choose Hero Button Time:" + new Timestamp(new Date().getTime()));
                    chooseHeroButton = true;
                    break;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Click Choose Hero");
                }
            }
            return chooseHeroButton;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Choose Hero Button Method");
            return false;
        }
    }

    private void sleep() {
        try {
            for (int i = 0; i < 15; i++) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Tap on the Screen with Attempt no." + (i + 1));
                Thread.sleep(9000);
                device.click(360, 711);
                Log.d(GlobalVariables.Tag_Name, "Clicked on Device Screen to Stay Awake with Attempt no." + (i + 1) + " and Time:" + new Timestamp(new Date().getTime()));
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sleep timeout");
        }
    }

    private boolean playGame() {
        try {
            boolean play = false;
            for (int i = 0; i < 15; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to Move and Hit Enemy with Attempt no." + (i + 1));

                    int x = device.getDisplayWidth();
                    int y = device.getDisplayHeight();
                    Log.d(GlobalVariables.Tag_Name, "Total Width of the Device Screen:- " + x);
                    Log.d(GlobalVariables.Tag_Name, "Total Height of the Device Screen:- " + y);
                    double width = x / 100.00;
                    double height = y / 100.00;

                    // Move Button Co-Ordinates
                    int w1 = (int) (width * 21.08);
                    int h1 = (int) (height * 83.33);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 21.08% is " + w1);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 83.33% is " + h1);

                    // Hit Button Co-Ordinates
                    int w2 = (int) (width * 98.73);
                    int h2 = (int) (height * 92.36);

                    Log.d(GlobalVariables.Tag_Name, "Value of Width after 98.73% is " + w2);
                    Log.d(GlobalVariables.Tag_Name, "Value of Height after 92.36% is " + h2);

                    // Moves Right
                    device.drag(w1, h1, w1 + 100, h1 - 40,3);
                    device.click(w2, h2);

                    // Moves Top
                    device.drag(w1, h1, w1 - 20, h1 - 200, 3);
                    device.click(w2, h2);

                    // Moves Left
                    device.drag(w1, h1, w1 - 190, h1 - 60, 3);
                    device.click(w2, h2);

                    // Moves Bottom
                    device.drag(w1, h1, w1, h1 + 100, 3);
                    device.click(w2, h2);

                    Log.d(GlobalVariables.Tag_Name, "Hero Moved and Clicked on Hit Button Time:" + new Timestamp(new Date().getTime()));
                    play = true;

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Play Game");
                }
            }
            return play;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Playing Game Method");
            return false;
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

//    private static String getScreenshotCommand() {
//        return "adb shell -p screencap /sdcard/" + new Timestamp(new Date().getTime()) + ".png";
//    }
//
//    static Runnable capScreenshot = new Runnable() {
//        public void run() {
//            try {
//                UiDevice device = null;
//                device.executeShellCommand(getScreenshotCommand());
//                Log.d(GlobalVariables.Tag_Name,"Captured Screen Successfully");
//            } catch (Exception e) {
//                Log.d(GlobalVariables.Tag_Name,"Error in taking Screenshot");
//            }
//        }
//    };

}

