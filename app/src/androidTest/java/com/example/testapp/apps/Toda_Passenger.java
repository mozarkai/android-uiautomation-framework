package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


public class Toda_Passenger implements AppClass{

    public static String testId;
    public String log = "";
    long timer, End_time_Video, click_on_video_time, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    boolean screenRecording, pcapFile;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String OUTPUT_LOG_FILE = "";
    private Intent intent;
    private Timestamp lt, st,et, vt, hpt;
    private UiDevice device;
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

            String homePageID = "Terms of Use";
            String checkBoxID = "android.widget.CheckBox";
            String pickupID = "Pick up";
            String droppOffID = "Drop off";
            String crossButtonID = "android.widget.Button";
            String enterFromID = "From";
            String enterFromText = "SM City East Ortigas";
//            String selectFromID = "SM City East Ortigas Ortigas Avenue Extension, Brgy, Pasig, Metro Manila, Philippines";
            String selectFromID = "Philippines";
            String enterToID = "To";
            String enterToText = "6 Gemini St.";
            String selectToID = "6 Gemini St. Villarica Subdivision Cainta, Metro Manila, Philippines";
            String priceID = "₱";
            String bookNowID = "Book now";
            String searchingDriverID = "Searching for a driver";

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            runTest = loadHomePage(homePageID);
            if (!runTest) {
                return 1;
            }

            runTest = checkBox(checkBoxID);
            if (!runTest) {
                return 1;
            }

            runTest = clickPickUp(pickupID);
            if (!runTest) {
                return 1;
            }

            runTest = crossButton(crossButtonID);
            if (!runTest) {
                return 1;
            }

            runTest = enterFrom(enterFromID,enterFromText);
            if (!runTest) {
                return 1;
            }

            runTest = selectFrom(selectFromID);
            if (!runTest) {
                return 1;
            }

            runTest = enterTo(enterToID,enterToText);
            if (!runTest) {
                return 1;
            }

            runTest = selectTo(selectToID);
            if (!runTest) {
                return 1;
            }

            runTest = price(priceID);
            if (!runTest) {
                return 1;
            }

            runTest = bookNow(bookNowID);
            if (!runTest) {
                return 1;
            }

            runTest = searchDriver(searchingDriverID);
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();

            KpiLogs.twoKPI(lt, st, vt);

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
            return 1;
        }
        return 0;
    }

    public boolean loadHomePage(String homePageID) {
        try {
            UiSelector homePageId = new UiSelector().textContains(homePageID);
            UiObject homePage = device.findObject(homePageId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (homePage.exists()) {
                    hpt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + hpt);
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page");
                }
            }
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
            return false;
        }
    }

    public boolean checkBox(String checkBoxID) {
        try {
            boolean checkBoxFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find check box with Attempt no." + (i + 1));

                    UiSelector checkBoxId = new UiSelector().className(checkBoxID);
                    UiObject checkBox = device.findObject(checkBoxId);
                    if (checkBox.waitForExists(GlobalVariables.Wait_Timeout)) {
                        checkBox.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
                        checkBoxFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Check Box");
                }
            }
            return checkBoxFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding check box Method");
            return false;
        }
    }

    public boolean clickPickUp(String pickupID) {
        try {

            boolean pickupFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find Pickup with Attempt no." + (i + 1));

                    UiSelector pickupId = new UiSelector().textContains(pickupID);
                    UiObject pickup = device.findObject(pickupId);
                    if (pickup.waitForExists(GlobalVariables.Wait_Timeout)) {
                        pickup.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Pickup Button Time:" + st);
                        pickupFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in clicking Pickup");
                }
            }
            return pickupFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Pickup Method");
            return false;
        }
    }

    public boolean crossButton(String crossButtonID) {
        try {

            boolean crossButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find Cross Button with Attempt no." + (i + 1));

                    UiSelector crossButtonId = new UiSelector().className(crossButtonID);
                    UiObject crossButton = device.findObject(crossButtonId);
                    if (crossButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        crossButton.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Cross Button Time:" + st);
                        crossButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in clicking Cross Button");
                }
            }
            return crossButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Cross Button Method");
            return false;
        }
    }

    public boolean enterFrom(String enterFromID,String enterFromText) {
        try {
            Log.d(GlobalVariables.Tag_Name,"Before Sleeping");
            Thread.sleep(30000);
            Log.d(GlobalVariables.Tag_Name,"After Sleeping");
            boolean enterFromtext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter From Text with Attempt no." + (i + 1));
                    UiSelector enterFromId =
                            new UiSelector().textContains(enterFromID);
                    UiObject enterFrom = device.findObject(enterFromId);

                    if (enterFrom.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterFrom.click();
                        device.executeShellCommand("input text "+ enterFromText);
//                        enterFrom.setText(enterFromText);
//                        device.pressEnter();
                        et = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Enter From Text" + et);
                        enterFromtext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter From Text Failed");
                }
            }
            return enterFromtext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering From Text");
            return false;
        }
    }

    public boolean selectFrom(String selectFromID) {

        try {
            boolean selectFromFound = false;
            for (int i = 0; i < 12; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to Select From with Attempt no." + (i + 1));

                    UiSelector selectFromId = new UiSelector().textContains(selectFromID);
                    UiObject selectFrom = device.findObject(selectFromId);
                    if (selectFrom.waitForExists(GlobalVariables.TimeOut)) {
                        selectFrom.click();
                        Log.d(
                                GlobalVariables.Tag_Name,
                                "Select From Time:" + new Timestamp(new Date().getTime()));
                        selectFromFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in Selecting From");
                }
            }
            return selectFromFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Select From Method");
            return false;
        }
    }

    public boolean enterTo(String enterToID,String enterToText) {
        try {
            boolean enterTotext = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter To Text with Attempt no." + (i + 1));
                    UiSelector enterToId =
                            new UiSelector().textContains(enterToID);
                    UiObject enterTo = device.findObject(enterToId);

                    if (enterTo.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterTo.setText(enterToText);
                        device.pressEnter();
                        et = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Enter To Text" + et);
                        enterTotext = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter To Text Failed");
                }
            }
            return enterTotext;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering To Text");
            return false;
        }
    }

    public boolean selectTo(String selectToID) {
        try {
            boolean selectToFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Trying to Select To with Attempt no." + (i + 1));

                    UiSelector selectToId = new UiSelector().textContains(selectToID);
                    UiObject selectTo = device.findObject(selectToId);
                    if (selectTo.waitForExists(GlobalVariables.TimeOut)) {
                        selectTo.click();
                        Log.d(
                                GlobalVariables.Tag_Name,
                                "Select To Time:" + new Timestamp(new Date().getTime()));
                        selectToFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Error in Selecting To");
                }
            }
            return selectToFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Select To Method");
            return false;
        }
    }

    public boolean price(String priceID) {
        try {
            boolean priceFound = false;

            UiSelector priceId = new UiSelector().textContains(priceID);
            UiObject price = device.findObject(priceId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (price.exists()) {
                    hpt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Price Appear Time:" + hpt);
                    priceFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Price");
                }
            }
            return priceFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Price");
            return false;
        }
    }

    public boolean bookNow(String bookNowID) {
        try {
            boolean bookNowButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find Book Now Button with Attempt no." + (i + 1));

                    UiSelector bookNowId = new UiSelector().textContains(bookNowID);
                    UiObject bookNow = device.findObject(bookNowId);
                    if (bookNow.waitForExists(GlobalVariables.Wait_Timeout)) {
                        bookNow.click();
                        st = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Book Now Button Time:" + st);
                        bookNowButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in clicking Book Now Button");
                }
            }
            return bookNowButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Book Now Button Method");
            return false;
        }
    }

    public boolean searchDriver(String searchDriverID) {
        try {
            boolean serachDriverFound = false;

            UiSelector searchDriverId = new UiSelector().textContains(searchDriverID);
            UiObject searchDriver = device.findObject(searchDriverId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (searchDriver.exists()) {
                    hpt = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Searched Driver Time:" + hpt);
                    serachDriverFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Searching for a Driver");
                }
            }
            return serachDriverFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Price");
            return false;
        }
    }

    public String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Toda_Passenger_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
