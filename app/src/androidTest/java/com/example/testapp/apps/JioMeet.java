package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.example.testapp.ExampleInstrumentedTest.vCap;

public class JioMeet implements AppClass{

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);
      context = TestApp.getContext();


      runTest = clickJoinMeetingButton();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Join Meeting Button");
        return 1;
      }
      runTest = joinMeetingPageLoaded();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Load Join Meeting Page");
        return 1;
      }
      runTest = enterMeetingID();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Meeting ID");
        return 1;
      }
      runTest = enterPassword();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Password");
        return 1;
      }
      runTest = enterName();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Name");
        return 1;
      }
      runTest = clickNextButton();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Next Button");
        return 1;
      }
      runTest = joinMeeting();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Join Meeting");
        return 1;
      }
      runTest = enteredMeeting();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Meeting Element");
        return 1;
      }
      runTest = loaderAppear(vCap);
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Check Loader Element");
        return 1;
      }
      runTest = clickScreen();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Screen");
        return 1;
      }
      runTest = leaveMeeting();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Leave Meeting");
        return 1;
      }
      runTest = leaveConfirmation();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Click Confirmation");
        return 1;
      }



      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private boolean clickJoinMeetingButton() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector searchAppear = new UiSelector().resourceId("com.jio.rilconferences:id/buttonJoinMeeting");
      UiObject searchAppearObject = device.findObject(searchAppear);

      UiSelector secSearchAppear = new UiSelector().textContains("Join").className("android.widget.Button");
      UiObject secSearchAppearObject = device.findObject(secSearchAppear);

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Clicking Join Meeting Button");
        if(searchAppearObject.exists())
        {
          searchAppearObject.click();
          if (!searchAppearObject.exists())
          {
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            Log.d(GlobalVariables.Tag_Name, "CLicked on First JoinButton Time:" + st);
            value = true;
            break;
          }
          else
          {
            Log.d(GlobalVariables.Tag_Name, "Miss Clicked");
          }
        }
        else if(secSearchAppearObject.exists())
        {
          secSearchAppearObject.click();
          if (!secSearchAppearObject.exists())
          {
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            Log.d(GlobalVariables.Tag_Name, "CLicked on First JoinButton Time:" + st);
            value = true;
            break;
          }
        }
        else
        {
          Log.d(GlobalVariables.Tag_Name, "Join Button Loading");
          Thread.sleep(200);
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Join Button");
      return false;
    }
    return value;
  }

  private boolean joinMeetingPageLoaded() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector joinMeetingSelector = new UiSelector().resourceId("com.jio.rilconferences:id/textJoinMeetingTitle");
      UiObject joinMeetingObject = device.findObject(joinMeetingSelector);

      UiSelector secJoinMeetingSelector = new UiSelector().resourceId("com.jio.rilconferences:id/inputMeetingLink");
      UiObject secJoinMeetingObject = device.findObject(secJoinMeetingSelector);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Load Join Meeting Page");
        if(joinMeetingObject.exists() || secJoinMeetingObject.exists())
        {
          Timestamp unUsableTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Join Meeting Page Loaded Successfully time: " + unUsableTime);
          value = true;
          break;
        }
        else
        {
          Log.d(GlobalVariables.Tag_Name, "Join Meeting Page Loading");
          Thread.sleep(200);
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error To Load Join Meeting Page");
      return false;
    }
    return value;
  }

  private boolean enterMeetingID() {
    boolean value = false;
    try {
      Thread.sleep(2000);
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector meetingIDField = new UiSelector().resourceId("com.jio.rilconferences:id/inputMeetingLink");
      UiObject meetingIDFieldObject = device.findObject(meetingIDField);

      UiSelector secMeetingIDField = new UiSelector().className("android.widget.EditText");
      UiObject secMeetingIDFieldObject = device.findObject(secMeetingIDField);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Click Meeting Field");
        if(meetingIDFieldObject.exists())
        {
          meetingIDFieldObject.click();
          Thread.sleep(2000);
          meetingIDFieldObject.setText("2646110995");
          Timestamp unUsableTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Enter Meeting ID Successfully time: " + unUsableTime);
          value = true;
          device.pressBack();
          break;
        }
        else if (secMeetingIDFieldObject.exists())
        {
          secMeetingIDFieldObject.click();
          Thread.sleep(2000);
          secMeetingIDFieldObject.setText("2646110995");
          Timestamp unUsableTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Enter Meeting ID Successfully time: " + unUsableTime);
          value = true;
          device.pressBack();
          break;
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Meeting ID");
      return false;
    }
    return value;
  }

  private boolean enterPassword() {
    boolean value = false;
    try {
      Thread.sleep(2000);
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/inputMeetingPassword");
      UiObject object = device.findObject(selector);

      UiSelector selectorSec = new UiSelector().textContains("Enter Meeting Password").className("android.widget.EditText");
      UiObject objectSec = device.findObject(selectorSec);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Click and Enter Password");
        if (object.exists()) {
          object.click();
//                    device.pressKeyCode(KeyEvent.KEYCODE_S, KeyEvent.META_SHIFT_ON);
          object.setText("8gbtH");
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name,"Enter Password Successfully Time: " + unUsable);
          value = true;
          device.pressBack();
          break;
        }
        else if (objectSec.exists()) {
          objectSec.click();
          objectSec.setText("8gbtH");
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name,"Enter Password Successfully Time: " + unUsable);
          value = true;
          device.pressBack();
          break;
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Password");
      return false;
    }
    return value;
  }

  private boolean enterName() {
    boolean value = false;
    try {
      Thread.sleep(2000);
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/inputUserName");
      UiObject object = device.findObject(selector);

      UiSelector selectorSec = new UiSelector().textContains("Enter Your Name").className("android.widget.EditText");
      UiObject objectSec = device.findObject(selectorSec);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Click and Enter Name");
        if (object.exists()) {
          object.click();
          object.setText("Mozark");
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name,"Enter Name Successfully Time: " + unUsable);
          value = true;
          device.pressBack();
          break;
        }
        else if (objectSec.exists()) {
          objectSec.click();
          objectSec.setText("Mozark");
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name,"Enter Name Successfully Time: " + unUsable);
          value = true;
          device.pressBack();
          break;
        }
        else
        {
          Log.d(GlobalVariables.Tag_Name,"No Name Field");
          value = true;
          break;
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Name");
      return false;
    }
    return value;
  }

  private boolean clickNextButton() {
    boolean value = false;
    try {
      Thread.sleep(2000);
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector nextButton = new UiSelector().resourceId("com.jio.rilconferences:id/buttonNext");
      UiObject nextButtonObject = device.findObject(nextButton);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Click Next Button");
        if (nextButtonObject.exists()) {
          nextButtonObject.click();
          Timestamp unUsableTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Next Button Clicked Successfully Time: " + unUsableTime);
          value = true;
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Next Button");
          Thread.sleep(200);
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Next Button");
      return value;
    }
    return value;
  }

  private boolean joinMeeting() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/buttonJoinMeeting");
      UiObject object = device.findObject(selector);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Click Join Button ");
        if (object.exists()) {
          object.click();
          vt = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
          value = true;
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Join");
          Thread.sleep(200);
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Join");
      return value;
    }
  }

  private boolean enteredMeeting() {
    boolean value = false;
    try {

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/controlButtonMainLayout");
      UiObject object = device.findObject(selector);

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Load The Meeting Page");
        if (object.exists()) {
          Log.d(GlobalVariables.Tag_Name, "Entered Meeting Successfully Time: " + new Timestamp(new Date().getTime()));
          value = true;
          break;
        }else {
          Log.d(GlobalVariables.Tag_Name,"Loading in Meeting Page");
          Thread.sleep(200);
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Meeting Page");
      return value;
    }
  }

  private boolean loaderAppear(Boolean vCap) {
    try {
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);

      UiSelector loaderId = new UiSelector()
          .resourceId("com.jio.rilconferences:id/textView_reconnecting");
      UiObject loader = device.findObject(loaderId);

      UiSelector secLoaderId = new UiSelector().textContains("Reconnecting");
      UiObject secLoader = device.findObject(secLoaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        if (loader.exists() || secLoader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderNo);
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean clickScreen() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/buttonLeave");
      UiObject object = device.findObject(selector);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Click Screen");
        if (!object.exists()) {
          int x = device.getDisplayWidth();
          int y = device.getDisplayHeight();
          double width = x / 100.00;
          double height = y / 100.00;
          int w = (int) (width * 50.00);
          int h = (int) (height * 40.00);
          device.click(w,h);
          Log.d(GlobalVariables.Tag_Name,"Clicked Screen");
          Thread.sleep(200);
        } else {
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Clicked Screen Successfully Time: " + unUsable);
          value = true;
          break;
        }
      }
      return value;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Screen");
      return value;
    }
  }

  private boolean leaveMeeting() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector leaveButton = new UiSelector().resourceId("com.jio.rilconferences:id/buttonLeave");
      UiObject leaveButtonObject = device.findObject(leaveButton);

      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name,"Clicking Leave Button");
        if (leaveButtonObject.exists()) {
          leaveButtonObject.click();
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Clicked Leave Button Time: " + unUsable);
          value = true;
          break;
        }
        else {
          Log.d(GlobalVariables.Tag_Name,"Loading Clicking Leave Button");
          Thread.sleep(200);
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Leave Button");
      return false;
    }
    return value;
  }
  private boolean leaveConfirmation() {
    boolean value = false;
    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      UiSelector selector = new UiSelector().resourceId("com.jio.rilconferences:id/meetingOptionLeave");
      UiObject object = device.findObject(selector);
      while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
        Log.d(GlobalVariables.Tag_Name,"Clicking Leave Confirmation");
        if (object.exists()) {
          object.click();
          Timestamp unUsable = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Clicked Leave Confirmation Time " + unUsable);
          value = true;
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Leave Confirmation");
          Thread.sleep(200);
        }
      }
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking Leave Confirmation");
      return false;
    }
    return value;
  }



  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.JioMeet_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
