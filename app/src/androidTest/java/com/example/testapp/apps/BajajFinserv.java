package com.example.testapp.apps;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;


public class BajajFinserv implements AppClass {

    public static String testId;
    public String log = "";
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    String mobileNumber = null;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    private Timestamp lt, sbat, sbt, sdbt, bnbt, vt, opat, plat, tpat, svt, tutt;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate=startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

//            startScreenRecording(testId, GlobalVariables.screenRecord_Time);
//            Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");

            File sdcard = Environment.getExternalStorageDirectory();

            try {
                File file = new File(sdcard, "file.txt");
                Scanner reader = new Scanner(file);
                while (reader.hasNextLine()) {
                    mobileNumber = reader.nextLine();
                    Log.d(GlobalVariables.Tag_Name, "Mobile Number is:" + mobileNumber);
                }
                reader.close();
            } catch (FileNotFoundException e) {
                Log.d(GlobalVariables.Tag_Name, "Error in finding mobile number from file");
                e.printStackTrace();
            }

            runTest = enterMobileNo(mobileNumber);
            if (!runTest) {
                return 1;
            }

            runTest = clickContinue();
            if (!runTest) {
                return 1;
            }

            runTest = salariedAppear();
            if (!runTest) {
                return 1;
            }

            runTest = clickSalaried();
            if (!runTest) {
                return 1;
            }

            runTest = offersPageAppear();
            if (!runTest) {
                return 1;
            }

            runTest = clickSeeDetails();
            if (!runTest) {
                return 1;
            }

            runTest = personalLoanDetails();
            if (!runTest) {
                return 1;
            }

            Thread.sleep(3000);
            device.pressBack();
            Thread.sleep(3000);

            runTest = clickBookNow();
            if (!runTest) {
                return 1;
            }

            runTest = travelEMIPageAppear();
            if (!runTest) {
                return 1;
            }

            runTest = playVideo();
            if (!runTest) {
                return 1;
            }

            runTest = bufferRead();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            Log.d(GlobalVariables.Tag_Name, "Video Recording Stopped Successfully");

            runTest = kpiCalculation();
            if (!runTest) {
                return 1;
            }

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData();
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.BajajFinserv_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean enterMobileNo(String mobileNumber) {
        try {
            boolean enterMobileNo = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to enter Mobile Number with Attempt no." + (i + 1));
                    UiSelector enterMobileField = new UiSelector().resourceId("org.altruist.BajajExperia:id/edt_mobile_no");
                    UiObject enterMobile = device.findObject(enterMobileField);

                    if (enterMobile.waitForExists(GlobalVariables.Wait_Timeout)) {
                        enterMobile.setText(mobileNumber);
                        Log.d(
                                GlobalVariables.Tag_Name,
                                "Entered Mobile Number Time:" + new Timestamp(new Date().getTime()));
                        enterMobileNo = true;
                        break;
                    }

                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Enter Mobile Number Failed");
                }
            }
            return enterMobileNo;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Entering Mobile Number");
            return false;
        }
    }

    private boolean clickContinue() {
        try {
            boolean continueButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find Continue button with Attempt no." + (i + 1));
                    UiSelector continueButtonID = new UiSelector().resourceId("org.altruist.BajajExperia:id/btn_continue");
                    UiObject continueButton = device.findObject(continueButtonID);
                    if (continueButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        continueButton.click();
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Continue Button Time:" + new Timestamp(new Date().getTime()));
                        continueButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Click on Continue Button Failed");
                }
            }
            return continueButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Continue Button Method");
            return false;
        }
    }

    private boolean salariedAppear() {
        try {
            boolean salaryButtonFound = false;

            UiSelector salariedID = new UiSelector().textContains("Salaried");
            UiObject salaried = device.findObject(salariedID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (salaried.exists()) {
                    Log.v(GlobalVariables.Tag_Name, "Stopwatch Time when Exists:" + stopWatch.getTime());
                    sbat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Salaried Button Appear Time:" + sbat);
                    salaryButtonFound = true;
                    return salaryButtonFound;
                } else {
                    Log.v(GlobalVariables.Tag_Name, "Stopwatch Time when Not Exists:" + stopWatch.getTime());
                    Log.d(GlobalVariables.Tag_Name, "Searching for Salaried Button");
                }
            }
            return salaryButtonFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Salaried Button");
            return false;
        }
    }

    private boolean clickSalaried() {
        try {
            boolean salariedButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find Salaried button with Attempt no." + (i + 1));
                    UiSelector salariedButtonID = new UiSelector().resourceId("org.altruist.BajajExperia:id/img_emp_type_salaried");
                    UiObject salariedButton = device.findObject(salariedButtonID);
                    if (salariedButton.waitForExists(GlobalVariables.Wait_Timeout)) {
                        salariedButton.click();
                        sbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Salaried Button Time:" + sbt);
                        salariedButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Salaried Button Failed");
                }
            }
            return salariedButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Salaried Button Method");
            return false;
        }
    }

    private boolean offersPageAppear() {
        try {
            boolean offersPageFound = false;

            UiSelector offersPageID = new UiSelector().resourceId("org.altruist.BajajExperia:id/iv_offer_icon");
            UiObject offersPage = device.findObject(offersPageID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (offersPage.exists() && offersPage.isEnabled()) {
                    opat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Offers Page Appear Time:" + opat);
                    offersPageFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Searching for Offers Page");
                }
            }
            return offersPageFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Offers Page");
            return false;
        }
    }

    private boolean clickSeeDetails() {
        try {
            boolean seeDetailsButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(
                            GlobalVariables.Tag_Name, "Trying to find Salaried button with Attempt no." + (i + 1));

                    UiSelector seeDetailsID = new UiSelector().textContains("See details");
                    UiObject seeDetails = device.findObject(seeDetailsID);
                    if (seeDetails.waitForExists(GlobalVariables.Wait_Timeout)) {
                        seeDetails.click();
                        sdbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on See Details Button Time:" + sdbt);
                        seeDetailsButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Click on See Details Button Failed");
                }
            }
            return seeDetailsButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding See Details Button Method");
            return false;
        }
    }

    private boolean personalLoanDetails() {
        try {
            boolean personalLoanFound = false;

            UiSelector personalloanID = new UiSelector().textContains("Personal Loan @1104/Lakh* EMI - Apply Instant Personal Loan Online");
            UiObject personalloan = device.findObject(personalloanID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (personalloan.exists() && personalloan.isFocused() && personalloan.isEnabled() && personalloan.isFocusable()) {
                    plat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Personal Loan Page Appear Time:" + plat);
                    personalLoanFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Searching for Personal Loan Page");
                }
            }
            return personalLoanFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Personal Loan Page");
            return false;
        }
    }

    private boolean clickBookNow() {
        try {
            boolean bookNowButtonFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to find Book Now button with Attempt no." + (i + 1));

                    UiSelector bookNowID = new UiSelector().textContains("BOOK NOW");
                    UiObject bookNow = device.findObject(bookNowID);
                    if (bookNow.waitForExists(GlobalVariables.Wait_Timeout) && bookNow.isClickable()) {
                        bookNow.click();
                        bnbt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked on Book Now Button Time:" + bnbt);
                        bookNowButtonFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Click on Book Now Button Failed");
                }
            }
            return bookNowButtonFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding Book Now Button Method");
            return false;
        }
    }

    private boolean travelEMIPageAppear() {
        try {
            boolean travelPageFound = false;

            UiSelector travelPageID = new UiSelector().text("Next");
            UiObject travelPage = device.findObject(travelPageID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (travelPage.isClickable() && travelPage.isEnabled()) {
                    tpat = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Travel EMI Page Appear Time:" + tpat);
                    travelPageFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Searching for Travel EMI Page");
                }
            }
            return travelPageFound;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Travel EMI Page");
            return false;
        }
    }

    public boolean playVideo() {
        try {
            boolean playBtnFound = false;
            for (int i = 0; i < 5; i++) {
                try {
                    Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
                    UiCollection playId = new UiCollection(new UiSelector().resourceId("video-gcc-b4ebe7da-b9f3-4320-83e5-368a8655914a"));
                    UiObject playBtn = playId.getChildByInstance(new UiSelector(), 1);

                    UiScrollable items = new UiScrollable(new UiSelector().className("android.webkit.WebView"));

                    UiObject Play1 = device.findObject(new UiSelector().textContains("How to use your Bajaj Finserv EMI Network Card on MakeMyTrip"));
                    UiObject Play3 = device.findObject(new UiSelector().textContains("Now make all your hotel bookings and pay for your flight tickets on EMI"));

                    if (items.scrollIntoView(Play1) && items.scrollIntoView(Play3)) {
                        playBtn.click();
                        vt = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
                        playBtnFound = true;
                        break;
                    }
                } catch (Exception e) {
                    Log.d(GlobalVariables.Tag_Name, "Click on Play Button Failed");
                }
            }
            return playBtnFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Play Video Method");
            return false;
        }
    }

    private boolean bufferRead() {
        try {

            UiSelector loaderId =
                    new UiSelector().resourceId("loading-spinner");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            int time = 0;
            int c = GlobalVariables.Delay_Time;
            UiCollection playId = new UiCollection(new UiSelector().resourceId("video-gcc-b4ebe7da-b9f3-4320-83e5-368a8655914a"));
            UiObject playBtn = playId.getChildByInstance(new UiSelector(), 1);
            boolean clickCheck = false;
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
                } else {
                    if (!clickCheck) {
                        playBtn.click();
                        clickCheck = true;
                    }
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
                }

                time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
                if (time > c) {
                    Utility.cpuUsage("Bajaj", OUTPUT_LOG_FILE_TEMP);
                    c += GlobalVariables.Delay_Time;
                }
            }
            return clickCheck;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    public boolean kpiCalculation() {
        boolean kpiValue = false;
        try {

            Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
            Log.d(GlobalVariables.Tag_Name, "Salaried Button Appear Time:" + sbat);
            Log.d(GlobalVariables.Tag_Name, "Clicked on Salaried Button Time:" + sbt);
            Log.d(GlobalVariables.Tag_Name, "Offers Page Appear Time:" + opat);
            Log.d(GlobalVariables.Tag_Name, "Clicked on See Details Button Time:" + sdbt);
            Log.d(GlobalVariables.Tag_Name, "Personal Loan Page Appear Time:" + plat);
            Log.d(GlobalVariables.Tag_Name, "Clicked on Book Now Button Time:" + bnbt);
            Log.d(GlobalVariables.Tag_Name, "Travel EMI Page Appear Time:" + tpat);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);

            double TLHP = (sbat.getTime() - ExampleInstrumentedTest.lt.getTime()) / 1000.0;
            double TLRP = (opat.getTime() - sbt.getTime()) / 1000.0;
            double TLPP = (plat.getTime() - sdbt.getTime()) / 1000.0;
            double TLTP = (tpat.getTime() - bnbt.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TLHP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Recommended Offer Page=" + TLRP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Personal Loan Page=" + TLPP);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Travel on EMI Page=" + TLTP);

            kpiValue = true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
        return kpiValue;
    }

    public boolean sendData() {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, "1.0", appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

}
