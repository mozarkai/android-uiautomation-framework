package com.example.testapp.apps;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

public class Snapchat implements AppClass {

    private Timestamp HPT,EST,SRT,CVPT,VPD,CVT,VPT;
    private Timestamp DeliveryTime,
            vt,
            et,
            ht,
            it,
            het,
            sat,
            ClickCartTime,
            TimeToLoad,
            ClickCheckoutTime,
            CheckoutAppearTime;

    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, heat, sqct, mdat, pbt, psat, bayt, bant, enterUPI_Balance, enterUPI_Payment;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = gotoSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Go To Search");
                return 1;
            }
            runTest = enterSearchText();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                return 1;
            }
            runTest = searchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Search Result");
                return 1;
            }
            runTest = clickSearchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Search Result");
                return 1;
            }
            runTest = videoPageDisplayed();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Display Videos");
                return 1;
            }
            runTest = scrollDown();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Scroll Down");
                return 1;
            }
            runTest = clickVideo();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Videos");
                return 1;
            }
            runTest = videoStartPlaying();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Play Videos");
                return 1;
            }
            runTest = bufferRead();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Buffer");
                return 1;
            }
            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check KPI's");
                return 1;
            }
            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }


    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.Timeout);
            device.executeShellCommand(getCommand(GlobalVariables.SnapchatPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }


    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Home Element");

                UiSelector homeElementId = new UiSelector().resourceId("com.snapchat.android:id/avatar_silhouette_3");
                UiObject homeElement = device.findObject(homeElementId);

                UiSelector secHomeElementId = new UiSelector().resourceId("com.snapchat.android:id/ngs_navigation_bar");
                UiObject secHomeElement = device.findObject(secHomeElementId);
                if (homeElement.exists() || secHomeElement.exists()) {
                    HPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + HPT);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return homeElementFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return false;
        }
    }

    private boolean gotoSearch() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking Search Button");

                UiSelector searchHome = new UiSelector().resourceId("com.snapchat.android:id/hova_header_search_icon");
                UiObject searchHomeObject = device.findObject(searchHome);
                if(searchHomeObject.exists())
                {
                    searchHomeObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search Button Clicked Successfully time: " + unUsableTime);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking search Button");
            return false;
        }
        return value;
    }

    private boolean enterSearchText() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            Log.d(GlobalVariables.Tag_Name, "Entering Text");

            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                device.pressKeyCode(KeyEvent.KEYCODE_E);
                device.pressKeyCode(KeyEvent.KEYCODE_N);
                device.pressKeyCode(KeyEvent.KEYCODE_T);
                device.pressKeyCode(KeyEvent.KEYCODE_E);
                device.pressKeyCode(KeyEvent.KEYCODE_R);
                device.pressKeyCode(KeyEvent.KEYCODE_T);
                device.pressKeyCode(KeyEvent.KEYCODE_A);
                device.pressKeyCode(KeyEvent.KEYCODE_I);
                device.pressKeyCode(KeyEvent.KEYCODE_N);
                device.pressKeyCode(KeyEvent.KEYCODE_M);
                device.pressKeyCode(KeyEvent.KEYCODE_E);
                device.pressKeyCode(KeyEvent.KEYCODE_N);
                device.pressKeyCode(KeyEvent.KEYCODE_T);
                device.pressBack();
                EST = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Enter Search Text Time: " + EST);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
        return value;
    }

    private boolean searchResult() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Search Result");

                UiSelector moreSelector = new UiSelector().textContains("Entertainment");
                UiObject moreObject = device.findObject(moreSelector);

                if (moreObject.exists()) {
                    SRT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search Result Time: " + SRT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return value;
        }
        return value;
    }

    private boolean clickSearchResult() {
        boolean value = false;
        try {
            Thread.sleep(5000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Click Search Result ");

                UiSelector newsInsiderSelector = new UiSelector().textContains("Entertainment").className("android.widget.TextView");
                UiObject newsInsiderObject = device.findObject(newsInsiderSelector);

                UiSelector searchResultsID = new UiSelector().textContains("Welcome").className("android.widget.TextView");
                UiObject searchResults = device.findObject(searchResultsID);

                if (newsInsiderObject.exists())
                {
                    newsInsiderObject.click();
                    CVPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Click Search Result Time: " + CVPT);
                    value = true;
                    break;
                }
                else if (searchResults.exists()) {
                    searchResults.click();
                    CVPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Click Search Result Time: " + CVPT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Search Result");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Search Result");
            return value;
        }
    }

    private boolean videoPageDisplayed() {
        boolean videoAppearFound = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Finding Videos Page");

                UiSelector selector = new UiSelector().textContains("Subscribers");
                UiObject object = device.findObject(selector);
                if (object.exists()) {
                    VPD = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Videos Appear Time:" + VPD);
                    videoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Video Page");
                    Thread.sleep(200);
                }
            }
            return videoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Video Page");
            return false;
        }
    }

    private boolean scrollDown() {
        boolean videoAppearFound = false;
        try {
            Log.d(GlobalVariables.Tag_Name,"Scroll Videos Page");
            int x = device.getDisplayWidth();
            int y = device.getDisplayHeight();
            double width = x / 100.00;
            double height = y / 100.00;
            int w = (int) (width * 50.00);
            int h = (int) (height * 40.00);
            device.drag(w, h, w - 20, h - 200, 5);
            videoAppearFound = true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Video Page");
            return false;
        }
        return videoAppearFound;
    }

    private boolean clickVideo() {
        boolean value = false;
        int indexCount = 1;
        try {
            Thread.sleep(5000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Clicking Video");
                UiSelector selector = new UiSelector().className("android.widget.TextView").index(indexCount);
                UiObject object = device.findObject(selector);

                UiSelector selectorSubsButton = new UiSelector().resourceId("com.snapchat.android:id/chrome_logo_thumbnail");
                UiObject objectSubsButton = device.findObject(selectorSubsButton);

                if (object.exists()) {
                    if (!objectSubsButton.exists())
                    {
                        int x = device.getDisplayWidth();
                        int y = device.getDisplayHeight();
                        double width = x / 100.00;
                        double height = y / 100.00;
                        int w = (int) (width * 50.00);
                        int h = (int) (height * 90.00);
                        device.click(w,h);
                        CVT = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Clicked Video: " + CVT);
                    }
                    else
                    {
                        value = true;
                        break;
                    }
                } else {
                    indexCount++;
                    if (indexCount == 5)
                    {
                        indexCount = 1;
                    }
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking Video");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Video");
            return false;
        }
        return value;
    }
    private boolean videoStartPlaying() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Check if Video Playing");

                UiSelector selector = new UiSelector().resourceId("com.snapchat.android:id/chrome_logo_thumbnail");
                UiObject object = device.findObject(selector);

                UiSelector loaderId = new UiSelector().resourceId("com.snapchat.android:id/loading_screen");
                UiObject loader = device.findObject(loaderId);
                if (object.exists() && !loader.exists()) {
                    VPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Video Playing: " + VPT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Video Playing");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Video Playing");
            return false;
        }
        return value;
    }

    private boolean bufferRead() {
        try {
            Log.d(GlobalVariables.Tag_Name, "Checking if Buffering");

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= 30000) {
                UiSelector loaderId = new UiSelector().resourceId("com.snapchat.android:id/loading_screen");
                UiObject loader = device.findObject(loaderId);

                UiSelector retryButtonSelector = new UiSelector().resourceId("com.snapchat.android:id/loading_error_retry_button");
                UiObject retryButtonObject = device.findObject(retryButtonSelector);
                if (loader.exists() || retryButtonObject.exists()) {
                    if (retryButtonObject.exists())
                    {
                        retryButtonObject.click();
                    }
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Loader Appear:YES Time: "  + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Loader Appear:NO Time: " + loaderNo);
                    Thread.sleep(200);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }
    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (HPT.getTime() - lt.getTime()) / 1000.0;
            double SRTi = (EST.getTime() - SRT.getTime()) / 1000.0;
            double TTVP = (VPD.getTime() - CVPT.getTime()) / 1000.0;
            double VPST = (VPT.getTime() - CVT.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRTi);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Video Page=" + TTVP);
            Log.d(GlobalVariables.Tag_Name, "Play Start Time=" + VPST);
            Log.d(GlobalVariables.Tag_Name, "Buffer Percentage=Back-end Calculation");
            Log.d(GlobalVariables.Tag_Name, "Buffer Count=Back-end Calculation");
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }


}
