package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class ALTBalaji extends Base implements AppClass {

    public static String testId;
    public String log = "";
    public Timestamp lt;
    public UiDevice device;
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    String location;
    String order, script;
    long testTimes;
    File file;
    String OUTPUT_LOG_FILE = "";
    String appName;
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

//      String searchButtonID = "com.balaji.alt:id/search_icon";
            String searchButtonID = "Search";
            String enterSearchID = "com.balaji.alt:id/auto_complete_text";
            String searchText = "Kehne ko humsafar \n";
            String searchResultsID = "com.balaji.alt:id/grid_view_search";
            String playButtonID = "com.balaji.alt:id/grid_view_search";
            String loaderID = "com.balaji.alt:id/exo_progress";

            context = TestApp.getContext();

            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = clickSearchButton(device, searchButtonID, GlobalVariables.descriptionType, 0);
            if (!runTest) {
                return 1;
            }

            runTest =
                    enterSearchText(device, enterSearchID, GlobalVariables.resourceIDType, 0, searchText);
            device.pressSearch();
            if (!runTest) {
                return 1;
            }

            runTest = searchResults(device, searchResultsID, GlobalVariables.resourceIDType, 0);
            if (!runTest) {
                return 1;
            }

            runTest = clickPlayButton(device, playButtonID, GlobalVariables.resourceIDType, 0);
            if (!runTest) {
                return 1;
            }

            runTest =
                    bufferRead(
                            testId,
                            GlobalVariables.TwoMins_Timeout,
                            device,
                            loaderID,
                            GlobalVariables.resourceIDType,
                            0);
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();

            KpiLogs.extraLogs(lt, st, est, srat, pvt);

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();

            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown(GlobalVariables.AltBalaji_Package, device);

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest =
                    sendData(job_id, device_id, testId, "completed", false, order, script, appName, ipAdress,startDate, appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

}
