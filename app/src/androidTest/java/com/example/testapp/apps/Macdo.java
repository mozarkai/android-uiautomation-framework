package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Macdo implements AppClass{

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    String OUTPUT_LOG_FILE = "";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    private Timestamp HPT, CMB, MPL,CRB,CPL;
    private UiDevice device;
    String startDate;
    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.startDate=startDate;
            this.order = order;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);
            context = TestApp.getContext();


            runTest = mcdoHomePageLoaded();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Home Page");
                return 1;
            }
            runTest = mcdoRemoveAds();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Remove Ads");
                return 1;
            }
            runTest = mcdoGotoMenuPage();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Go To Menu Page");
                return 1;
            }
            runTest = mcdoMenuPageLoaded();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Menu Page");
                return 1;
            }
            runTest = mcdoClickOnMeal();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Meal");
                return 1;
            }
            runTest = mcdoLoadingOnClickedMeal();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable Load Search Result");
                return 1;
            }
            runTest = mcdoClickOnChickenTab();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Chicken Tab");
                return 1;
            }
            runTest = mcdoClickFillet();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Fillet Menu");
                return 1;
            }
            runTest = mcdoAddToMyBag();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Add To My Bag");
                return 1;
            }
            runTest = mcdoSubtotalElementLoaded();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Subtotal Element");
                return 1;
            }
            runTest = mcdoClickOnCheckOut();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Checkout");
                return 1;
            }
            runTest = mcdoMyBagPageAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Checkout Page");
                return 1;
            }
            runTest = mcdoDeletePreviousSummary();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Delete Previous Order");
                return 1;
            }
            runTest = mcdoClickReview();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Review Button");
                return 1;
            }
            runTest = mcdoPaymentMethodPageAppear();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Load Payment Method");
                return 1;
            }
            kpiCalculation();



            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();

            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }



    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.Mcdo_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
    private boolean mcdoHomePageLoaded() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector firstHomeSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/cancelButton");
            UiObject firstHomeObject = device.findObject(firstHomeSelector);

            UiSelector secHomeSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/imageSlide");
            UiObject secHomeObject = device.findObject(secHomeSelector);

            UiSelector confirmLocationSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/positiveButton");
            UiObject confirmLocationObject = device.findObject(confirmLocationSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Load Home Page");
                if(firstHomeObject.exists() || secHomeObject.exists() || confirmLocationObject.exists())
                {
                    HPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_HomePage_Time + HPT);
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name, "Loading in Home Page");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Home Page");
            return false;
        }
        return value;
    }

    private boolean mcdoRemoveAds() {
        boolean value = false;
        try {
            UiSelector firstAds = new UiSelector().resourceId("ph.mobext.mcdelivery:id/cancelButton");
            UiObject firstAdsObject = device.findObject(firstAds);

            UiSelector confirmLocationSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/positiveButton");
            UiObject confirmLocationObject = device.findObject(confirmLocationSelector);
            Log.d(GlobalVariables.Tag_Name, "Trying to Remove Ads");
            if(firstAdsObject.waitForExists(5000))
            {
                firstAdsObject.click();
                Thread.sleep(3000);
            }
            if (confirmLocationObject.waitForExists(5000))
            {
                confirmLocationObject.click();
                Thread.sleep(3000);
            }
            else
            {
                Log.d(GlobalVariables.Tag_Name, "No Ads");
            }
            Timestamp unUsableTime = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Remove Ads Successfully: " + unUsableTime);
            value = true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error Checking Ads");
            return false;
        }
        return value;
    }

    private boolean mcdoGotoMenuPage() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector menuSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/menuFragment");
            UiObject menuObject = device.findObject(menuSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Click Menu Button");
                if(menuObject.exists())
                {
                    menuObject.click();
                    CMB = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Click_On_Menu_Time + CMB);
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name, "Click On Menu Button Loading");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error Clicking Menue Button");
            return false;
        }
        return value;
    }

    private boolean mcdoMenuPageLoaded() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector loadingSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/progressIndicator");
            UiObject loadingObject = device.findObject(loadingSelector);

            UiSelector menuSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/menuImage");
            UiObject menuPageObject = device.findObject(menuSelector);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Load Menu Page");
                if(!loadingObject.exists() || menuPageObject.exists())
                {
                    MPL = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Menu_page_appear_Time + MPL);
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name, "Loading in Menu Page");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Entering Meeting ID");
            return false;
        }
        return value;
    }

    private boolean mcdoClickOnMeal() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/menuImage");
            UiObject object = device.findObject(selector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Click Meal");
                if (object.exists()) {
                    object.click();
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Meal Successfully Time: " + unUsable);
                    value = true;
                    break;
                }
                else {
                    Log.d(GlobalVariables.Tag_Name, "Loading in Clicking Meal");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Meal");
            return false;
        }
        return value;
    }

    private boolean mcdoLoadingOnClickedMeal() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/productMealImage");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/productName");
            UiObject secObject = device.findObject(secSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to View Clicked Meal");
                if (object.exists() || secObject.exists()) {
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"Clicked Meal Loaded Successfully Time: ");
                    value = true;
                    break;
                }
                else
                {
                    Log.d(GlobalVariables.Tag_Name,"Loading in Clicked Meal");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Clicked Meal");
            return false;
        }
        return value;
    }

    private boolean mcdoClickOnChickenTab() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().descriptionContains("Chicken");
            UiObject object = device.findObject(selector);

            UiScrollable scrollable = new UiScrollable(new UiSelector().className("android.widget.HorizontalScrollView"));
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Click Chicken Tab");
                if (object.exists()) {
                    object.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Chicken Tab Successfully Time: " + unUsableTime);
                    value = true;
                    break;
                }
                else {
                    scrollable.scrollIntoView(object);
                    Log.d(GlobalVariables.Tag_Name, "Scrolling in Clicking Tab");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Chicken Tab");
            return value;
        }
        return value;
    }

    private boolean mcdoClickFillet() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().textContains("McCrispy Chicken Fillet Meal");
            UiObject object = device.findObject(selector);

            UiScrollable scrollable = new UiScrollable(new UiSelector().resourceId("ph.mobext.mcdelivery:id/menuCategoriesViewPager"));

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking on Chicken Fillet");
                if (object.exists()) {
                    object.click();
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Fillet Meal Successfully Time: " + unUsable);
                    value = true;
                    break;
                }
                else {
                    Thread.sleep(2000);
                    scrollable.scrollTextIntoView("McCrispy Chicken Fillet Meal");
                    Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Fillet");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Fillet Menu");
            return value;
        }
    }

    private boolean mcdoAddToMyBag() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/addToBagButton").enabled(true).className("android.widget.Button");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().textContains("Add to My Bag").enabled(true).className("android.widget.Button");
            UiObject secObject = device.findObject(secSelector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {

                Log.d(GlobalVariables.Tag_Name, "Trying to Click Add To My Bag");
                if (object.exists()) {
                    Thread.sleep(2000);
                    object.click();
                    Log.d(GlobalVariables.Tag_Name, "Clicked Add To My Bag Successfully Time: " + new Timestamp(new Date().getTime()));
//                    Log.d(GlobalVariables.Tag_Name,GlobalVariables.Click_On_Cart_Time);
                    value = true;
                    break;
                }else if (secObject.exists())
                {
                    secObject.click();
                    Thread.sleep(2000);
                    Log.d(GlobalVariables.Tag_Name, "Clicked Add To My Bag Successfully Time: " + new Timestamp(new Date().getTime()));
                    Log.d(GlobalVariables.Tag_Name,GlobalVariables.Click_On_Cart_Time);
                    value = true;
                    break;
                }
                else {
                    Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Add to My Bag");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Meeting Page");
            return value;
        }
    }

    private boolean mcdoSubtotalElementLoaded() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector subtotalElement = new UiSelector().resourceId("ph.mobext.mcdelivery:id/productMealImage");
            UiObject subtotalObject = device.findObject(subtotalElement);

            UiSelector secSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/productName");
            UiObject secObject = device.findObject(secSelector);

            UiSelector subTotal = new UiSelector().textContains("Subtotal");
            UiObject subTotalObject = device.findObject(subTotal);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (subTotalObject.exists())
                {
                    if (subtotalObject.exists() || secObject.exists()) {
                        Timestamp unUsable = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, "Subtotal Appeared Successfully" + unUsable);
//                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.Cart_items_appear_time + unUsable);
                        break;
                    } else {
                        Log.d(GlobalVariables.Tag_Name, "Loading in Image");
                        Thread.sleep(200);
                    }
                }else {
                    Log.d(GlobalVariables.Tag_Name, "Loading in Subtotal");
                    Thread.sleep(200);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }

    private boolean mcdoClickOnCheckOut() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/goToCheckoutButton").clickable(true);
            UiObject object = device.findObject(selector);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Trying to Click Check Out");
                if (!object.exists()) {
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Checkout Button Successfully" + unUsable);
                    value = true;
                    break;
                } else {
                    object.click();
                    Log.d(GlobalVariables.Tag_Name, "Loading in Clicking Checkout");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Checkout");
            return value;
        }
    }

    private boolean mcdoMyBagPageAppear() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/productName");
            UiObject object = device.findObject(selector);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Trying to Load Checkout Page");
                if (object.exists()) {
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,"My Bag Page Loaded Successfully" + unUsable);
                    value = true;
                    break;
                }
                else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Checkout Page");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Checkout Page");
            return false;
        }
        return value;
    }

    private boolean mcdoDeletePreviousSummary() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/orderSummaryRV");
            UiObject object = device.findObject(selector);

            UiSelector deleteButton = new UiSelector().resourceId("ph.mobext.mcdelivery:id/deleteButton");
            UiObject deleteObject = device.findObject(deleteButton);

            UiSelector delete = new UiSelector().resourceId("ph.mobext.mcdelivery:id/positiveButton");
            UiObject deleteConfirmation = device.findObject(delete);

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Trying to Delete Previous Order");
                if (object.getChildCount() > 1) {
                    deleteObject.click();
                    Thread.sleep(1000);
                    deleteConfirmation.click();
                    Timestamp unUsable = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Successfully Deleted Previous order" + unUsable);
                    Thread.sleep(10000);
                }
                else {
                    Log.d(GlobalVariables.Tag_Name,"No Previous Order");
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Checkout Page");
            return false;
        }
        return value;
    }

    private boolean mcdoClickReview() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().textContains("Review your address and pay");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/reviewButton");
            UiObject secObject = device.findObject(secSelector);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Clicking Review Button");
                if (object.exists()) {
                    object.click();
                    CRB = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,GlobalVariables.Click_On_CheckOut_time);
                    Log.d(GlobalVariables.Tag_Name, "Clicked Review Button Successfully Time " + CRB);
                    value = true;
                    break;
                }else if (secObject.exists())
                {
                    secObject.click();
                    CRB = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name,GlobalVariables.Click_On_CheckOut_time);
                    Log.d(GlobalVariables.Tag_Name, "Clicked Review Button Successfully Time " + CRB);
                    value = true;
                    break;
                }else {
                    Log.d(GlobalVariables.Tag_Name,"Loading in Clicking Review");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Review Button");
            return false;
        }
        return value;
    }

    private boolean mcdoPaymentMethodPageAppear() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            UiSelector selector = new UiSelector().resourceId("ph.mobext.mcdelivery:id/radioCashOnDelivery");
            UiObject object = device.findObject(selector);

            UiSelector secSelector = new UiSelector().textContains("GCash");
            UiObject secObject = device.findObject(secSelector);
            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Trying to Load Payment Method Page");
                if (object.exists() || secObject.exists()) {
                    CPL = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.Checkout_page_appear_time + CPL);
                    value = true;
                    break;
                }else {
                    Log.d(GlobalVariables.Tag_Name,"Loading in Payment Method Page");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Payment Method Page");
            return false;
        }
        return value;
    }

    private boolean kpiCalculation() {
        try {
            Thread.sleep(5000);
            double TTLH = (HPT.getTime() - lt.getTime()) / 1000.0;
            double TTLM = (MPL.getTime() - CMB.getTime()) / 1000.0;
            double TTCP = (CPL.getTime() - CRB.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Menu Page=" + TTLM);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Checkout Page=" + TTCP);
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }



//    private boolean kpiCalculation() {
//        try {
//            Thread.sleep(5000);
//            double TTLH = (HPT.getTime() - ExampleInstrumentedTest.alt.getTime()) / 1000.0;
//            double TTLM = (MPL.getTime() - CMB.getTime()) / 1000.0;
//            double TTCP = (CPL.getTime() - CRB.getTime()) / 1000.0;
//
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Menu Page=" + TTLM);
//            Log.d(GlobalVariables.Tag_Name, "Time To Load Checkout Page=" + TTCP);
//            return true;
//
//        } catch (Exception e) {
//            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
//            e.printStackTrace();
//            return false;
//        }
//    }

}
