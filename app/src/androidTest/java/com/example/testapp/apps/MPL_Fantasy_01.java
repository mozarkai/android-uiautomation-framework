package com.example.testapp.apps;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_Fantasy_01 extends MPL_BaseTest {
    private int n = 5;

    public int testRun(UiDevice device, Timestamp lt) {
        boolean runTest = false;

        // Fantasy page ids

        String FantasyId = "Fantasy";
        String OkayButtonId = "Okay";
        String MplFantasyPageId = "MPL Fantasy";
        String firstMatchCardId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup";
        String indexFirstMatchCardId = "0";
        String firstMatchCardId1 = "12 Hrs 16 Mins";
        String allContestId = "All Contests";
        String EntryFeeId = "Entry Fee";
        String prizePoolId = "Prize Pool";
        String contestcardId = "android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]";
        //                      /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]

        String indexContestId = "3";
        String teamsId = "Teams";
        String selById = "Sel by";
        String wkId = "WK (0)";
        String CreditsId = "Credits"; // (click 2 times for increasing order)
        String firstPlayersId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup";
        //android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup
        // android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup
        // android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup

        //  sellby_3rd = /android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[2]
        String secondPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup";
        String thirdPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup";
        String index = "0";
        String batId = "BAT (0)";
        String ARId = "AR (0)";
        String BowlerId = "BOW (0)";
        String fourthPlayerId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup";
        String elevenOutOfElevenId = "11/11";
        String NextButtonId = "Next";
        String chooseCaptainPageId = "Choose Captain(C) And Vice Captain(VC)";
        String captainId = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]";
        String captianIndex = "4";
        String vcID = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup/android.view.ViewGroup[3]";
        String vcIndex = "5";
        String saveTeamId = "Save Team";
        String registerTeamId = "& Register";
        String joinedId = "Joined";
        String editTeamsId = "Edit Teams";

        Log.d(GlobalVariables.Tag_Name, "Executing fantasy tests");

        runTest = clickButtonWithText(device, FantasyId,  "fantasy tab", "Successfully clicked on fantasy tab", "Unable to click on fantasy tab");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, OkayButtonId,  "Okay button", "Successfully clicked on okay button", "Unable to click on okay button");

        runTest = checkForElementText(device, MplFantasyPageId,  "Fantasy page Id", "Successfully loaded Fantasy Page", "unable to load  Fantasy Page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithCollections(device, firstMatchCardId,  "First match card", "Successfully clicked on first match card", "unable to click on first match card");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device, allContestId,  "All contests text", "Successfully loaded All contest page", "unable to load All contest page");
        if (!runTest) {
            return 1;
        }

        runTest = clickOnContestCard(device, contestcardId,  "first contest", "Successfully clicked on first contest", "Unable to click on first contest");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device, selById,  "Sell by text", "Successfully loaded team creation page", "Unable to load team creation page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }

        runTest = selectPlayersBasedonIndex(device, 1,  "select first player", "Successfully selected first player", "unable to select the first player");
        if (!runTest) {
            return 1;
        }

        runTest = selectPlayersBasedonIndex(device, 22,  "select second player", "Successfully selected second player", "unable to select the second player");
        if (!runTest) {
            return 1;
        }

     runTest = clickButtonWithText(device, batId,  "bat text", "Successfully clicked on Bat text", "Unable to click on Bat text");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }
       runTest = selectPlayersBasedonIndex(device, 1,  "select first player", "Successfully selected first player", "unable to select the first player");
        if (!runTest) {
            return 1;
        }
         runTest = selectPlayersBasedonIndex(device, 22,  "select second player", "Successfully selected second player", "unable to select the second player");
        if (!runTest) {
            return 1;
        }
       runTest = selectPlayersBasedonIndex(device, 28,  "select third player", "Successfully selected third player", "unable to select the third player");
        if (!runTest) {
            return 1;
        }

        runTest = selectPlayersBasedonIndex(device, 58,  "select fourth player", "Successfully selected fourth player", "unable to select the fourth player");
        if (!runTest) {
            return 1;
        }

//58,64

        runTest = clickButtonWithText(device, ARId,  "All-rounder text", "Successfully clicked on All-rounder text", "Unable to click on All-rounder text");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }
        runTest = selectPlayersBasedonIndex(device, 1,  "select first player", "Successfully selected first player", "unable to select the first player");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, BowlerId,  "bowl text", "Successfully clicked on bowl text", "Unable to click on bowl text");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device, CreditsId,  "credits text", "Successfully clicked on credits", "Unable to click on credits");
        if (!runTest) {
            return 1;
        }
        runTest = selectPlayersBasedonIndex(device, 1,  "select first player", "Successfully selected first player", "unable to select the first player");
        if (!runTest) {
            return 1;
        }
         runTest = selectPlayersBasedonIndex(device, 22,  "select second player", "Successfully selected second player", "unable to select the second player");
        if (!runTest) {
            return 1;
        }
        runTest = selectPlayersBasedonIndex(device, 28,  "select third player", "Successfully selected third player", "unable to select the third player");
        if (!runTest) {
            return 1;
        }

        runTest = selectPlayersBasedonIndex(device, 56,  "select fourth player", "Successfully selected fourth player", "unable to select the fourth player");
        if (!runTest) {
            return 1;
        }

        /*runTest = selectPlayersBasedonIndex(device, 64,  "select fifth player", "Successfully selected fifth player", "unable to select the fifth player");
        if (!runTest) {
            return 1;
        }*/


        runTest = checkForElementText(device, elevenOutOfElevenId,  "selected team members count", "Successfully selected 11 members", "unable to selct all 11 members");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, NextButtonId,  "Next button", "Successfully clicked on Next button", "Unable to click on Next button");
        if (!runTest) {
            return 1;
        }
        runTest = checkForElementText(device, chooseCaptainPageId,  "Choose C and VC page Id", "Successfully loaded Choose C and VC page", "unable to load  Choose C and VC Page");
        if (!runTest) {
            return 1;
        }

        runTest = selectCaptainOrViceCaptain(device,5,0,4,"Captain id","Successfully selected captain","Unable to select the captain");
        if (!runTest) {
            return 1;
        }
        runTest = selectCaptainOrViceCaptain(device,2,3,5,"Vice Captain id","Successfully selected vice captain","Unable to select the vice captain");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device, saveTeamId,  "save team button", "Successfully clicked on save team button", "Unable to click on save team button");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithTextContains(device, registerTeamId,  "Register Team button", "Successfully clicked on Register team button", "Unable to click on Register team button");
        if (!runTest) {
            return 1;
        }
        runTest = checkForElementText(device, editTeamsId,  "Edit team button", "Successfully joined contest", "unable to join the contest");
        if (!runTest) {
            return 1;
        }

        device.drag(4,44,4,4,3);

        fantasyKpiCalculations();
        return 0;
    }



    public void fantasyKpiCalculations() {
        try {


            double fantasyTabTime = getTimeFromKpi("fantasy tab");
            double mplFantasyTextTime = getTimeFromKpi("Fantasy page Id");
            double fantasyPageLoadTime = mplFantasyTextTime - fantasyTabTime;
            Log.d(GlobalVariables.Tag_Name, "Time to load fantasy page " + fantasyPageLoadTime);

            double matchCardTime = getTimeFromKpi("First match card");
            double  allContestTime =  getTimeFromKpi("All contests text");
            double allContestPageLoadTime = allContestTime - matchCardTime;
            Log.d(GlobalVariables.Tag_Name, "Time to load AllContest page " + allContestPageLoadTime);

            double firstContestTime = getTimeFromKpi("first contest");
            double sellByTextVisibleTime = getTimeFromKpi("Sell by text");
            double teamCreationPageLoadTime = sellByTextVisibleTime-firstContestTime;
            Log.d(GlobalVariables.Tag_Name, "Time to load Team creation page " + teamCreationPageLoadTime);

            double saveTeamTime = getTimeFromKpi("save team button");
            double registerTeamTime = getTimeFromKpi("Register Team button");
            double registerPageLoadTime = registerTeamTime-saveTeamTime;
            Log.d(GlobalVariables.Tag_Name, "Time to load Registration page " + registerPageLoadTime);

            double editTeamTime = getTimeFromKpi("Edit team button");
            double AllContestPageTime = editTeamTime - registerTeamTime;
            Log.d(GlobalVariables.Tag_Name, "Time to load joining contest page " + AllContestPageTime);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Unable to calculate Time to load fantasy page ");
        }

    }

}
