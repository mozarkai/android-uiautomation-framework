package com.example.testapp.apps;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_Login_01 extends MPL_BaseTest {

    private int n = 5;

    public int testRun(UiDevice device, Timestamp lt, String mobileNumber) {
        boolean runTest = false;

        // login page ids

        String login_textId = "Login to MPL";
        String enterMobileNumberId = "Enter Mobile Number";
        String getOTPButtonId = "Get OTP and Login";
        String enterOTPId = "Enter 6 digit OTP";
        String enterOTPTextBoxId = "••••••";
        String verifyOTPButtonId = "Verify OTP & Login";

        runTest = checkForElementText(device, login_textId,  "Login to MPL text", "Login page loded successfully", "Unable to load Login page");
        if (!runTest) {
            return 1;
        }

        runTest = typeTextWithText(device, enterMobileNumberId, mobileNumber,  "Enter mobile number text box ", "Successfully entered mobile number", "Unable to pass the mobile number");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithTextContains(device, getOTPButtonId,  "get otp button ", "Successfully clicked on Get OTP button ", "Unable to click on Get OTP button");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device, enterOTPId,  "Enter OTP Page", "Successfully loaded ", "Unable to enter OTP");
        if (!runTest) {
            return 1;
        }

        runTest = typeTextWithText(device, enterOTPTextBoxId, GlobalVariables.MPL_OTP,  "Enter OTP text box", "Successfully enterd OTP", "Unable to enter OTP");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithTextContains(device, verifyOTPButtonId,  "Verify OTP button", "Successfully clicked on Verify OTP button", "Unable to click on Verify OTP button");
        if (!runTest) {
            return 1;
        }


        return 0;
    }
}
