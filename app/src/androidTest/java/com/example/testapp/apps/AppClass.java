package com.example.testapp.apps;

import androidx.test.uiautomator.UiDevice;

import java.sql.Timestamp;

public interface AppClass {

    int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate);

}

