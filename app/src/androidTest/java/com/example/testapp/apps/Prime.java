package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Prime implements AppClass{

  public static String testId;
  public String log = "";
  long timer, End_time_Video, click_on_video_time, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  boolean screenRecording, pcapFile;
  String location;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String order, script;
  long testTimes;
  String appName;
  String startDate;
  private Intent intent;
  private Timestamp lt, st, vt,sat, pt, before_spinner, Video_end_time, videoStartTime, loader_time;
  private UiDevice device;
  private String OUTPUT_LOG_FILE = "";

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.location = location;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      runTest = initialLoader();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Initial Loader");
        return 1;
      }

      runTest = homeElement();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
        return 1;
      }

      runTest = searchAppear();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Search Results");
        return 1;
      }

      runTest = selectVideo();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Select Video");
        return 1;
      }

      runTest = play();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Find Play Button");
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        DataHolder.getInstance().setFailureReason("Unable To Play Video");
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Prime_Package));
    } catch (Exception e) {
        Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean initialLoader() {
    boolean initial_loader = false;

    try {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        UiSelector loaderId =
            new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/loading_animation");
        UiObject loader = device.findObject(loaderId);
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Initial Loader:YES " + loaderYes);
          Thread.sleep(500);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Initial Loader:NO " + loaderNo);
          initial_loader = true;
          break;
        }
      }

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in finding Initial Loader");
      return initial_loader;
    }
    return initial_loader;
  }

  private boolean homeElement() {
    try {
      boolean homeElement = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find homeElement button with Attempt no." + (i + 1));
          UiSelector searchId = new UiSelector().text("TV Shows");
          UiObject searchAppear = device.findObject(searchId);

          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)
              && searchAppear.isEnabled()) {
            homeElement = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in homeElement" + new Timestamp(new Date().getTime()));
        }
      }
      return homeElement;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding homeElement Method");
      return false;
    }
  }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().text("Find");
          UiObject searchAppear = device.findObject(searchId);

          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in search Appear" + new Timestamp(new Date().getTime()));
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));
          UiSelector enterSearchField =
              new UiSelector()
                  .resourceId("com.amazon.avod.thirdpartyclient:id/find_search_box_input");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.click();
            enterSearch_text.setText("Kung Fu Panda The paws of destiny \n ");
            device.pressEnter();
            Log.d(
                GlobalVariables.Tag_Name,
                GlobalVariables.Enter_Search_Time + new Timestamp(new Date().getTime()));
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      UiSelector searchResultsID =
              new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/myStuffItemRoot");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          break;
        } else {
          Log.d(
                  GlobalVariables.Tag_Name,
                  "Loading Search Results:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean selectVideo() {
    try {
      boolean selectVideoBtn = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to select video with Attempt no.:" + (i + 1));
        try {
          device.wait(
              Until.hasObject(By.res("com.amazon.avod.thirdpartyclient:id/myStuffItemRoot")), 2);
          UiCollection videoId =
              new UiCollection(
                  new UiSelector()
                      .resourceId("com.amazon.avod.thirdpartyclient:id/myStuffItemRoot"));
          UiObject selectVideo = videoId.getChildByInstance(new UiSelector(), 0);
          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();

            Log.d(GlobalVariables.Tag_Name, "select video:" + new Timestamp(new Date().getTime()));
            selectVideoBtn = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "select video Failed");
        }
      }
      return selectVideoBtn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in selecting video");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {
          device.wait(
                  Until.hasObject(
                          By.res("com.amazon.avod.thirdpartyclient:id/detail_page_header_play_button")),
                  2);

          // Scrollable true id - resource-id
          //com.amazon.avod.thirdpartyclient:id/detail_page_scroll_root

          UiSelector playId =
                  new UiSelector()
                          .resourceId("com.amazon.avod.thirdpartyclient:id/detail_page_header_play_button");
          UiObject playBtn = device.findObject(playId);
          if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playBtnFound = true;
            break;
          } else {
            UiScrollable scrollID = new UiScrollable(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/detail_page_scroll_root"));
            scrollID.scrollIntoView(playId);
            if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
              playBtn.click();
              vt = new Timestamp(new Date().getTime());
              Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
              Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
              playBtnFound = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead() {
    try {
//      RecordScreen recordScreen = new RecordScreen();
//      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000);

      UiSelector loaderId =
          new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/LoadingSpinner");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.amazon", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
