package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class VootWebApp_Coldstart implements AppClass{

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  int check;
  String Email;
  String Password;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  ClearChromeHistory cch = new ClearChromeHistory();
  private Timestamp lt, st, vt, et, ht, het, sat, Elements_LoadTime;
  private UiDevice device;
  String startDate;

  @Override
  public int testRun(int job_id, String device_id, String ipAdress, UiDevice device, Timestamp lt, String order, String script, String location, String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;
      this.Email = Email;
      this.Password = Password;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time,vCap);

      check = cch.chromeTestRun(device);
      Log.d(GlobalVariables.Tag_Name, "Enter URL Time from Chrome" + cch.Enter_URLTime);

      if (check == 1) {
        return 1;
      }

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      runTest = menu();
      if (!runTest) {
        return 1;
      }

      runTest = loginButton();
      if (!runTest) {
        return 1;
      }

      runTest = email();
      if (!runTest) {
        return 1;
      }

      runTest = continueButton();
      if (!runTest) {
        return 1;
      }

      runTest = password();
      if (!runTest) {
        return 1;
      }

      runTest = loginButton2();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements1();
      if (!runTest) {
        return 1;
      }

      runTest = searchAppear();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = homeButton();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements1();
      if (!runTest) {
        return 1;
      }

      runTest = play();
      if (!runTest) {
        return 1;
      }

      runTest = play2();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      //            KpiLogs.fourKPI(lt, st, et, sat, ht, het, vt);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Chrome_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean menu() {
    try {
      boolean menuButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Menu button with Attempt no." + (i + 1));

          UiSelector menuId = new UiSelector().text("Menu");
          UiObject menu = device.findObject(menuId);
          if (menu.waitForExists(GlobalVariables.Wait_Timeout)) {
            menu.click();
            Log.d(
                GlobalVariables.Tag_Name, "Clicked on Menu:" + new Timestamp(new Date().getTime()));
            menuButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Menu Button Failed");
          device.click(100, 100);
        }
      }
      return menuButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Menu Button Method");
      return false;
    }
  }

  private boolean email() {
    try {
      boolean emailButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Email button with Attempt no." + (i + 1));

          UiSelector emailId = new UiSelector().resourceId("email");
          UiObject email = device.findObject(emailId);
          if (email.waitForExists(GlobalVariables.Wait_Timeout)) {
            email.click();
            email.setText(Email);
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Email:" + new Timestamp(new Date().getTime()));
            emailButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Email Button Failed");
          device.click(100, 100);
        }
      }
      return emailButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Email Button Method");
      return false;
    }
  }

  private boolean continueButton() {

    try {
      boolean continueButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Continue button with Attempt no." + (i + 1));

          UiSelector continueId = new UiSelector().textContains("continue");
          UiObject continueButton = device.findObject(continueId);
          if (continueButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            continueButton.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Continue:" + new Timestamp(new Date().getTime()));
            continueButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Continue Button Failed");
          device.click(100, 100);
        }
      }
      return continueButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Continue Button Method");
      return false;
    }
  }

  private boolean password() {
    try {
      boolean passwordButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Password button with Attempt no." + (i + 1));

          UiSelector passwordId = new UiSelector().resourceId("password");
          UiObject passwordButton = device.findObject(passwordId);
          if (passwordButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            passwordButton.click();
            passwordButton.setText(Password);
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Password:" + new Timestamp(new Date().getTime()));
            passwordButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Password Button Failed");
          device.click(100, 100);
        }
      }
      return passwordButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Password Button Method");
      return false;
    }
  }

  private boolean loginButton() {
    try {
      boolean loginButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Login button with Attempt no." + (i + 1));

          UiSelector loginId = new UiSelector().textContains("login");
          UiObject login = device.findObject(loginId);
          if (login.waitForExists(GlobalVariables.Wait_Timeout) && login.isEnabled()) {
            login.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Login:" + new Timestamp(new Date().getTime()));
            loginButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Login Button Failed");
          device.click(100, 100);
        }
      }
      return loginButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Login Button Method");
      return false;
    }
  }

  private boolean loginButton2() {
    try {
      boolean loginButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Login button with Attempt no." + (i + 1));

          UiSelector loginId = new UiSelector().textContains("login");
          UiObject login = device.findObject(loginId);
          if (login.waitForExists(GlobalVariables.Wait_Timeout) && login.isEnabled()) {
            //                        Thread.sleep(12000);
            //                        login.click();
            device.pressEnter();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Login:" + new Timestamp(new Date().getTime()));
            loginButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Login Button Failed");
        }
      }
      return loginButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Login Button Method");
      return false;
    }
  }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().text("Search Icon");
          UiObject searchAppear = device.findObject(searchId);
          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Search Button Appear:" + st);
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in search Appear:" + new Timestamp(new Date().getTime()));
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));
          UiSelector enterSearchField = new UiSelector().resourceId("standard-full-width");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.setText("Big boss \n ");
            device.pressEnter();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      UiSelector searchResultsID = new UiSelector().textContains("items");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Search Results:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean homeButton() {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Home button with Attempt no." + (i + 1));

          UiSelector homeID = new UiSelector().text("closeIcon");
          UiObject home = device.findObject(homeID);
          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            home.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Home_Button_Time + ht);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click Home Button Failed");
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Button Method");
      return false;
    }
  }

  private boolean homeElements() {
    try {
      UiSelector homeElementsID = new UiSelector().text("ALL");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          Elements_LoadTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + Elements_LoadTime);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean homeElements1() {
    try {
      UiSelector homeElementsID = new UiSelector().text("ALL");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          het = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + het);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          //                    UiCollection playId = new UiCollection(new
          // UiSelector().resourceId("com.tv.v18.viola:id/vh_hero_card_list"));
          //                    UiObject playBtn = playId.getChildByInstance(new UiSelector(),1);

          if (GlobalVariables.PremiumEmail.equalsIgnoreCase(Email)) {
            UiSelector playId = new UiSelector().textContains("+");
            UiObject playBtn = device.findObject(playId);

            UiSelector playId1 = new UiSelector().textContains("Select");
            UiObject playBtn1 = device.findObject(playId1);

            if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)
                || playBtn1.waitForExists(GlobalVariables.Wait_Timeout)) {
              playBtn.click();
              vt = new Timestamp(new Date().getTime());
              Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
              Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
              playBtnFound = true;
              break;
            }

          } else {
            UiSelector playId = new UiSelector().textContains("+");
            UiObject playBtn = device.findObject(playId);

            UiSelector playId1 = new UiSelector().textContains("Select");
            UiObject playBtn1 = device.findObject(playId1);
            if (playBtn1.waitForExists(GlobalVariables.Wait_Timeout)) {
              Thread.sleep(4000);
            } else {
              if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
                playBtn.click();
                vt = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
                playBtnFound = true;
                break;
              }
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean play2() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          //                    UiCollection playId = new UiCollection(new
          // UiSelector().resourceId("com.tv.v18.viola:id/vh_hero_card_list"));
          //                    UiObject playBtn = playId.getChildByInstance(new UiSelector(),1);

          UiSelector playId = new UiSelector().textContains("Play");
          UiObject playBtn = device.findObject(playId);

          if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on play:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead() {
    try {

      //            UiSelector adLoaderID = new
      // UiSelector().resourceId("com.tv.v18.viola:id/ad_progress_loader");
      //            UiObject adLoader = device.findObject(adLoaderID);
      //            adLoader.exists();

      UiSelector loaderId = new UiSelector().textContains("pixel.gif");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.tv.v18", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (Elements_LoadTime.getTime() - cch.Enter_URLTime.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double TLP = (het.getTime() - ht.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Page = " + TLP);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
      e.printStackTrace();
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }

}
