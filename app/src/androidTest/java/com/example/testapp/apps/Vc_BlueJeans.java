package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Vc_BlueJeans implements AppClass {

  public static String testId;
  public String log = "";
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String startDate;
  String OUTPUT_LOG_FILE = "";
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      Utility.permission(GlobalVariables.BlueJeans_Package);

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }

      Utility.log(id, log);

      runTest = joinEvent();
      if (!runTest) {
        return 1;
      }

      runTest = enterMeetingId();
      if (!runTest) {
        return 1;
      }

      runTest = enterPassword();
      if (!runTest) {
        return 1;
      }

      runTest = appAudioVideo();
      if (!runTest) {
        return 1;
      }

      runTest = progressBar();
      if (!runTest) {
        return 1;
      }

      runTest = enterName();
      if (runTest) {
        runTest = clickNext();
        if (!runTest) {
          clickNext();
        }
      }
      runTest = microphoneOn();
      //            if(!runTest) {
      //                return 1;
      //            }

      runTest = videoOn();
      //            if(!runTest) {
      //                return 1;
      //            }

      runTest = joinMeeting();
      if (!runTest) {
        return 1;
      }

      runTest = loaderAppear(vCap);
      if (!runTest) {
        return 1;
      }

      runTest = endMeeting();
      if (!runTest) {
        return 1;
      }

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private boolean joinEvent() {
    try {
      boolean joinEvent = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Event Join Meeting button with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/join_meeting");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            joinMeeting.click();
            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            joinEvent = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Event Join Meeting Button Failed");
        }
      }
      return joinEvent;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Event Join Meeting Button Method");
      return false;
    }
  }

  private boolean enterMeetingId() {
    try {
      boolean enterMeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Meeting ID with Attempt no." + (i + 1));

          //                    UiSelector meetID = new
          // UiSelector().resourceId("com.bluejeansnet.Base:id/validation_box");
          //                    UiObject meet = device.findObject(meetID);

          UiSelector enterMeetingID = new UiSelector().textContains("Meeting");
          UiObject meetingId = device.findObject(enterMeetingID);

          if (meetingId.waitForExists(GlobalVariables.Wait_Timeout)) {
            meetingId.click();
            //                        meetingId.setText("790297898");
            meetingId.setText("8229212000");
            device.pressEnter();
            enterMeeting = true;
            break;
          } else {
            UiCollection meetID =
                new UiCollection(
                    new UiSelector().resourceId("com.bluejeansnet.Base:id/validation_box"));
            UiObject meet = meetID.getChildByInstance(new UiSelector(), 0);
            meet.click();
            meet.clearTextField();
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Meeting ID Failed");
        }
      }
      return enterMeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Enter Meeting ID Method");
      return false;
    }
  }

  private boolean enterPassword() {
    try {
      boolean enterPwd = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to Enter Password text with Attempt no." + (i + 1));

          UiSelector pwdID = new UiSelector().textContains("Passcode");
          UiObject pwd = device.findObject(pwdID);

          if (pwd.waitForExists(GlobalVariables.Wait_Timeout)) {
            pwd.click();
            //                        pwd.setText("8525");
            pwd.setText("4715");
            device.pressEnter();
            enterPwd = true;
            break;
          } else {
            //                        UiSelector passwordID = new
            // UiSelector().resourceId("com.bluejeansnet.Base:id/validation_box");
            //                    UiObject password = device.findObject(passwordID);

            UiCollection passwordID =
                new UiCollection(
                    new UiSelector().resourceId("com.bluejeansnet.Base:id/passcode_field"));
            UiObject password = passwordID.getChildByInstance(new UiSelector(), 3);
            password.click();
            password.clearTextField();
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Entering Password");
        }
      }
      return enterPwd;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Password Method");
      return false;
    }
  }

  private boolean enterName() {
    try {
      boolean EnterName = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to Enter Name with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/validation_box");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.LAUNCH_TIMEOUT)) {
            joinMeeting.click();
            joinMeeting.setText("Mozark");
            EnterName = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on EnterName Meeting Button Failed");
        }
      }
      return EnterName;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding EnterName Meeting Button Method");
      return false;
    }
  }

  private boolean clickNext() {
    try {
      boolean ClickNext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Next button with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/join_button");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.LAUNCH_TIMEOUT)) {
            joinMeeting.click();

            ClickNext = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Next Button Failed");
        }
      }
      return ClickNext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Next Button Method");
      return false;
    }
  }

  private boolean appAudioVideo() {
    try {
      boolean appAudioVideo = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Start Meeting button with Attempt no." + (i + 1));

          UiSelector appAudioVideoID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/app_join_layout");
          UiObject audiovideo = device.findObject(appAudioVideoID);

          if (audiovideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            audiovideo.click();
            appAudioVideo = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on App Audio Video Button Failed");
        }
      }
      return appAudioVideo;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Clicking App Audio Video Method");
      return false;
    }
  }

  private boolean progressBar() {
    try {

      Thread.sleep(1000);

      UiSelector loaderId = new UiSelector().resourceId("com.bluejeansnet.Base:id/progress_bar");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Log.d(
              GlobalVariables.Tag_Name,
              "First Progress Loader No Time:" + new Timestamp(new Date().getTime()));
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Progress Bar Method");
      return false;
    }
  }

  private boolean microphoneOn() {
    boolean microphoneOn = false;
    try {
      for (int i = 0; i < 5; i++) {
        try {

          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Microphone ON button with Attempt no." + (i + 1));

          UiSelector microphoneID = new UiSelector().textContains("Microphone On");
          UiObject microphone = device.findObject(microphoneID);

          if (microphone.waitForExists(GlobalVariables.Wait_Timeout)) {
            microphone.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Turned ON Microphone Time:" + new Timestamp(new Date().getTime()));
            microphoneOn = true;
            break;
          } else {

            UiSelector microphoneIDs = new UiSelector().textContains("Microphone Off");
            UiObject microphones = device.findObject(microphoneIDs);

            if (microphones.waitForExists(GlobalVariables.Wait_Timeout)) {
              microphoneOn = true;
              break;
            }
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Microphone ON Failed");
        }
      }
      return microphoneOn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding StartMeeting  Method");
      return false;
    }
  }

  private boolean videoOn() {
    boolean videoOn = false;

    try {
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Video ON button with Attempt no." + (i + 1));

          UiSelector videoID = new UiSelector().textContains("Video On");
          UiObject video = device.findObject(videoID);

          if (video.waitForExists(GlobalVariables.Wait_Timeout)) {
            video.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Turned ON Video Time:" + new Timestamp(new Date().getTime()));
            videoOn = true;
            break;
          } else {
            UiSelector videoIDs = new UiSelector().textContains("Video Off");
            UiObject videos = device.findObject(videoIDs);

            if (videos.waitForExists(GlobalVariables.Wait_Timeout)) {
              videoOn = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Video On Failed");
        }
      }
      return videoOn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Video ON  Method");
      return false;
    }
  }

  private boolean joinMeeting() {
    try {
      boolean joinmeeting = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Join Meeting button with Attempt no." + (i + 1));

          UiSelector joinmeetingID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/join_button");
          UiObject joinMeeting = device.findObject(joinmeetingID);

          if (joinMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            joinMeeting.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.JoinMeetingClickTime + vt);
            joinmeeting = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Join Meeting Button Failed");
        }
      }
      return joinmeeting;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Join Meeting Button Method");
      return false;
    }
  }

  private boolean loaderAppear(boolean vCap) {
    try {

      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_TimeMid / 1000, vCap);

      UiSelector loaderId = new UiSelector().resourceId("com.bluejeansnet.Base:id/more_button");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;
      UiSelector gotIt = new UiSelector().resourceId("com.bluejeansnet.Base:id/got_it");
      UiObject endgotIt = device.findObject(gotIt);

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_TimeMid) {

        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_No + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          if (endgotIt.exists()) {
            endgotIt.click();
          }
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loading_Yes + loaderNo);
        }
      }

      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean endMeeting() {
    try {
      boolean endMeet = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find End Meeting Button with Attempt no." + (i + 1));

          UiSelector endMeetingID =
              new UiSelector().resourceId("com.bluejeansnet.Base:id/end_call_button");
          UiObject endMeeting = device.findObject(endMeetingID);

          if (endMeeting.waitForExists(GlobalVariables.Wait_Timeout)) {
            endMeeting.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked End Meeting Button Time:" + new Timestamp(new Date().getTime()));
            endMeet = true;
            break;
          } else {

            UiSelector gotIt = new UiSelector().resourceId("com.bluejeansnet.Base:id/got_it");
            UiObject endgotIt = device.findObject(gotIt);

            if (endgotIt.waitForExists(GlobalVariables.Wait_Timeout)) {
              endgotIt.click();
            }
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click On End Meeting Button Failed");
        }
      }
      return endMeet;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding End Meeting Method");
      return false;
    }
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.BlueJeans_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
