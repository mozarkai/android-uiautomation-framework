package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class MPL extends Base implements AppClass {

  public static String testId;
  public String log = "";
  public Timestamp lt;
  public UiDevice device;
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  File file;
  String OUTPUT_LOG_FILE = "";
  String appName;
  String  startDate;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      Log.d(GlobalVariables.Tag_Name,"Before Video Recording Start Time:" + new Timestamp(new Date().getTime()));
      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
      Log.d(GlobalVariables.Tag_Name,"After Video Recording Start Time:" + new Timestamp(new Date().getTime()));

      String spinID = "Spin Today's Wheel";
      String homePageID = "All Games";
      String liveButtonID = "Live";
      String searchResultsID = "LIVE";
      String playButtonID = "earned";
      String videoStartedID = "by";
      String loaderID = "connectivity";
      String exitVideoID = "Yes";
      String homeButtonID = "All Games";
      String homeElementsID = "All Games";
      String selectGameID = "2048";
      String freeButtonID = "FREE";
      String battleForFreeID = "BATTLE FOR FREE";
      String gameLoaderID = "Loading";
      String gameLoaderID1 = "MOBILE PREMIER LEAGUE";
      String gameLoaderID2 = "Loading game";
      String actionView = "view";
      String actionClick = "click";

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
                job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      runTest = loadHomePage(device, homePageID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = clickLiveButtonMPL(device, liveButtonID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = searchResults(device, searchResultsID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = clickPlayButton(device, playButtonID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = videoStarted(device, videoStartedID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest =
              mplBufferRead(
                      GlobalVariables.HalfMin_Timout,
                      device,
                      loaderID,
                      GlobalVariables.textType,
                      0);
      if (!runTest) {
        return 1;
      }

      runTest = exitVideoPage(device, exitVideoID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = homeButton(device, homeButtonID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = homeElements(device, homeElementsID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = selectGame(device, selectGameID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = freeButton(device, freeButtonID, GlobalVariables.textType, 0, actionView);
      if (!runTest) {
        return 1;
      }

      Thread.sleep(2000);
      runTest = freeButton(device, freeButtonID, GlobalVariables.textType, 0, actionClick);
      if (!runTest) {
        return 1;
      }

      runTest = clickBattleFreeButton(device, battleForFreeID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = gameLoader(device, gameLoaderID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = gameLoader1(device, gameLoaderID1, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = gameLoader2(device, gameLoaderID2, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      runTest = kpiCalculation();
      if (!runTest) {
        return 1;
      }

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown(GlobalVariables.MPL_Package, device);

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest =
              sendData(job_id, device_id, testId, "completed", false, order, script, appName, ipAdress,startDate,appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }
}