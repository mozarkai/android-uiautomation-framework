package com.example.testapp.apps;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;

import com.mozark.uiautomatorlibrary.utils.GlobalVariables;

import java.sql.Timestamp;

public class MPL_MoneyIn_01 extends MPL_BaseTest {
    private int n = 5;
    //payments
    public int testRun(UiDevice device, Timestamp lt) {
        boolean runTest = false;

        String walletIcon = "";
        String walletXpath = "//android.view.ViewGroup[1]//android.view.ViewGroup[1]//android.view.ViewGroup[4]";
        //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]
        int walletindex = 3;
        String typesOfCashText = "Types of cash in MPL";
        String okayButton = "Okay";
        String WalletPageText = "Wallet";
        String addCashButton = "Add Cash";
        String OKAYButton = "OKAY";
        String addDepositCashText = "Add Deposit Cash";
        String yourBalanceText = "Your Balance: ₹ 440";
        String scratchCardText = "2 ScratchCards";
        String proceedToAddcashButton = "Proceed To Add Cash";
        String applycouponstext = "Apply Coupons & Offers";
        String enterCouponCodeText = "Enter Coupon code/ View All";
        String enterAmountTextBox = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]";
        String scratchCardAmount = "//android.view.ViewGroup[1]//android.view.ViewGroup[1]//android.widget.HorizontalScrollView//android.view.ViewGroup[4]/android.view.ViewGroup";
        String paymentOptionPageText = "Payment Options";
        String chooseYourBankText = "Choose Your Bank";
        String iciciText = "ICICI Netbanking";
        String welcomePageId = "Welcome to Razorpay";
        String successButtonId = "Success";
        String depositStatusText = "Deposit Status";
        String paymentSuccessfullyText = "Payment Successful";
        String scratchcardsTextInPaymentSuccessFullPage = "Scratch Cards";
        String offersPage = "Offers";
        String firstScratchCard = "//android.view.ViewGroup[1]//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup";
        String highlightedScratchCard = "//android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup";
        String youWonText = "You won";
        String CongratulationsText = "Congratulations!!";
        String DepositedtoBonuscashText = "Deposited to Bonus cash";

        Log.d(GlobalVariables.Tag_Name, "Executing Money-In tests");

        runTest = clickButtonWithText(device,walletIcon,"Wallet icon","Successfully clicked on Wallet icon","Unable to click on Wallet icon");
        if (!runTest) {
            return 1;
        }

        /*runTest = clickOnWalletIcon(device,  "Wallet tab", "Successfully clicked on Wallet tab", "Unable to click on Wallet tab");
        if (!runTest) {
            return 1;
        }
*/
       // runTest = checkForElementText(device,typesOfCashText,"TypesOfCashText","Successfully TypesOfCash pop up appears","TypesOfCash pop up not appears");


        //runTest = clickButtonWithText(device,okayButton,"okay button","Successfully clicked on okay button","Unable to click on okay button");

        runTest = checkForElementText(device,WalletPageText,"WalletPageText","Successfully Wallet page is loaded","Unable to load Wallet page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device,addCashButton,"Add cash button","Successfully clicked on Add cash button","Unable to click on Add cash button");
        if (!runTest) {
            return 1;
        }

      //  runTest = clickButtonWithText(device,OKAYButton,"OKAY button","Successfully clicked on OKAY button","Unable to click on OKAY button");

        runTest = checkForElementText(device,addDepositCashText,"AddDepositCashText","Successfully loaded Add deposit cash page","Unable to load Add deposit cash page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithTextContains(device,proceedToAddcashButton,"proceedToAddcashButton","Successfully clicked on proceedToAddcashButton","Unable to click on proceedToAddcashButton");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,paymentOptionPageText,"PaymentOptionText","Successfully loaded PaymentOption page","Unable to load PaymentOption page");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device,chooseYourBankText,"ChooseYourBank","Successfully clicked on ChooseYourBank link","Unable to click on ChooseYourBank link");
        if (!runTest) {
            return 1;
        }
        runTest = clickButtonWithText(device,iciciText,"ICICIText","Successfully clicked on ICICI link","Unable to click on ICICI link");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,welcomePageId,"RazorPayText","Successfully loaded Razorpay page","Unable to load Razorpay page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithText(device,successButtonId,"SuccessButtonIdText","Successfully clicked on Success button","Unable to click on Success button");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,depositStatusText,"depositStatusText","Successfully loaded Deposit status page","Unable to load Deposit status page");
        if (!runTest) {
            return 1;
        }

        runTest = clickButtonWithTextContains(device,scratchcardsTextInPaymentSuccessFullPage,"scratchcardText","Successfully clicked on scratchcardText button","Unable to click on scratchcardText button");
        if (!runTest) {
            return 1;
        }

        runTest = checkForElementText(device,offersPage,"offersText","Successfully loaded Offers page","Unable to load Offers page");
        if (!runTest) {
            return 1;
        }

        return 0;
    }




}
