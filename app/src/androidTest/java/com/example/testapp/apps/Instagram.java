package com.example.testapp.apps;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import android.content.Context;
import android.util.Log;


import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;


public class Instagram implements AppClass {

    private Timestamp HPT,EST,SRT,CVPT,VPD,CVT,VPT;
    private Timestamp DeliveryTime,
            vt,
            et,
            ht,
            it,
            het,
            sat,
            ClickCartTime,
            TimeToLoad,
            ClickCheckoutTime,
            CheckoutAppearTime;

    int loopCount = 1;

    public static String testId;
    public String log = "";
    //    long total_time = 0;
//    ArrayList<Long> arrayList;
    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    //    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    String order, script;
    long testTimes;
    String appName;
    //    private Intent intent;
    private Timestamp lt, heat, sqct, mdat, pbt, psat, bayt, bant, enterUPI_Balance, enterUPI_Payment;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress, startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);
            runTest = homeElement();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }
            runTest = gotoSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Go To Search");
                return 1;
            }
            runTest = clickSearchField();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Search Field");
                return 1;
            }
            runTest = enterSearchText();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                return 1;
            }
            runTest = searchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Search Result");
                return 1;
            }
            runTest = clickSearchResult();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Search Result");
                return 1;
            }
            runTest = pageInfoDisplayed();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Display Page");
                return 1;
            }
            runTest = gotoVideoPage();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Go To Video Page");
                return 1;
            }
            runTest = videoPageDisplayed();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Display Videos");
                return 1;
            }
            runTest = clickVideo();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Click Videos");
                return 1;
            }
            runTest = videoStartPlaying();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Play Videos");
                return 1;
            }
            runTest = bufferRead();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check Buffer");
                return 1;
            }
            runTest = kpiCalculation();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Check KPI's");
                return 1;
            }


            stopScreenRecording();
            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }


    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.Timeout);
            device.executeShellCommand(getCommand(GlobalVariables.InstagramPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Home Element");

                UiSelector homeElementId = new UiSelector().resourceId("com.instagram.android:id/action_bar_left_button");
                UiObject homeElement = device.findObject(homeElementId);

                UiSelector secHomeElementId = new UiSelector().resourceId("com.instagram.android:id/gap_binder_group");
                UiObject secHomeElement = device.findObject(secHomeElementId);
                if (homeElement.exists() || secHomeElement.exists()) {
                    HPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + HPT);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                    Thread.sleep(200);
                }
            }
            return homeElementFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return false;
        }
    }

    private boolean gotoSearch() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking Search Button");

                UiSelector searchHome = new UiSelector().resourceId("com.instagram.android:id/search_tab");
                UiObject searchHomeObject = device.findObject(searchHome);
                if(searchHomeObject.exists())
                {
                    searchHomeObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search Button Clicked Successfully time: " + unUsableTime);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking search Button");
            return false;
        }
        return value;
    }

    private boolean clickSearchField() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Clicking Search Field");

                UiSelector searchHome = new UiSelector().className("android.widget.EditText");
                UiObject searchHomeObject = device.findObject(searchHome);
                if(searchHomeObject.exists())
                {
                    searchHomeObject.click();
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search Field Clicked Successfully time: " + unUsableTime);
                    value = true;
                    break;
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking search Field");
            return false;
        }
        return value;
    }

    private boolean enterSearchText() {
        boolean value = false;
        try {
            Thread.sleep(2000);
            Log.d(GlobalVariables.Tag_Name, "Entering Text");

            UiSelector selector = new UiSelector().className("android.widget.EditText");
            UiObject object = device.findObject(selector);

            if (object.waitForExists(GlobalVariables.OneMin_Timeout)) {
                object.setText("NEWS");
                device.pressEnter();
                EST = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Enter Search Text Time: " + EST);
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
            return false;
        }
        return value;
    }

    private boolean searchResult() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Finding Search Result");

                UiSelector seeAllSelector = new UiSelector().resourceId("com.instagram.android:id/recycler_view");
                UiObject seeAllObject = device.findObject(seeAllSelector);

                if (seeAllObject.exists()) {
                    SRT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Search Result Time: " + SRT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
            return value;
        }
        return value;
    }

    private boolean clickSearchResult() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Click Search Result ");

                UiSelector searchResultsID = new UiSelector().resourceId("com.instagram.android:id/row_search_user_username");
                UiObject searchResults = device.findObject(searchResultsID);

                if (searchResults.exists()) {
                    searchResults.click();
                    Timestamp unUsabletime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Click Search Result Time: " + unUsabletime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Search Result");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Search Result");
            return value;
        }
    }

    private boolean pageInfoDisplayed() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name, "Page info displayed");

                UiSelector searchResultsID = new UiSelector().resourceId("com.instagram.android:id/profile_viewpager");
                UiObject searchResults = device.findObject(searchResultsID);

                if (searchResults.exists()) {
                    Timestamp unUsableTime = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Page info Displayed Time: " + unUsableTime);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Page info Displayed");
                    Thread.sleep(200);
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Page info Displayed");
            return value;
        }
    }

    private boolean gotoVideoPage() {
        boolean liveButtonFound = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Go to Video Page");

                UiSelector videoPageSelector = new UiSelector().descriptionContains("Video").resourceId("com.instagram.android:id/profile_tab_icon_view");
                UiObject videoPageObject = device.findObject(videoPageSelector);

                if (videoPageObject.exists()) {
                    videoPageObject.click();
                    CVPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked on Video Button Time:" + CVPT);
                    liveButtonFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Video Button");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking video  Button");
            return false;
        }
        return liveButtonFound;
    }

    private boolean videoPageDisplayed() {
        boolean videoAppearFound = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Finding Videos Page");

                UiSelector videosId1 = new UiSelector().resourceId("com.instagram.android:id/video_thumbnail");
                UiObject videoAppear1 = device.findObject(videosId1);

                if (videoAppear1.exists()) {
                    VPD = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Videos Appear Time:" + VPD);
                    videoAppearFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading for Video Page");
                    Thread.sleep(200);
                }
            }
            return videoAppearFound;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Loading Video Page");
            return false;
        }
    }

    private boolean clickVideo() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Clicking Video");

                UiSelector selector = new UiSelector().resourceId("com.instagram.android:id/video_thumbnail");
                UiObject object = device.findObject(selector);

                if (object.exists()) {
                    object.click();
                    CVT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Clicked Video: " + CVT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Clicking Video");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Clicking Video");
            return false;
        }
        return value;
    }
    private boolean videoStartPlaying() {
        boolean value = false;
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                Log.d(GlobalVariables.Tag_Name,"Check if Video Playing");

                UiSelector selector = new UiSelector().resourceId("com.instagram.android:id/first_frame_image");
                UiObject object = device.findObject(selector);

                UiSelector profileSelector = new UiSelector().resourceId("com.instagram.android:id/profile_header_container");
                UiObject profileObject = device.findObject(profileSelector);

                if (object.exists() || profileObject.exists()) {
                    VPT = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Video Playing: " + VPT);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name,"Loading Video Playing");
                    Thread.sleep(200);
                }
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Video Playing");
            return false;
        }
        return value;
    }

    private boolean bufferRead() {
        try {
            Log.d(GlobalVariables.Tag_Name, "Checking if Buffering");

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= 30000) {
                UiSelector loaderId = new UiSelector().resourceId("com.instagram.android:id/loading_spinner");
                UiObject loader = device.findObject(loaderId);
                if (loader.exists()) {
                    Timestamp loaderYes = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Loader Appear:YES Time: "  + loaderYes);
                } else {
                    Timestamp loaderNo = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Loader Appear:NO Time: " + loaderNo);
                    Thread.sleep(200);
                }
            }
            return true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
            return false;
        }
    }
    private boolean kpiCalculation() {
        try {
            Thread.sleep(10000);
            double TTLH = (HPT.getTime() -lt.getTime()) / 1000.0;
            double SRTi = (EST.getTime() - SRT.getTime()) / 1000.0;
            double TTVP = (VPD.getTime() - CVPT.getTime()) / 1000.0;
            double VPST = (VPT.getTime() - CVT.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Search Result Time=" + SRTi);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Video Page=" + TTVP);
            Log.d(GlobalVariables.Tag_Name, "Play Start Time=" + VPST);
            Log.d(GlobalVariables.Tag_Name, "Buffer Percentage= Back-end Calculation");
            Log.d(GlobalVariables.Tag_Name, "Buffer Count= Back-end Calculation");
            return true;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
            e.printStackTrace();
            return false;
        }
    }

}
