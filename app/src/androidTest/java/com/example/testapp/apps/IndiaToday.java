package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class IndiaToday implements AppClass {

  public static String testId;
  public String log = "";
  long timer, End_time_Video, click_on_video_time, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  int job_id;
  String device_id;
  String ipAdress, location = "Gurgaon";
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  boolean pcapFile;
  String order, script;
  long testTimes;
  String appName;
  private Intent intent;
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  private String OUTPUT_LOG_FILE = "";
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.location = location;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = shareAppear();
      if (!runTest) {
        return 1;
      }

      runTest = liveTV();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead(vCap);
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
      return 1;
    }
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.IndiaToday_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean shareAppear() {
    try {
      boolean shareButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find share button with Attempt no." + (i + 1));
          //                    UiSelector shareId = new
          // UiSelector().resourceId("com.indiatoday:id/iv_widget_share");
          //                    UiSelector shareId = new
          // UiSelector().resourceId("com.indiatoday:id/ic_share");
          UiSelector shareId = new UiSelector().resourceId("com.indiatoday:id/tv_topnews_title");
          UiObject shareAppear = device.findObject(shareId);
          if (shareAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Share Button Appear Time:" + st);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            shareButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Share Button Appear Time:No" + new Timestamp(new Date().getTime()));
        }
      }
      return shareButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Share Button Method");
      return false;
    }
  }

  private boolean liveTV() {
    try {
      boolean liveBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click Live TV with Attempt no.:" + (i + 1));
        try {

          UiSelector liveTvId = new UiSelector().resourceId("com.indiatoday:id/ivLiveTV");
          UiObject liveTvBtn = device.findObject(liveTvId);
          if (liveTvBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveTvBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on LiveTv:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            liveBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return liveBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in LiveTV");
      return false;
    }
  }

  private boolean loader1() {
    try {
      pt = new Timestamp(new Date().getTime());

      for (int i = 0; i < 500; i++) {
        pt = new Timestamp(new Date().getTime());
        Log.d(GlobalVariables.Tag_Name, "Trying to find Loader:" + (i + 1));
        try {
          device.wait(Until.hasObject(By.res("com.indiatoday:id/progress_exoplayer")), 0);
          UiSelector loaderId = new UiSelector().resourceId("com.indiatoday:id/progress_exoplayer");
          UiObject loader = device.findObject(loaderId);
          boolean loaderExist = loader.exists();
          Log.d(GlobalVariables.Tag_Name, "Found Loader Id");
          if (loaderExist) {
            Log.d(
                GlobalVariables.Tag_Name,
                "Loader Appear:Yes" + new Timestamp(new Date().getTime()));
          } else {
            Log.d(GlobalVariables.Tag_Name, "Loader Appear:No" + pt);
            before_spinner = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Play Start Time:" + before_spinner);
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Loader Appear:No" + pt);
          before_spinner = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Play Start Time:" + before_spinner);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Loader");
      return false;
    }
  }

  private boolean loaderTry() {
    int Value_of_i = 0;
    try {
      Boolean loader_yes = false;
      Boolean loader_no = false;
      int Buffer_Count = 0;
      for (int j = 0; j < 1000; j++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to Find Loader" + (j + 1));
        //              device.wait(Until.hasObject(By.res("com.indiatoday:id/progress_exoplayer")),
        // 0);
        //                UiObject spinner = new UiObject(new
        // UiSelector().resourceId("com.indiatoday:id/progress_exoplayer"));
        UiSelector spinnerId = new UiSelector().resourceId("com.indiatoday:id/progress_exoplayer");
        UiObject spinner = device.findObject(spinnerId);

        boolean spinnerExist = spinner.exists();
        if (spinnerExist && spinner.isEnabled()) {
          before_spinner = new Timestamp(new Date().getTime());
          // before_spinner_long = before_spinner.getTime();
          Log.d(GlobalVariables.Tag_Name, "before spinner time :" + before_spinner);
          Log.d(
              GlobalVariables.Tag_Name,
              "Video play time spinner for loop :" + new Timestamp(new Date().getTime()));
          Value_of_i = j;

        } else {
          if (loader_no == false) {
            before_spinner = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Play Start Time:" + before_spinner);
            break;
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      Log.d(GlobalVariables.Tag_Name, "Unable to Find Spinner ID");
    }
    return Value_of_i <= 990;
  }

  private boolean bufferRead(boolean vCap) {
    try {
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);

      UiSelector loaderId = new UiSelector().resourceId("com.indiatoday:id/progress_exoplayer");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.india", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
