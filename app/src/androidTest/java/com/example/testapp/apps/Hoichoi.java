package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;
import androidx.test.uiautomator.UiDevice;
import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Hoichoi extends Base implements AppClass{

  public static String testId;
  public String log = "";
  public Timestamp lt;
  public UiDevice device;
  long End_time_Video, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String ipAdress;
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  String location;
  String order, script;
  long testTimes;
  File file;
  String OUTPUT_LOG_FILE = "";
  String appName;
  String startDate;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      String homePageID = "TRENDING";
      String searchButtonID = "com.viewlift.hoichoi:id/app_cms_search_button";
      String enterSearchID = "com.viewlift.hoichoi:id/search_src_text";
      String searchText = "Hello Kolkata \n";
      String searchResultsID = "VIDEOS";
      //      String selectVideoID = "com.viewlift.hoichoi:id/collectionGridItemView";
      String selectVideoID = "Hello Kolkata";
      String playButtonID = "com.viewlift.hoichoi:id/playButton";
      String loaderID = "com.viewlift.hoichoi:id/video_loading_progress_indicator";

      context = TestApp.getContext();

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = loadHomePage(device, homePageID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = clickSearchButton(device, searchButtonID, GlobalVariables.resourceIDType, 0);
      if (!runTest) {
        return 1;
      }

      runTest =
          enterSearchText(device, enterSearchID, GlobalVariables.resourceIDType, 0, searchText);
      device.pressEnter();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults(device, searchResultsID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = selectVideo(device, selectVideoID, GlobalVariables.textType, 0);
      if (!runTest) {
        return 1;
      }

      runTest = clickPlayButton(device, playButtonID, GlobalVariables.resourceIDType, 0);
      if (!runTest) {
        return 1;
      }

      runTest =
          bufferRead(
              testId,
              GlobalVariables.TwoMins_Timeout,
              device,
              loaderID,
              GlobalVariables.resourceIDType,
              0);
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.extraLogs(lt, st, est, srat, pvt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown(GlobalVariables.Hoichoi_Package, device);

      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest =
          sendData(job_id, device_id, testId, "completed", false, order, script, appName, ipAdress,startDate, appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }
}
