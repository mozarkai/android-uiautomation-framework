package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;

public class Seevii implements AppClass {

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String order, script;
  long testTimes;
  String appName;
  String OUTPUT_LOG_FILE = "";
  private Timestamp lt, st, et, sat, vt;
  private UiDevice device;
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {
      context = TestApp.getContext();

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.startDate=startDate;
      this.lt = lt;
      this.location = location;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = homeElements();
      if (!runTest) {
        return 1;
      }

      runTest = searchAppear();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = selectVideo();
      if (!runTest) {
        return 1;
      }

      runTest = play();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.extraLogs(lt, st, et, sat, vt);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      // new LogsChecker(context, "stop").execute();

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
      e.printStackTrace();
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Seevii_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean homeElements() {
    try {
      UiSelector homeElementsID = new UiSelector().resourceId("net.mbc.shahid:id/img_watch_arrow");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          st = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          //                    UiCollection searchSelector = new UiCollection(new
          // UiSelector().resourceId("com.google.android.youtube:id/toolbar"));
          //                    searchSelector.click();
          //                    Log.d(GlobalVariables.Tag_Name, "toolbar is clicked:");
          //
          //                    UiObject search = searchSelector.getChildByInstance(new
          // UiSelector(), 3);

          UiSelector searchId = new UiSelector().descriptionContains("بحث");
          UiObject searchAppear = device.findObject(searchId);

          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Search Button" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Search Button Failed");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));

          UiSelector enterSearchField =
              new UiSelector().resourceId("net.mbc.shahid:id/search_src_text");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.click();
            enterSearch_text.setText("test \n ");
            device.pressEnter();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean clickSearch() {
    try {
      boolean clickSearch = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to clickSearch with Attempt no." + (i + 1));

          UiSelector enterSearchField =
              new UiSelector().resourceId("com.balaji.alt:id/search_icon");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.click();
            device.pressEnter();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Search_Time + et);
            clickSearch = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search clickSearch Failed");
        }
      }
      return clickSearch;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in clickSearch Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      UiSelector searchResultsID = new UiSelector().resourceId("net.mbc.shahid:id/recycler_view");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Result_Time + sat);
          break;
        } else {
          Log.d(GlobalVariables.Tag_Name, "Loading Search Results");
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean selectVideo() {
    try {
      boolean selectVideoBtn = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to select video with Attempt no.:" + (i + 1));
        try {

          UiCollection videoId =
              new UiCollection(new UiSelector().resourceId("net.mbc.shahid:id/recycler_view"));
          UiObject selectVideo = videoId.getChildByInstance(new UiSelector(), 33);

          if (selectVideo.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectVideo.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Select Video Time:" + new Timestamp(new Date().getTime()));
            selectVideoBtn = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "select video Failed");
        }
      }
      return selectVideoBtn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in selecting video");
      return false;
    }
  }

  private boolean play() {
    try {
      boolean playBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to play with Attempt no.:" + (i + 1));
        try {

          //                    UiCollection playBtnContainerID = new UiCollection(new
          // UiSelector().resourceId("net.mbc.shahid:id/img_watch_arrow"));
          //                    UiObject playBtn = playBtnContainerID.getChildByInstance(new
          // UiSelector(), 4);

          UiSelector playBtnID = new UiSelector().resourceId("net.mbc.shahid:id/img_watch_arrow");
          UiObject playBtn = device.findObject(playBtnID);

          if (playBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            playBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Play Failed");
        }
      }
      return playBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in play");
      return false;
    }
  }

  private boolean bufferRead() {

    try {

      UiSelector loaderId = new UiSelector().resourceId("net.mbc.shahid:id/loading_layout");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("Seevii", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public void kpiCalculation() {
    try {
      double TTLH = (st.getTime() - lt.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      //    double PST = (het.getTime() - ht.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search Results Time = " + SRT);
      //            Log.d(GlobalVariables.Tag_Name, "Time To Load Page = " + TLP);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
