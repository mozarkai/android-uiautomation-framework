package com.example.testapp.apps;

import android.content.Context;
import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class IIFLMarkets implements AppClass{
    public static String testId;
    public String log = "";

    int job_id;
    String device_id;
    String ipAdress;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String appName;
    private Timestamp lt, het, cct, lsct, pop, dp;
    private UiDevice device;
    private String OUTPUT_LOG_FILE = "";

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion,String startDate){

        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.location = location;
            this.order = order;
            this.script = script;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;
            this.appName = appName;

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            if (job_id != -1) {
                SendStatus sendStatus = new SendStatus();
                sendStatus.status(
                        job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
            }
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time,pCap);

            runTest = homeElement();
            if (!runTest) {
                return 1;
            }

            runTest = loadSensex();
            if (!runTest) {
                return 1;
            }

            stopScreenRecording();
            kpiCalculation();

            Utility.batteryStats(OUTPUT_LOG_FILE);
            context = TestApp.getContext();
            new AquamarkPcap(context, "stop",pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id,pCap);

            runTest = sendData(appVersion,startDate);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;

    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.IIFLMarkets_Package));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    private boolean homeElement() {
        boolean homeElementFound = false;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiSelector homeElementId =
                        new UiSelector().description("Open Drawer");
                UiObject homeElement = device.findObject(homeElementId);
                if (homeElement.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    homeElementFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Home Page Elements");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Home Element");
            return homeElementFound;
        }
        return homeElementFound;
    }

    private boolean loadSensex() {
        boolean loadSensexFound = false;

        try {
            UiObject searchOption = device.findObject(new UiSelector().resourceId("com.indiainfoline:id/action_watchlist_search"));
            searchOption.click();
            UiObject searchText = device.findObject(new UiSelector().resourceId("com.indiainfoline:id/search_src_text"));
            searchText.click();
            searchText.setText("nippon india etf nifty bees");
            searchText.waitForExists(GlobalVariables.Wait_Timeout);
            UiObject niftyBees = device.findObject(new UiSelector().resourceId("com.indiainfoline:id/txt_display_name"));
            niftyBees.click();
            UiObject detailOption = device.findObject(new UiSelector().text("DETAIL"));
            detailOption.click();
            UiObject monthlyOption = device.findObject(new UiSelector().descriptionContains("1M"));
            monthlyOption.waitForExists(GlobalVariables.Wait_Timeout);
            monthlyOption.click();
            cct = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked On 1 Month:" + cct);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
                UiObject loaderImage = device.findObject(new UiSelector().resourceId("loaderImg"));
                if (!loaderImage.exists()) {
                    lsct = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Sensex Chart Appear Time:" + lsct);
                    loadSensexFound = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Loading Sensex Chart");
                }
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in finding Sensex Chart");
            return loadSensexFound;
        }
        return loadSensexFound;
    }

    private void kpiCalculation() {
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double TTLSC = (lsct.getTime() - cct.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page=" + TTLH);
            Log.d(GlobalVariables.Tag_Name, "Time To Load Stock Chart=" + TTLSC);

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation" + e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean sendData(String appVersion, String startDate) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }
}
