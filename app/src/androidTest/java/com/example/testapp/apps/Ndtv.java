package com.example.testapp.apps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.record.RecordScreen;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.example.testapp.ExampleInstrumentedTest.OUTPUT_LOG_FILE_TEMP;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class Ndtv implements AppClass {

  public static String testId;
  public String log = "";
  long timer, End_time_Video, click_on_video_time, video_start_time;
  long total_time = 0;
  ArrayList<Long> arrayList;
  int Buffer_Count = 0;
  int job_id;
  String device_id;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  String ipAdress, location = "Gurgaon";
  double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
  Context context;
  boolean pcapFile;
  String order, script;
  long testTimes;
  String appName;
  private Intent intent;
  private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
  private UiDevice device;
  private String OUTPUT_LOG_FILE = "";
  String startDate;
  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.location = location;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);

      SendStatus sendStatus = new SendStatus();
      sendStatus.status(
          job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("the no of cores are  "+Utility.getNumOfCores() +" the total
        // ram is "+ Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = toolbarAppear();
      if (!runTest) {
        return 1;
      }

      runTest = toolbarBtn();
      if (!runTest) {
        return 1;
      }

      runTest = liveTV();
      if (!runTest) {
        return 1;
      }

      runTest = playLiveNews();
      if (!runTest) {
        return 1;
      }

      runTest = bufferRead(vCap);
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twoKPI(lt, st, vt);

      Utility.batteryStats(OUTPUT_LOG_FILE);
      Context context = TestApp.getContext();
      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.NDTV_Package));
    } catch (Exception e) {
        Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean toolbarAppear() {
    try {
      boolean toolbarButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find toolbar button with Attempt no." + (i + 1));

          UiSelector shareId = new UiSelector().resourceId("com.july.ndtv:id/toolbar");
          UiObject shareAppear = device.findObject(shareId);
          if (shareAppear.waitForExists(GlobalVariables.Wait_Timeout)) {

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "ToolBar Button Appear Time:" + st);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Appear_Time + st);
            toolbarButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "ToolBar Button Appear Time:No" + new Timestamp(new Date().getTime()));
        }
      }
      return toolbarButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ToolBar Button Method");
      return false;
    }
  }

  private boolean toolbarBtn() {
    try {
      boolean toolbarBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click Toolbar Menu with Attempt no.:" + (i + 1));
        try {

          //                    UiSelector toolbarBtnId = new
          // UiSelector().resourceId("com.july.ndtv:id/toolbar");
          //                    UiObject toolbarBtn = device.findObject(toolbarBtnId);

          UiCollection toolbarSelector =
              new UiCollection(new UiSelector().resourceId("com.july.ndtv:id/toolbar"));
          toolbarSelector.click();
          UiObject toolbar = toolbarSelector.getChildByInstance(new UiSelector(), 1);

          if (toolbar.waitForExists(GlobalVariables.Wait_Timeout)) {
            toolbar.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Click on Toolbar Menu:" + new Timestamp(new Date().getTime()));
            toolbarBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Toolbar Menu Failed");
        }
      }
      return toolbarBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Toolbar Btn");
      return false;
    }
  }

  private boolean liveTV() {
    try {
      boolean liveBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click Live TV with Attempt no.:" + (i + 1));
        try {

          UiObject liveTvBtn =
              device.findObject(
                  new UiSelector().text("Live TV").className("android.widget.TextView"));
          if (liveTvBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveTvBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on LiveTv:" + vt);
            liveBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Live TV Failed");
        }
      }
      return liveBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in LiveTV");
      return false;
    }
  }

  private boolean playLiveNews() {
    try {
      boolean playLiveBtnFound = false;
      for (int i = 0; i < 10; i++) {
        Log.d(
            GlobalVariables.Tag_Name, "Trying to Click Play Live News with Attempt no.:" + (i + 1));
        try {

          UiSelector liveTvId = new UiSelector().resourceId("com.july.ndtv:id/show_now_playing");
          UiObject liveTvBtn = device.findObject(liveTvId);
          if (liveTvBtn.waitForExists(GlobalVariables.Wait_Timeout)) {
            liveTvBtn.click();
            vt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Click on Play Live News:" + vt);
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Play_Video_Time + vt);
            playLiveBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Play Live News Failed");
        }
      }
      return playLiveBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Play Live News");
      return false;
    }
  }

  private boolean bufferRead(boolean vCap) {

    try {
      RecordScreen recordScreen = new RecordScreen();
      recordScreen.startRecord(testId, (int) GlobalVariables.Video_Run_Time / 1000, vCap);

      UiSelector loaderId = new UiSelector().resourceId("com.july.ndtv:id/loading_indicator");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Loader_Appear_No + loaderNo);
        }

        time = Integer.parseInt(String.valueOf(stopWatch.getTime()));
        if (time > c) {
          Utility.cpuUsage("com.july", OUTPUT_LOG_FILE_TEMP);
          c += GlobalVariables.Delay_Time;
        }
      }

      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
