package com.example.testapp.apps;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.shapes.OvalShape;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

public class GoogleDrive implements AppClass {

    public static String testId;
    public String log = "";
    long End_time_Video, video_start_time;
    long total_time = 0;
    ArrayList<Long> arrayList;
    int Buffer_Count = 0;
    int job_id;
    String device_id;
    String ipAdress;
    double time_Load_Home, Play_Start_time, Buffer_percentage, Buffer_percentage_time;
    Context context;
    String location;
    String order, script;
    long testTimes;
    String OUTPUT_LOG_FILE = "";
    String appName;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    private Timestamp lt, st, vt, pt, before_spinner, Video_end_time, videoStartTime;
    Timestamp het,ub1,db1,ub2,db2,uploadingYes1,uploadingNo1,uploadingYes2,uploadingNo2,downloadingYes1,downloadingNo1,downloadingYes2,downloadingNo2;
    private UiDevice device;
    String startDate;

    public int testRun(
            int job_id,
            String device_id,
            String ipAdress,
            UiDevice device,
            Timestamp lt,
            String order,
            String script,
            String location,
            String appName, boolean vCap, boolean pCap, String appVersion, String startDate) {
        boolean runTest = true;
        try {

            this.job_id = job_id;
            this.device_id = device_id;
            this.ipAdress = ipAdress;
            this.device = device;
            this.lt = lt;
            this.order = order;
            this.startDate = startDate;
            this.script = script;
            this.location = location;
            Timestamp testTime = new Timestamp(new Date().getTime());
            testTimes = testTime.getTime();
            testId = device_id + testTimes;
            ExampleInstrumentedTest.testId = testId;

            this.appName = appName;

            context = TestApp.getContext();

            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            String run = "Running";
            updateDeviceStatus.update(device_id, ipAdress, run);

            context = TestApp.getContext();
            int id = android.os.Process.myPid();
            try {

                log = Utility.fileName(testId) + ".txt";
                OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
                ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
                //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
                // Utility.getTotalRAM() +"\n");
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Utility.log(id, log);

            startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

            runTest = homePageElements();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Element");
                return 1;
            }

            runTest = filesButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Files Button");
                return 1;
            }

            runTest = homeUploadButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Home Page Upload Button");
                return 1;
            }

            runTest = uploadButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Upload Button");
                return 1;
            }

//            runTest = toolbarButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectDownloads();
//            if (!runTest) {
//                return 1;
//            }

            runTest = clickSearchButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Search Button");
                return 1;
            }

            runTest = enterSearch();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Enter Search Text");
                return 1;
            }

            runTest = selectFolder();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Select Folder");
                return 1;
            }

//            runTest = selectSubFolder();
//            if (!runTest) {
//                return 1;
//            }

            runTest = selectFile();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Select File");
                return 1;
            }

            runTest = filesButton1();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Files Button");
                return 1;
            }

            runTest = loaderUpload1();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Upload File");
                return 1;
            }

//            Thread.sleep(30000);

            runTest = clearButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Clear Button");
                return 1;
            }

            runTest = moreActionButton();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find More Action Button");
                return 1;
            }

            runTest = clickDownload1();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Download Button");
                return 1;
            }

            runTest = loaderDownload1();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Download File");
                return 1;
            }

//            Thread.sleep(30000);

            runTest = remove();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Remove Button");
                return 1;
            }

            runTest = moveToBin();
            if (!runTest) {
                DataHolder.getInstance().setFailureReason("Unable To Find Move To Bin Element");
                return 1;
            }

//            runTest = filesButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = homeUploadButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = uploadButton();
//            if (!runTest) {
//                return 1;
//            }
//
////            runTest = toolbarButton();
////            if (!runTest) {
////                return 1;
////            }
////
////            runTest = selectDownloads();
////            if (!runTest) {
////                return 1;
////            }
//
//            runTest = clickSearchButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = enterSearch();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = selectFolder();
//            if (!runTest) {
//                return 1;
//            }
//
////            runTest = selectSubFolder();
////            if (!runTest) {
////                return 1;
////            }
//
//            runTest = selectFile();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = filesButton2();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = loaderUpload2();
//            if (!runTest) {
//                return 1;
//            }
//
//            Thread.sleep(30000);
//
//            runTest = clearButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = moreActionButton();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = clickDownload2();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = loaderDownload2();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = remove();
//            if (!runTest) {
//                return 1;
//            }
//
//            runTest = moveToBin();
//            if (!runTest) {
//                return 1;
//            }

            stopScreenRecording();
            kpiCalculation();

//            runTest = kpiCalculation();
//            if (!runTest) {
//                return 1;
//            }

            Utility.batteryStats(OUTPUT_LOG_FILE);
            Context context = TestApp.getContext();
            new AquamarkPcap(context, "stop", pCap).execute();

            Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

            tearDown();
            Utility.sendPcap(testId, ipAdress, device_id, pCap);

            deleteFiles();
            runTest = sendData(appVersion);
            if (!runTest) {
                return 1;
            }

            Utility.logFileGeneration(testId, log, ipAdress, device_id);

            return 0;

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
        }
        return 0;
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    public void tearDown() {
        try {
            Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
            device.executeShellCommand(getCommand(GlobalVariables.GoogleDrivePackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
        }
    }

    public boolean sendData(String appVersion) {
        try {
            SendStatus sendStatus = new SendStatus();
            return sendStatus.status(
                    job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress, startDate);
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
            return false;
        }
    }


    private boolean homePageElements() {
        boolean value = false;
        try {
            UiSelector elementId = new UiSelector().resourceId("com.google.android.apps.docs:id/branded_fab");
            UiObject element = device.findObject(elementId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.OneMin_Timeout) {
                if (element.exists()) {
                    het = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Appear Time:" + het);
                    value = true;
                    break;
                } else {
                    Log.d(GlobalVariables.Tag_Name, "Home Elements Loading");
                }
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in Home Elements");
        }
        return value;
    }

    private boolean filesButton() {
        boolean value = false;
        try {

            UiSelector elementID = new UiSelector().descriptionContains("Files");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked On Files Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in Click On Files");
        }
        return value;
    }

    private boolean homeUploadButton() {
        boolean value = false;
        try {
            UiSelector elementID = new UiSelector().resourceId("com.google.android.apps.docs:id/branded_fab");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked on Home Upload Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in Home Upload Button");
        }
        return value;
    }

    private boolean uploadButton() {
        boolean value = false;
        try {
            UiSelector elementID = new UiSelector().descriptionContains("Upload");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked on Upload Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Finding upload Button Method");
        }
        return value;
    }

    private boolean toolbarButton() {
        boolean value = false;
        try {
            UiSelector elementID = new UiSelector().descriptionContains("Show roots");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked On Menu Hamburger icon Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in toolbar Button");
        }
        return value;
    }

    private boolean selectDownloads() {
        boolean value = false;
        try {

            UiSelector elementID = new UiSelector().text("Downloads");
            UiObject element = device.findObject(elementID);

            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked on Downloads Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }

        } catch (Exception e) {

            Log.d(GlobalVariables.Tag_Name, "Select Download Failed");
        }
        return value;
    }

    private boolean clickSearchButton() {
        boolean value = false;
        try {
            UiSelector elementID = new UiSelector().descriptionContains("Search");
            UiObject element = device.findObject(elementID);

            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Clicked On Search Button Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Click On Search Button Failed");
        }
        return value;
    }

    private boolean enterSearch() {
        boolean value = false;
        try {
            UiCollection element =
                    new UiCollection(new UiSelector().text("Search…"));

            if (element.waitForExists(GlobalVariables.Wait_Timeout)) {
                element.click();
                element.setText("GoogleDrive");
                device.pressEnter();
                Log.d(GlobalVariables.Tag_Name, "Entered Search Text Time:" + new Timestamp(new Date().getTime()));
                Thread.sleep(5000);
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Enter Search Button Failed");
        }
        return value;
    }

    private boolean selectFolder() {
        boolean value = false;
        try {

//            UiCollection elementID =
//                    new UiCollection(new UiSelector().resourceId("com.google.android.apps.docs:id/dir_list"));
//
//            UiObject element = elementID.getChildByInstance(new UiSelector(), 1);
            UiSelector elementID = new UiSelector().textContains("GoogleDriveFiles");
            UiObject element = device.findObject(elementID);

            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                Log.d(GlobalVariables.Tag_Name, "Selected Folder Time:" + new Timestamp(new Date().getTime()));
                Thread.sleep(2000);
                value = true;
            }

        } catch (Exception e) {

            Log.d(GlobalVariables.Tag_Name, "Select Folder Failed");
        }
        return value;
    }

//    private boolean selectSubFolder() {
//        boolean value = false;
//        try {
//            UiCollection elementID =
//                    new UiCollection(new UiSelector().resourceId("com.google.android.apps.docs:id/dir_list"));
//
//            UiObject element = elementID.getChildByInstance(new UiSelector(), 1);
//
//            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
//                element.click();
//                Log.d(GlobalVariables.Tag_Name, "Selected Sub Folder Time:" + new Timestamp(new Date().getTime()));
//                value = true;
//            }
//
//        } catch (Exception e) {
//
//            Log.d(GlobalVariables.Tag_Name, "Click on Select Sub Folder Failed");
//        }
//        return value;
//    }

    private boolean selectFile() {
        boolean value = false;
        try {

            UiCollection elementID1 =
                    new UiCollection(new UiSelector().resourceId("com.google.android.documentsui:id/dir_list"));

            UiObject element1 = elementID1.getChildByInstance(new UiSelector(), 2);

            UiCollection elementID2 =
                    new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

            UiObject element2 = elementID2.getChildByInstance(new UiSelector(), 2);

            if (element1.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element1.click();
                Log.d(GlobalVariables.Tag_Name, "Selected File Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }
            else {
                element2.click();
                Log.d(GlobalVariables.Tag_Name, "Selected File Time:" + new Timestamp(new Date().getTime()));
                value = true;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on Select File Failed");
        }
        return value;
    }

    private boolean filesButton1() {
        boolean value = false;
        try {

            UiSelector elementID = new UiSelector().descriptionContains("Files");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                ub1 = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, GlobalVariables.UploadClickTime + ub1);
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in Click On Upload");
        }
            return value;
}

    private boolean loaderUpload1() {
        boolean value = false;
        try {

//            UiSelector loaderId =
//                    new UiSelector().resourceId("com.google.android.apps.docs:id/uploading_view");
//            UiObject loader = device.findObject(loaderId);
            UiSelector loaderId =
                    new UiSelector().textContains("Uploading");
            UiObject loader = device.findObject(loaderId);


            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.TwoMins_Timeout) {
                if (loader.exists()) {
                    uploadingYes1 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Uploading + uploadingYes1);
                    Thread.sleep(200);
                } else {
                    uploadingNo1 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Uploaded + uploadingNo1);
                    value = true;
                    break;
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in File Uploading");
            return false;
        }
    }

    private boolean clearButton() {
        boolean value = false;
        try {
                    device.swipe(0, 0, 50, 50, 3);
                    device.swipe(0, 150, 150, 150, 1);

                    Thread.sleep(2000);

                    UiSelector elementId = new UiSelector().descriptionContains("Clear,Button");
                    UiObject element = device.findObject(elementId);
                    if (element.waitForExists(GlobalVariables.Wait_Timeout)) {
                        element.click();
                        Log.d(GlobalVariables.Tag_Name,"Clicked on Clear Button Time:" + new Timestamp(new Date().getTime()));
                        value = true;
                    }
                    device.swipe(0, 0, 50, 50, 3);
                    device.swipe(0, 150, 150, 150, 1);
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Clear Button");
                }
            return value;
    }

    private boolean moreActionButton() {
        boolean value = false;
        try {
            UiCollection elementID =
                    new UiCollection(
                            new UiSelector()
                                    .resourceId("com.google.android.apps.docs:id/more_actions_button"));
            UiObject element = elementID.getChildByInstance(new UiSelector(), 0);
            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                value = true;
            }

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Click on More Action Button Failed");
        }
        return value;
    }

    private boolean clickDownload1() {
            boolean value = false;
                try {

                    UiScrollable items =
                            new UiScrollable(
                                    new UiSelector()
                                            .resourceId("com.google.android.apps.docs:id/menu_recycler_view"));
                    items.setAsVerticalList();
                    items.scrollToEnd(5, 15);

                    UiSelector elementID = new UiSelector().text("Download");
                    UiObject element = device.findObject(elementID);

                    if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                        element.click();
                        db1 = new Timestamp(new Date().getTime());
                        Log.d(GlobalVariables.Tag_Name, GlobalVariables.DownloadClickTime + db1);

                        device.swipe(0, 0, 50, 50, 3);
                        device.swipe(0, 150, 150, 150, 1);

                        Thread.sleep(2000);
                        value = true;
                    }

                } catch (Exception e) {

                    Log.d(GlobalVariables.Tag_Name, "Click on Download Failed");
                }
            return value;
    }

    private boolean loaderDownload1() {
       boolean value = false;
        try {

            UiSelector loaderId = new UiSelector().text("Google Drive");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.TwoMins_Timeout) {
                if (loader.exists()) {
                    downloadingYes1 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloading + downloadingYes1);
                    Thread.sleep(200);
                } else {
                    downloadingNo1 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, GlobalVariables.File_Downloaded + downloadingNo1);
                    device.pressBack();
                    value = true;
                    break;
                }
            }

            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in File Downloading");
            return false;
        }
    }

    private boolean remove() {
            boolean value = false;
                try {

                    UiCollection elementID =
                            new UiCollection(
                                    new UiSelector().resourceId("com.google.android.apps.docs:id/document_layout"));

                    UiObject element = elementID.getChildByInstance(new UiSelector(), 0);

                    if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                        element.longClick();
                        Thread.sleep(2000);
                        UiSelector elementID1 = new UiSelector().descriptionContains("Remove");
                        UiObject element1 = device.findObject(elementID1);
                        if (element1.waitForExists(GlobalVariables.Wait_Timeout)) {
                            element1.click();
                            Log.d(GlobalVariables.Tag_Name,"Clicked On Remove Button Time:" + new Timestamp(new Date().getTime()));
                            value = true;
                        }
                    }
                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Remove Button");
                }
            return value;
    }

    private boolean moveToBin() {
            boolean value = false;
                try {
                    UiSelector elementID = new UiSelector().resourceId("com.google.android.apps.docs:id/positive_button");
                    UiObject element = device.findObject(elementID);
                    if (element.waitForExists(GlobalVariables.Wait_Timeout)) {
                        element.click();
                        Log.d(GlobalVariables.Tag_Name,"Clicked On Move To Bin Time:" + new Timestamp(new Date().getTime()));
                        value = true;
                    }

                } catch (Exception e) {
                    Log.d(
                            GlobalVariables.Tag_Name,
                            "Error in Move To Bin");
                }
            return value;
    }

    private boolean filesButton2() {
        boolean value = false;
        try {

            UiSelector elementID = new UiSelector().descriptionContains("Files");
            UiObject element = device.findObject(elementID);
            if (element.waitForExists(GlobalVariables.Wait_Timeout)) {
                element.click();
                ub2 = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name, "Click On Upload Button 2nd Time:" + ub2);
                value = true;
            }
        } catch (Exception e) {
            Log.d(
                    GlobalVariables.Tag_Name,
                    "Error in Click On 2nd Upload");
        }
        return value;
    }

    private boolean loaderUpload2() {
        boolean value = false;
        try {

//            UiSelector loaderId =
//                    new UiSelector().resourceId("com.google.android.apps.docs:id/uploading_view");
//            UiObject loader = device.findObject(loaderId);

            UiSelector loaderId =
                    new UiSelector().textContains("Uploading");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.TwoMins_Timeout) {
                if (loader.exists()) {
                    uploadingYes2 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "File Uploading 2nd Time:" + uploadingYes2);
                    Thread.sleep(200);
                } else {
                    uploadingNo2 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "File Uploaded 2nd Time:" + uploadingNo2);
                    value = true;
                    break;
                }
            }
            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in File Uploading 2nd Time");
            return false;
        }
    }

    private boolean clickDownload2() {
        boolean value = false;
        try {

            UiScrollable items =
                    new UiScrollable(
                            new UiSelector()
                                    .resourceId("com.google.android.apps.docs:id/menu_recycler_view"));
            items.setAsVerticalList();
            items.scrollToEnd(5, 15);

            UiSelector elementID = new UiSelector().text("Download");
            UiObject element = device.findObject(elementID);

            if (element.waitForExists(GlobalVariables.OneMin_Timeout)) {
                element.click();
                db2 = new Timestamp(new Date().getTime());
                Log.d(GlobalVariables.Tag_Name,"Click On Download Button 2nd Time:" + db2);

                device.swipe(0, 0, 50, 50, 3);
                device.swipe(0, 150, 150, 150, 1);

                Thread.sleep(2000);
                value = true;
            }

        } catch (Exception e) {

            Log.d(GlobalVariables.Tag_Name, "Click on Download Failed 2nd Time");
        }
        return value;
    }

    private boolean loaderDownload2() {
       boolean value = false;
        try {

            UiSelector loaderId = new UiSelector().text("Google Drive");
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= GlobalVariables.TwoMins_Timeout) {
                if (loader.exists()) {
                    downloadingYes2 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "File is Downloading 2nd Time:" + downloadingYes2);
                    Thread.sleep(200);
                } else {
                    downloadingNo2 = new Timestamp(new Date().getTime());
                    Log.d(GlobalVariables.Tag_Name, "File Downloaded 2nd Time:" + downloadingNo2);
                    device.pressBack();
                    value = true;
                    break;
                }
            }

            return value;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in File Downloading 2nd Time");
            return false;
        }
    }

    public void deleteFiles() {
        try {
            Process processcmd = Runtime.getRuntime().exec("su");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(processcmd.getInputStream()));

            OutputStream os = processcmd.getOutputStream();
            // below code for capture screenshot and placed in internal storage.
            os.write("adb shell\n".getBytes());
            // below code for increase brightness.
            os.write("su \n".getBytes());

            os.write("cd /sdcard/Download && rm -rf !(\"GoogleDriveFiles\")".getBytes());
            os.close();
            os.flush();
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name,"Error in Shell Delete Files Method");
        }
    }

    public boolean kpiCalculation() {
        boolean value = false;
        try {
            double TTLH = (het.getTime() - lt.getTime()) / 1000.0;
            double UT1 = (uploadingNo1.getTime() - ub1.getTime()) / 1000.0;
            double DT1 = (downloadingNo1.getTime() - db1.getTime()) / 1000.0;
//            double UT2 = (uploadingNo2.getTime() - ub2.getTime()) / 1000.0;
//            double DT2 = (downloadingNo2.getTime() - db2.getTime()) / 1000.0;

            Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
            Log.d(GlobalVariables.Tag_Name, "File Uploading Time = " + UT1);
            Log.d(GlobalVariables.Tag_Name, "File Downloading Time = " + DT1);
//            Log.d(GlobalVariables.Tag_Name, "File Uploading 2nd Time = " + UT2);
//            Log.d(GlobalVariables.Tag_Name, "File Downloading 2nd Time = " + DT2);
            value = true;
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
        }
        return value;
    }

}
