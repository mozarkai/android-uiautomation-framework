package com.example.testapp.apps;

import android.content.Context;
import android.graphics.Rect;
import android.util.Log;

import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import com.example.testapp.ExampleInstrumentedTest;
import com.example.testapp.utility.TestApp;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.KpiLogs;
import com.mozark.uiautomatorlibrary.utils.SendStatus;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import static com.mozark.uiautomatorlibrary.utils.Utility.stopScreenRecording;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;

import org.apache.commons.lang3.time.StopWatch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;


public class Twitter implements AppClass {

  public static String testId;
  public String log = "";
  int job_id;
  String device_id;
  String ipAdress;
  Context context;
  String location;
  String order, script;
  long testTimes;
  String appName;
  String startDate;
  String OUTPUT_LOG_FILE = "";
  int check;
  String Email;
  String Password;
  String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
  private Timestamp lt,
      ht,
      st,
      et,
      sat,
      Elements_LoadTime,
      login_Button_Time,
      post_Text,
      post_Image,
      Image_Tweet,
      Text_Tweet;
  private UiDevice device;

  public int testRun(
          int job_id,
          String device_id,
          String ipAdress,
          UiDevice device,
          Timestamp lt,
          String order,
          String script,
          String location,
          String appName, boolean vCap, boolean pCap, String appVersion,String startDate) {
    boolean runTest = true;
    try {

      Utility.permission(GlobalVariables.Twitter_Package);

      this.job_id = job_id;
      this.device_id = device_id;
      this.ipAdress = ipAdress;
      this.device = device;
      this.lt = lt;
      this.startDate=startDate;
      this.order = order;
      this.script = script;
      this.location = location;
      Timestamp testTime = new Timestamp(new Date().getTime());
      testTimes = testTime.getTime();
      testId = device_id + testTimes;
      ExampleInstrumentedTest.testId = testId;
      this.appName = appName;
      //            this.Email = Email;
      //            this.Password = Password;

      context = TestApp.getContext();

      UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
      String run = "Running";
      updateDeviceStatus.update(device_id, ipAdress, run);
      if (job_id != -1) {
        SendStatus sendStatus = new SendStatus();
        sendStatus.status(
            job_id, device_id, testId, "running", false, order, script, appVersion, appName, ipAdress,startDate);
      }

      int id = android.os.Process.myPid();
      try {

        log = Utility.fileName(testId) + ".txt";
        OUTPUT_LOG_FILE = Utility.fileName(testId) + "stats.txt";
        ExampleInstrumentedTest.OUTPUT_LOG_FILE = OUTPUT_LOG_FILE;
        BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_LOG_FILE));
        //                writer.write("Cores|"+Utility.getNumOfCores()+"   " +"Ram|"+
        // Utility.getTotalRAM() +"\n");
        writer.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Utility.log(id, log);

      //            check = cch.chromeTestRun(device);
      //            Log.d(GlobalVariables.Tag_Name,"Enter URL Time from Chrome" +
      // cch.Enter_URLTime);
      //
      //            if(check == 1){
      //                return 1;
      //            }

      //            runTest = firstLogin();
      //            if (!runTest) {
      //                return 1;
      //            }

      startScreenRecording(testId, GlobalVariables.screenRecord_Time, vCap);

      runTest = firstLogin();
      if (!runTest) {
        runTest = menu();
        if (!runTest) {
          return 1;
        }

        runTest = clickSettings();
        if (!runTest) {
          return 1;
        }

        runTest = clickAccount();
        if (!runTest) {
          return 1;
        }

        runTest = logout();
        if (!runTest) {
          return 1;
        }

        runTest = logoutOk();
        if (!runTest) {
          return 1;
        }

        runTest = firstLogin();
        if (!runTest) {
          return 1;
        }
      }

      runTest = email();
      if (!runTest) {
        return 1;
      }

      runTest = password();
      if (!runTest) {
        return 1;
      }

      runTest = loginButton();
      if (!runTest) {
        return 1;
      }

      //            locationPopup();
      //
      //            permissionPopup();

      runTest = homeElements1();
      if (!runTest) {
        return 1;
      }

      runTest = searchAppear();
      if (!runTest) {
        return 1;
      }

      runTest = search();
      if (!runTest) {
        return 1;
      }

      runTest = enterSearch();
      if (!runTest) {
        return 1;
      }

      runTest = searchResults();
      if (!runTest) {
        return 1;
      }

      runTest = backButton();
      if (!runTest) {
        return 1;
      }

      runTest = homeButton();
      if (!runTest) {
        return 1;
      }

      runTest = homeElements2();
      if (!runTest) {
        return 1;
      }

      runTest = newTweet();
      if (!runTest) {
        return 1;
      }

      runTest = enterTextTweet();
      if (!runTest) {
        return 1;
      }

      runTest = postTextTweet();
      if (!runTest) {
        return 1;
      }

      runTest = textBuffer();
      if (!runTest) {
        return 1;
      }

      runTest = newTweet();
      if (!runTest) {
        return 1;
      }

      runTest = enterImageTweet();
      if (!runTest) {
        return 1;
      }

      runTest = photosButton();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = galleryButton();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = selectMore();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = toolbarButton();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = selectStorage();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = searchFile();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = searchFileEdit();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = selectFolder();
      if (!runTest) {
        return 1;
      }

      Thread.sleep(5000);
      runTest = selectFile();
      if (!runTest) {
        return 1;
      }

      runTest = postImageTweet();
      if (!runTest) {
        return 1;
      }

      runTest = imageBuffer();
      if (!runTest) {
        return 1;
      }

      runTest = menu();
      if (!runTest) {
        return 1;
      }

      runTest = clickSettings();
      if (!runTest) {
        return 1;
      }

      runTest = clickAccount();
      if (!runTest) {
        return 1;
      }

      runTest = logout();
      if (!runTest) {
        return 1;
      }

      runTest = logoutOk();
      if (!runTest) {
        return 1;
      }

      stopScreenRecording();

      KpiLogs.twitterLogs(
          lt,
          st,
          login_Button_Time,
          Elements_LoadTime,
          et,
          sat,
          post_Text,
          Text_Tweet,
          post_Image,
          Image_Tweet);

      kpiCalculation();

      Utility.batteryStats(OUTPUT_LOG_FILE);
      context = TestApp.getContext();

      new AquamarkPcap(context, "stop", pCap).execute();

      Log.d(GlobalVariables.Tag_Name, "The pcap is stopped");

      tearDown();
      Utility.sendPcap(testId, ipAdress, device_id, pCap);

      runTest = sendData(appVersion);
      if (!runTest) {
        return 1;
      }

      Utility.logFileGeneration(testId, log, ipAdress, device_id);

      return 0;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Test Run");
    }
    return 0;
  }

  private String getCommand(String appPackage) {
    return "am force-stop " + appPackage;
  }

  public void tearDown() {
    try {
      Thread.sleep(GlobalVariables.LAUNCH_TIMEOUT);
      device.executeShellCommand(getCommand(GlobalVariables.Twitter_Package));
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in closing the app");
    }
  }

  private boolean progressBar1() {
    try {

      Thread.sleep(1000);
      UiSelector loaderId = new UiSelector().textContains("Loading…");
      //            UiSelector loaderId = new
      // UiSelector().resourceId("com.qatarliving.classifieds:id/messageTv");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      int time = 0;
      int c = GlobalVariables.Delay_Time;

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Log.d(
              GlobalVariables.Tag_Name,
              "First Progress Loader No Time:" + new Timestamp(new Date().getTime()));
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Progress Bar1 Method");
      return false;
    }
  }

  private boolean firstLogin() {
    try {
      boolean loginFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find First Login button with Attempt no." + (i + 1));

          UiSelector loginID = new UiSelector().textContains("Log in");
          UiObject login = device.findObject(loginID);
          if (login.waitForExists(GlobalVariables.Wait_Timeout)) {

            Rect r = login.getBounds();

            Rect loginReactangle = login.getVisibleBounds();

            Log.d(
                GlobalVariables.Tag_Name,
                "Visible Bounds of Login rectangle is:" + loginReactangle);
            Log.d(GlobalVariables.Tag_Name, "Bounds of Login rectangle is:" + r);

            int right_Of_bounds = loginReactangle.right;

            Log.d(
                GlobalVariables.Tag_Name,
                "The Right side Value of Login rectangle is:" + right_Of_bounds);

            double width_of_right = (right_Of_bounds / 100) * 10;

            Log.d(
                GlobalVariables.Tag_Name,
                "The Center Value of Height is:" + loginReactangle.centerY());
            Log.d(GlobalVariables.Tag_Name, "the Center Value of Width is:" + width_of_right);

            int width = (int) (right_Of_bounds - width_of_right);

            Log.d(GlobalVariables.Tag_Name, "The Value of Width to Click:" + width);
            Log.d(
                GlobalVariables.Tag_Name,
                "The Value of Height to Click:" + loginReactangle.centerY());

            device.click(width, loginReactangle.centerY());

            st = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on First Login Time:" + st);

            loginFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in First Login");
        }
      }
      return loginFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding First Login Button Method");
      return false;
    }
  }

  private boolean login() {
    try {
      boolean loginFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Login with Attempt no." + (i + 1));

          UiSelector loginId = new UiSelector().resourceId("com.twitter.android:id/detail_text");
          UiSelector loginId2 = new UiSelector().textContains("Log in");
          UiObject login2 = device.findObject(loginId2);

          UiCollection loginID =
              new UiCollection(new UiSelector().resourceId("com.twitter.android:id/sign_in_text"));
          UiObject login = loginID.getChildByInstance(new UiSelector(), 1);

          if (login2.waitForExists(GlobalVariables.Wait_Timeout)) {
            //                        Thread.sleep(20000);
            login2.click();
            //                        login2.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Login:" + new Timestamp(new Date().getTime()));
            loginFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Login Failed");
        }
      }
      return loginFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Login Method");
      return false;
    }
  }

  private boolean email() {
    try {
      boolean emailButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Email button with Attempt no." + (i + 1));

          UiSelector emailId =
              new UiSelector().resourceId("com.twitter.android:id/login_identifier");
          UiObject email = device.findObject(emailId);
          if (email.waitForExists(GlobalVariables.Wait_Timeout)) {
            email.click();
            email.setText(GlobalVariables.TwitterEmail);
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Email:" + new Timestamp(new Date().getTime()));
            emailButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Email Button Failed");
        }
      }
      return emailButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Email Button Method");
      return false;
    }
  }

  private boolean password() {
    try {
      boolean passwordButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Password button with Attempt no." + (i + 1));

          UiSelector passwordId =
              new UiSelector().resourceId("com.twitter.android:id/login_password");
          UiObject passwordButton = device.findObject(passwordId);
          if (passwordButton.waitForExists(GlobalVariables.Wait_Timeout)) {
            passwordButton.click();
            passwordButton.setText(GlobalVariables.TwitterPassword);
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Password:" + new Timestamp(new Date().getTime()));
            passwordButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Password Button Failed");
        }
      }
      return passwordButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Password Button Method");
      return false;
    }
  }

  private boolean loginButton() {
    try {
      boolean loginButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Login button with Attempt no." + (i + 1));

          UiSelector loginId = new UiSelector().resourceId("com.twitter.android:id/login_login");
          UiObject login = device.findObject(loginId);
          if (login.waitForExists(GlobalVariables.Wait_Timeout) && login.isEnabled()) {
            login.click();
            login_Button_Time = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Login Time:" + login_Button_Time);
            loginButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Login Button Failed");
        }
      }
      return loginButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Login Button Method");
      return false;
    }
  }

  private boolean homeElements1() {
    try {

      boolean locationOK = false;
      UiSelector homeElementsID =
          new UiSelector().resourceId("com.twitter.android:id/tweet_curation_action");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          Elements_LoadTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + Elements_LoadTime);
          break;
        } else {

          if (!locationOK) {
            UiSelector locationId = new UiSelector().resourceId("android:id/button1");
            UiObject locationOk = device.findObject(locationId);
            if (locationOk.exists()) {
              locationOk.click();
              locationOK = true;
            }
          }
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean locationPopup() {

    try {
      boolean locationOkFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Location OK button with Attempt no." + (i + 1));

          UiSelector locationId = new UiSelector().resourceId("android:id/button1");
          UiObject locationOk = device.findObject(locationId);
          if (locationOk.waitForExists(GlobalVariables.Wait_Timeout)) {
            locationOk.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Location OK:" + new Timestamp(new Date().getTime()));
            locationOkFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Location OK Failed");
        }
      }
      return locationOkFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Location OK Method");
      return false;
    }
  }

  private boolean permissionPopup() {

    try {
      boolean permissionAllowFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Permission Allow button with Attempt no." + (i + 1));

          UiSelector permissionId =
              new UiSelector()
                  .resourceId("com.android.packageinstaller:id/permission_allow_button");
          UiObject permissionAllow = device.findObject(permissionId);
          if (permissionAllow.waitForExists(GlobalVariables.Wait_Timeout)) {
            permissionAllow.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Permission Allow:" + new Timestamp(new Date().getTime()));
            permissionAllowFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Permission Allow Failed");
        }
      }
      return permissionAllowFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Permission Allow Method");
      return false;
    }
  }

  //    private boolean searchAppear() {
  //        try {
  //            boolean searchButtonFound = false;
  //            for (int i = 0; i < 5; i++) {
  //                try {
  //                    Log.d(GlobalVariables.Tag_Name, "Trying to find search button with Attempt
  // no." + (i + 1));
  //
  //                        UiCollection searchID = new UiCollection(new
  // UiSelector().resourceId("com.twitter.android:id/tabs"));
  //                        UiObject search2 = searchID.getChildByInstance(new UiSelector(), 5);
  //                        if (search2.waitForExists(GlobalVariables.TimeOut)) {
  //                            search2.click();
  //                            Log.d(GlobalVariables.Tag_Name, "Search Button Appear Tabs2:" + new
  // Timestamp(new Date().getTime()));
  //                            searchButtonFound = true;
  //                            break;
  //                        }
  //
  //
  //
  //                } catch (Exception e) {
  //                    Log.d(GlobalVariables.Tag_Name, "Error in search Appear:" + new
  // Timestamp(new Date().getTime()));
  //                }
  //            }
  //            return searchButtonFound;
  //        } catch (Exception e) {
  //            Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
  //            return false;
  //        }
  //    }

  private boolean searchAppear() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().descriptionContains("Explore");
          UiObject searchAppear = device.findObject(searchId);
          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Search Button Appear:" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          } else {
            UiCollection searchID =
                new UiCollection(new UiSelector().resourceId("com.twitter.android:id/tabs"));
            UiObject search2 = searchID.getChildByInstance(new UiSelector(), 5);

            if (search2.waitForExists(GlobalVariables.TimeOut)) {
              search2.click();
              Log.d(
                  GlobalVariables.Tag_Name,
                  "Search Button Appear Tabs2:" + new Timestamp(new Date().getTime()));
              searchButtonFound = true;
              break;
            }
          }
        } catch (Exception e) {
          Log.d(
              GlobalVariables.Tag_Name,
              "Error in search Appear:" + new Timestamp(new Date().getTime()));
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean search() {
    try {
      boolean searchButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find search button with Attempt no." + (i + 1));

          UiSelector searchId = new UiSelector().resourceId("com.twitter.android:id/query_view");
          UiObject searchAppear = device.findObject(searchId);
          if (searchAppear.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchAppear.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Search Button Appear2:" + new Timestamp(new Date().getTime()));
            searchButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in search Appear2");
        }
      }
      return searchButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding search Button Method");
      return false;
    }
  }

  private boolean enterSearch() {
    try {
      boolean enterSearchtext = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to enter Search with Attempt no." + (i + 1));
          UiSelector enterSearchField = new UiSelector().resourceId("com.twitter.android:id/query");
          UiObject enterSearch_text = device.findObject(enterSearchField);

          if (enterSearch_text.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterSearch_text.setText("#VisitQatar \n ");
            device.pressEnter();
            et = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.Enter_Tag_Search_Time + et);
            enterSearchtext = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Text Failed");
        }
      }
      return enterSearchtext;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in entering Search Text");
      return false;
    }
  }

  private boolean searchResults() {
    try {
      UiSelector searchResultsID =
          new UiSelector().resourceId("com.twitter.android:id/tweet_curation_action");
      UiObject searchResults = device.findObject(searchResultsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (searchResults.exists()) {
          sat = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Search_Tag_Result_Time + sat);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Search Results:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Search Results");
      return false;
    }
  }

  private boolean backButton() {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Back button with Attempt no." + (i + 1));

          UiSelector homeID = new UiSelector().descriptionContains("Navigate up");
          UiObject home = device.findObject(homeID);

          if (home.waitForExists(GlobalVariables.Wait_Timeout)) {
            home.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Back Button:" + ht);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click Back Button Failed");
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Back Button Method");
      return false;
    }
  }

  private boolean homeButton() {
    try {
      boolean homeButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Home button with Attempt no." + (i + 1));

          UiSelector homeID2 = new UiSelector().descriptionContains("Home Tab");
          UiObject home2 = device.findObject(homeID2);

          if (home2.waitForExists(GlobalVariables.Wait_Timeout)) {
            home2.click();
            ht = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Home Tab" + ht);
            homeButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click Home Tab Button Failed");
        }
      }
      return homeButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Home Tab Button Method");
      return false;
    }
  }

  private boolean homeElements2() {
    try {
      UiSelector homeElementsID =
          new UiSelector().resourceId("com.twitter.android:id/tweet_curation_action");
      UiObject homeElements = device.findObject(homeElementsID);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (homeElements.exists()) {
          Elements_LoadTime = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Page_Time + Elements_LoadTime);
          break;
        } else {
          Log.d(
              GlobalVariables.Tag_Name,
              "Loading Home Page Elements:" + new Timestamp(new Date().getTime()));
        }
      }
      return true;

    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in getting Home Page Elements");
      return false;
    }
  }

  private boolean newTweet() {
    try {
      boolean newTweetBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(GlobalVariables.Tag_Name, "Trying to click on New Tweet with Attempt no.:" + (i + 1));
        try {

          UiSelector newTweetId =
              new UiSelector().resourceId("com.twitter.android:id/composer_write");
          UiObject newTweet = device.findObject(newTweetId);

          if (newTweet.waitForExists(GlobalVariables.Wait_Timeout)) {
            newTweet.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on New Tweet" + new Timestamp(new Date().getTime()));
            newTweetBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on New Tweet Failed");
        }
      }
      return newTweetBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error New Tweet Method");
      return false;
    }
  }

  private boolean enterTextTweet() {
    try {
      boolean enterTweetBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(
            GlobalVariables.Tag_Name, "Trying to click on Enter Tweet with Attempt no.:" + (i + 1));
        try {

          UiSelector enterTweetId =
              new UiSelector().resourceId("com.twitter.android:id/tweet_text");
          UiObject enterTweet = device.findObject(enterTweetId);

          if (enterTweet.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterTweet.setText(GlobalVariables.TwitterText + new Timestamp(new Date().getTime()));
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Enter Tweet:" + new Timestamp(new Date().getTime()));
            enterTweetBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Enter Tweet Failed");
        }
      }
      return enterTweetBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error Enter Tweet Method");
      return false;
    }
  }

  private boolean postTextTweet() {
    try {
      boolean postTweetBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(
            GlobalVariables.Tag_Name, "Trying to click on Post Tweet with Attempt no.:" + (i + 1));
        try {

          UiSelector postTweetId =
              new UiSelector().resourceId("com.twitter.android:id/button_tweet");
          UiObject postTweet = device.findObject(postTweetId);

          if (postTweet.waitForExists(GlobalVariables.Wait_Timeout)) {
            postTweet.click();
            post_Text = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Post Text Tweet Time:" + post_Text);
            postTweetBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Post Tweet Failed");
        }
      }
      return postTweetBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error Post Tweet Method");
      return false;
    }
  }

  private boolean textBuffer() {
    try {

      UiSelector loaderId = new UiSelector().resourceId("com.twitter.android:id/main_progress_bar");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Text_Tweet = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Text Tweet Appear Time:" + Text_Tweet);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean enterImageTweet() {
    try {
      boolean enterTweetBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(
            GlobalVariables.Tag_Name, "Trying to click on Enter Tweet with Attempt no.:" + (i + 1));
        try {

          UiSelector enterTweetId =
              new UiSelector().resourceId("com.twitter.android:id/tweet_text");
          UiObject enterTweet = device.findObject(enterTweetId);

          if (enterTweet.waitForExists(GlobalVariables.Wait_Timeout)) {
            enterTweet.setText("Tweet Time:" + new Timestamp(new Date().getTime()));
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Enter Tweet:" + new Timestamp(new Date().getTime()));
            enterTweetBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Enter Tweet Failed");
        }
      }
      return enterTweetBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error Enter Tweet Method");
      return false;
    }
  }

  private boolean photosButton() {
    try {
      boolean photosButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on photos button with Attempt no." + (i + 1));

          //                    UiCollection photosID = new UiCollection(new
          // UiSelector().resourceId("com.twitter.android:id/gallery"));
          //                    UiObject photos = photosID.getChildByInstance(new UiSelector(), 1);

          UiSelector photosID = new UiSelector().descriptionContains("Photos");
          UiObject photos = device.findObject(photosID);

          if (photos.waitForExists(GlobalVariables.Wait_Timeout)) {
            photos.click();
            photosButtonFound = true;
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Photos:" + new Timestamp(new Date().getTime()));
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in photos Button");
        }
      }
      return photosButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding photos Button Method");
      return false;
    }
  }

  private boolean galleryButton() {
    try {
      boolean galleryButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on Gallery button with Attempt no." + (i + 1));

          UiSelector galleryID =
              new UiSelector().resourceId("com.twitter.android:id/gallery_toolbar_spinner");
          UiObject gallery = device.findObject(galleryID);

          if (gallery.waitForExists(GlobalVariables.Wait_Timeout)) {
            gallery.click();
            galleryButtonFound = true;
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Gallery:" + new Timestamp(new Date().getTime()));
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in Gallery Button");
        }
      }
      return galleryButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Gallery Button Method");
      return false;
    }
  }

  private boolean selectMore() {
    try {
      boolean moreButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on More button with Attempt no." + (i + 1));

          UiSelector moreID = new UiSelector().textContains("More");
          UiObject more = device.findObject(moreID);

          UiScrollable scroll =
              new UiScrollable(new UiSelector().resourceId("com.twitter.android:id/gallery_name"));
          scroll.scrollToEnd(5);

          if (more.waitForExists(GlobalVariables.Wait_Timeout)) {
            more.click();
            moreButtonFound = true;
            Log.d(
                GlobalVariables.Tag_Name, "Clicked on More:" + new Timestamp(new Date().getTime()));
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in More Button");
        }
      }
      return moreButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding More Button Method");
      return false;
    }
  }

  private boolean toolbarButton() {
    try {
      boolean toolbarButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to click on toolbar button with Attempt no." + (i + 1));

          UiSelector toolbarID = new UiSelector().descriptionContains("Show roots");
          UiObject toolbar = device.findObject(toolbarID);
          if (toolbar.waitForExists(GlobalVariables.Wait_Timeout)) {
            toolbar.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on ToolBar Menu:" + new Timestamp(new Date().getTime()));
            toolbarButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Error in ToolBar Menu Button");
        }
      }
      return toolbarButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding ToolBar Menu Button Method");
      return false;
    }
  }

  private boolean selectStorage() {
    try {
      boolean selectBtnFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to select Storage with Attempt no.:" + (i + 1));

          UiCollection storageID =
              new UiCollection(
                  new UiSelector().resourceId("com.android.documentsui:id/roots_list"));
          UiObject storage = storageID.getChildByInstance(new UiSelector(), 24);

          if (storage.waitForExists(GlobalVariables.Wait_Timeout)) {
            storage.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Storage Menu:" + new Timestamp(new Date().getTime()));
            selectBtnFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Storage Failed");
        }
      }
      return selectBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Storage Method");
      return false;
    }
  }

  private boolean searchFile() {
    try {
      boolean seacrhFileBtn = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find seacrh file button with Attempt no." + (i + 1));

          UiSelector searchID =
              new UiSelector().resourceId("com.android.documentsui:id/option_menu_search");
          UiObject searchSelector = device.findObject(searchID);

          //                    UiSelector searchID = new
          // UiSelector().descriptionContains("Search");
          //                    UiObject searchSelector = device.findObject(searchID);

          if (searchSelector.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchSelector.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Search File:" + new Timestamp(new Date().getTime()));
            seacrhFileBtn = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Search File Failed");
        }
      }
      return seacrhFileBtn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Seacrh Button Method");
      return false;
    }
  }

  private boolean searchFileEdit() {
    try {
      boolean searchEditFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to enter seacrh button with Attempt no." + (i + 1));

          UiCollection searchSelector =
              new UiCollection(new UiSelector().resourceId("android:id/search_src_text"));

          UiSelector searchEditID = new UiSelector().resourceId("android:id/search_src_text");
          UiObject searchEdit = device.findObject(searchEditID);

          if (searchEdit.waitForExists(GlobalVariables.Wait_Timeout)) {
            searchEdit.setText("Twitter Images");
            device.pressEnter();
            Log.d(
                GlobalVariables.Tag_Name,
                "Entered Search File:" + new Timestamp(new Date().getTime()));
            searchEditFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Enter Search Failed");
        }
      }
      return searchEditFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Entering Search Button Method");
      return false;
    }
  }

  private boolean selectFolder() {
    try {
      boolean selectFolderBtn = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to select Folder with Attempt no.:" + (i + 1));

          UiCollection selectFolderID =
              new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));
          UiObject selectFolder = selectFolderID.getChildByInstance(new UiSelector(), 1);

          if (selectFolder.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectFolder.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Select Folder Time:" + new Timestamp(new Date().getTime()));
            selectFolderBtn = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on select Folder Failed");
        }
      }
      return selectFolderBtn;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in select Folder Method");
      return false;
    }
  }

  private boolean selectFile() {
    try {
      boolean selectFileFound = false;
      for (int i = 0; i < 5; i++) {

        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to select File with Attempt no.:" + (i + 1));

          UiCollection selectFileID =
              new UiCollection(new UiSelector().resourceId("com.android.documentsui:id/dir_list"));
          UiObject selectFile = selectFileID.getChildByInstance(new UiSelector(), 1);

          if (selectFile.waitForExists(GlobalVariables.Wait_Timeout)) {
            selectFile.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Select File Time:" + new Timestamp(new Date().getTime()));
            selectFileFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on select File Failed");
        }
      }
      return selectFileFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in selecting File");
      return false;
    }
  }

  private boolean postImageTweet() {
    try {
      boolean postTweetBtnFound = false;
      for (int i = 0; i < 5; i++) {
        Log.d(
            GlobalVariables.Tag_Name, "Trying to click on Post Tweet with Attempt no.:" + (i + 1));
        try {

          UiSelector postTweetId =
              new UiSelector().resourceId("com.twitter.android:id/button_tweet");
          UiObject postTweet = device.findObject(postTweetId);

          if (postTweet.waitForExists(GlobalVariables.Wait_Timeout)) {
            postTweet.click();
            post_Image = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, "Clicked on Post Image Tweet Time:" + post_Image);
            postTweetBtnFound = true;
            break;
          }

        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Post Image Tweet Failed");
        }
      }
      return postTweetBtnFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error Post Image Tweet Method");
      return false;
    }
  }

  private boolean imageBuffer() {
    try {

      UiSelector loaderId = new UiSelector().resourceId("com.twitter.android:id/main_progress_bar");
      UiObject loader = device.findObject(loaderId);

      StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      while (stopWatch.getTime() <= GlobalVariables.Video_Run_Time) {
        if (loader.exists()) {
          Timestamp loaderYes = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_Yes + loaderYes);
        } else {
          Timestamp loaderNo = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, GlobalVariables.Load_No + loaderNo);
          Image_Tweet = new Timestamp(new Date().getTime());
          Log.d(GlobalVariables.Tag_Name, "Image Tweet Appear Time:" + Image_Tweet);
          break;
        }
      }
      return true;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Buffer Reading");
      return false;
    }
  }

  private boolean menu() {
    try {
      boolean menuButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(GlobalVariables.Tag_Name, "Trying to find Menu button with Attempt no." + (i + 1));

          UiSelector menuId = new UiSelector().descriptionContains("Show navigation drawer");
          UiObject menu = device.findObject(menuId);
          if (menu.waitForExists(GlobalVariables.Wait_Timeout)) {
            menu.click();
            Log.d(
                GlobalVariables.Tag_Name, "Clicked on Menu:" + new Timestamp(new Date().getTime()));
            menuButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Menu Button Failed");
        }
      }
      return menuButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Menu Button Method");
      return false;
    }
  }

  private boolean clickSettings() {
    try {
      boolean settingsFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Settings button with Attempt no." + (i + 1));

          UiSelector settingsId = new UiSelector().textContains("Settings and privacy");
          UiObject settings = device.findObject(settingsId);
          if (settings.waitForExists(GlobalVariables.Wait_Timeout)) {
            settings.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Settings:" + new Timestamp(new Date().getTime()));
            settingsFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Settings Button Failed");
        }
      }
      return settingsFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Settings Button Method");
      return false;
    }
  }

  private boolean clickAccount() {
    try {
      boolean accountButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Account button with Attempt no." + (i + 1));

          UiSelector accountId = new UiSelector().textContains("Account");
          UiObject account = device.findObject(accountId);
          if (account.waitForExists(GlobalVariables.Wait_Timeout)) {
            account.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Account:" + new Timestamp(new Date().getTime()));
            accountButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Account Button Failed");
        }
      }
      return accountButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Account Button Method");
      return false;
    }
  }

  private boolean logout() {
    try {
      boolean logoutButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name, "Trying to find Logout button with Attempt no." + (i + 1));

          UiSelector logoutId = new UiSelector().textContains("Log out");
          UiObject logout = device.findObject(logoutId);

          UiScrollable scroll = new UiScrollable(new UiSelector().resourceId("android:id/list"));
          scroll.scrollToEnd(5);

          if (logout.waitForExists(GlobalVariables.Wait_Timeout)) {
            logout.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Logout:" + new Timestamp(new Date().getTime()));
            logoutButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Logout Button Failed");
        }
      }
      return logoutButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Logout Button Method");
      return false;
    }
  }

  private boolean logoutOk() {
    try {
      boolean logoutOkButtonFound = false;
      for (int i = 0; i < 5; i++) {
        try {
          Log.d(
              GlobalVariables.Tag_Name,
              "Trying to find Logout OK button with Attempt no." + (i + 1));

          UiSelector logoutId = new UiSelector().textContains("Ok");
          UiObject logout = device.findObject(logoutId);
          if (logout.waitForExists(GlobalVariables.Wait_Timeout)) {
            logout.click();
            Log.d(
                GlobalVariables.Tag_Name,
                "Clicked on Logout OK:" + new Timestamp(new Date().getTime()));
            logoutOkButtonFound = true;
            break;
          }
        } catch (Exception e) {
          Log.d(GlobalVariables.Tag_Name, "Click on Logout OK Button Failed");
        }
      }
      return logoutOkButtonFound;
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in Finding Logout OK Button Method");
      return false;
    }
  }

  public void kpiCalculation() {
    try {

      double TLP = (st.getTime() - lt.getTime()) / 1000.0;
      double TTLH = (Elements_LoadTime.getTime() - login_Button_Time.getTime()) / 1000.0;
      double SRT = (sat.getTime() - et.getTime()) / 1000.0;
      double PTT = (Text_Tweet.getTime() - post_Text.getTime()) / 1000.0;
      double PIT = (Image_Tweet.getTime() - post_Image.getTime()) / 1000.0;

      Log.d(GlobalVariables.Tag_Name, "Time To Launch App = " + TLP);
      Log.d(GlobalVariables.Tag_Name, "Time To Load Home Page = " + TTLH);
      Log.d(GlobalVariables.Tag_Name, "Search #Tag Results Time = " + SRT);
      Log.d(GlobalVariables.Tag_Name, "Time To Post Text Tweet = " + PTT);
      Log.d(GlobalVariables.Tag_Name, "Time To Post Image Tweet = " + PIT);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in KPI Calculation");
      e.printStackTrace();
    }
  }

  public boolean sendData(String appVersion) {
    try {
      SendStatus sendStatus = new SendStatus();
      return sendStatus.status(
          job_id, device_id, testId, "completed", false, order, script, appVersion, appName, ipAdress,startDate);
    } catch (Exception e) {
      Log.d(GlobalVariables.Tag_Name, "Error in sending the Data to the Server");
      return false;
    }
  }
}
