package com.example.testapp;

import static com.mozark.uiautomatorlibrary.utils.Utility.callCellInfo;
import static com.mozark.uiautomatorlibrary.utils.record.RecordScreen.startScreenRecording;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.Until;

import com.example.testapp.apps.*;
import com.example.testapp.utility.UtilityClass;
import com.mozark.uiautomatorlibrary.getdata.GetData;
import com.mozark.uiautomatorlibrary.mozarkmodel.DeviceInfoKpis;
import com.mozark.uiautomatorlibrary.mozarkmodel.SendDataToUIAutomator;
import com.mozark.uiautomatorlibrary.senddata.Json;
import com.mozark.uiautomatorlibrary.senddata.Request;
import com.mozark.uiautomatorlibrary.senddata.SendData;
import com.mozark.uiautomatorlibrary.senddata.SendDataApi;
import com.mozark.uiautomatorlibrary.utils.AquamarkPcap;
import com.mozark.uiautomatorlibrary.utils.Data;
import com.mozark.uiautomatorlibrary.utils.DataHolder;
import com.mozark.uiautomatorlibrary.utils.GlobalVariables;
import com.mozark.uiautomatorlibrary.utils.Helper;
import com.mozark.uiautomatorlibrary.utils.JsonUtil;
import com.mozark.uiautomatorlibrary.utils.UpdateDeviceStatus;
import com.mozark.uiautomatorlibrary.utils.Utility;
import com.mozark.uiautomatorlibrary.utils.aqua5gmark.NetworkDetails;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends Base {

    public static final boolean IS_ACTIVE_AQUA5GMARK = true;
    public static String OUTPUT_LOG_FILE_TEMP = "/sdcard/stats.txt";
    public static String OUTPUT_LOG_FILE_TEMP_BATTARY_STATS = "/sdcard/batstats.txt";
    public static String OUTPUT_LOG_FILE_TEMP_COPY = "/sdcard/statsCopy.txt", OUTPUT_LOG_FILE = "";
    public static String testId = "",
            startDate = "";
    public static String apName,
            location,
            device_id,
            ipAdress,
            orderId,
            scriptId,
            isp,
            lat,
            lng;
    public static boolean pCap = true, vCap = true;
    public static boolean isFromIntent = false;
    public static Timestamp lt;
    public static SendDataToUIAutomator sendDataToUIAutomator;
    String storedPcapFilePath = "/sdcard/capturedpcapfile.pcap";
    int job_id;
    Context context = ApplicationProvider.getApplicationContext();
    private UiDevice device;

    @Before
    public void startMainActivityFromHomeScreen() {

        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // sendBroadcastForCapturing("","START_FPS");
        final String launcherPackage = device.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        device.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), GlobalVariables.LAUNCH_TIMEOUT);
        UtilityClass.appendLogFileByDate("UIAutomator is start method startMainActivityFromHomeScreen");
        Utility.checkFile("start");
        try {
            if ((new File(OUTPUT_LOG_FILE_TEMP_BATTARY_STATS)).exists()) {
                (new File(OUTPUT_LOG_FILE_TEMP_BATTARY_STATS)).delete();
                (new File(OUTPUT_LOG_FILE_TEMP_BATTARY_STATS)).createNewFile();
            }
            try {
                String date = "";
                Date varDate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
                date = dateFormat.format(varDate);
                System.out.println("Date :" + date);
                AquaMarkSharePreferences.getInstance().setStartDate(date.toUpperCase());
                if (new File(OUTPUT_LOG_FILE_TEMP).exists()) {
                    new File(OUTPUT_LOG_FILE_TEMP).delete();
                    new File(OUTPUT_LOG_FILE_TEMP).createNewFile();
                }
            } catch (Exception e) {
                Log.e("", "" + e.getMessage());
                UtilityClass.appendLogFileByDate(
                        "UIAutomator is start method startMainActivityFromHomeScreen  startDate : "
                                + startDate);
            }

            File yourFile = new File("/sdcard/QosbeeFiles/SendDataToServer.txt");
            try {

                FileInputStream fis = new FileInputStream(new File(String.valueOf(yourFile)));
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String text;
                while ((text = br.readLine()) != null) {
                    sb.append(text).append("\n");
                }
                System.out.println("the file details is " + sb.toString());

                fis.close();
                UtilityClass.appendLogFileByDate(
                        "UIAutomator is start method startMainActivityFromHomeScreen App launch info : "
                                + sb.toString());
                String fileContent = sb.toString();
                Log.d(GlobalVariables.Tag_Name, "Mozark Request file : " + fileContent);
                UtilityClass.appendLogFileByDate(
                        "UIAutomator is start method startMainActivityFromHomeScreen  reading file content : "
                                + fileContent);
                sendDataToUIAutomator =
                        (SendDataToUIAutomator) JsonUtil.toModel(fileContent, SendDataToUIAutomator.class);
                System.out.println(
                        "the file details sendDataToUIAutomator====sendDataToUIAutomator   is "
                                + JsonUtil.toJson(sendDataToUIAutomator));

                if (sendDataToUIAutomator.getDeviceInfoKpis() == null) {
                    sendDataToUIAutomator.setDeviceInfoKpis(new DeviceInfoKpis());
                }

                if (sendDataToUIAutomator.getWifiInfoModel() != null
                        && !sendDataToUIAutomator.getWifiInfoModel().isEmpty()) {
                    sendDataToUIAutomator
                            .getDeviceInfoKpis()
                            .setWifiInfoModels(sendDataToUIAutomator.getWifiInfoModel());
                } else {
                    sendDataToUIAutomator.getDeviceInfoKpis().setWifiInfoModels(null);
                }

                if (sendDataToUIAutomator.getCellInfoModel() != null
                        && !sendDataToUIAutomator.getCellInfoModel().isEmpty()) {
                    sendDataToUIAutomator
                            .getDeviceInfoKpis()
                            .setCellInfoModels(sendDataToUIAutomator.getCellInfoModel());
                } else {
                    sendDataToUIAutomator.getDeviceInfoKpis().setCellInfoModels(null);
                }

                UtilityClass.appendLogFileByDate(
                        "the file details sendDataToUIAutomator====sendDataToUIAutomator   is "
                                + JsonUtil.toJson(sendDataToUIAutomator));
                Utility.clearLogCache();
                if (sendDataToUIAutomator != null) {
                    job_id = sendDataToUIAutomator.getJobid();
                    System.out.println("jobid is " + job_id);
                    device_id = sendDataToUIAutomator.getMobileimei();
                    System.out.println("device_id is " + device_id);
                    ipAdress = sendDataToUIAutomator.getIpadress();
                    System.out.println("ipadress is " + ipAdress);
                    location = sendDataToUIAutomator.getLocation();
                    System.out.println("location is " + location);
                    lat = sendDataToUIAutomator.getLat();
                    System.out.println("lat is " + lat);
                    lng = sendDataToUIAutomator.getLng();
                    System.out.println("lng is " + lng);
                    orderId = sendDataToUIAutomator.getOrder();
                    System.out.println("orderId is " + orderId);
                    scriptId = sendDataToUIAutomator.getScripId();
                    System.out.println("scriptId is " + scriptId);
                    pCap = sendDataToUIAutomator.ispCap();
                    System.out.println("pCap is " + pCap);
                    vCap = sendDataToUIAutomator.isvCap();
                    System.out.println("vCap is " + vCap);
                    isp = sendDataToUIAutomator.getIsp();
                    System.out.println("isp is " + isp);
                    DataHolder.getInstance().setIsp(isp);
                    isFromIntent = sendDataToUIAutomator.isFromIntent();
                    System.out.println("isFromIntent  " + isFromIntent);
                    DataHolder.getInstance().setFromIntent(isFromIntent);

                    if (!sendDataToUIAutomator.getAppname().isEmpty()) {
                        apName = sendDataToUIAutomator.getAppname();
                        System.out.println("appName is " + apName);
                        File source = new File("/sdcard/log.txt");
                        if (source.exists()) {
                            source.delete();
                        }
                        fis.close();

                    } else {
                        UtilityClass.appendLogFileByDate(
                                "UIAutomator is start method startMainActivityFromHomeScreen   ENTER  sendDataToUIAutomator is NULL ");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Utility.cleanBattery();

            if (apName.equalsIgnoreCase("Hotstar")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("Youtube")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);

            }

            if (apName.equalsIgnoreCase("NDTV")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.NDTV_Package);
            }

            if (apName.equalsIgnoreCase("TimesNow")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.TimesNow_Package);
            }

            if (apName.equalsIgnoreCase("IndiaToday")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.IndiaToday_Package);
            }

            if (apName.equalsIgnoreCase("Ola")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Ola_Package);
            }

            if (apName.equalsIgnoreCase("Uber")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Uber_Package);
            }

            if (apName.equalsIgnoreCase("Netflix")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Netflix_Package);
            }

            if (apName.equalsIgnoreCase("WhatsApp")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.WhatsApp_Package);
            }
            if (apName.equalsIgnoreCase("Voot_Voot")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Voot_Package);
            }

            if (apName.equalsIgnoreCase("Voot_Zee5")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Zee5_Package);
            }

            if (apName.equalsIgnoreCase("Voot_Hotstar")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("Sony_SonyLiv")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SonyLiv_Package);
            }

            if (apName.equalsIgnoreCase("Sony_Voot")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Voot_Package);
            }

            if (apName.equalsIgnoreCase("Sony_Hotstar")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("Sony_Zee5")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Zee5_Package);
            }

            if (apName.equalsIgnoreCase("Sony_YouTube")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("AirtelXstream")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.AirtelTV_Package);
            }

            if (apName.equalsIgnoreCase("Zoom")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.ZoomPackage);
            }

            if (apName.equalsIgnoreCase("Webex")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.WebexPackage);
            }

            if (apName.equalsIgnoreCase("Teams")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.TeamsPackage);
            }

            if (apName.equalsIgnoreCase("Disney_Hotstar")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("HotstarLive")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("SonylivLive")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SonyLiv_Package);
            }

            if (apName.equalsIgnoreCase("VootLive")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Voot_Package);
            }

            if (apName.equalsIgnoreCase("YouTubeLive")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("Zee5Live")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Zee5_Package);
            }

            if (apName.equalsIgnoreCase("OCS")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.OSN_Package);
            }

            if (apName.equalsIgnoreCase("Prime")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Prime_Package);
            }

            if (apName.equalsIgnoreCase("GoogleMeet")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoogleMeetPackage);
            }

            if (apName.equalsIgnoreCase("UnionBank")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.UnionBank_Package);
            }

            if (apName.equalsIgnoreCase("SecurityBank")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SecurityBank_Package);
            }

            if (apName.equalsIgnoreCase("BPI")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.BPI_Mobile_Package);
            }

            if (apName.equalsIgnoreCase("GoogleDrive")) {
//                select5Gmark(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoogleDrivePackage);
            }

            if (apName.equalsIgnoreCase("cloudnine")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.CLOUD_NINE_PACKAGE);
            }
            if (apName.equalsIgnoreCase("VcZoom")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.ZoomPackage);
            }

            if (apName.equalsIgnoreCase("VcGoogleMeet")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoogleMeetPackage);
            }

            if (apName.equalsIgnoreCase("VcTeams")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.TeamsPackage);
            }

            if (apName.equalsIgnoreCase("SonyLiv_New")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SonyLiv_Package);
            }

            if (apName.equalsIgnoreCase("VcWebex")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.WebexPackage);
            }

            if (apName.equalsIgnoreCase("VootPremium_Hotstart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Chrome_Package);
            }

            if (apName.equalsIgnoreCase("VootNonPremium_Hotstart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Chrome_Package);
            }

            if (apName.equalsIgnoreCase("VootPremium_Coldstart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Chrome_Package);
            }

            if (apName.equalsIgnoreCase("VootNonPremium_Coldstart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Chrome_Package);
            }

            if (apName.equalsIgnoreCase("PureGold")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.PURE_GOLD_PACKAGE);
            }

            if (apName.equalsIgnoreCase("Qatar_Youtube")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("Qatar_Classifieds")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Qatar_Classifieds_Package);
            }

            if (apName.equalsIgnoreCase("Qatar_Twitter")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Twitter_Package);
            }

            if (apName.equalsIgnoreCase("BlueJeans")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.BlueJeans_Package);
            }

            if (apName.equalsIgnoreCase("GoToMeeting")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoToMeeting_Package);
            }

            if (apName.equalsIgnoreCase("VcBlueJeans")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.BlueJeans_Package);
            }

            if (apName.equalsIgnoreCase("VcGoToMeeting")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoToMeeting_Package);
            }

            if (apName.equalsIgnoreCase("ALTBalaji")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.AltBalaji_Package);
            }

            if (apName.equalsIgnoreCase("Hoichoi")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hoichoi_Package);
            }

            if (apName.equalsIgnoreCase("JioMeet")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.JioMeet_Package);
            }

            if (apName.equalsIgnoreCase("Vc_JioMeet")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.JioMeet_Package);
            }

            if (apName.equalsIgnoreCase("Shahid_VOD_ColdStart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Shahid_Package);
            }

            if (apName.equalsIgnoreCase("SwitchTV_VOD_ColdStart")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SwitchTV_Package);
            }

            if (apName.equalsIgnoreCase("OSN_VOD")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.OSN_Package);
            }

            if (apName.equalsIgnoreCase("VodafoneTV")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.VodafoneTV_Package);
            }

            if (apName.equalsIgnoreCase("MPL")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MPL_Package);
            }

            if (apName.equalsIgnoreCase("DisneyPlus")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.DisneyPlus_Package);
            }

            if (apName.equalsIgnoreCase("Toda_Passenger")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Toda_Passenger_Package);
            }

            if (apName.equalsIgnoreCase("BajajFinserv")) {
                startAppActivity(GlobalVariables.BajajFinserv_Package);
                startScreenRecording(testId, GlobalVariables.screenRecord_Time, sendDataToUIAutomator.isvCap());
                Log.d(GlobalVariables.Tag_Name, "Video Recording Started Successfully");
            }

            if (apName.equalsIgnoreCase("MobileLegends")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                device.executeShellCommand(com.mozark.uiautomatorlibrary.utils.UtilityClass.getScreenshotCommand());
                startAppActivity(GlobalVariables.MobileLegends_Package);
            }

            if (apName.equalsIgnoreCase("Facebook Live")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Facebook_Package);
            }

            if (apName.equalsIgnoreCase("TikTok")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Tiktok_Package);
            }

            if (apName.equalsIgnoreCase("MPL_Tournament_1")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MPL_Package);
            }

            if (apName.equalsIgnoreCase("MPL_Money_1")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MPL_Package);
            }

            if (apName.equalsIgnoreCase("MPL_Fantasy_1")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MPL_Package);
            }

            if (apName.equalsIgnoreCase("BajajFinserv_ETB_1")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.BajajFinserv_Package);
            }

            if (apName.equalsIgnoreCase("BajajFinserv_ETB_2")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.BajajFinserv_Package);
            }

            if (apName.equalsIgnoreCase("HDFCSecurities")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.HDFCSecurities_Package);
            }

            if (apName.equalsIgnoreCase("HDFCSecuritiesBuy")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.HDFCSecurities_Package);
            }

            if (apName.equalsIgnoreCase("HDFCSecuritiesSell")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.HDFCSecurities_Package);
            }

            if (apName.equalsIgnoreCase("AngelBroking")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.AngelBroking_Package);
            }

            if (apName.equalsIgnoreCase("AngelBrokingBuy")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.AngelBroking_Package);
            }

            if (apName.equalsIgnoreCase("AngelBrokingSell")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.AngelBroking_Package);
            }

            if (apName.equalsIgnoreCase("IIFLMarkets")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.IIFLMarkets_Package);
            }

            if (apName.equalsIgnoreCase("IIFLMarketsBuy")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.IIFLMarkets_Package);
            }

            if (apName.equalsIgnoreCase("IIFLMarketsSell")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.IIFLMarkets_Package);
            }

            if (apName.equalsIgnoreCase("UpstoxPro")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.UpstoxPro_Package);
            }

            if (apName.equalsIgnoreCase("UpstoxProBuy")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.UpstoxPro_Package);
            }

            if (apName.equalsIgnoreCase("UpstoxProSell")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.UpstoxPro_Package);
            }

            if (apName.equalsIgnoreCase("Hotstar_Singapore")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.HotstarSingapore_Package);
            }

            if (apName.equalsIgnoreCase("DisneyPlus_USA")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.DisneyPlusUSA_Package);
            }

            if (apName.equalsIgnoreCase("Pulse")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Pulse_Package);
            }

            if (apName.equalsIgnoreCase("YouTubeLive_France")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("YouTubeLiveFrance")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("YouTubeLive_180")) {
//                select5Gmark(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("Netflix_180")) {
//                select5Gmark(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Netflix_Package);
            }

            if (apName.equalsIgnoreCase("Zoom_180")) {
//                select5Gmark(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.ZoomPackage);
            }

            if (apName.equalsIgnoreCase("Hotstar_IPL_Live")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("Hotstar_News_Live")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hotstar_Package);
            }

            if (apName.equalsIgnoreCase("OTV-Prod")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.OTVProd_Package);
            }

            if (apName.equalsIgnoreCase("OTV-PreProd")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.OTVPreProd_Package);
            }

            if (apName.equalsIgnoreCase("MK Ref")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MKRef_Package);
            }

            if (apName.equalsIgnoreCase("Vodafone TV Hu")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.VodafoneTVHungary_Package);
            }

            if (apName.equalsIgnoreCase("Viber")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Viber_Package);
            }

            if (apName.equalsIgnoreCase("Shopee")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Shopee_Package);
            }
            if (apName.equalsIgnoreCase("Lazada")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Lazada_Package);
            }
            if (apName.equalsIgnoreCase("Youtube Globe")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }
            if (apName.equalsIgnoreCase("Telegram")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Telegram_Package);
            }

            if (apName.equalsIgnoreCase("Zoom_India")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.ZoomPackage);
            }

            if (apName.equalsIgnoreCase("CODM")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.CODM_Package);
            }
            if (apName.equalsIgnoreCase("Globe Facebook")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Facebook_Package);
            }
            if (apName.equalsIgnoreCase("Gcash")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GCash_Package);
            }
            if (apName.equalsIgnoreCase("Grab")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Grab_Package);
            }
            if (apName.equalsIgnoreCase("WAZE")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Waze_Package);
            }
            if (apName.equalsIgnoreCase("Gmovies")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GmoviePackage);
            }
            if (apName.equalsIgnoreCase("Spotify")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Spotify_Package);
            }
            if (apName.equalsIgnoreCase("GoogleMap")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GoogleMap_Package);
            }
            if (apName.equalsIgnoreCase("GrabBooking")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Grab_Package);
            }
            if (apName.equalsIgnoreCase("Orange Mali Sugu")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.OrangeMaliPackage);
            }
            if (apName.equalsIgnoreCase("GoogleDrive_Globe")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GDrive_Package);
            }

            if (apName.equalsIgnoreCase("Hangout")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Hangouts_Package);
            }
            if (apName.equalsIgnoreCase("FB Messenger")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Messenger_Package);
            }

            if (apName.equalsIgnoreCase("Kumu")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Kumu_Package);
            }
            if (apName.equalsIgnoreCase("GlobeOne")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GlobeOne_Package);
            }
            if (apName.equalsIgnoreCase("Globe At Home")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GlobeAtHome_Package);
            }

            if (apName.equalsIgnoreCase("Globe_MicrosoftTeams")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MicrosoftTeams_Package);
            }


            if (apName.equalsIgnoreCase("NewGlobeOne")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.NewGlobeOne_Package);
            }
            if (apName.equalsIgnoreCase("Globe_Skype")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Skype_Package);
            }
            if (apName.equalsIgnoreCase("ML_EU")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.MobileLegends_Package);
            }

            if (apName.equalsIgnoreCase("PhonePe")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.PhonePe_Package);
            }

            if (apName.equalsIgnoreCase("GooglePay")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.GPAY_Package);
            }

            if (apName.equalsIgnoreCase("Paytm")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Paytm_Package);
            }

            if (apName.equalsIgnoreCase("Tiktok_Qatar")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.TikTokQatar_Package);
            }

            if (apName.equalsIgnoreCase("Youtube_1")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.YouTube_Package);
            }

            if (apName.equalsIgnoreCase("Nykaa")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Nykaa_Package);
            }

            if (apName.equalsIgnoreCase("Myntra")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Myntra_Package);
            }
            if (apName.equalsIgnoreCase("Instagram")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.InstagramPackage);
            }

            if (apName.equalsIgnoreCase("Snapchat")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.SnapchatPackage);
            }

            if (apName.equalsIgnoreCase("Shahid")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.ShahidPackage);
            }

            if (apName.equalsIgnoreCase("MCDO")) {
                callCellInfo(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                select5GmarkScript(GlobalVariables.fiveGmark, device_id, ipAdress, context, device, JsonUtil.toJson(sendDataToUIAutomator));
                startAppActivity(GlobalVariables.Mcdo_Package);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void useAppContext() {
        try {
            UtilityClass.appendLogFileByDate("=========  useAppContext : Running.....==========");
            Log.d(GlobalVariables.Tag_Name, "useAppContext launch");

            // HOTSTAR
            if (apName.equalsIgnoreCase("Hotstar")) {
//        Utility.clearCache(GlobalVariables.Hotstar_Package);
//        runHotstar();
                runApp(GlobalVariables.Hotstar_Package, new Hotstar(), true);
            }

            // YOUTUBE
            if (apName.equalsIgnoreCase("Youtube")) {
                runApp(GlobalVariables.YouTube_Package, new YouTube(), false);
            }

            // NDTV
            if (apName.equalsIgnoreCase("Ndtv")) {
                runApp(GlobalVariables.NDTV_Package, new Ndtv(), true);
            }

            // TIMESNOW
            if (apName.equalsIgnoreCase("TimesNow")) {
                runApp(GlobalVariables.TimesNow_Package, new TimesNow(), true);
            }

            // INDIA TODAY
            if (apName.equalsIgnoreCase("IndiaToday")) {
                runApp(GlobalVariables.IndiaToday_Package, new IndiaToday(), true);
                //runApp(GlobalVariables.Five_gmarkPro_Package, new Aqua5gmarkPro(), true);
            }

            // OLA
            if (apName.equalsIgnoreCase("Ola")) {
                runApp(GlobalVariables.Ola_Package, new Ola(), true);
            }

            // UBER
            if (apName.equalsIgnoreCase("Uber")) {
                runApp(GlobalVariables.Uber_Package, new Uber(), true);
            }

            // Netflix
            if (apName.equalsIgnoreCase("Netflix")) {
                runApp(GlobalVariables.Netflix_Package, new Netflix(), true);
            }

            // WhatsApp
            if (apName.equalsIgnoreCase("WhatsApp")) {
                runApp(GlobalVariables.WhatsApp_Package, new WhatsApp(), true);
            }

            // VOOT_VOOT
            if (apName.equalsIgnoreCase("Voot_Voot")) {
                runApp(GlobalVariables.Voot_Package, new Voot_Voot(), true);
            }

            // VOOT_Zee5
            if (apName.equalsIgnoreCase("Voot_Zee5")) {
                runApp(GlobalVariables.Zee5_Package, new Voot_Zee5(), true);
            }

            // VOOT_Hotstar
            if (apName.equalsIgnoreCase("Voot_Hotstar")) {
                runApp(GlobalVariables.Hotstar_Package, new Voot_Hotstar(), true);
            }

            // Sony_SonyLiv
            if (apName.equalsIgnoreCase("Sony_SonyLiv")) {
                runApp(GlobalVariables.SonyLiv_Package, new Sony_SonyLiv(), true);
            }

            // Sony_Voot
            if (apName.equalsIgnoreCase("Sony_Voot")) {
                runApp(GlobalVariables.Voot_Package, new Sony_Voot(), true);
            }

            // Sony_Hotstar
            if (apName.equalsIgnoreCase("Sony_Hotstar")) {
                runApp(GlobalVariables.Hotstar_Package, new Sony_Hotstar(), true);
            }

            // Sony_Zee5
            if (apName.equalsIgnoreCase("Sony_Zee5")) {
                runApp(GlobalVariables.Zee5_Package, new Sony_Zee5(), true);
            }

            // Sony_YouTube
            if (apName.equalsIgnoreCase("Sony_YouTube")) {
                runApp(GlobalVariables.YouTube_Package, new Sony_YouTube(), true);
            }

            // Airtel Xstream
            if (apName.equalsIgnoreCase("AirtelXstream")) {
                runApp(GlobalVariables.AirtelTV_Package, new AirtelXstream(), true);
            }

            // Zoom
            if (apName.equalsIgnoreCase("Zoom")) {
                runApp(GlobalVariables.ZoomPackage, new Zoom(), true);
            }

            // Webex
            if (apName.equalsIgnoreCase("Webex")) {
                runApp(GlobalVariables.WebexPackage, new Webex(), true);
            }

            // Teams
            if (apName.equalsIgnoreCase("Teams")) {
                runApp(GlobalVariables.TeamsPackage, new Teams(), true);
            }

            // Disney Hotstar
            if (apName.equalsIgnoreCase("Disney_Hotstar")) {
                runApp(GlobalVariables.Hotstar_Package, new Disney_Hotstar(), true);
            }

            // Hotstar Live
            if (apName.equalsIgnoreCase("HotstarLive")) {
                runApp(GlobalVariables.Hotstar_Package, new HotstarLive(), true);
            }

            // Sonyliv Live
            if (apName.equalsIgnoreCase("SonylivLive")) {
                runApp(GlobalVariables.SonyLiv_Package, new SonylivLive(), true);
            }

            // Voot Live
            if (apName.equalsIgnoreCase("VootLive")) {
                runApp(GlobalVariables.Voot_Package, new VootLive(), true);
            }

            // YouTube Live
            if (apName.equalsIgnoreCase("YouTubeLive")) {
                runApp(GlobalVariables.YouTube_Package, new YouTubeLive(), true);
            }

            // Zee5 Live
            if (apName.equalsIgnoreCase("Zee5Live")) {
                runApp(GlobalVariables.Zee5_Package, new Zee5Live(), true);
            }

            // OCS
            if (apName.equalsIgnoreCase("OCS")) {
                runApp(GlobalVariables.OCS_Package, new OCS(), true);
            }

            // Prime
            if (apName.equalsIgnoreCase("Prime")) {
                runApp(GlobalVariables.Prime_Package, new Prime(), true);
            }

            // GoogleMeet
            if (apName.equalsIgnoreCase("GoogleMeet")) {
                UtilityClass.appendLogFileByDate("useAppContext GoogleMeet : Running ");
                runApp(GlobalVariables.GoogleMeetPackage, new GoogleMeet(), true);
            }

            // Union Bank
            if (apName.equalsIgnoreCase("UnionBank")) {
                runApp(GlobalVariables.UnionBank_Package, new UnionBank(), true);
            }

            // Security Bank
            if (apName.equalsIgnoreCase("SecurityBank")) {
                runApp(GlobalVariables.SecurityBank_Package, new SecurityBank(), true);
            }

            // BPI_Mobile Bank
            if (apName.equalsIgnoreCase("BPI")) {
                runApp(GlobalVariables.BPI_Mobile_Package, new BPI_Mobile(), true);
            }

            // GoogleDrive
            if (apName.equalsIgnoreCase("GoogleDrive")) {
                runApp(GlobalVariables.GoogleDrivePackage, new GoogleDrive(), true);
            }

            // CloudNine
            if (apName.equalsIgnoreCase("cloudnine")) {
                runApp(GlobalVariables.CLOUD_NINE_PACKAGE, new CloudNine(), true);
            }

            // SonyLiv_New Purpal Logo
            if (apName.equalsIgnoreCase("SonyLiv_New")) {
                runApp(GlobalVariables.SonyLiv_Package, new SonyLiv_New(), true);
            }

            // Zoom 3mins
            if (apName.equalsIgnoreCase("VcZoom")) {
                runApp(GlobalVariables.ZoomPackage, new Vc_Zoom(), true);
            }

            // Google Meet 3Mins
            if (apName.equalsIgnoreCase("VcGoogleMeet")) {
                runApp(GlobalVariables.GoogleMeetPackage, new Vc_GoogleMeet(), true);
            }

            // Teams 3Mins
            if (apName.equalsIgnoreCase("VcTeams")) {
                runApp(GlobalVariables.TeamsPackage, new Vc_Teams(), true);
            }

            // Webex 3Mins
            if (apName.equalsIgnoreCase("VcWebex")) {
                runApp(GlobalVariables.WebexPackage, new Vc_Webex(), true);
            }

            // Voot Premium Hotstart
            if (apName.equalsIgnoreCase("VootPremium_Hotstart")) {
                Utility.clearCache(GlobalVariables.Chrome_Package);
//        runVootPremium_Hotstart(GlobalVariables.PremiumEmail, GlobalVariables.PremiumPwd);
            }

            // Voot NonPremium Hotstart
            if (apName.equalsIgnoreCase("VootNonPremium_Hotstart")) {
                Utility.clearCache(GlobalVariables.Chrome_Package);
//        runVootNonPremium_Hotstart(GlobalVariables.NonPremiumEmail, GlobalVariables.NonPremiumPwd);
            }

            // Voot Premium Coldstart
            if (apName.equalsIgnoreCase("VootPremium_Coldstart")) {
                Utility.clearCache(GlobalVariables.Chrome_Package);
//        runVootPremium_Coldstart(GlobalVariables.PremiumEmail, GlobalVariables.PremiumPwd);
            }

            // Voot NonPremium Coldstart
            if (apName.equalsIgnoreCase("VootNonPremium_Coldstart")) {
                Utility.clearCache(GlobalVariables.Chrome_Package);
//        runVootNonPremium_Coldstart(GlobalVariables.NonPremiumEmail, GlobalVariables.NonPremiumPwd);
            }

            // Pure Gold
            if (apName.equalsIgnoreCase("PureGold")) {
                runApp(GlobalVariables.PURE_GOLD_PACKAGE, new PureGold(), true);
            }

            // YouTube Qatar
            if (apName.equalsIgnoreCase("Qatar_Youtube")) {
                runApp(GlobalVariables.YouTube_Package, new YouTube_Qatar(), true);
            }

            // Qatar Classifieds
            if (apName.equalsIgnoreCase("Qatar_Classifieds")) {
                runApp(GlobalVariables.Qatar_Classifieds_Package, new Qatar_Classifieds(), true);
            }

            // Twitter
            if (apName.equalsIgnoreCase("Qatar_Twitter")) {
                runApp(GlobalVariables.Twitter_Package, new Twitter(), true);
            }

            // BlueJeans
            if (apName.equalsIgnoreCase("BlueJeans")) {
                runApp(GlobalVariables.BlueJeans_Package, new BlueJeans(), true);
            }

            // GoToMeeting
            if (apName.equalsIgnoreCase("GoToMeeting")) {
                runApp(GlobalVariables.GoToMeeting_Package, new GoToMeeting(), true);
            }

            // VC_BlueJeans
            if (apName.equalsIgnoreCase("VcBlueJeans")) {
                runApp(GlobalVariables.BlueJeans_Package, new Vc_BlueJeans(), true);
            }

            // VC_GoToMeeting
            if (apName.equalsIgnoreCase("VcGoToMeeting")) {
                runApp(GlobalVariables.GoToMeeting_Package, new Vc_GoToMeeting(), true);
            }

            // ALTBalaji
            if (apName.equalsIgnoreCase("ALTBalaji")) {
                runApp(GlobalVariables.AltBalaji_Package, new ALTBalaji(), true);
            }

            // Hoichoi
            if (apName.equalsIgnoreCase("Hoichoi")) {
                runApp(GlobalVariables.Hoichoi_Package, new Hoichoi(), true);
            }

            // JioMeet
            if (apName.equalsIgnoreCase("JioMeet")) {
                runApp(GlobalVariables.JioMeet_Package, new JioMeet(), true);
            }

            // Vc_JioMeet
            if (apName.equalsIgnoreCase("Vc_JioMeet")) {
                runApp(GlobalVariables.JioMeet_Package, new Vc_JioMeet(), true);
            }

            // Shahid_VOD
            if (apName.equalsIgnoreCase("Shahid_VOD_ColdStart")) {
                runApp(GlobalVariables.Shahid_Package, new Shahid_VOD(), true);
            }

            // SwitchTV_VOD
            if (apName.equalsIgnoreCase("SwitchTV_VOD_ColdStart")) {
                runApp(GlobalVariables.SwitchTV_Package, new SwitchTV_VOD(), true);
            }

            // OSN_VOD
            if (apName.equalsIgnoreCase("OSN_VOD")) {
                runApp(GlobalVariables.OSN_Package, new OSN_VOD(), true);
            }

            // VodafoneTv
            if (apName.equalsIgnoreCase("VodafoneTV")) {
                runApp(GlobalVariables.VodafoneTV_Package, new VodafoneTV(), true);
            }

            // MPL
            if (apName.equalsIgnoreCase("MPL")) {
                runApp(GlobalVariables.MPL_Package, new MPL(), true);
            }

            // DISNEYPLUS
            if (apName.equalsIgnoreCase("DisneyPlus")) {
                runApp(GlobalVariables.DisneyPlus_Package, new DisneyPlus(), true);
            }

            // TODA Passenger App
            if (apName.equalsIgnoreCase("Toda_Passenger")) {
                runApp(GlobalVariables.Toda_Passenger_Package, new Toda_Passenger(), true);
            }

            // Bajaj Finserv
            if (apName.equalsIgnoreCase("BajajFinserv")) {
                runApp(GlobalVariables.BajajFinserv_Package, new BajajFinserv(), true);
            }

            // Mobile Legends
            if (apName.equalsIgnoreCase("MobileLegends")) {
                runApp(GlobalVariables.MobileLegends_Package, new MobileLegends(), true);
            }

            // Facebook
            if (apName.equalsIgnoreCase("Facebook Live")) {
                runApp(GlobalVariables.Facebook_Package, new Facebook(), true);
            }

            // Tiktok
            if (apName.equalsIgnoreCase("TikTok")) {
                runApp(GlobalVariables.Tiktok_Package, new Tiktok(), true);
            }

            // MPL_Tournament_1
            if (apName.equalsIgnoreCase("MPL_Tournament_1")) {
                runApp(GlobalVariables.MPL_Package, new MPL_Tournament_1(), true);
            }

            // MPL_Money_1
            if (apName.equalsIgnoreCase("MPL_Money_1")) {
                runApp(GlobalVariables.MPL_Package, new MPL_Money_1(), true);
            }

            // MPL_Fantasy_1
            if (apName.equalsIgnoreCase("MPL_Fantasy_1")) {
                runApp(GlobalVariables.MPL_Package, new MPL_Fantasy_1(), true);
            }

            // BajajFinserv_ETB_1
            if (apName.equalsIgnoreCase("BajajFinserv_ETB_1")) {
                runApp(GlobalVariables.BajajFinserv_Package, new BajajFinserv_ETB1(), true);
            }

            // BajajFinserv_ETB_2
            if (apName.equalsIgnoreCase("BajajFinserv_ETB_2")) {
                runApp(GlobalVariables.BajajFinserv_Package, new BajajFinserv_ETB2(), true);
            }

            // HDFC Securities
            if (apName.equalsIgnoreCase("HDFCSecurities")) {
                runApp(GlobalVariables.HDFCSecurities_Package, new HDFCSecurities(), true);
            }

            if (apName.equalsIgnoreCase("HDFCSecuritiesBuy")) {
                runApp(GlobalVariables.HDFCSecurities_Package, new HDFCSecuritiesBuy(), true);
            }

            if (apName.equalsIgnoreCase("HDFCSecuritiesSell")) {
                runApp(GlobalVariables.HDFCSecurities_Package, new HDFCSecuritiesSell(), true);
            }

            // Angel Broking
            if (apName.equalsIgnoreCase("AngelBroking")) {
                runApp(GlobalVariables.AngelBroking_Package, new AngelBroking(), true);
            }

            if (apName.equalsIgnoreCase("AngelBrokingBuy")) {
                runApp(GlobalVariables.AngelBroking_Package, new AngelBrokingBuy(), true);
            }

            if (apName.equalsIgnoreCase("AngelBrokingSell")) {
                runApp(GlobalVariables.AngelBroking_Package, new AngelBrokingSell(), true);
            }

            // IIFL Markets
            if (apName.equalsIgnoreCase("IIFLMarkets")) {
                runApp(GlobalVariables.IIFLMarkets_Package, new IIFLMarkets(), true);
            }

            if (apName.equalsIgnoreCase("IIFLMarketsBuy")) {
                runApp(GlobalVariables.IIFLMarkets_Package, new IIFLMarketsBuy(), true);
            }

            if (apName.equalsIgnoreCase("IIFLMarketsSell")) {
                runApp(GlobalVariables.IIFLMarkets_Package, new IIFLMarketsSell(), true);
            }

            // Upstox Pro
            if (apName.equalsIgnoreCase("UpstoxPro")) {
                runApp(GlobalVariables.UpstoxPro_Package, new UpstoxPro(), true);
            }

            if (apName.equalsIgnoreCase("UpstoxProBuy")) {
                runApp(GlobalVariables.UpstoxPro_Package, new UpstoxProBuy(), true);
            }

            if (apName.equalsIgnoreCase("UpstoxProSell")) {
                runApp(GlobalVariables.UpstoxPro_Package, new UpstoxProSell(), true);
            }

            // Hotstar_Singapore
            if (apName.equalsIgnoreCase("Hotstar_Singapore")) {
                runApp(GlobalVariables.HotstarSingapore_Package, new Hotstar_Singapore(), true);
            }

            // DisneyPlus_USA
            if (apName.equalsIgnoreCase("DisneyPlus_USA")) {
                runApp(GlobalVariables.DisneyPlusUSA_Package, new DisneyPlus_USA(), true);
            }

            // Pulse
            if (apName.equalsIgnoreCase("Pulse")) {
                runApp(GlobalVariables.Pulse_Package, new Pulse(), true);
            }

            // YouTubeLive_France
            if (apName.equalsIgnoreCase("YouTubeLive_France")) {
                runApp(GlobalVariables.YouTube_Package, new YouTubeLive_France(), true);
            }

            // YouTubeLive_France For 4 KPI's
            if (apName.equalsIgnoreCase("YouTubeLiveFrance")) {
                runApp(GlobalVariables.YouTube_Package, new YouTubeLiveFrance(), true);
            }

            // YouTubeLive For 180seconds
            if (apName.equalsIgnoreCase("YouTubeLive_180")) {
                runApp(GlobalVariables.YouTube_Package, new YouTubeLive_180(), true);
            }

            // Netflix For 180seconds
            if (apName.equalsIgnoreCase("Netflix_180")) {
                runApp(GlobalVariables.Netflix_Package, new Netflix_180(), true);
            }

            // Zoom For 180seconds
            if (apName.equalsIgnoreCase("Zoom_180")) {
                runApp(GlobalVariables.ZoomPackage, new Zoom_180(), true);
            }

            // Hotstar IPL Live
            if (apName.equalsIgnoreCase("Hotstar_IPL_Live")) {
                runApp(GlobalVariables.Hotstar_Package, new Hotstar_IPL_Live(), true);
            }

            // Hotstar News Live
            if (apName.equalsIgnoreCase("Hotstar_News_Live")) {
                runApp(GlobalVariables.Hotstar_Package, new Hotstar_News_Live(), true);
            }

            //OTV Prod
            if (apName.equalsIgnoreCase("OTV-Prod")) {
                runApp(GlobalVariables.OTVProd_Package, new OTVProd(), true);
            }

            //OTV Pre Prod
            if (apName.equalsIgnoreCase("OTV-PreProd")) {
                runApp(GlobalVariables.OTVPreProd_Package, new OTVPreLid(), true);
            }

            //MK Ref
            if (apName.equalsIgnoreCase("MK Ref")) {
                runApp(GlobalVariables.MKRef_Package, new MKRef(), true);
            }

            // Vodafone TV Hungary
            if (apName.equalsIgnoreCase("Vodafone TV Hu")) {
                runApp(GlobalVariables.VodafoneTVHungary_Package, new VodafoneTVHungary(), true);
            }

            // Viber
            if (apName.equalsIgnoreCase("Viber")) {
                runApp(GlobalVariables.Viber_Package, new Viber(), true);
            }

            //Shoppe
            if (apName.equalsIgnoreCase("Shopee")) {
                runApp(GlobalVariables.Shopee_Package, new Shopee(), true);
            }

            //Lazada
            if (apName.equalsIgnoreCase("Lazada")) {
                runApp(GlobalVariables.Lazada_Package, new Lazada(), true);
            }

            //Globe Youtube
            if (apName.equalsIgnoreCase("Youtube Globe")) {
                runApp(GlobalVariables.YouTube_Package, new Youtube_Globe(), true);
            }

            //Telegram
            if (apName.equalsIgnoreCase("Telegram")) {
                runApp(GlobalVariables.Telegram_Package, new Telegram(), true);
            }

            //Zoom For One Minute India
            if (apName.equalsIgnoreCase("Zoom_India")) {
                runApp(GlobalVariables.ZoomPackage, new Zoom_India(), true);
            }

            // CODM Game App For Globe
            if (apName.equalsIgnoreCase("CODM")) {
                runApp(GlobalVariables.CODM_Package, new CODM(), true);
            }

            //Facebook for Globe
            if (apName.equalsIgnoreCase("Globe Facebook")) {
                runApp(GlobalVariables.Facebook_Package, new Globe_Facebook(), true);
            }

            //Gcash for Globe
            if (apName.equalsIgnoreCase("Gcash")) {
                runApp(GlobalVariables.GCash_Package, new Gcash(), true);
            }

            //Grab for Globe
            if (apName.equalsIgnoreCase("Grab")) {
                runApp(GlobalVariables.Grab_Package, new Grab(), true);
            }

            //Waze for Globe
            if (apName.equalsIgnoreCase("WAZE")) {
                runApp(GlobalVariables.Waze_Package, new Waze(), true);
            }

            //Gmovies for Globe
            if (apName.equalsIgnoreCase("Gmovies")) {
                runApp(GlobalVariables.GmoviePackage, new Gmovies(), true);
            }

            //Spotify for globe
            if (apName.equalsIgnoreCase("Spotify")) {
                runApp(GlobalVariables.Spotify_Package, new Spotify(), true);
            }

            //GoogleMap for globe
            if (apName.equalsIgnoreCase("GoogleMap")) {
                runApp(GlobalVariables.GoogleMap_Package, new GoogleMap(), true);
            }

            //GrabBooking for globe
            if (apName.equalsIgnoreCase("GrabBooking")) {
                runApp(GlobalVariables.Grab_Package, new GrabBooking(), true);
            }

            //OrangeMali
            if (apName.equalsIgnoreCase("Orange Mali Sugu")) {
                runApp(GlobalVariables.OrangeMaliPackage, new OrangeMali(), true);
            }


            // GoogleDrive_Globe
            if (apName.equalsIgnoreCase("GoogleDrive_Globe")) {
                runApp(GlobalVariables.GDrive_Package, new GDrive_Globe(), true);
            }

            //OrangeMali
            if (apName.equalsIgnoreCase("Hangout")) {
                runApp(GlobalVariables.Hangouts_Package, new Hangouts(), true);
            }

            //FB Messenger
            if (apName.equalsIgnoreCase("FB Messenger")) {
                runApp(GlobalVariables.Messenger_Package, new Messenger(), true);
            }


            //Globe Kumu
            if (apName.equalsIgnoreCase("Kumu")) {
                runApp(GlobalVariables.Kumu_Package, new Kumu(), true);
            }

            //Globe Globe One
            if (apName.equalsIgnoreCase("GlobeOne")) {
                runApp(GlobalVariables.GlobeOne_Package, new GlobeOne(), true);
            }
            //Globe   Globe At Home
            if (apName.equalsIgnoreCase("Globe At Home")) {
                runApp(GlobalVariables.GlobeAtHome_Package, new GlobeAtHome(), true);
            }
            //Globe   Globe_MicrosoftTeams
            if (apName.equalsIgnoreCase("Globe_MicrosoftTeams")) {
                runApp(GlobalVariables.MicrosoftTeams_Package, new Globe_MicrosoftTeams(), true);
            }
            //Globe  Globe_Skype
            if (apName.equalsIgnoreCase("Globe_Skype")) {
                runApp(GlobalVariables.Skype_Package, new Globe_Skype(), true);
            }
            //Globe   NewGlobeOne
            if (apName.equalsIgnoreCase("NewGlobeOne")) {
                runApp(GlobalVariables.NewGlobeOne_Package, new NewGlobeOne(), true);
            }
            //  ByTel Mobile Legends for France
            if (apName.equalsIgnoreCase("ML_EU")) {
                runApp(GlobalVariables.MobileLegends_Package, new ML_EU(), true);
            }

            //  Finctech PhonePe
            if (apName.equalsIgnoreCase("PhonePe")) {
                runApp(GlobalVariables.PhonePe_Package, new PhonePe(), true);
            }

            //  Finctech Google Pay
            if (apName.equalsIgnoreCase("GooglePay")) {
                runApp(GlobalVariables.GPAY_Package, new GPay(), true);
            }

            // Fintech Paytm app
            if (apName.equalsIgnoreCase("Paytm")) {
                runApp(GlobalVariables.Paytm_Package, new Paytm(), true);
            }

            // TikTok for Qatar
            if (apName.equalsIgnoreCase("Tiktok_Qatar")) {
                runApp(GlobalVariables.TikTokQatar_Package, new TikTok_Qatar(), true);
            }

            if (apName.equalsIgnoreCase("Youtube_1")) {
                runApp(GlobalVariables.YouTube_Package, new YouTube_1(), true);
            }


            // Nykaa
            if (apName.equalsIgnoreCase("Nykaa")) {
                runApp(GlobalVariables.Nykaa_Package, new Nykaa(), true);
            }

            // Myntra
            if (apName.equalsIgnoreCase("Myntra")) {
                runApp(GlobalVariables.Myntra_Package, new Myntra(), true);
            }

            //Instagram
            if (apName.equalsIgnoreCase("Instagram")) {
                runApp(GlobalVariables.InstagramPackage, new Instagram(), true);
            }

            //Snapchat
            if (apName.equalsIgnoreCase("Snapchat")) {
                runApp(GlobalVariables.SnapchatPackage, new Snapchat(), true);
            }

            //Shahid
            if (apName.equalsIgnoreCase("Shahid")) {
                runApp(GlobalVariables.ShahidPackage, new Shahid(), true);
            }


            //MCDO
            if (apName.equalsIgnoreCase("MCDO")) {
                runApp(GlobalVariables.Mcdo_Package, new Macdo(), true);
            }
            try {
                String cpuData = UtilityClass.readFileResult(new File(OUTPUT_LOG_FILE_TEMP));
                String cpuFormatedData = UtilityClass.parseCPUDetailAsPer(cpuData);
                Utility.appendAllStrToFile(OUTPUT_LOG_FILE, cpuFormatedData);
                String battaryData =
                        UtilityClass.readFileResult(new File(OUTPUT_LOG_FILE_TEMP_BATTARY_STATS));
                Utility.appendAllStrToFile(
                        OUTPUT_LOG_FILE, Utility.getNumOfCores() + "|" + Utility.getTotalRAM() + "\n");
                Utility.appendAllStrToFile(OUTPUT_LOG_FILE, battaryData);
                String result = UtilityClass.removeExtraFiles(OUTPUT_LOG_FILE);
                PrintWriter writer = new PrintWriter(OUTPUT_LOG_FILE);
                writer.print("");
                writer.close();
                Utility.appendAllStrToFile(OUTPUT_LOG_FILE, result);
                Utility.copy((new File(OUTPUT_LOG_FILE)), (new File(OUTPUT_LOG_FILE_TEMP_COPY)));
            } catch (Exception e) {
                UtilityClass.appendLogFileByDate("useAppContext : Running " + e.toString());
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            UtilityClass.appendLogFileByDate("useAppContext : Running " + e.toString());
        }
    }

    public void retrofit(String testId, String appVersion) {
        try {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            NetworkDetails networkDetails = com.mozark.uiautomatorlibrary.utils.UtilityClass.get5gMarkData();

            Retrofit retroclient =
                    new Retrofit.Builder()
                            .baseUrl(ipAdress)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(client)
                            .build();
            SendDataApi service = retroclient.create(SendDataApi.class);
            SendData sendData =
                    new SendData(
                            new Json(
                                    new Request(
                                            new Data(
                                                    job_id,
                                                    device_id,
                                                    testId,
                                                    "Failed" + " - " + DataHolder.getInstance().getFailureReason(),
                                                    false,
                                                    Integer.parseInt(orderId),
                                                    Integer.parseInt(scriptId),
                                                    appVersion,
                                                    apName, com.mozark.uiautomatorlibrary.AquaMarkSharePreferences.getInstance(context).getStartDate()))));

            if (networkDetails != null) {
                String isp = DataHolder.getInstance().getIsp();
                if (isp != null & !isp.isEmpty()) {
                    networkDetails.isp = isp;
                }
                sendData
                        .getJson()
                        .getRequest()
                        .getData()
                        .setNetworkDetails(networkDetails);

            }
            Data data =
                    new Data(
                            job_id,
                            device_id,
                            testId,
                            "Failed" + " - " + DataHolder.getInstance().getFailureReason(),
                            false,
                            Integer.valueOf(orderId),
                            Integer.valueOf(scriptId),
                            appVersion,
                            apName, com.mozark.uiautomatorlibrary.AquaMarkSharePreferences.getInstance(context).getStartDate());
//            if (isFromIntent) {
//                Utility.writeForIntent(data,
//                        Intent_Launching_Application_Result, "Failed" + " - " + DataHolder.getInstance().getFailureReason());
//            }
            UtilityClass.appendLogFileByDate(
                    "UIAutomator SendStatus Request with failed : " + JsonUtil.toJson(sendData));
            Call<GetData> call = service.getStringScalar(sendData);
            Log.d("", "SendStatus -> " + JsonUtil.toJson(sendData));
            call.enqueue(
                    new Callback<GetData>() {
                        @Override
                        public void onResponse(Call<GetData> call, Response<GetData> response) {
                            Log.d("", "SendStatus -> " + JsonUtil.toJson(response.body()));
                            UtilityClass.appendLogFileByDate(
                                    "UIAutomator SendStatus Response : " + JsonUtil.toJson(response.body()));
                            String code = response.body().getJson().getResponse().getStatusCode();
                            if (code.equalsIgnoreCase("0")) {
                                Data data =
                                        new Data(
                                                job_id,
                                                device_id,
                                                testId,
                                                "Failed" + " - " + DataHolder.getInstance().getFailureReason(),
                                                false,
                                                Integer.valueOf(orderId),
                                                Integer.valueOf(scriptId),
                                                appVersion,
                                                apName, com.mozark.uiautomatorlibrary.AquaMarkSharePreferences.getInstance(context).getStartDate());
                                UtilityClass.deleteAqua5gmarkFile();
                                File sourceM = new File("/sdcard/logM.txt");
                                if (sourceM.exists()) {
                                    sourceM.delete();
                                }
                                File file = new File(storedPcapFilePath);
                                if (file.exists()) {
                                    file.delete();
                                }
                                UtilityClass.deleteTestFolderWhichSiFailed(testId);
                            }
                        }

                        @Override
                        public void onFailure(Call<GetData> call, Throwable t) {
                            UtilityClass.appendLogFileByDate(
                                    "UIAutomator SendStatus Response failed : " + JsonUtil.toJson(t.getMessage()));
                        }
                    });

        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Retrofit");
        }
    }

    private String getCommand(String appPackage) {
        return "am force-stop " + appPackage;
    }

    private void startAppActivity(String appPackage) {
        try {
            Log.d(GlobalVariables.Tag_Name, "Launching the App Activity");
            new AquamarkPcap(context, "start", pCap).execute();

            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(appPackage);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            lt = new Timestamp(new Date().getTime());
            Log.d(GlobalVariables.Tag_Name, GlobalVariables.App_Launch_Time + lt);
            device.hasObject(By.pkg(appPackage));
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Launching the App Activity");
            e.printStackTrace();
        }
    }

    private void runApp(String appPackage, AppClass appClass, boolean clearCache) {
        try {

            if (clearCache)
                Utility.clearCache(appPackage);

            String appVersion = com.mozark.uiautomatorlibrary.utils.UtilityClass.getAppVersion(appPackage, context);

            Helper.detectAppVersionName(appPackage);
            int check =
                    appClass.testRun(
                            job_id, device_id, ipAdress, device, lt, orderId, scriptId, location, apName, sendDataToUIAutomator.isvCap(), sendDataToUIAutomator.ispCap(), appVersion, com.mozark.uiautomatorlibrary.AquaMarkSharePreferences.getInstance(context).getStartDate());
            if (check == 1) {

                UtilityClass.deleteTestFolderWhichSiFailed(testId);
                device.executeShellCommand(getCommand(appPackage));
                UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
                updateDeviceStatus.update(device_id, ipAdress, "Available");
                retrofit(ExampleInstrumentedTest.testId, appVersion);
            }
            if (check == 0) {
                UtilityClass.deleteAqua5gmarkFile();
                UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
                updateDeviceStatus.update(device_id, ipAdress, "Available");
                Runtime.getRuntime().exec("logcat -b all -c");
                device.executeShellCommand(getCommand(appPackage));
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in running Apps");
            e.printStackTrace();
            UtilityClass.appendLogFileByDate("useAppContext : Running " + e.toString());
        }
    }

    public static void select5GmarkScript(boolean check, String device_id, String ipAdress, Context context, UiDevice device, String sendDataToUIAutomator) {
        try {
            UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
            updateDeviceStatus.update(device_id, ipAdress, "Running");
            new fiveGMark(context, device).fiveGMarkSpeedTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendBroadcastForCapturing(String testID, String action) {
        // CALL   sendBroadcastForCapturing("testID123","START_FPS")  TimeInterval minimum  60 Seconds
        // action means  "START_FPS"
        // action means  "STOP_FPS"
        final Intent intent = new Intent();
        intent.setAction("com.mozark.trafficstats.intent.TEST");
        intent.putExtra("action", action);
        intent.putExtra("testid", testID);
        intent.setComponent(
                new ComponentName("com.mozark.trafficstats", "com.mozark.trafficstats.NetStatsReceiver"));
        ApplicationProvider.getApplicationContext().sendBroadcast(intent);
    }

    @After
    public void tearDown() throws Exception {
        try {
            //  sendBroadcastForCapturing("","STOP_FPS");
            Utility.copyFpsFile(Utility.fileName(testId));
            UtilityClass.appendLogFileByDate("Tear down of UIAutomator");
            UtilityClass.deleteCellInfoFile();
            if (ExampleInstrumentedTest.IS_ACTIVE_AQUA5GMARK) {
                UpdateDeviceStatus updateDeviceStatus = new UpdateDeviceStatus();
                updateDeviceStatus.update(device_id, ipAdress, "Available");
                File pCapFile = new File(storedPcapFilePath);
                if (pCapFile.exists()) {
                    pCapFile.delete();
                }
                Utility.deleteFileIfxist(Utility.SCRIPT_RUNNING);
                Utility.deleteFileIfxist(Utility.RUNNING_LOG_STATUS);
            }
        } catch (Exception e) {
            Log.d(GlobalVariables.Tag_Name, "Error in Example Tear Down Method");
        }
        Log.d(GlobalVariables.Tag_Name, "tearDown ======================= tearDown ================");
    }
}
