package com.example.testapp;

import android.content.Context;
import android.content.res.AssetManager;
import com.example.testapp.utility.TestApp;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

public class BaseClass implements Serializable {

  private static Properties properties;
  private static Context context;
  private static boolean networkConnected;

  static {
    try {
      setContext();
      setProperties();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void setContext() {
    BaseClass.context = TestApp.getContext();
  }

  public static Properties getProperties() {
    return properties;
  }

  public static void setProperties() {
    try {
      properties = new Properties();
      AssetManager assetManager = context.getAssets();
      InputStream inputStream = assetManager.open("config.properties");
      properties.load(inputStream);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Context getContext() {
    return context;
  }
}
