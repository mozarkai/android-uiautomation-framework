package com.example.testapp.fragments.results;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.testapp.Data;
import com.example.testapp.DataController;
import com.example.testapp.R;
import com.example.testapp.utility.UtilityClass;
import java.util.List;

public class FragmentResults extends Fragment {

  private View mRoot;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    setHasOptionsMenu(true);
    mRoot = inflater.inflate(R.layout.fragment_results, container, false);
    initUI();
    return mRoot;
  }

  private void initUI() {
    initRecyclerView();
  }

  private void initRecyclerView() {
    TestIDSelectListener testIDSelectListener = new TestIDSelectListener() {
      @Override
      public void onClickTestID(Data data) {
        initTestResultUI(data);
      }
    };
    // ...
    // Lookup the recyclerview in activity layout
    RecyclerView rvResults = mRoot.findViewById(R.id.rv_test_history);
    // Initialize Results
    List<Data> dataList;
    if (DataController.getInstance().getDataList() == null || DataController.getInstance()
        .getDataList().isEmpty()) {
      dataList = UtilityClass.readResult();
      DataController.getInstance().setDataList(dataList);
    } else {
      dataList = DataController.getInstance().getDataList();
    }
    // Create adapter passing in the sample user data
    ResultsAdapter adapter = new ResultsAdapter(getActivity(), dataList, testIDSelectListener);
    // Attach the adapter to the recyclerview to populate items
    rvResults.setAdapter(adapter);
    // Set layout manager to position the items
    rvResults.setLayoutManager(new LinearLayoutManager(getActivity()));
  }


  private void initTestResultUI(Data data) {
    ((TextView) mRoot.findViewById(R.id.tv_date_time)).setText("Time : " + data.getDateTime());
    ((TextView) mRoot.findViewById(R.id.tv_test_id)).setText("Test ID : " + data.getTestID());
    ((TextView) mRoot.findViewById(R.id.tv_app_name)).setText("App : " + data.getAppName());
    ((TextView) mRoot.findViewById(R.id.tv_location)).setText("Location : " + data.getLocation());
    ((TextView) mRoot.findViewById(R.id.tv_app_start_time_val))
        .setText("" + data.getHomepageloadtime());
    ((TextView) mRoot.findViewById(R.id.tv_time_to_load_homepage_val))
        .setText("" + data.getHomepageloadtime());
    ((TextView) mRoot.findViewById(R.id.tv_play_start_time_video))
        .setText("" + data.getPlaystarttime());
    ((TextView) mRoot.findViewById(R.id.tv_buffer_interruption_count))
        .setText("" + data.getBuffercount());
    ((TextView) mRoot.findViewById(R.id.tv_re_buffer_ratio))
        .setText("" + data.getBufferpercentage());
    ((TextView) mRoot.findViewById(R.id.tv_pcap)).setText("PCap : NO");
    ((TextView) mRoot.findViewById(R.id.tv_vcap)).setText("VCap : NO");
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_chats, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_chat) {
      Toast.makeText(getActivity(), "Clicked on " + item.getTitle(), Toast.LENGTH_SHORT)
          .show();
    }
    return true;
  }
}

