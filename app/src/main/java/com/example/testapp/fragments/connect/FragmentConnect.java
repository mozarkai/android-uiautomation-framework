package com.example.testapp.fragments.connect;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.example.testapp.R;

public class FragmentConnect extends Fragment {

  RadioGroup portal;
  RadioButton portal_radio_selection;
  Button confirm, connect;
  TextView portal_selection, change, portalValue;
  RelativeLayout testRelative, changePortal, relative_disconnect;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_connect, container, false);
    portal = view.findViewById(R.id.radioPortal);
    confirm = view.findViewById(R.id.confirm);
    portal_selection = view.findViewById(R.id.portalSelection);
    testRelative = view.findViewById(R.id.testRelative_connect);
    changePortal = view.findViewById(R.id.changePortal);
    change = view.findViewById(R.id.change);
    connect = view.findViewById(R.id.connect);
    relative_disconnect = view.findViewById(R.id.relative_disconnect);
    portalValue = view.findViewById(R.id.portalValue);

    connect.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        relative_disconnect.setVisibility(View.VISIBLE);
        testRelative.setVisibility(View.GONE);
        portalValue.setText(portal_selection.getText());


      }
    });
    change.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        changePortal.setVisibility(View.VISIBLE);
        testRelative.setVisibility(View.GONE);
//
      }
    });

    confirm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int selectedId = portal.getCheckedRadioButtonId();
        portal_radio_selection = view.findViewById(selectedId);
        portal_selection.setText(portal_radio_selection.getText());
        testRelative.setVisibility(View.VISIBLE);
        changePortal.setVisibility(View.GONE);
      }
    });

    return view;
  }

}
