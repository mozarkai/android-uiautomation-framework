package com.example.testapp.fragments.results;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.testapp.Data;
import com.example.testapp.R;
import java.util.ArrayList;
import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ViewHolder> {

  List<Data> dataList = new ArrayList<>();
  Context context;
  TestIDSelectListener testIDSelectListener;

  public ResultsAdapter(Context context, List<Data> dataList,
      TestIDSelectListener testIDSelectListener) {
    this.context = context;
    this.dataList = dataList;
    this.testIDSelectListener = testIDSelectListener;
    if (this.dataList != null && !this.dataList.isEmpty()) {
      this.dataList.get(0).setSelect(true);
      testIDSelectListener.onClickTestID(this.dataList.get(0));
    }

  }

  // ... constructor and member variables

  // Usually involves inflating a layout from XML and returning the holder
  @Override
  public ResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);

    // Inflate the custom layout
    View contactView = inflater.inflate(R.layout.item_result_row, parent, false);
    // Return a new holder instance
    ViewHolder viewHolder = new ViewHolder(contactView);
    return viewHolder;
  }

  // Involves populating data into the item through holder
  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(ResultsAdapter.ViewHolder viewHolder, int position) {
    // Get the data model based on position
    Data data = dataList.get(position);
    // Set item views based on your views and data model
    viewHolder.test_id.setText("" + data.getTestID());
    String date = "NA";
    try {
      date = data.getDateTime().substring(0, 10);
    } catch (Exception e) {
      e.printStackTrace();
    }
    viewHolder.test_date.setText("" + date);
    viewHolder.test_script
        .setText("" + data.getPackageName().substring(data.getPackageName().lastIndexOf(".") + 1));
    viewHolder.test_status.setText("" + data.getStatus());
    viewHolder.test_id.setChecked(data.isSelect());
    viewHolder.test_id.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        for (Data data1 : dataList) {
          data1.setSelect(false);
        }
        dataList.get(position).setSelect(true);
        testIDSelectListener.onClickTestID(data);
        notifyDataSetChanged();
      }
    });
  }

  // Returns the total count of items in the list
  @Override
  public int getItemCount() {
    return dataList.size();
  }

  // Provide a direct reference to each of the views within a data item
  // Used to cache the views within the item layout for fast access
  public class ViewHolder extends RecyclerView.ViewHolder {

    // Your holder should contain a member variable
    // for any view that will be set as you render a row
    public RadioButton test_id;
    public TextView test_date;
    public TextView test_script;
    public TextView test_status;


    // We also create a constructor that accepts the entire item row
    // and does the view lookups to find each subview
    public ViewHolder(View itemView) {
      // Stores the itemView in a public final member variable that can be used
      // to access the context from any ViewHolder instance.
      super(itemView);
      test_id = (RadioButton) itemView.findViewById(R.id.test_id);
      test_date = (TextView) itemView.findViewById(R.id.test_date);
      test_script = (TextView) itemView.findViewById(R.id.test_script);
      test_status = (TextView) itemView.findViewById(R.id.test_status);
    }
  }
}