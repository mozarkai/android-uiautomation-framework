package com.example.testapp.fragments.test;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import com.example.testapp.AquaMarkSharePreferences;
import com.example.testapp.R;
import com.example.testapp.api_interface.JobIdGeneration;
import com.example.testapp.api_interface.Retroclient;
import com.example.testapp.models.job_id_generation.job_id_request.Data;
import com.example.testapp.models.job_id_generation.job_id_request.GenerateJobId;
import com.example.testapp.models.job_id_generation.job_id_request.Json;
import com.example.testapp.models.job_id_generation.job_id_request.Request;
import com.example.testapp.models.job_id_generation.job_id_response.GenrateJobIdResponse;
import com.example.testapp.utility.ExecuteCommand;
import com.example.testapp.utility.SharePrefrancClass;
import com.example.testapp.utility.TestApp;
import com.example.testapp.utility.UtilityClass;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTest extends Fragment {

  String appName;
  SharedPreferences sharedpreferences;
  String deviceId;
  Properties properties;
  TextView textmanually;
  Button start, abort, pcap, pcapstart;
  RelativeLayout relativeLayoutAbort, relativeLayoutNote, testRelative, pcaprelative, pcapstarted_relative;
  boolean flag_pcap = false;

  @RequiresApi(api = Build.VERSION_CODES.M)
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_test, container, false);
    final Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
    start = view.findViewById(R.id.button);
    abort = view.findViewById(R.id.Abort);
    relativeLayoutAbort = view.findViewById(R.id.relative_abort);
    relativeLayoutNote = view.findViewById(R.id.relative_note);
    pcap = view.findViewById(R.id.pcap);
    testRelative = view.findViewById(R.id.testRelative);
    pcaprelative = view.findViewById(R.id.pcaprelative);
    pcapstart = view.findViewById(R.id.pcapstart);
    textmanually = view.findViewById(R.id.textmanually);
    pcapstarted_relative = view.findViewById(R.id.pcapstarted_relative);

    // Spinner click listene

    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      public void onItemSelected(AdapterView<?> parent, View view,
          int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    // Spinner Drop down elements
    List<String> categories = new ArrayList<String>();
    categories.add("Hotstar");
    categories.add("Youtube");
    categories.add("Ndtv");
    categories.add("India Today");
    categories.add("Times Now");
    setProperties();

    // Creating adapter for spinner
    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(TestApp.getContext(),
        android.R.layout.simple_spinner_item, categories);

    // Drop down layout style - list view with radio button
    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    // attaching data adapter to spinner
    spinner.setAdapter(dataAdapter);

    TelephonyManager telephonyManager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    //deviceId = telephonyManager.getDeviceId();

    SharePrefrancClass.getInstance(TestApp.getContext()).savePref("deviceId", deviceId);

    start.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        appName = spinner.getSelectedItem().toString();

        retrofit(appName, AquaMarkSharePreferences.getInstance().getDeviceId());
//                start.setVisibility(View.GONE);
//                relativeLayoutNote.setVisibility(View.GONE);
//                relativeLayoutAbort.setVisibility(View.VISIBLE);

      }
    });

    abort.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        //  retrofit(appName,deviceId);
        start.setVisibility(View.VISIBLE);
        relativeLayoutNote.setVisibility(View.VISIBLE);
        relativeLayoutAbort.setVisibility(View.GONE);


      }
    });

    pcap.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (!flag_pcap) {
          testRelative.setVisibility(View.GONE);
          pcaprelative.setVisibility(View.VISIBLE);
          pcap.setBackgroundResource(R.drawable.button_background_clicked);
          clickStartPcap();
          flag_pcap = true;

        } else {
          testRelative.setVisibility(View.VISIBLE);
          pcaprelative.setVisibility(View.GONE);
          pcap.setBackgroundResource(R.drawable.button_background);

          flag_pcap = false;
        }


      }
    });

    return view;


  }

  public void retrofit(String appName, String deviceId) {
    Retroclient retroclient = new Retroclient();
    JobIdGeneration service = retroclient.getClient().create(JobIdGeneration.class);
    Call<GenrateJobIdResponse> call = service
        .getJobId(new GenerateJobId(new Json(new Request(new Data(appName, deviceId)))));
    call.enqueue(new Callback<GenrateJobIdResponse>() {
      @Override
      public void onResponse(Call<GenrateJobIdResponse> call,
          Response<GenrateJobIdResponse> response) {
        GenrateJobIdResponse entity = response.body();
        String code = entity.getJson().getResponse().getStatusCode();
        String msg = entity.getJson().getResponse().getStatusMessage();
        if (code.equalsIgnoreCase(properties.getProperty("success_status_code"))) {
          String jobId = entity.getJson().getResponse().getData().getJobId();
          boolean pcap = entity.getJson().getResponse().getData().getCaptureStatus()
              .isPcapCapture();
          boolean vcap = entity.getJson().getResponse().getData().getCaptureStatus()
              .isScreenCapture();

          UtilityClass.SendDataToTestApp(TestApp.getContext(), Integer.parseInt(jobId),
              AquaMarkSharePreferences.getInstance().getDeviceId(), appName,
              properties.getProperty("baseUrl"), pcap, vcap);

          new ExecuteCommand(getActivity(), "start_script").execute();


        } else {
          String message = "Oops,the job id is not assign";
          showDialog(msg);

        }
      }

      @Override
      public void onFailure(Call<GenrateJobIdResponse> call, Throwable t) {

      }
    });
  }

  private void setProperties() {
    try {
      properties = new Properties();
      AssetManager assetManager = TestApp.getContext().getAssets();
      InputStream inputStream = assetManager.open("config.properties");
      properties.load(inputStream);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void showDialog(String message) {
    try {

      final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(
          getActivity());
      final android.app.AlertDialog alertDialog = builder.create();

      LayoutInflater inflater = (LayoutInflater) TestApp.getContext()
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View view = inflater.inflate(R.layout.custom_dialog, null);
      TextView custom_message = (TextView) view.findViewById(R.id.custom_message);

      Button custom_ok = (Button) view.findViewById(R.id.btnNo);
      //custom_title.setText(title);
      custom_message.setText(message);
      custom_ok.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          alertDialog.dismiss();


        }
      });

      alertDialog.setView(view);
      alertDialog.show();
      alertDialog.setCancelable(false);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public void clickStartPcap() {
    pcapstart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        pcapstart.setVisibility(View.GONE);
        textmanually.setVisibility(View.GONE);
        pcapstarted_relative.setVisibility(View.VISIBLE);

      }
    });


  }


}
