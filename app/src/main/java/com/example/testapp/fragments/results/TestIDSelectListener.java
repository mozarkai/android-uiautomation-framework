package com.example.testapp.fragments.results;

import com.example.testapp.Data;

public interface TestIDSelectListener {

  void onClickTestID(Data data);
}
