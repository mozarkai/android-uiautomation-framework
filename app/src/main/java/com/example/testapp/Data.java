package com.example.testapp;

import java.util.Comparator;

public class Data {

  public static Comparator<Data> comparator1 = (o1, o2) -> {
    try {
      return (int) (o2.TimeStamp - o1.TimeStamp);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 1;
  };
  private int jobid;
  private int buffercount;
  private double homepageloadtime;
  private double bufferpercentage;
  private int latency;
  private double networkspeed;
  private String appversion;
  private String deviceid;
  private double playstarttime;
  private String status;
  private String packageName;
  private String ipadress;
  private String DateTime = "";
  private long TimeStamp = System.currentTimeMillis();
  private String TestID = "";
  private String appName = "";
  private String location = "";
  private boolean select = false;

  public Data(int jobid, int buffercount, double homepageloadtime, double bufferpercentage,
      int latency, double networkspeed, String appversion, String deviceid, double playstarttime,
      String status, String packageName) {
    this.jobid = jobid;
    this.buffercount = buffercount;
    this.homepageloadtime = homepageloadtime;
    this.bufferpercentage = bufferpercentage;
    this.latency = latency;
    this.networkspeed = networkspeed;
    this.appversion = appversion;
    this.deviceid = deviceid;
    this.playstarttime = playstarttime;
    this.status = status;
    this.packageName = packageName;
  }

  public Data(int jobid, int buffercount, double homepageloadtime, double bufferpercentage,
      int latency, double networkspeed, String appversion, String deviceid, double playstarttime,
      String status, String packageName, String ipAddress) {
    this.jobid = jobid;
    this.buffercount = buffercount;
    this.homepageloadtime = homepageloadtime;
    this.bufferpercentage = bufferpercentage;
    this.latency = latency;
    this.networkspeed = networkspeed;
    this.appversion = appversion;
    this.deviceid = deviceid;
    this.playstarttime = playstarttime;
    this.status = status;
    this.packageName = packageName;
    this.ipadress = ipAddress;
    this.TestID = jobid + (("" + TimeStamp).substring(("" + TimeStamp).length() / 3));
  }

  public Data(int jobid, String deviceid, String status) {
    this.jobid = jobid;
    this.deviceid = deviceid;
    this.status = status;
  }

  public int getJobid() {
    return jobid;
  }

  public void setJobid(int jobid) {
    this.jobid = jobid;
  }

  public int getBuffercount() {
    return buffercount;
  }

  public void setBuffercount(int buffercount) {
    this.buffercount = buffercount;
  }

  public double getHomepageloadtime() {
    return homepageloadtime;
  }

  public void setHomepageloadtime(double homepageloadtime) {
    this.homepageloadtime = homepageloadtime;
  }

  public double getBufferpercentage() {
    return bufferpercentage;
  }

  public void setBufferpercentage(double bufferpercentage) {
    this.bufferpercentage = bufferpercentage;
  }

  public int getLatency() {
    return latency;
  }

  public void setLatency(int latency) {
    this.latency = latency;
  }

  public double getNetworkspeed() {
    return networkspeed;
  }

  public void setNetworkspeed(double networkspeed) {
    this.networkspeed = networkspeed;
  }

  public String getAppversion() {
    return appversion;
  }

  public void setAppversion(String appversion) {
    this.appversion = appversion;
  }

  public String getDeviceid() {
    return deviceid;
  }

  public void setDeviceid(String deviceid) {
    this.deviceid = deviceid;
  }

  public double getPlaystarttime() {
    return playstarttime;
  }

  public void setPlaystarttime(double playstarttime) {
    this.playstarttime = playstarttime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public String getIpadress() {
    return ipadress;
  }

  public void setIpadress(String ipadress) {
    this.ipadress = ipadress;
  }

  public String getDateTime() {
    if (DateTime == null) {

    }
    return DateTime;
  }

  public void setDateTime(String dateTime) {
    DateTime = dateTime;
  }

  public long getTimeStamp() {
    return TimeStamp;
  }

  public void setTimeStamp(long timeStamp) {
    TimeStamp = timeStamp;
  }

  public String getTestID() {
    if (TestID == null) {
      TestID = jobid + (("" + TimeStamp).substring(("" + TimeStamp).length() / 3));
    }
    return TestID;
  }

  public void setTestID(String testID) {
    TestID = testID;
  }

  public boolean isSelect() {
    return select;
  }

  public void setSelect(boolean select) {
    this.select = select;
  }

  public String getAppName() {
    if (appName == null) {
      appName = packageName.substring(packageName.lastIndexOf(".") + 1);
    }
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getLocation() {
    if (location == null) {
      location = "Gurgaon";
    }
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public String toString() {
    return "\n\n" +
        "\nDate Time          :  " + DateTime +
        "\nJob ID             :  " + jobid +
        "\nBuffer Count       :  " + buffercount +
        "\nHome Page Load Time:  " + homepageloadtime +
        "\nBuffer Percentage  :  " + bufferpercentage +
        "\nLatency            :  " + latency +
        "\nNetwork Speed      :  " + networkspeed +
        "\nApp Version        :  " + appversion +
        "\nDevice ID          :  " + deviceid +
        "\nPlay Start Time    :  " + playstarttime +
        "\nStatus             :  " + status +
        "\nPackage Name       :  " + packageName +
        "\nTest ID            :  " + TestID +
        "\nIp Adress          :  " + ipadress + "\n\n\n";
  }
}

