package com.example.testapp.location;

import static com.example.testapp.activity.MainActivity.isShowingSetting;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import com.example.testapp.R;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationTracker extends Service implements LocationListener {

  // The minimum distance to change updates in meters
  private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
  // The minimum time between updates in milliseconds
  private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
  // Get Class Name
  private static String TAG = LocationTracker.class.getName();
  public double latitude;
  public double longitude;
  // Declaring a Location Manager
  protected LocationManager locationManager;
  String locality = null;
  // flag for GPS Status
  boolean isGPSEnabled = false;
  // flag for network status
  boolean isNetworkEnabled = false;
  // flag for GPS Tracking is enabled
  boolean isGPSTrackingEnabled = false;
  Location location;
  Activity activity;
  // How many Geocoder should return our GPSTracker
  int geocoderMaxResults = 1;
  private Context mContext;
  // Store LocationManager.GPS_PROVIDER or LocationManager.NETWORK_PROVIDER information
  private String provider_info;

  public LocationTracker() {

  }

  public LocationTracker(Context context) {
    this.mContext = context;
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        try {
          getLocation();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

  }

  /**
   * Try to get my current location by GPS or Network Provider
   */
  @SuppressLint("MissingPermission")
  public void getLocation() {

    try {
      locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

      //getting GPS status
      isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

      //getting network status
      isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

      System.out.println("isGPSEnabled " + isGPSEnabled);
      // Try to get location if you GPS Service is enabled
      if (isGPSEnabled) {
        this.isGPSTrackingEnabled = true;

        Log.d(TAG, "Application use GPS Service");

        /*
         * This provider determines location using
         * satellites. Depending on conditions, this provider may take a while to return
         * a location fix.
         */

        provider_info = LocationManager.GPS_PROVIDER;

      } else if (isNetworkEnabled) { // Try to get location if you Network Service is enabled
        this.isGPSTrackingEnabled = true;

        Log.d(TAG, "Application use Network State to get GPS coordinates");

        /*
         * This provider determines location based on
         * availability of cell tower and WiFi access points. Results are retrieved
         * by means of a network lookup.
         */
        provider_info = LocationManager.NETWORK_PROVIDER;

      }

      // Application can use GPS or Network Provider
      if (provider_info != null && !provider_info.isEmpty()) {
//                locationManager.requestLocationUpdates(provider, 5000, 0, mylistener, Looper.getMainLooper());
        locationManager.requestLocationUpdates(
            provider_info,
            MIN_TIME_BW_UPDATES,
            MIN_DISTANCE_CHANGE_FOR_UPDATES,
            this
            ,
            Looper.getMainLooper()
        );

        if (locationManager != null) {
          location = locationManager.getLastKnownLocation(provider_info);
          updateGPSCoordinates();
        }
      }
    } catch (Exception e) {
      //e.printStackTrace();
      Log.e(TAG, "Impossible to connect to LocationManager", e);
    }
  }

  /**
   * Update GPSTracker latitude and longitude
   */
  public void updateGPSCoordinates() {
    if (location != null) {
      latitude = location.getLatitude();
      longitude = location.getLongitude();
    }
  }

  /**
   * GPSTracker latitude getter and setter
   *
   * @return latitude
   */
  public double getLatitude() {
    if (location != null) {
      latitude = location.getLatitude();
    }

    return latitude;
  }

  /**
   * GPSTracker longitude getter and setter
   *
   * @return
   */
  public double getLongitude() {
    if (location != null) {
      longitude = location.getLongitude();
    }

    return longitude;
  }

  /**
   * GPSTracker isGPSTrackingEnabled getter. Check GPS/wifi is enabled
   */
  public boolean getIsGPSTrackingEnabled() {

    return this.isGPSTrackingEnabled;
  }

  /**
   * Stop using GPS listener Calling this method will stop using GPS in your app
   */
  public void stopUsingGPS() {
    if (locationManager != null) {
      locationManager.removeUpdates(LocationTracker.this);
    }
  }

  /**
   * Function to show settings alert dialog
   */

  public void showSettingsAlert() {
    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

    //Setting Dialog Title
    alertDialog.setTitle(R.string.GPSAlertDialogTitle);

    //Setting Dialog Message
    alertDialog.setMessage(R.string.GPSAlertDialogMessage);

    //On Pressing Setting button
    alertDialog.setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mContext.startActivity(intent);
      }
    });

    //On pressing cancel button
    alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        dialog.cancel();
        isShowingSetting = false;
      }
    });

    alertDialog.show();
  }

  /**
   * Get list of address by latitude and longitude
   *
   * @return null or List<Address>
   */
  public List<Address> getGeocoderAddress(Context context) {
    if (location != null) {

      Geocoder geocoder = new Geocoder(context, Locale.getDefault());

      try {
        /**
         * Geocoder.getFromLocation - Returns an array of Addresses
         * that are known to describe the area immediately surrounding the given latitude and longitude.
         */
        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

        return addresses;
      } catch (IOException e) {
        //e.printStackTrace();
        Log.e(TAG, "Impossible to connect to Geocoder", e);
      }
    }

    return null;
  }

  /**
   * Try to get AddressLine
   *
   * @return null or addressLine
   */
  public String getAddressLine(Context context) {
    List<Address> addresses = getGeocoderAddress(context);

    if (addresses != null && addresses.size() > 0) {
      Address address = addresses.get(0);
      String addressLine = address.getAddressLine(0);

      return addressLine;
    } else {
      return null;
    }
  }

  /**
   * Try to get Locality
   *
   * @return null or locality
   */
  public String getLocality(Context context) {
    List<Address> addresses = getGeocoderAddress(context);

    if (addresses != null && addresses.size() > 0) {
      Address address = addresses.get(0);
      String locality = address.getLocality();

      return locality;
    } else {
      return null;
    }
  }

  /**
   * Try to get Postal Code
   *
   * @return null or postalCode
   */
  public String getPostalCode(Context context) {
    List<Address> addresses = getGeocoderAddress(context);

    if (addresses != null && addresses.size() > 0) {
      Address address = addresses.get(0);
      String postalCode = address.getPostalCode();

      return postalCode;
    } else {
      return null;
    }
  }

  /**
   * Try to get CountryName
   *
   * @return null or postalCode
   */
  public String getCountryName(Context context) {
    List<Address> addresses = getGeocoderAddress(context);
    if (addresses != null && addresses.size() > 0) {
      Address address = addresses.get(0);
      String countryName = address.getCountryName();

      return countryName;
    } else {
      return null;
    }
  }

  @Override
  public void onLocationChanged(Location location) {
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
  }

  @Override
  public void onProviderEnabled(String provider) {
  }

  @Override
  public void onProviderDisabled(String provider) {
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }





        /*Context context;
        boolean isGPSEnabled = false;
        // flag for network status
        boolean isNetworkEnabled = false;
        // flag for GPS status
        boolean canGetLocation = false;
        Location location; // location
        double latitude; // latitude
        double longitude; // longitude
        // The minimum distance to change Updates in meters
        private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
        // The minimum time between updates in milliseconds
        private static final long MIN_TIME_BW_UPDATES = 200 * 10 * 1; // 2 seconds
        // Declaring a Location Manager
        protected LocationManager locationManager;
        public LocationTracker(Context context) {
            this.context = context;
            getLocation();
        }
        @Override public void onLocationChanged(Location location) {

        }

        @Override public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        @Override public void onProviderEnabled(String provider) {
        }
        @Override public void onProviderDisabled(String provider) {
        }
        @Nullable
        @Override public IBinder onBind(Intent intent) {
            return null;
        }
        @SuppressLint("MissingPermission")
        public Location getLocation() {
            try {
                locationManager = (LocationManager) context
                        .getSystemService(LOCATION_SERVICE);
// getting GPS status
                isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
// getting network status
                isNetworkEnabled = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                    // Log.e(“Network-GPS”, “Disable”);
                } else {
                    this.canGetLocation = true;
                    // First get location from Network Provider
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        // Log.e(“Network”, “Network”);
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    } else
                        // if GPS Enabled get lat/long using GPS Services
                        if (isGPSEnabled) {
                            if (location == null) {
                                locationManager.requestLocationUpdates(
                                        LocationManager.GPS_PROVIDER,
                                        MIN_TIME_BW_UPDATES,
                                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                                //Log.e(“GPS Enabled”, “GPS Enabled”);
                                if (locationManager != null) {
                                    location = locationManager
                                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (location != null) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                    }
                                }
                            }
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return location;
        }
        public double getLatitude() {
            if (location != null) {
                latitude = location.getLatitude();
            }
            return latitude;
        }
        public double getLongitude() {
            if (location != null) {
                longitude = location.getLongitude();
            }
            return longitude;
        }
        public boolean canGetLocation() {
            return this.canGetLocation;
        }
        public void showSettingsAlert() {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle("GPS settings");
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }*/

  public String location(final Context context1, final double latitude, final double longitude) {
    Thread thread = new Thread() {
      @Override
      public void run() {
        Geocoder geocoder = new Geocoder(context1, Locale.getDefault());
        try {
          List<Address> addressList = geocoder.getFromLocation(
              latitude, longitude, 1);
          if (addressList != null && addressList.size() > 0) {
            Address address = addressList.get(0);
            locality = address.getLocality();

          }
        } catch (IOException e) {
          Log.e(TAG, "Unable connect to Geocoder", e);
        } finally {
                        /*Message message = Message.obtain();
                        message.setTarget(handler);
                        if (result != null) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n\nAddress:\n" + result;
                            bundle.putString("address", result);
                            message.setData(bundle);
                        } else {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n Unable to get address for this lat-long.";
                            bundle.putString("address", result);
                            message.setData(bundle);
                        }
                        message.sendToTarget();*/
        }
      }
    };
    thread.start();

    return locality;
  }
}
