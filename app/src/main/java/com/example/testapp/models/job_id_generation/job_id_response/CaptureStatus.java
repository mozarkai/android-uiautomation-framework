package com.example.testapp.models.job_id_generation.job_id_response;

public class CaptureStatus {

  private boolean screenCapture;

  private boolean pcapCapture;

  public CaptureStatus(boolean screenCapture, boolean pcapCapture) {
    this.screenCapture = screenCapture;
    this.pcapCapture = pcapCapture;
  }

  public boolean isScreenCapture() {
    return screenCapture;
  }

  public void setScreenCapture(boolean screenCapture) {
    this.screenCapture = screenCapture;
  }

  public boolean isPcapCapture() {
    return pcapCapture;
  }

  public void setPcapCapture(boolean pcapCapture) {
    this.pcapCapture = pcapCapture;
  }

  @Override
  public String toString() {
    return "ClassPojo [screenCapture = " + screenCapture + ", pcapCapture = " + pcapCapture + "]";
  }
}

