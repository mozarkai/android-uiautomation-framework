package com.example.testapp.models.job_id_generation.job_id_request;

public class GenerateJobId {

  private Json json;

  public GenerateJobId(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
