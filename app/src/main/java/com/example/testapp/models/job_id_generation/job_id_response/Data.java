package com.example.testapp.models.job_id_generation.job_id_response;

public class Data {

  private String jobId;
  private int port;
  private String status;

  private CaptureStatus captureStatus;

  public Data(String jobId, CaptureStatus captureStatus) {
    this.jobId = jobId;
    this.captureStatus = captureStatus;
  }

  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }

  public CaptureStatus getCaptureStatus() {
    return captureStatus;
  }

  public void setCaptureStatus(CaptureStatus captureStatus) {
    this.captureStatus = captureStatus;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "ClassPojo [jobId = " + jobId + ", captureStatus = " + captureStatus + "]";
  }
}
