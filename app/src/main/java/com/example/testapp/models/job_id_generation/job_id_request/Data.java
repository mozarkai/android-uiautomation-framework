package com.example.testapp.models.job_id_generation.job_id_request;


public class Data {

  private String appName;

  private String deviceid;

  private String status;

  private String city;
  private Boolean inOffice;

  public Data(String status, String city, Boolean inOffice) {
    this.status = status;
    this.city = city;
    this.inOffice = inOffice;
  }

  public Data(String appName, String deviceid) {
    this.appName = appName;
    this.deviceid = deviceid;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getDeviceid() {
    return deviceid;
  }

  public void setDeviceid(String deviceid) {
    this.deviceid = deviceid;
  }

  @Override
  public String toString() {
    return "ClassPojo [appName = " + appName + ", deviceid = " + deviceid + "]";
  }
}
