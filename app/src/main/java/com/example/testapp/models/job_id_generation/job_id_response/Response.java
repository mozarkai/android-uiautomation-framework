package com.example.testapp.models.job_id_generation.job_id_response;

public class Response {

  private Data data;

  private String statusMessage;

  private String statusCode;

  public Response(Data data, String statusMessage, String statusCode) {
    this.data = data;
    this.statusMessage = statusMessage;
    this.statusCode = statusCode;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  @Override
  public String toString() {
    return "ClassPojo [data = " + data + ", statusMessage = " + statusMessage + ", statusCode = "
        + statusCode + "]";
  }
}

