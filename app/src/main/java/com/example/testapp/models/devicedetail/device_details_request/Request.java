package com.example.testapp.models.devicedetail.device_details_request;

public class Request {

  private Data data;

  public Request(Data data) {
    this.data = data;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "ClassPojo [data = " + data + "]";
  }
}
