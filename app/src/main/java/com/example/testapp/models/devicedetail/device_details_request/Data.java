package com.example.testapp.models.devicedetail.device_details_request;

import org.json.JSONObject;

public class Data {


  private String status;
  private String city;
  private String fcmId;
  private String model;
  private String os;
  private String version;
  private String carrier;
  private String operator;
  private JSONObject all;

  public Data(String status, String city, String fcmId, String model, String os, String version,
      String carrier, String operator, JSONObject all) {
    this.status = status;
    this.city = city;
    this.fcmId = fcmId;
    this.model = model;
    this.os = os;
    this.version = version;
    this.carrier = carrier;
    this.operator = operator;
    this.all = all;
  }

  public Data() {

  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getFcmId() {
    return fcmId;
  }

  public void setFcmId(String fcmId) {
    this.fcmId = fcmId;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public JSONObject getAll() {
    return all;
  }

  public void setAll(JSONObject all) {
    this.all = all;
  }
}
