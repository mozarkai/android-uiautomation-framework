package com.example.testapp.models.job_id_generation.job_id_request;

public class Request {

  private Data data;

  public Request(Data data) {
    this.data = data;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "ClassPojo [data = " + data + "]";
  }
}
