package com.example.testapp.models.devicedetail.device_details_response;

public class Response {

  private String statusMessage;

  private String statusCode;

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  @Override
  public String toString() {
    return "ClassPojo [statusMessage = " + statusMessage + ", statusCode = " + statusCode + "]";
  }
}



