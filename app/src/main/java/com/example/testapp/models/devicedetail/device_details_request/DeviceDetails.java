package com.example.testapp.models.devicedetail.device_details_request;

public class DeviceDetails {

  private Json json;

  public DeviceDetails(Json json) {
    this.json = json;
  }

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
