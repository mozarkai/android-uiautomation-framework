package com.example.testapp.models.devicedetail.device_details_response;


public class Json {

  private Response response;

  public Response getResponse() {
    return response;
  }

  public void setResponse(Response response) {
    this.response = response;
  }

  @Override
  public String toString() {
    return "ClassPojo [response = " + response + "]";
  }
}

