package com.example.testapp.models.devicedetail.device_details_response;

public class DeviceDetailsResponse {

  private Json json;

  public Json getJson() {
    return json;
  }

  public void setJson(Json json) {
    this.json = json;
  }

  @Override
  public String toString() {
    return "ClassPojo [json = " + json + "]";
  }
}
