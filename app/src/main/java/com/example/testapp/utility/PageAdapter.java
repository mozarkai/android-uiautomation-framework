package com.example.testapp.utility;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.example.testapp.fragments.connect.FragmentConnect;
import com.example.testapp.fragments.results.FragmentResults;
import com.example.testapp.fragments.results.pcap.FragmentCapture;
import com.example.testapp.fragments.test.FragmentTest;

/**
 * Created by abdalla on 2/18/18.
 */

public class PageAdapter extends FragmentPagerAdapter {

  private int numOfTabs;

  public PageAdapter(FragmentManager fm, int numOfTabs) {
    super(fm);
    this.numOfTabs = numOfTabs;
  }

  @Override
  public Fragment getItem(int position) {
    switch (position) {
      case 0:
        return new FragmentConnect();
      case 1:
        return new FragmentTest();
      case 2:
        return new FragmentResults();
      case 3:
        return new FragmentCapture();
      default:
        return null;
    }
  }

  @Override
  public int getCount() {
    return numOfTabs;
  }
}
