package com.example.testapp.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import androidx.annotation.RequiresApi;
import com.example.testapp.AquaMarkSharePreferences;
import com.example.testapp.models.devicedetail.device_details_request.Data;

public class DeviceDetail {

  Data data = new Data();

  //prior details

  public void getNetworkType() {
    Context context = TestApp.getContext();
    String networkType = NetworkType(context);
    Log.d("Aquamark", "NetworkType  is " + networkType);

    AquaMarkSharePreferences.getInstance().setCarrier(networkType);


  }


  public void getOperator() {
    String operator = Operator();
    Log.d("Aquamark", "Operator is " + operator);
    data.setOperator(operator);
    AquaMarkSharePreferences.getInstance().setOperator(operator);

  }

  public void getOs() {
    String os = android.os.Build.VERSION.RELEASE;
    Log.d("Aquamark", "Os version  is " + os);

    AquaMarkSharePreferences.getInstance().setVersion(os);


  }

  public void getModel() {
    String model = Build.MODEL;
    Log.d("Aquamark", "Model name is " + model);

    AquaMarkSharePreferences.getInstance().setModel(model);


  }

  public void platform() {
    String platform = "Android";
    AquaMarkSharePreferences.getInstance().setPlatform(platform);
  }

//other details


  public boolean getAirplane() {
    Context context = TestApp.getContext();
    Boolean airplane = AirplaneModeOn(context);
    Log.d("Aquamark", "Airplane   is " + airplane);
    return airplane;

  }


  @RequiresApi(api = Build.VERSION_CODES.M)
  public int getSignalStrength() {
    TelephonyManager telephonyManager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    if (TestApp.getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    Activity#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for Activity#requestPermissions for more details.

    }
    CellInfoGsm cellinfogsm = null;
    if (telephonyManager != null) {
      try {
        //TODO punit please fix it
        cellinfogsm = (CellInfoGsm) telephonyManager.getAllCellInfo().get(0);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    CellSignalStrengthGsm cellSignalStrengthGsm = null;
    if (cellinfogsm != null) {
      cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
    }
    if (cellSignalStrengthGsm != null) {
      cellSignalStrengthGsm.getDbm();
    }

    int signal = 0;
    if (cellSignalStrengthGsm != null) {
      signal = cellSignalStrengthGsm.getDbm();
    }

    Log.d("Aquamark", "Signal Strenth  is " + signal);
    return signal;

  }


  @RequiresApi(api = Build.VERSION_CODES.M)
  public int getSignalStrengthQuality() {
    TelephonyManager telephonyManager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    if (TestApp.getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    Activity#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for Activity#requestPermissions for more details.

    }
    CellInfoGsm cellinfogsm = null;
    if (telephonyManager != null) {
      try {
        cellinfogsm = (CellInfoGsm) telephonyManager.getAllCellInfo().get(0);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    CellSignalStrengthGsm cellSignalStrengthGsm = null;
    if (cellinfogsm != null) {
      cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
    }
    if (cellSignalStrengthGsm != null) {
      cellSignalStrengthGsm.getDbm();
    }

    int signalqulity = 0;
    if (cellSignalStrengthGsm != null) {
      signalqulity = cellSignalStrengthGsm.getLevel();
    }

    Log.d("Aquamark", "Signal Strenth  is " + signalqulity);
    return signalqulity;

  }

  public String getABI() {
    String ABI = Build.SUPPORTED_ABIS[0];
    Log.d("Aquamark", "ABI   is " + ABI);
    return ABI;

  }


  public String getNetworkClass() {
    String network = NetworkClass();
    Log.d("Aquamark", "Network is " + network);
    return network;

  }


  public String getFormFcator() {
    String form = FormFactor();
    Log.d("Aquamark", "Form Factor is " + form);
    return form;

  }


  public int getSdk() {
    int sdk = Build.VERSION.SDK_INT;
    Log.d("Aquamark", "SDK version  is " + sdk);
    return sdk;


  }


  public String getManufacture() {
    String manufacturer = Build.MANUFACTURER;
    Log.d("Aquamark", "manufacturer name is " + manufacturer);
    return manufacturer;

  }

  public void getResolution() {
    Context context = TestApp.getContext();
    String width = getScreenResolutionwidth(context);
    String height = getScreenResolutionHeight(context);
    Log.d("Aquamark", "width  is " + width);

    Log.d("Aquamark", "height  is " + height);


  }

  public String getScreenResolutionwidth(Context context) {
    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    DisplayMetrics metrics = new DisplayMetrics();
    display.getMetrics(metrics);
    int widths = metrics.widthPixels;
    int height = metrics.heightPixels;
    String width = String.valueOf(widths);
    return width;
  }

  public String getScreenResolutionHeight(Context context) {
    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    DisplayMetrics metrics = new DisplayMetrics();
    display.getMetrics(metrics);

    int heights = metrics.heightPixels;
    String height = String.valueOf(heights);
    return height;
  }

  public String FormFactor() {
    TelephonyManager manager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    if (manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
      return "TABLET";
    } else {
      return "PHONE";
    }
  }

  public String Operator() {
    TelephonyManager manager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    String carrierName = manager.getNetworkOperatorName();

    return carrierName;

  }


  private String NetworkClass() {
    // network type
    TelephonyManager mTelephonyManager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    int networkType = mTelephonyManager != null ? mTelephonyManager.getNetworkType() : 0;
    switch (networkType) {
      case TelephonyManager.NETWORK_TYPE_UNKNOWN:
        return "Unknown network";
      case TelephonyManager.NETWORK_TYPE_GSM:
        return " GSM";
      case TelephonyManager.NETWORK_TYPE_CDMA:
      case TelephonyManager.NETWORK_TYPE_1xRTT:
      case TelephonyManager.NETWORK_TYPE_IDEN:
        return " 2G";
      case TelephonyManager.NETWORK_TYPE_GPRS:
        return " GPRS (2.5G)";
      case TelephonyManager.NETWORK_TYPE_EDGE:
        return " EDGE (2.75G)";
      case TelephonyManager.NETWORK_TYPE_UMTS:
      case TelephonyManager.NETWORK_TYPE_EVDO_0:
      case TelephonyManager.NETWORK_TYPE_EVDO_A:
      case TelephonyManager.NETWORK_TYPE_EVDO_B:
        return " 3G";
      case TelephonyManager.NETWORK_TYPE_HSPA:
      case TelephonyManager.NETWORK_TYPE_HSDPA:
      case TelephonyManager.NETWORK_TYPE_HSUPA:
        return " H (3G+)";
      case TelephonyManager.NETWORK_TYPE_EHRPD:
      case TelephonyManager.NETWORK_TYPE_HSPAP:
      case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
        return " H+ (3G++)";
      case TelephonyManager.NETWORK_TYPE_LTE:
      case TelephonyManager.NETWORK_TYPE_IWLAN:
        return " 4G";
      default:
        return " 4G+";
    }
  }

  public String NetworkType(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    boolean airplane = AirplaneModeOn(context);
    if (airplane && !isOnline()) {
      return "No network type in airplane mode";
    } else if (isOnline()) {
      NetworkInfo info = cm.getActiveNetworkInfo();
      return info.getTypeName();
    } else {
      return "No network type if no internet";
    }
  }

  public boolean AirplaneModeOn(Context context) {
    return Settings.System.getInt(
        context.getContentResolver(),
        Settings.System.AIRPLANE_MODE_ON,
        0) != 0;
  }

  protected boolean isOnline() {
    ConnectivityManager cm = (ConnectivityManager) TestApp.getContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    return netInfo != null && netInfo.isConnectedOrConnecting();
  }


  @RequiresApi(api = Build.VERSION_CODES.M)
  public String getImeiNumber() {
    TelephonyManager telephonyManager = (TelephonyManager) TestApp.getContext()
        .getSystemService(Context.TELEPHONY_SERVICE);
    if (TestApp.getContext().checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
        != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    Activity#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for Activity#requestPermissions for more details.

    }
    String imei = telephonyManager.getDeviceId();
    Log.d("Aquamark", "Imei  is " + imei);
    return imei;

  }


}
