package com.example.testapp.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import com.example.testapp.AquaMarkSharePreferences;
import com.example.testapp.R;

public class BatteryInfo {

  AquaMarkSharePreferences aquaMarkSharePreferences;
  private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      updateBatteryData(intent);
    }
  };

  public void loadBatterySection() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
    intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
    intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

    TestApp.getContext().registerReceiver(batteryInfoReceiver, intentFilter);

  }

  private void updateBatteryData(Intent intent) {
    boolean present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);

    if (present) {
      int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
      int healthLbl = -1;

      switch (health) {
        case BatteryManager.BATTERY_HEALTH_COLD:
          healthLbl = R.string.battery_health_cold;
          break;

        case BatteryManager.BATTERY_HEALTH_DEAD:
          healthLbl = R.string.battery_health_dead;
          break;

        case BatteryManager.BATTERY_HEALTH_GOOD:
          healthLbl = R.string.battery_health_good;
          break;

        case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
          healthLbl = R.string.battery_health_over_voltage;
          break;

        case BatteryManager.BATTERY_HEALTH_OVERHEAT:
          healthLbl = R.string.battery_health_overheat;
          break;

        case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
          healthLbl = R.string.battery_health_unspecified_failure;
          break;

        case BatteryManager.BATTERY_HEALTH_UNKNOWN:
        default:
          break;
      }

      if (healthLbl != -1) {
        // display battery health ...
        Log.d("Health : ", TestApp.getContext().getString(healthLbl));
        String healths = TestApp.getContext().getString(healthLbl);

        AquaMarkSharePreferences.getInstance().setHealth(healths);


      }

      // Calculate Battery Pourcentage ...
      int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
      int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

      if (level != -1 && scale != -1) {
        int batteryPct = (int) ((level / (float) scale) * 100f);
        Log.d("Battery Pct : ", batteryPct + " %");
        AquaMarkSharePreferences.getInstance().setPercentage(String.valueOf(batteryPct));
      }

      int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
      int pluggedLbl = R.string.battery_plugged_none;

      switch (plugged) {
        case BatteryManager.BATTERY_PLUGGED_WIRELESS:
          pluggedLbl = R.string.battery_plugged_wireless;
          break;

        case BatteryManager.BATTERY_PLUGGED_USB:
          pluggedLbl = R.string.battery_plugged_usb;
          break;

        case BatteryManager.BATTERY_PLUGGED_AC:
          pluggedLbl = R.string.battery_plugged_ac;
          break;

        default:
          pluggedLbl = R.string.battery_plugged_none;
          break;
      }

      // display plugged status ...
      Log.d("Plugged : ", TestApp.getContext().getString(pluggedLbl));
      String pluggeds = TestApp.getContext().getString(pluggedLbl);
      AquaMarkSharePreferences.getInstance().setPlugged(pluggeds);
      int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
      int statusLbl = R.string.battery_status_discharging;

      switch (status) {
        case BatteryManager.BATTERY_STATUS_CHARGING:
          statusLbl = R.string.battery_status_charging;
          break;

        case BatteryManager.BATTERY_STATUS_DISCHARGING:
          statusLbl = R.string.battery_status_discharging;
          break;

        case BatteryManager.BATTERY_STATUS_FULL:
          statusLbl = R.string.battery_status_full;
          break;

        case BatteryManager.BATTERY_STATUS_UNKNOWN:
          statusLbl = -1;
          break;

        case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
        default:
          statusLbl = R.string.battery_status_discharging;
          break;
      }

      if (statusLbl != -1) {
        Log.d("Battery Charging Status", "" + TestApp.getContext().getString(statusLbl));
        String statss = TestApp.getContext().getString(statusLbl);
        AquaMarkSharePreferences.getInstance().setCharging(statss);

      }

      if (intent.getExtras() != null) {
        String technology = intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);

        if (!"".equals(technology)) {
          Log.d("Technology : ", "" + technology);
          AquaMarkSharePreferences.getInstance().setTech(technology);
        }
      }

      int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);

      if (temperature > 0) {
        float temp = ((float) temperature) / 10f;
        Log.d("Temperature : ", +temp + "°C");
        String temps = String.valueOf(temperature);
        AquaMarkSharePreferences.getInstance().setTemperature(temps);
      }

      int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);

      if (voltage > 0) {
        Log.d("Voltage : ", +voltage + " mV");
        AquaMarkSharePreferences.getInstance().setVoltage(String.valueOf(voltage));
      }

      long capacity = getBatteryCapacity(TestApp.getContext());

      if (capacity > 0) {
        Log.d("Capacity ", +capacity + " mAh");
        AquaMarkSharePreferences.getInstance().setCapacity(String.valueOf(capacity));
      }

    } else {
      Toast.makeText(TestApp.getContext(), "No Battery present", Toast.LENGTH_SHORT).show();
    }

  }

  public long getBatteryCapacity(Context ctx) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      BatteryManager mBatteryManager = (BatteryManager) ctx
          .getSystemService(Context.BATTERY_SERVICE);
      Long chargeCounter = mBatteryManager
          .getLongProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER);
      Long capacity = mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

      if (chargeCounter != null && capacity != null) {
        long value = (long) (((float) chargeCounter / (float) capacity) * 100f);
        return value;
      }
    }

    return 0;
  }


}
