package com.example.testapp.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class MarshmallowPermissions {

  public static final String PERMISSIONS_GRANTED = "permissions_granted";
  private static final String PREF_NAME = "marshmallow_permissions";
  SharedPreferences pref;
  SharedPreferences.Editor editor;
  Context context;
  //
  int PRIVATE_MODE = 0;


  // Constructor
  public MarshmallowPermissions(Context context) {

    this.context = context;
    pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    editor = pref.edit();
  }


  /**
   * set network status
   **/
  public void setPermissionStatus(Boolean networkStatus) {
    // Storing unique team name in pref
    editor.putBoolean(PERMISSIONS_GRANTED, networkStatus);
    editor.commit();
    // commit changes
  }

  // get network status
  public Boolean getPermissionsStatus() {
    return pref.getBoolean(PERMISSIONS_GRANTED, false);
  }

  public void clear() {
    editor.clear().commit();
  }
}
