package com.example.testapp.api_interface;

import com.example.testapp.models.devicedetail.device_details_request.DeviceDetails;
import com.example.testapp.models.devicedetail.device_details_response.DeviceDetailsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DeviceDetailsAPI {

  @Headers("Content-Type: application/json")

  @PUT("auth/devices/{deviceid}")
  Call<DeviceDetailsResponse> postData(@Path("deviceid") String deviceid, @Body DeviceDetails body);

}
