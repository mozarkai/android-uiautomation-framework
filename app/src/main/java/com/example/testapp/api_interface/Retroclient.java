package com.example.testapp.api_interface;


import android.content.Context;
import android.content.res.AssetManager;
import com.example.testapp.utility.TestApp;
import java.io.InputStream;
import java.util.Properties;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retroclient {

  static Properties properties;
  public Retrofit retrofit;
  Context context;

  private static void setProperties() {
    try {
      properties = new Properties();

      AssetManager assetManager = TestApp.getContext().getAssets();
      InputStream inputStream = assetManager.open("config.properties");
      properties.load(inputStream);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Retrofit getClient() {

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    setProperties();
    retrofit = new Retrofit.Builder()
        .baseUrl(properties.getProperty("baseUrl"))
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build();

    return retrofit;
  }

}
