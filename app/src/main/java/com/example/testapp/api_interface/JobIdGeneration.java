package com.example.testapp.api_interface;

import com.example.testapp.models.job_id_generation.job_id_request.GenerateJobId;
import com.example.testapp.models.job_id_generation.job_id_response.GenrateJobIdResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JobIdGeneration {

  @Headers("Content-Type: application/json")
  @POST("IceBerg/Performance/createjob")
  Call<GenrateJobIdResponse> getJobId(@Body GenerateJobId body);

  @Headers("Content-Type: application/json")
  @GET("IceBerg/port/{deviceid}")
  Call<GenrateJobIdResponse> getPortNumber(@Path("deviceid") String deviceId);

  @Headers("Content-Type: application/json")
  @GET("IceBerg/port/status/{port}")
  Call<GenrateJobIdResponse> getPortNumberStatus(@Path("port") int port);

  @Headers("Content-Type: application/json")
  @PUT("IceBerg/port/{deviceid}")
  Call<GenrateJobIdResponse> updateDeviceStatus(@Path("deviceid") String deviceid,
      @Body GenerateJobId body);
}
