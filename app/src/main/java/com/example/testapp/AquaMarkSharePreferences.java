package com.example.testapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.example.testapp.utility.TestApp;

public class AquaMarkSharePreferences {

    private static final String TAG = AquaMarkSharePreferences.class.getName();
    private static final String NAME = "pref_aquamark";
    private static AquaMarkSharePreferences _instance;
    private SharedPreferences preferences;

    public AquaMarkSharePreferences() {
        preferences = TestApp.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static AquaMarkSharePreferences getInstance() {
        if (_instance == null) {
            _instance = new AquaMarkSharePreferences();
        }
        return _instance;
    }

    public void clearSharePreference() {
        _instance.clear();
    }

    /**
     * This Method Clear shared preference.
     */
    protected void clear() {
        Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    protected void commit() {
        preferences.edit().apply();
    }

    private void setString(String key, String value) {
        if (key != null && value != null) {
            try {
                if (preferences != null) {
                    Editor editor = preferences.edit();
                    editor.putString(key, value);
                    editor.apply();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private void setLong(String key, long value) {
        if (key != null) {
            try {
                if (preferences != null) {
                    Editor editor = preferences.edit();
                    editor.putLong(key, value);
                    editor.apply();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private void setInt(String key, int value) {
        if (key != null) {
            try {
                if (preferences != null) {
                    Editor editor = preferences.edit();
                    editor.putInt(key, value);
                    editor.apply();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private void setDouble(String key, double value) {
        if (key != null) {
            try {
                if (preferences != null) {
                    Editor editor = preferences.edit();
                    editor.putLong(key, Double.doubleToRawLongBits(value));
                    editor.apply();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private void setBoolean(String key, boolean value) {
        if (key != null) {
            try {
                if (preferences != null) {
                    Editor editor = preferences.edit();
                    editor.putBoolean(key, value);
                    editor.apply();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private int getInt(String key, int defaultValue) {
        if (preferences != null && key != null && preferences.contains(key)) {
            return preferences.getInt(key, defaultValue);
        }
        return defaultValue;
    }

    private long getLong(String key, long defaultValue) {
        if (preferences != null && key != null && preferences.contains(key)) {
            return preferences.getLong(key, defaultValue);
        }
        return defaultValue;
    }

    private boolean getBoolean(String key, boolean defaultValue) {
        if (preferences != null && key != null && preferences.contains(key)) {
            return preferences.getBoolean(key, defaultValue);
        }
        return defaultValue;
    }

    private String getString(String key, String defaultValue) {

        if (preferences != null && key != null && preferences.contains(key)) {
            return preferences.getString(key, defaultValue);
        }
        return defaultValue;

    }

    private double getDouble(String key, double defaultValue) {
        if (preferences != null && key != null && preferences.contains(key)) {
            return preferences.getFloat(key, (float) defaultValue);
        }
        return defaultValue;
    }

    private void putBoolean(String key, boolean value) {
        try {
            if (preferences != null) {
                Editor editor = preferences.edit();
                editor.putBoolean(key, value);
                editor.apply();
            }
        } catch (Exception e) {
            Log.e(TAG, "Unable Put Boolean in Shared preference", e);
        }
    }


    public String getIpAddress() {
        return getString(Keys.IP_ADDRESS, "52.183.134.250");
    }

    public void setIpAddress(String ipAddress) {
        setString(Keys.IP_ADDRESS, ipAddress);
    }

    public String getDeviceId() {
        return getString(Keys.DEVICE_ID, "");
    }

    public void setDeviceId(String uuid) {
        setString(Keys.DEVICE_ID, uuid);
    }

    public String getFcmId() {
        return getString(Keys.FCM_ID, "");
    }

    public void setFcmId(String uuid) {
        setString(Keys.FCM_ID, uuid);
    }

    public String getLatLng() {
        return getString(Keys.LAT_LNG, "");
    }

    public void setLatLng(String uuid) {
        setString(Keys.LAT_LNG, uuid);
    }

    public int getPortNumber() {
        return getInt(Keys.PORT, -1);
    }

    public void setPortNumber(int uuid) {
        setInt(Keys.PORT, uuid);
    }

    public String getLocationAddress() {
        return getString(Keys.LAT_LNG_ADDRESS, "");
    }

    public void setLocationAddress(String uuid) {
        setString(Keys.LAT_LNG_ADDRESS, uuid);
    }

    public String getHealth() {
        return getString(Keys.health, "");

    }

    public void setHealth(String health) {
        setString(Keys.health, health);
    }

    public String getPlugged() {
        return getString(Keys.plugged, "");

    }

    public void setPlugged(String plugged) {
        setString(Keys.plugged, plugged);
    }

    public String getPercentage() {
        return getString(Keys.percentage, "");

    }

    public void setPercentage(String percentage) {
        setString(Keys.percentage, percentage);
    }

    public String getCharging() {
        return getString(Keys.charging, "");

    }

    public void setCharging(String charging) {
        setString(Keys.charging, charging);
    }

    public String getTech() {
        return getString(Keys.tech, "");

    }

    public void setTech(String tech) {
        setString(Keys.tech, tech);
    }

    public String getTemperature() {
        return getString(Keys.temperature, "");

    }

    public void setTemperature(String temperature) {
        setString(Keys.temperature, temperature);
    }

    public String getVoltage() {
        return getString(Keys.voltage, "");

    }

    public void setVoltage(String voltage) {
        setString(Keys.voltage, voltage);
    }

    public String getCapacity() {
        return getString(Keys.capacity, "");

    }

    public void setCapacity(String capacity) {
        setString(Keys.capacity, capacity);
    }

    public String getModel() {
        return getString(Keys.model, "");

    }

    public void setModel(String model) {
        setString(Keys.model, model);
    }

    public String getVersion() {
        return getString(Keys.version, "");

    }

    public void setVersion(String version) {
        setString(Keys.version, version);
    }

    public String getPlatform() {
        return getString(Keys.platform, "");

    }

    public void setPlatform(String platform) {
        setString(Keys.platform, platform);
    }

    public String getOperator() {
        return getString(Keys.operator, "");

    }

    public void setOperator(String operator) {
        setString(Keys.operator, operator);
    }

    public String getCarrier() {
        return getString(Keys.carrier, "");

    }

    public void setCarrier(String carrier) {
        setString(Keys.carrier, carrier);
    }

    public int getServerConnection() {
        return getInt(Keys.SERVER_URL_TYPE, 1);
    }

    public void setServerConnection(int carrier) {
        setInt(Keys.SERVER_URL_TYPE, carrier);
    }

    public void setStartDate(String startDate) {
        setString(Keys.startDate, startDate);
    }

    private interface Keys {

        String DEVICE_ID = "device_id";
        String FCM_ID = "fcm_id";
        String IP_ADDRESS = "fcm_id";
        String PORT = "port";
        String LAT_LNG = "latlng";
        String LAT_LNG_ADDRESS = "lat_lng_address";
        String health = "health";
        String percentage = "percentage";
        String plugged = "plugged";
        String charging = "charging";
        String tech = "tech";
        String temperature = "temperature";
        String voltage = "Voltage";
        String capacity = "capacity";
        String model = "model";
        String version = "version";
        String operator = "operator";
        String carrier = "carrier";
        String platform = "platform";
        String SERVER_URL_TYPE = "connection_url_type";
        String startDate = "start_date";
    }
}