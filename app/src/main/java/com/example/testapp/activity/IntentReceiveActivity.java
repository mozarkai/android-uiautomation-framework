package com.example.testapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Process;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.testapp.R;
import com.example.testapp.utility.TestApp;

import org.json.JSONException;
import org.json.JSONObject;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.MODIFY_PHONE_STATE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_NUMBERS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.example.testapp.utility.UtilityClass.isTestAppExist;

public class IntentReceiveActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static Activity activityMain;
    private static JSONObject finalJsonObject;
    private TextView text_view_countdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_receive);
        text_view_countdown = findViewById(R.id.text_view_countdown);
        handleSendText(text_view_countdown, this);
    }

    private void showDialogError() {

    }


    public boolean checkPermission() {
        int resultCam = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int resultLoc = ContextCompat
                .checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int resultLocCource = ContextCompat
                .checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        int resultPhoneState = ContextCompat
                .checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        int resultInternet = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
        int resultReadExternal = ContextCompat
                .checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int resultWriteExternal = ContextCompat
                .checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return resultLoc == PackageManager.PERMISSION_GRANTED
                && resultInternet == PackageManager.PERMISSION_GRANTED
                && resultPhoneState == PackageManager.PERMISSION_GRANTED
                && resultReadExternal == PackageManager.PERMISSION_GRANTED
                && resultWriteExternal == PackageManager.PERMISSION_GRANTED
                && resultCam == PackageManager.PERMISSION_GRANTED
                && resultLocCource == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)
                                || shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)
                                || shouldShowRequestPermissionRationale(READ_PHONE_STATE) ||
                                shouldShowRequestPermissionRationale(INTERNET)
                                || shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE) ||
                                shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)
                                || shouldShowRequestPermissionRationale(MODIFY_PHONE_STATE)) {
                       /*     showMessageOKCancel("You need to allow access to both the permissions",
                                    (dialog, which) -> {
                                        requestPermissions(PERMISSIONS,
                                                PERMISSION_REQUEST_CODE);
                                        if (checkPermissions()) {

                                            fetchDeviceDetail();
                                        }
                                    });
                            return;*/
                        }
                    }
                }
                break;
        }
    }


    String[] PERMISSIONS = {Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_CONTACTS,
            READ_SMS, READ_PHONE_NUMBERS,
            CAMERA,
            ACCESS_FINE_LOCATION,
            ACCESS_COARSE_LOCATION,
            INTERNET,
            READ_PHONE_STATE,
            READ_EXTERNAL_STORAGE,
            WRITE_EXTERNAL_STORAGE,
            MODIFY_PHONE_STATE};

    public void requestPermission() {

        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);

    }


    public void handleSendText(TextView text_view_countdown, Activity activity) {
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                text_view_countdown.setText(" " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                try {
                    IntentAppstart();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // text_view_countdown.setText("done!");

            }

        }.start();
    }

    private void IntentAppstart() {
        Intent intent = new Intent();
        intent.setAction("launch.me.action.LAUNCH_IT_UP");
        startActivity(intent);
        finishAffinity();
        Process.killProcess(Process.myPid());
    }


}