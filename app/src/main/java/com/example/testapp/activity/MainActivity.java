package com.example.testapp.activity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.MANAGE_EXTERNAL_STORAGE;
import static android.Manifest.permission.MODIFY_PHONE_STATE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_NUMBERS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import com.example.testapp.AquaMarkSharePreferences;
import com.example.testapp.BuildConfig;
import com.example.testapp.R;
import com.example.testapp.api_interface.DeviceDetailsAPI;
import com.example.testapp.busevent.MessageEvent;
import com.example.testapp.location.LocationTracker;
import com.example.testapp.models.devicedetail.device_details_request.Data;
import com.example.testapp.models.devicedetail.device_details_request.DeviceDetails;
import com.example.testapp.models.devicedetail.device_details_request.Json;
import com.example.testapp.models.devicedetail.device_details_request.Request;
import com.example.testapp.models.devicedetail.device_details_response.DeviceDetailsResponse;
import com.example.testapp.portforwardingconnection.StfConnectionService;
import com.example.testapp.utility.BatteryInfo;
import com.example.testapp.utility.DeviceDetail;
import com.example.testapp.utility.MarshmallowPermissions;
import com.example.testapp.utility.PageAdapter;
import com.example.testapp.utility.TestApp;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

  public static final int PERMISSION_ALL = 1;
  private static final String TAG = MainActivity.class.getName();
  // location updates interval - 10sec
  private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
  final static int REQUEST_CODE = 333;
  // fastest updates interval - 5 sec
  // location updates will be received if another app is requesting the locations
  // than your app can handle
  private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
  private static final int REQUEST_CHECK_SETTINGS = 100;
  private static final int PERMISSION_REQUEST_CODE = 200;
  public static boolean isShowingSetting = false;
  Intent mServiceIntent;
  Toolbar toolbar;
  TabLayout tabLayout;
  ViewPager viewPager;
  PageAdapter pageAdapter;
  TabItem tabConnect;
  TabItem tabTest;
  TabItem tabResults, tabCapture;
  JSONObject detailsExtra = new JSONObject();
  AquaMarkSharePreferences aquaMarkSharePreferences;
  String airplane, signal, signalQuality, ABI, network, SDK, manufacture, width, heigth, formFactor, ImeiNumber;
  String health, percentage, plugged, charging, technology, temperature, volatge, capacity;
  String model, version, networkType, platform, operator;
  //permission requried above android version 5
  String[] PERMISSIONS = {Manifest.permission.ACCESS_NETWORK_STATE,
      Manifest.permission.READ_CONTACTS,
      READ_SMS, READ_PHONE_NUMBERS,
      CAMERA,
      ACCESS_FINE_LOCATION,
      ACCESS_COARSE_LOCATION,
      Manifest.permission.INTERNET,
      READ_PHONE_STATE,
      Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.WRITE_EXTERNAL_STORAGE,
      MODIFY_PHONE_STATE,MANAGE_EXTERNAL_STORAGE};
  LocationTracker locationTracker;
  private StfConnectionService mStfConnectionService;
  private TelephonyManager telephonyManager;
  private MarshmallowPermissions marshmallowPermissions;
  private View view;
  private String currentLocation = "NA";
  // location last updated time
  private String mLastUpdateTime;
  // bunch of location related apis
  private FusedLocationProviderClient mFusedLocationClient;
  private SettingsClient mSettingsClient;
  //=======================           LOCATION DETECTION        ======================================
  private LocationRequest mLocationRequest;
  private LocationSettingsRequest mLocationSettingsRequest;
  private LocationCallback mLocationCallback;
  private Location mCurrentLocation;
  // boolean flag to toggle the ui
  private Boolean mRequestingLocationUpdates = true;

  // Check app has a specific permission
  public static boolean hasPermissions(String TAG, Context context, String... permissions) {
    if (SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
      for (String permission : permissions) {
        //      Log.w(TAG, "hasPermissions " + permissions);
        if (ActivityCompat.checkSelfPermission(context, permission)
            != PackageManager.PERMISSION_GRANTED) {
          return false;
        }
      }
    }
    //marshmallowPermissions.setPermissionStatus(true);
    return true;
  }

  private boolean isMyServiceRunning(Class<?> serviceClass) {
    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningServiceInfo service : manager
        .getRunningServices(Integer.MAX_VALUE)) {
      if (serviceClass.getName().equals(service.service.getClassName())) {
        Log.i("isMyServiceRunning?", true + "");
        return true;
      }
    }
    Log.i("isMyServiceRunning?", false + "");
    return false;
  }

  private void requestPermission() {

    ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);

  }

  private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
    new AlertDialog.Builder(MainActivity.this)
        .setMessage(message)
        .setPositiveButton("OK", okListener)
        .setNegativeButton("Cancel", null)
        .create()
        .show();
  }

  @SuppressLint("HardwareIds")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    EventBus.getDefault().register(this);
    toolbar = findViewById(R.id.toolbar);
    toolbar.setTitle(getResources().getString(R.string.app_name));
    setSupportActionBar(toolbar);
    tabLayout = findViewById(R.id.tablayout);
    tabConnect = findViewById(R.id.tabConnect);
    tabTest = findViewById(R.id.tabTest);
    tabResults = findViewById(R.id.tabResults);
    tabCapture = findViewById(R.id.tabCapture);
    viewPager = findViewById(R.id.viewPager);
    RequestPermission_Dialog();
    pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
    viewPager.setAdapter(pageAdapter);
    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        if (tab.getPosition() == 1) {
          toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              R.color.white));
          tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              R.color.white));
          if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                R.color.white));
          }
        } else if (tab.getPosition() == 2) {
          toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              android.R.color.white));
          tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              android.R.color.white));
          if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                android.R.color.white));
          }
        } else {
          toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              R.color.white));
          tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
              R.color.white));
          if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this,
                R.color.white));
          }
        }
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {

      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        mStfConnectionService = new StfConnectionService(TestApp.getContext());
//        mServiceIntent = new Intent(this, mStfConnectionService.getClass());
//        if (checkPermission())
//            fetchDeviceDetail();

    //check permission
    marshmallowPermissions = new MarshmallowPermissions(this);

    if (!marshmallowPermissions.getPermissionsStatus()) {
      if (!hasPermissions("loginActivity", this, PERMISSIONS)) {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

      } else {
        if (SDK_INT >= Build.VERSION_CODES.M) {
          if (checkSelfPermission(READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
          }
        }
        String mobile_Imei = telephonyManager.getDeviceId();
        System.out.println("Qosbeebelow6 " + mobile_Imei);
        AquaMarkSharePreferences.getInstance().setDeviceId(mobile_Imei);
      }
    }
    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    if (SDK_INT >= Build.VERSION_CODES.M) {
      if (checkSelfPermission(READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        //    Activity#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for Activity#requestPermissions for more details.
        return;
      }
    }
    if (telephonyManager != null) {
      try {
        String deviceId = telephonyManager.getDeviceId();
        AquaMarkSharePreferences.getInstance().setDeviceId(deviceId);
      } catch (Exception e) {
        String device_unique_id = Settings.Secure.getString(getContentResolver(),
            Settings.Secure.ANDROID_ID);
        AquaMarkSharePreferences.getInstance().setDeviceId(device_unique_id);
      }
    }
    view = tabConnect;
    if (!checkPermission()) {
      requestPermission();
    } else {
//            if (!isMyServiceRunning(mStfConnectionService.getClass())) {
//                startService(mServiceIntent);
//            }
    }
    locationTracker = new LocationTracker(this);
    init();
    // restore the values from saved instance state
    restoreValuesFromBundle(savedInstanceState);
    requestSuperUserPermission();
  }

  private void requestSuperUserPermission() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Process processcmd = Runtime.getRuntime().exec("su");
          OutputStream os = processcmd.getOutputStream();
          //below code for capture screenshot and placed in internal storage.
          os.write("adb shell\n".getBytes());
          //below code for increase brightness.
          os.write("am kill  com.example.testapp.test".getBytes());
          os.flush();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  private void fetchDeviceDetail() {

    //Get the device details

    DeviceDetail detail = new DeviceDetail();
    detail.getModel();
    detail.getOs();
    detail.platform();
    detail.getOperator();
    detail.getNetworkType();
    BatteryInfo batteryInfo = new BatteryInfo();
    batteryInfo.loadBatterySection();

    airplane = String.valueOf(detail.getAirplane());
    if (SDK_INT >= Build.VERSION_CODES.M) {
      signal = String.valueOf(detail.getSignalStrength());
    }
    if (SDK_INT >= Build.VERSION_CODES.M) {
      signalQuality = String.valueOf(detail.getSignalStrengthQuality());
    }
    ABI = detail.getABI();
    network = detail.getNetworkClass();
    SDK = String.valueOf(detail.getSdk());
    manufacture = detail.getManufacture();
    heigth = detail.getScreenResolutionHeight(TestApp.getContext());
    width = detail.getScreenResolutionwidth(TestApp.getContext());
    formFactor = detail.getFormFcator();
    if (SDK_INT >= Build.VERSION_CODES.M) {
      ImeiNumber = detail.getImeiNumber();
    }

    aquaMarkSharePreferences = new AquaMarkSharePreferences();

    health = AquaMarkSharePreferences.getInstance().getHealth();
    Log.d("Aquamark", health);

    percentage = AquaMarkSharePreferences.getInstance().getPercentage();
    plugged = AquaMarkSharePreferences.getInstance().getPlugged();
    charging = AquaMarkSharePreferences.getInstance().getCharging();
    technology = AquaMarkSharePreferences.getInstance().getTech();
    temperature = AquaMarkSharePreferences.getInstance().getTemperature();
    volatge = AquaMarkSharePreferences.getInstance().getVoltage();
    capacity = AquaMarkSharePreferences.getInstance().getCapacity();

    try {
      detailsExtra.put("airplane", airplane);
      detailsExtra.put("signal", signal);
      detailsExtra.put("signalQuality", signalQuality);
      detailsExtra.put("ABI", ABI);
      detailsExtra.put("network", network);
      detailsExtra.put("SDK", SDK);
      detailsExtra.put("width", width);
      detailsExtra.put("height", heigth);
      detailsExtra.put("Formfactir", formFactor);
      detailsExtra.put("Imei", ImeiNumber);
      detailsExtra.put("health", health);
      detailsExtra.put("percentage", percentage);
      detailsExtra.put("plugged", plugged);
      detailsExtra.put("charging", charging);
      detailsExtra.put("technology", technology);
      detailsExtra.put("temperature", temperature);
      detailsExtra.put("voltage", volatge);
      detailsExtra.put("capacity", capacity);

      Log.d("Aquamark", String.valueOf(detailsExtra));
    } catch (JSONException e) {
      e.printStackTrace();
    }

    model = AquaMarkSharePreferences.getInstance().getModel();
    version = AquaMarkSharePreferences.getInstance().getVersion();
    platform = AquaMarkSharePreferences.getInstance().getPlatform();
    operator = AquaMarkSharePreferences.getInstance().getOperator();
    networkType = AquaMarkSharePreferences.getInstance().getCarrier();
    //Device Details hit
    retrofit();
  }

  @Override
  protected void onResume() {
    super.onResume();
    // Resuming location updates depending on button state and
    // allowed permissions
    if (mRequestingLocationUpdates && checkPermissions()) {
      startLocationUpdates();
      try {
        Runtime.getRuntime().exec("su");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    updateLocationUI();
  }

  @Override
  protected void onDestroy() {
//        stopService(mServiceIntent);
    Log.i("MAINACT", "onDestroy!");
    super.onDestroy();
    EventBus.getDefault().unregister(this);

  }

  private boolean checkPermission() {
    int resultCam = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
    int resultLoc = ContextCompat
        .checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
    int resultLocCource = ContextCompat
        .checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
    int resultPhoneState = ContextCompat
        .checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
    int resultInternet = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
    int resultReadExternal = ContextCompat
        .checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
    int resultWriteExternal = ContextCompat
        .checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

    int resultManageExternal=ContextCompat.checkSelfPermission(getApplicationContext(),MANAGE_EXTERNAL_STORAGE);
//        int resultModifyPhone = ContextCompat.checkSelfPermission(getApplicationContext(), MODIFY_PHONE_STATE);

    return resultLoc == PackageManager.PERMISSION_GRANTED
        && resultInternet == PackageManager.PERMISSION_GRANTED
        && resultPhoneState == PackageManager.PERMISSION_GRANTED
        && resultReadExternal == PackageManager.PERMISSION_GRANTED
        && resultWriteExternal == PackageManager.PERMISSION_GRANTED
        && resultCam == PackageManager.PERMISSION_GRANTED
            && resultManageExternal == PackageManager.PERMISSION_GRANTED
        && resultLocCource == PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case PERMISSION_REQUEST_CODE:
        if (grantResults.length > 0) {
          if (SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)
                || shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)
                || shouldShowRequestPermissionRationale(READ_PHONE_STATE) ||
                shouldShowRequestPermissionRationale(INTERNET)
                || shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE) ||
                shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)
                || shouldShowRequestPermissionRationale(MODIFY_PHONE_STATE) || shouldShowRequestPermissionRationale(MANAGE_EXTERNAL_STORAGE)) {
              showMessageOKCancel("You need to allow access to both the permissions",
                  (dialog, which) -> {
                    requestPermissions(PERMISSIONS,
                        PERMISSION_REQUEST_CODE);
                    if (checkPermissions()) {
                      requestSuperUserPermission();
                      startLocationButtonClick();
                    }
                  });
              return;
            }
          }
        }
        break;
    }
  }

  @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
  public void onMessageEvent(MessageEvent event) {
//        Toast.makeText(getActivity(), "Order is bumping", Toast.LENGTH_SHORT).show();//Not getting called
    Log.d("", "onMessageEvent : " + event.toString());
//        JsonUtil.toModel(event.toString(), OrderDetail.class);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        isShowingSetting = true;
        locationTracker.showSettingsAlert();
      }
    });
  }

  private void init() {
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    mSettingsClient = LocationServices.getSettingsClient(this);

    mLocationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        // location is received
        mCurrentLocation = locationResult.getLastLocation();
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        updateLocationUI();
      }
    };

    mRequestingLocationUpdates = false;

    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
    mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
    builder.addLocationRequest(mLocationRequest);
    mLocationSettingsRequest = builder.build();
  }

  /**
   * Starting location updates Check whether location settings are satisfied and then location
   * updates will be requested
   */
  private void startLocationUpdates() {
    if (mSettingsClient != null) {
      mSettingsClient
          .checkLocationSettings(mLocationSettingsRequest)
          .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
              Log.i(TAG, "All location settings are satisfied.");

              Toast.makeText(getApplicationContext(), "Started location updates!",
                  Toast.LENGTH_SHORT).show();

              //noinspection MissingPermission
              mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                  mLocationCallback, Looper.myLooper());

              updateLocationUI();
            }
          })
          .addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
              int statusCode = ((ApiException) e).getStatusCode();
              switch (statusCode) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                  Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                      "location settings ");
                  try {
                    // Show the dialog by calling startResolutionForResult(), and check the
                    // result in onActivityResult().
                    ResolvableApiException rae = (ResolvableApiException) e;
                    rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                  } catch (IntentSender.SendIntentException sie) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                  }
                  break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                  String errorMessage = "Location settings are inadequate, and cannot be " +
                      "fixed here. Fix in Settings.";
                  Log.e(TAG, errorMessage);

                  Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
              }

              updateLocationUI();
            }
          });
    }
  }

  public void startLocationButtonClick() {
    // Requesting ACCESS_FINE_LOCATION using Dexter library
    Dexter.withActivity(this)
        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        .withListener(new PermissionListener() {
          @Override
          public void onPermissionGranted(PermissionGrantedResponse response) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
          }

          @Override
          public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
              // open device settings when the permission is
              // denied permanently
              openSettings();
            }
          }

          @Override
          public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
              PermissionToken token) {
            token.continuePermissionRequest();
          }
        }).check();
  }

  public void stopLocationButtonClick() {
    mRequestingLocationUpdates = false;
    stopLocationUpdates();
  }

  public void stopLocationUpdates() {
    // Removing location updates
    mFusedLocationClient
        .removeLocationUpdates(mLocationCallback)
        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
          @Override
          public void onComplete(@NonNull Task<Void> task) {
            Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT)
                .show();
          }
        });
  }

  /**
   * Update the UI displaying the location data and toggling the buttons
   */
  private void updateLocationUI() {
    try {
      if (mCurrentLocation != null) {
        AquaMarkSharePreferences.getInstance()
            .setLatLng(mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude());
        double latitude = mCurrentLocation.getLatitude();
        double longitude = mCurrentLocation.getLongitude();
        Geocoder gcd = new Geocoder(MainActivity.this, Locale.getDefault());

        List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);

        if (addresses.size() > 0) {
          currentLocation = addresses.get(0).getLocality();
          AquaMarkSharePreferences.getInstance().setLocationAddress(currentLocation);
          System.out.println("city " + addresses.get(0).getLocality());

        } else {
          currentLocation = "NA";
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void openSettings() {
    Intent intent = new Intent();
    intent.setAction(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts("package",
        BuildConfig.APPLICATION_ID, null);
    intent.setData(uri);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
  }

  private boolean checkPermissions() {
    int permissionState = ActivityCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION);
    return permissionState == PackageManager.PERMISSION_GRANTED;
  }

  /**
   * Restoring values from saved instance state
   */
  private void restoreValuesFromBundle(Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      if (savedInstanceState.containsKey("is_requesting_updates")) {
        mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
      }

      if (savedInstanceState.containsKey("last_known_location")) {
        mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
      }

      if (savedInstanceState.containsKey("last_updated_on")) {
        mLastUpdateTime = savedInstanceState.getString("last_updated_on");
      }
    }

    updateLocationUI();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
    outState.putParcelable("last_known_location", mCurrentLocation);
    outState.putString("last_updated_on", mLastUpdateTime);

  }


  public void retrofit() {
    try {
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

      Retrofit retroclient = new Retrofit.Builder()
          .baseUrl("http://52.183.134.250:8080/")
          .addConverterFactory(GsonConverterFactory.create())
          .client(client)
          .build();

      DeviceDetailsAPI service = retroclient.create(DeviceDetailsAPI.class);
      Call<DeviceDetailsResponse> call = service.postData(ImeiNumber, new DeviceDetails(new Json(
          new Request(
              new Data("offline", "bangalore", "213213123", model, platform, version, networkType,
                  operator, detailsExtra)))));
      call.enqueue(new Callback<DeviceDetailsResponse>() {
        @Override
        public void onResponse(Call<DeviceDetailsResponse> call,
            Response<DeviceDetailsResponse> response) {

        }

        @Override
        public void onFailure(Call<DeviceDetailsResponse> call, Throwable t) {

        }
      });
    } catch (Exception e) {
      e.printStackTrace();
      Log.d("Aquamark", "Exception in retro e" + e.getMessage());

    }

  }

  public void RequestPermission_Dialog() {
    if (SDK_INT >= Build.VERSION_CODES.R) {
      try {
        Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setData(Uri.parse(String.format("package:%s", new Object[]{getApplicationContext().getPackageName()})));
        startActivityForResult(intent, 2000);
      } catch (Exception e) {
        Intent obj = new Intent();
        obj.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
        startActivityForResult(obj, 2000);
      }
    } else {
      ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, REQUEST_CODE);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 2000) {
      if (SDK_INT >= Build.VERSION_CODES.R) {
        if (Environment.isExternalStorageManager()) {
          //msg show karo
          //move to next activity
        } else {

        }
      }
    }
  }

  public boolean permission() {
    if (SDK_INT >= Build.VERSION_CODES.R) { // R is Android 11
      return Environment.isExternalStorageManager();
    } else {
      int write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
      int read = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);

      return write == PackageManager.PERMISSION_GRANTED
              && read == PackageManager.PERMISSION_GRANTED;
    }

  }

}
