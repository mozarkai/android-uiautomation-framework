package com.example.testapp;

import java.util.ArrayList;
import java.util.List;

public class DataController {

  private static final DataController ourInstance = new DataController();
  List<Data> dataList = new ArrayList<>();

  private DataController() {
  }

  public static DataController getInstance() {
    return ourInstance;
  }

  public List<Data> getDataList() {
    return dataList;
  }

  public void setDataList(List<Data> dataList) {
    this.dataList = dataList;
  }
}
