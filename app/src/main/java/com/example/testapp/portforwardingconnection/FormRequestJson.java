package com.example.testapp.portforwardingconnection;

import android.content.Context;
import com.example.testapp.AquaMarkSharePreferences;
import java.util.Properties;
import org.json.JSONObject;

public class FormRequestJson {

  private static Properties properties;
  private static Context mContext;

  // send aknowledgement to server for capturing pcap and screen record
  public static String uploadCaptureReqJsonStr(int jobId) {
    String result = null;
    String mobile_Imei = AquaMarkSharePreferences.getInstance().getDeviceId();
    JSONObject jFinal = new JSONObject();
    JSONObject jReqest = new JSONObject();
    JSONObject jMain = new JSONObject();
    JSONObject jData = new JSONObject();

    try {

      jData.put("jobid", jobId);
      jData.put("deviceid", mobile_Imei);
      jData.put("status", "Running");

      jMain.put("data", jData);
      jReqest.put("request", jMain);
      jFinal.put("json", jReqest);

      String finalJsonStr = jFinal.toString();
      result = finalJsonStr;

      System.out.println("Send req : aknowledgement for capturing req :" + result);

    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  // Request for fcm
  public static String getJsonRequestFcm() {

    String result = null;

    JSONObject jFinal = new JSONObject();
    JSONObject jReqest = new JSONObject();
    JSONObject jMain = new JSONObject();
    JSONObject jData = new JSONObject();

    try {

      String fcmid = AquaMarkSharePreferences.getInstance().getFcmId();
      if (!fcmid.equals("")) {
        jData.put("fcmId", fcmid);
      }

      jMain.put("data", jData);
      jReqest.put("request", jMain);
      jFinal.put("json", jReqest);

      String finalJsonStr = jFinal.toString();
      result = finalJsonStr;

      System.out.println("Send req : fcm req :" + result);

    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  public static String uploadVideoReqJsonStr(int jobid) {
    String result = null;

    JSONObject jFinal = new JSONObject();
    JSONObject jReqest = new JSONObject();
    JSONObject jMain = new JSONObject();
    JSONObject jData = new JSONObject();

    try {
      String mobileIme = AquaMarkSharePreferences.getInstance().getDeviceId();
      jData.put("deviceId", mobileIme);
      jData.put("jobId", jobid);

      jMain.put("data", jData);
      jReqest.put("request", jMain);
      jFinal.put("json", jReqest);

      String finalJsonStr = jFinal.toString();
      result = finalJsonStr;

      System.out.println("Get video req :" + result);

    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  public static String getJsonRequest(String requestType) {

    String result = null;

    JSONObject jFinal = new JSONObject();
    JSONObject jReqest = new JSONObject();
    JSONObject jMain = new JSONObject();
    JSONObject jData = new JSONObject();

    try {

      if (requestType.equals("port")) {
        jData.put("status", "offline");

      } else {
        String inOut = "true";
        jData.put("city", requestType);
        jData.put("inOffice", inOut);
      }

      jMain.put("data", jData);
      jReqest.put("request", jMain);
      jFinal.put("json", jReqest);

      String finalJsonStr = jFinal.toString();
      result = finalJsonStr;

      System.out.println("send city req : " + result);

    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }
}
