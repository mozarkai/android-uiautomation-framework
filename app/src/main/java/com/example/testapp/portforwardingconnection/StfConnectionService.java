package com.example.testapp.portforwardingconnection;

import static com.example.testapp.utility.UtilityClass.isNetworkConnected;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.example.testapp.AquaMarkSharePreferences;
import com.example.testapp.BaseClass;
import com.example.testapp.api_interface.JobIdGeneration;
import com.example.testapp.api_interface.Retroclient;
import com.example.testapp.models.job_id_generation.job_id_request.Data;
import com.example.testapp.models.job_id_generation.job_id_request.GenerateJobId;
import com.example.testapp.models.job_id_generation.job_id_request.Json;
import com.example.testapp.models.job_id_generation.job_id_request.Request;
import com.example.testapp.models.job_id_generation.job_id_response.GenrateJobIdResponse;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StfConnectionService extends Service {

  public int counter = 0;
  Context applicationContext;
  BaseClass baseclass;
  String lhost = "localhost";
  int lport = 5556;
  List<String> commands = new ArrayList<>();
  int checkDisconnectCounter = 0;
  long currentTimeStamp;
  long oldTime = 0;
  private ChannelShell mchannel;
  private Session session;
  private String hostname, username, password;
  private boolean mConnected = false;
  private Timer timer;
  private TimerTask timerTask;

  public StfConnectionService() {
  }

  public StfConnectionService(Context applicationContext) {
    super();
    Log.i("Mozark ", "here I am!");
    this.applicationContext = applicationContext;
    baseclass = new BaseClass();
  }

  private static void sendCommands(Channel channel, List<String> commands) {

    // send commands in server
    try {

      PrintStream out = new PrintStream(channel.getOutputStream());

      out.println("#!/bin/bash");
      for (String command : commands) {
        out.println(command);
        System.out.println("suvarna : Finished sending commands! " + command);
      }
      //out.println("exit");
      out.flush();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null;
    // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);
    startTimer();
    return START_STICKY;
  }

  private Channel getChannel() {
    // open server channel
    if (isNetworkConnected(this)) {
      if (mchannel == null || !mchannel.isConnected()) {
        try {
          mchannel = (ChannelShell) getSession().openChannel("shell");
          mchannel.connect();

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return mchannel;
  }

  private Session getSession() {
    // connect to server
    if (isNetworkConnected(this)) {

      try {
        if (session == null || !session.isConnected()) {
          session = connect(hostname, username, password);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return session;
  }

  private Session connect(String hostname, String username, String password) {

    // port forward using jsch
    JSch jSch = new JSch();

    try {
            /*AssetManager am = getAssets();
            InputStream inputStream = am.open("privateKey.pem");
            File file = createFileFromInputStream(inputStream);
*/
      //session.setConfig("PreferredAuthentications", "password");
      //jSch.addIdentity(file.getAbsolutePath());

      System.out.println(
          "suvarna : Connecting SSH to " + hostname + " - Please wait for few seconds... ");

      session = jSch.getSession(username, hostname, 22);
      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      session.setConfig(config);
      session.setPassword(password);
      session.setServerAliveInterval(600000);
      session.setServerAliveCountMax(5);
      session.connect();
      session
          .setPortForwardingR(AquaMarkSharePreferences.getInstance().getPortNumber(), lhost, lport);

      System.out.println(
          "suvarna : Connected! " + hostname + " : " + AquaMarkSharePreferences.getInstance()
              .getPortNumber() + " -> " + lhost + " : " + lport);

           /* // create a session connected to port 2233 on the local host.
            Session secondSession = jSch.getSession(username, "localhost", 5556);
            secondSession.setPassword("Welcome@1234");
            secondSession.setConfig("StrictHostKeyChecking", "no");

            secondSession.connect(); // now we're connected to the secondary system
            Channel channel = secondSession.openChannel("exec");
            ((ChannelExec)channel).setCommand("hostname");

            channel.setInputStream(null);
            channel.connect();
*/
    } catch (Exception e) {
      e.printStackTrace();
    }

    return session;
  }

  private void connectStfConnection() {
    if (AquaMarkSharePreferences.getInstance().getPortNumber() == -1) {
      callGetPortNumber();
    }
    if (AquaMarkSharePreferences.getInstance().getPortNumber() != -1 && (mchannel == null
        || !mchannel.isConnected() || mchannel.isClosed())) {
      makingStfConnection();
      mConnected = false;
    }
    if (mchannel != null && mchannel.isConnected()) {
      if (!mConnected) {
        checkDeviceStatusFromServer();
      }
    }
  }

  private void callGetPortNumber() {
    Retroclient retroclient = new Retroclient();
    JobIdGeneration service = retroclient.getClient().create(JobIdGeneration.class);
    Call<GenrateJobIdResponse> call = service
        .getPortNumber(AquaMarkSharePreferences.getInstance().getDeviceId());
    call.enqueue(new Callback<GenrateJobIdResponse>() {
      @Override
      public void onResponse(Call<GenrateJobIdResponse> call,
          Response<GenrateJobIdResponse> response) {
        GenrateJobIdResponse entity = response.body();
        String code = null;
        if (entity != null) {
          code = entity.getJson().getResponse().getStatusCode();
        }
        if (code != null) {
          if (code.equalsIgnoreCase("0")) {
            int port = entity.getJson().getResponse().getData().getPort();
            AquaMarkSharePreferences.getInstance().setPortNumber(port);
          } else {
            String message = "Oops,the job id is not assign";
            //                    showDialog(message);
          }
        }
      }

      @Override
      public void onFailure(Call<GenrateJobIdResponse> call, Throwable t) {

      }
    });
  }

  private void makingStfConnection() {
    try {
      loadPropertyInfo();
      int availablePort = AquaMarkSharePreferences.getInstance().getPortNumber();
      commands.add("adb connect localhost:" + availablePort);

      Channel channel = getChannel();
      System.out.println("suvarna : Sending commands... " + channel.isConnected());
      sendCommands(channel, commands);
      System.out.println("suvarna : Finished sending commands! " + channel.isConnected());
    } catch (Exception e) {
      System.out.println("suvarna : An error ocurred during executeCommands: " + e);
    }
  }

  private void loadPropertyInfo() {
    Properties properties = BaseClass.getProperties();
    if (AquaMarkSharePreferences.getInstance().getIpAddress()
        .equals(properties.getProperty("QA_IPAddress"))) {
      username = properties.getProperty("QAserverUserName");
      password = properties.getProperty("QAserverPassword");
      hostname = properties.getProperty("QA_IPAddress");

    } else if (AquaMarkSharePreferences.getInstance().getIpAddress()
        .equals(properties.getProperty("PROD_IPAddress"))) {
      username = properties.getProperty("PRODserverUserName");
      password = properties.getProperty("PRODserverPassword");
      hostname = properties.getProperty("PROD_IPAddress");

    } else if (AquaMarkSharePreferences.getInstance().getIpAddress()
        .equals(properties.getProperty("STB_IPAddress"))) {
      username = properties.getProperty("QAserverUserName");
      password = properties.getProperty("QAserverPassword");
      hostname = properties.getProperty("STB_IPAddress");
    }
  }

  private void checkDeviceStatusFromServer() {
    currentTimeStamp = System.currentTimeMillis();
    Retroclient retroclient = new Retroclient();
    JobIdGeneration service = retroclient.getClient().create(JobIdGeneration.class);
    Call<GenrateJobIdResponse> call = service
        .getPortNumberStatus(AquaMarkSharePreferences.getInstance().getPortNumber());
    call.enqueue(new Callback<GenrateJobIdResponse>() {
      @Override
      public void onResponse(Call<GenrateJobIdResponse> call,
          Response<GenrateJobIdResponse> response) {
        checkDisconnectCounter++;
        GenrateJobIdResponse entity = response.body();
        String code = null;
        if (entity != null) {
          code = entity.getJson().getResponse().getStatusCode();
        }
        if (code != null) {
          if (code.equalsIgnoreCase("0")) {
            String status = entity.getJson().getResponse().getData().getStatus();
            if (status.equalsIgnoreCase("connected")) {
              mConnected = true;
              updateDeviceDetailStatus();
              return;
            }
          } else {
            mConnected = false;
            Log.i("Mozark in timer", "Waiting to connect adb " + (checkDisconnectCounter++));
            if (checkDisconnectCounter > 10) {
              checkDisconnectCounter = 0;
              mchannel.disconnect();
            }
          }
        }
        mConnected = false;
      }

      @Override
      public void onFailure(Call<GenrateJobIdResponse> call, Throwable t) {

      }
    });
  }

  private void updateDeviceDetailStatus() {
    Retroclient retroclient = new Retroclient();
    GenerateJobId generateJobId = new GenerateJobId(new Json(new Request(
        new Data("online", AquaMarkSharePreferences.getInstance().getLocationAddress(), true))));
    JobIdGeneration service = retroclient.getClient().create(JobIdGeneration.class);
    Call<GenrateJobIdResponse> call = service
        .updateDeviceStatus(AquaMarkSharePreferences.getInstance().getDeviceId(), generateJobId);
    call.enqueue(new Callback<GenrateJobIdResponse>() {
      @Override
      public void onResponse(Call<GenrateJobIdResponse> call,
          Response<GenrateJobIdResponse> response) {
        checkDisconnectCounter++;
        GenrateJobIdResponse entity = response.body();
        String code = null;
        if (entity != null) {
          code = entity.getJson().getResponse().getStatusCode();
        }
        if (code != null) {
          mConnected = code.equalsIgnoreCase("0");
        }
        mConnected = false;
      }

      @Override
      public void onFailure(Call<GenrateJobIdResponse> call, Throwable t) {

      }
    });
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    Log.i("EXIT", "ondestroy!");
    Intent broadcastIntent = new Intent(this, StfRestarterBroadcastReceiver.class);
    sendBroadcast(broadcastIntent);
    stoptimertask();
  }

  public void startTimer() {
    //set a new Timer
    timer = new Timer();
    currentTimeStamp = System.currentTimeMillis();
    //initialize the TimerTask's job
    initializeTimerTask();

    //schedule the timer, to wake up every 1 second
    timer.schedule(timerTask, 30000, 30000); //
  }

  /**
   * it sets the timer to print the counter every x seconds
   */

  public void initializeTimerTask() {
    timerTask = new TimerTask() {
      public void run() {
        Log.i("Mozark in timer", "in timer ++++  " + (counter++));
        connectStfConnection();
      }
    };
  }

  /**
   * not needed
   */
  public void stoptimertask() {
    //stop the timer, if it's not already null
    if (timer != null) {
      timer.cancel();
      timer = null;
    }
  }
}
